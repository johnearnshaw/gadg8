﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace Gadg8
{
    class Debugging
    {
        public static void TryAttachDebugger(int processId)
        {
            try
            {

                new Thread((state) =>
                {
                    var sExePath = Path.Combine(Environment.SystemDirectory, "VSJitDebugger.exe");
                    var process = Process.Start(sExePath, " -p " + processId);
                    process.WaitForExit();
                }).Start();
            }
            catch (Exception ex)
            {
                var sExMessage = (ex.InnerException != null ?
                    ex.InnerException.Message + "; " : string.Empty)
                    + ex.Message
                    + ((ex.TargetSite != null) ? ex.TargetSite.ToString() : string.Empty);
                Debug.Print(ex.GetType().Name + ": " + sExMessage + ex.StackTrace);
            }
        }
    }
}
