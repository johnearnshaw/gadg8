﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Reflection;

namespace Gadg8
{
    class Startup
    {
        private static string ApplicationName = "Gadg8 Desktop Gadgets";

        public static void AddToStartup()
        {
            string path = Helpers.ToShortPathName(Assembly.GetExecutingAssembly().Location);
            
            RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (key.GetValue(ApplicationName) == null || (string)key.GetValue(ApplicationName) != path)
                key.SetValue(ApplicationName, path);
        }

        public static void RemoveFromStartup()
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (key.GetValue(ApplicationName) != null)
                key.DeleteValue(ApplicationName);
        }

        public static bool CheckStartup()
        {
            string path = Helpers.ToShortPathName(Assembly.GetExecutingAssembly().Location);
            
            RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            if (key.GetValue(ApplicationName) != null && (string)key.GetValue(ApplicationName) == path)
                return true;
            return false;
        }
    }
}
