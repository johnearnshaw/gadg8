﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;
using System.Net;

namespace Gadg8
{
    public class AsyncDownloader
    {
        #region Events
        #region EventArgs
        public class DownloadProgressEventArgs : EventArgs
        {
            public readonly string Url;
            public readonly string Path;
            public readonly long TotalBytes;
            public readonly int BytesRead;
            public readonly int PercentRead;
            public readonly int BytesPerSecond;
            public bool Cancel = false;

            public DownloadProgressEventArgs(string url, string path, long totalBytes, int bytesRead, int percentRead, int bytesPerSecond)
            {
                this.Url = url;
                this.Path = path;
                this.TotalBytes = totalBytes;
                this.BytesRead = bytesRead;
                this.PercentRead = percentRead;
                this.BytesPerSecond = bytesPerSecond;
            }
        }

        public class DownloadFailedEventArgs : EventArgs
        {
            public readonly string Url;
            public readonly string Path;
            public readonly Exception Error;
            public bool Retry = false;

            public DownloadFailedEventArgs(string url, string path, Exception error)
            {
                this.Url = url;
                this.Path = path;
                this.Error = error;
            }
        }

        public class DownloadCompleteEventArgs : EventArgs
        {
            public readonly string Url;
            public readonly string Path;

            public DownloadCompleteEventArgs(string url, string path)
            {
                this.Url = url;
                this.Path = path;
            }
        }
        #endregion
        
        // Delegate declarations.
        public delegate void DownloadProgressEventHandler(object sender, DownloadProgressEventArgs e);
        public delegate void DownloadCompleteEventHandler(object sender, DownloadCompleteEventArgs e);
        public delegate void DownloadFailedEventHandler(object sender, DownloadFailedEventArgs e);

        // Events
        public event DownloadProgressEventHandler DownloadProgress;
        public event DownloadCompleteEventHandler DownloadComplete;
        public event DownloadFailedEventHandler DownloadFailed;

        #region Invoker Methods
        protected virtual void InvokeDownloadProgress(DownloadProgressEventArgs e)
        {
            
            DownloadProgressEventHandler h = DownloadProgress;
            if (h != null)
            {
                h(this, e);
            }
        }

        protected virtual void InvokeDownloadComplete(DownloadCompleteEventArgs e)
        {
            DownloadCompleteEventHandler h = DownloadComplete;
            if (h != null)
            {
                h(this, e);
            }
        }

        protected virtual void InvokeDownloadFailed(DownloadFailedEventArgs e)
        {
            DownloadFailedEventHandler h = DownloadFailed;
            if (h != null)
            {
                h(this, e);
            }
        }
        #endregion
        #endregion

        public AsyncDownloader()
        {
        }

        public void DownloadFile(string url, string destinationPath = "")
	    {
		    object[] args = new object[] {url, destinationPath, 0};

		    System.Threading.Thread AsyncThread = new System.Threading.Thread(ThreadedDownload);
		    AsyncThread.IsBackground = true;
		    AsyncThread.Start(args);
	    }

        private void ResumeDownload(string url, string destinationPath = "", int position = 0)
        {
            object[] args = new object[] { url, destinationPath, position };

            System.Threading.Thread AsyncThread = new System.Threading.Thread(ThreadedDownload);
            AsyncThread.IsBackground = true;
            AsyncThread.Start(args);
        }

        private void ThreadedDownload(object args)
        {
            object[] argsArray = (object[])args;
            string url = (string)argsArray[0];
            string path = (string)argsArray[1];
            string filename = url.Substring(url.Length - (url.LastIndexOf("/") + 1));
            long totalBytes = 0;
            int startpos = (int)argsArray[2];
            int bytesRead = 0;
            int percentRead = 0;
            System.IO.FileStream writeStream = null;

            Stopwatch speedtimer = new Stopwatch(); // Used for calculating download speed
            double currentSpeed = 0;

            if (path == "")
            {
                path = Helpers.AppPath + filename;
            }
            else if (path.Replace("/", "\\").EndsWith("\\"))
            {
                path += filename;
            }

            HttpWebRequest request = null;
            HttpWebResponse response = null;
            try
            {
                request = (HttpWebRequest)WebRequest.Create(url);
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex)
            {
                DownloadFailedEventArgs df = new DownloadFailedEventArgs(url, path, ex);
                InvokeDownloadFailed(df);
                if (df.Retry)
                    ResumeDownload(url, path, bytesRead); 
                return;
            }

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                DownloadFailedEventArgs df = new DownloadFailedEventArgs(url, path, new Exception("Download request failed with status code " + response.StatusCode.ToString(), new Exception(response.StatusCode.ToString())));
                InvokeDownloadFailed(df);
                if (df.Retry)
                    ResumeDownload(url, path, bytesRead); 
                return;
            }

            request.AddRange(startpos); //set starting point for resume

            totalBytes = response.ContentLength; // Size of the response (in bytes)

            DownloadProgressEventArgs dp = new DownloadProgressEventArgs(url, path, totalBytes, bytesRead, percentRead, percentRead);
            InvokeDownloadProgress(dp);
            if (dp.Cancel)
                return;
            
            if (startpos > 0 && System.IO.File.Exists(path))
            {
                //for resume support
                writeStream = new System.IO.FileStream(path, System.IO.FileMode.Open);
                writeStream.Position = startpos;
            }
            else
            {
                try
                {
                    writeStream = new System.IO.FileStream(path, System.IO.FileMode.Create);
                }
                catch
                {
                    return;
                }
            }

            int readings = 0;

            do
            {
                speedtimer.Start();

                byte[] cycleBytes = new byte[4096];
                int readCycle = response.GetResponseStream().Read(cycleBytes, 0, 4096);

                bytesRead += readCycle;
                percentRead = (int)((bytesRead * 100) / totalBytes);

                dp = new DownloadProgressEventArgs(url, path, totalBytes, bytesRead, percentRead, percentRead);
                InvokeDownloadProgress(dp);
                if (dp.Cancel)
                    return;

                if (readCycle == 0)
                {
                    DownloadFailedEventArgs df = new DownloadFailedEventArgs(url, path, new Exception("Download request timed out", new Exception("timed out")));
                    InvokeDownloadFailed(df);
                    if (df.Retry)
                        ResumeDownload(url, path, bytesRead);
                    return;
                }

                writeStream.Write(cycleBytes, 0, readCycle);

                speedtimer.Stop();

                readings += 1;
                //For increase precision, _
                if (readings >= 5)
                {
                    // the speed is calculated only every five cycles
                    currentSpeed = 20480 / (speedtimer.ElapsedMilliseconds / 1000);
                    speedtimer.Reset();
                    readings = 0;
                }
            } while (bytesRead < totalBytes);

            //Close the streams
            response.GetResponseStream().Close();
            writeStream.Close();

            DownloadCompleteEventArgs dc = new DownloadCompleteEventArgs(url, path);
            InvokeDownloadComplete(dc);
        }
    }
}
