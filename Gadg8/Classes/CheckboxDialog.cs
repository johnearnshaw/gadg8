﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;

namespace Gadg8
{
    public class CheckboxDialog
    {
        public static DialogResult Show(IWin32Window owner, string Text, string CheckboxText, ref bool Checked, string Caption = "")
        {
            Form form = new Form();
            form.Load += form_Load;
            Panel panel = new Panel();
            Label label = new Label();
            CheckBox checkbox = new CheckBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            form.ClientSize = new Size(420, 150);

            panel.BackColor = SystemColors.ControlLightLight;

            checkbox.Checked = Checked;
            checkbox.Text = CheckboxText;
            form.Text = Caption;
            label.Text = Text;
            label.AutoSize = true;
            label.MaximumSize = new Size(400, 0);
            label.Font = SystemFonts.DialogFont;

            buttonOk.Text = Localize.Strings.Yes;
            buttonCancel.Text = Localize.Strings.No;
            buttonOk.DialogResult = DialogResult.Yes;
            buttonCancel.DialogResult = DialogResult.No;
            panel.SetBounds(0, 0, 420, 100);
            panel.Anchor = panel.Anchor | AnchorStyles.Right | AnchorStyles.Bottom;
            

            label.SetBounds(10, 20, 372, 13);
            checkbox.SetBounds(12, 70, 400, 20);
            checkbox.Anchor = AnchorStyles.Left | AnchorStyles.Bottom;

            buttonOk.SetBounds(252, 116, 75, 23);
            buttonCancel.SetBounds(333, 116, 75, 23);

            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            panel.Controls.AddRange(new Control[] { label, checkbox} );
            form.Controls.AddRange(new Control[] { panel, buttonOk, buttonCancel });

            form.ClientSize = new Size(420, 20 + label.Height + 20 + checkbox.Height + 20 + buttonOk.Height + 10);

            form.StartPosition = FormStartPosition.CenterScreen;
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.MaximizeBox = false;
            form.MinimizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            CheckBox chk = new CheckBox() { Text = CheckboxText, Checked = Checked, AutoSize = true, Margin = new Padding(10, 0, 10, 20) };
            

            DialogResult dialogResult = form.ShowDialog(owner);
            Checked = checkbox.Checked;
            return dialogResult;
        }

        private static void form_Load(object sender, EventArgs e)
        {
            Native.SetForegroundWindow(((Form)sender).Handle);
        }
    }
}
