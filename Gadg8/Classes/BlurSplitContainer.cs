﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Gadg8
{
    public class BlurSplitContainer : SplitContainer
    {
        public BlurSplitContainer()
        {
            this.Panel1.Cursor = Cursors.Default;
            this.Panel2.Cursor = Cursors.Default;
            this.Cursor = Cursors.SizeNS;
            
            this.PreviewKeyDown += BlurSplitContainerPreviewKeyDown;
            this.MouseDown += BlurSplitContainerMouseDown;

            this.SetStyle(ControlStyles.Selectable, false);
        }

        private void BlurSplitContainerMouseDown(object sender, MouseEventArgs e)
        {
            this.SetStyle(ControlStyles.Selectable, false);
        }

        private void BlurSplitContainerPreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Tab)
            {
                this.SetStyle(ControlStyles.Selectable, true);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
        }
    }
}
