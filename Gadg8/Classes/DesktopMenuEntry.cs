﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using Microsoft.Win32;

namespace Gadg8
{
    class DesktopMenuEntry
    {
        public static void AddContextMenuEntry()
        {
            string icon = "\"" + Assembly.GetExecutingAssembly().Location + "\"";
            string app = "\"" + Helpers.ToShortPathName(Assembly.GetExecutingAssembly().Location) + "\" \"1\"";
            string regpath = @"Software\Classes\DesktopBackground\Shell\Gadg8 Gadgets";

            RegistryKey regkey = Registry.CurrentUser.OpenSubKey(regpath + "\\command", true);
            if (regkey == null)
                regkey = Registry.CurrentUser.CreateSubKey(regpath + "\\command");

            regkey.SetValue("", app);
            regkey.Close();

            regkey = Registry.CurrentUser.OpenSubKey(regpath, true);
            regkey.SetValue("Position", "Bottom");
            regkey.SetValue("Icon", icon);
            regkey.Close();
        }

        public static void RemoveContextMenuEntry()
        {
            string regpath = @"Software\Classes\DesktopBackground\Shell\Gadg8 Gadgets";

            Registry.CurrentUser.DeleteSubKey(regpath + "\\command", false);
            Registry.CurrentUser.DeleteSubKey(regpath, false);
        }

        public static bool CheckContextMenuEntry()
        {
            bool ret = false;

            string app = "\"" + Helpers.ToShortPathName(Assembly.GetExecutingAssembly().Location) + "\" \"1\"";
            string regpath = @"Software\Classes\DesktopBackground\Shell\Gadg8 Gadgets";

            RegistryKey regkey = Registry.CurrentUser.OpenSubKey(regpath + "\\command", true);
            if (regkey != null)
            {
                try
                {
                    if (regkey.GetValue("").ToString() == app)
                        ret = true;
                }
                catch
                {
                }

                regkey.Close();
            }

            return ret;
        }
    }
}
