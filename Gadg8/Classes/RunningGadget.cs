﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Drawing;
using Gadg8Gadget;

namespace Gadg8
{
    public class RunningGadget
    {
        public Process GadgetProcess;
        public IntPtr GadgetHandle;
        public Rectangle GadgetLocation;
        public GadgetManifest GadgetXml;
        public bool UserClosed;
        public string Path;
        public string UniqueId;
        public int Strikes = 0;
    }
}
