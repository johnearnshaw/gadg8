﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;
using Shell32;
using System.Runtime.InteropServices;

namespace Gadg8
{
    public class AsyncGadgetExtractor
    {
        #region MSCabinet
        public class MSCabinet
        {
            #region P/invoke
            private delegate uint PSP_FILE_CALLBACK(uint context, uint notification, IntPtr param1, IntPtr param2);

            [DllImport("SetupApi.dll", CharSet = CharSet.Auto)]
            private static extern bool SetupIterateCabinet(string cabinetFile, uint reserved, PSP_FILE_CALLBACK callBack, uint context);

            [DllImport("Kernel32.dll", CharSet = CharSet.Auto)]
            private static extern uint GetLastError();

            [DllImport("Kernel32.dll", CharSet = CharSet.Auto)]
            private static extern bool FileTimeToSystemTime(FILETIME lpFileTime, SYSTEMTIME lpSystemTime);

            [DllImport("Kernel32.dll", CharSet = CharSet.Auto)]
            private static extern bool FileTimeToLocalFileTime(FILETIME lpFileTime, FILETIME lpLocalFileTime);

            [DllImport("Kernel32.dll", CharSet = CharSet.Auto)]
            private static extern bool DosDateTimeToFileTime(ushort wFatDate, ushort wFatTime, FILETIME lpFileTime);

            private const uint NO_ERROR = 0;
            private const uint ERROR = 1;
            private const uint SPFILENOTIFY_FILEINCABINET = 0x00000011;
            private const uint SPFILENOTIFY_NEEDNEWCABINET = 0x00000012;
            private const uint SPFILENOTIFY_FILEEXTRACTED = 0x00000013;

            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
            private class SYSTEMTIME
            {
                public ushort wYear;
                public ushort wMonth;
                public ushort wDayOfWeek;
                public ushort wDay;
                public ushort wHour;
                public ushort wMinute;
                public ushort wSecond;
                public ushort wMilliseconds;
            }

            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
            private class FILETIME
            {
                public uint dwLowDateTime;
                public uint dwHighDateTime;
            }

            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
            private class FILEPATHS
            {
                public String Target;
                public String Source;
                public uint Win32Error;
                public uint Flags;
            }

            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
            private class FILE_IN_CABINET_INFO
            {
                public String NameInCabinet;
                public uint FileSize;
                public uint Win32Error;
                public ushort DosDate;
                public ushort DosTime;
                public ushort DosAttribs;

                [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
                public System.String FullTargetName;
            }

            private enum FILEOP : uint
            {
                FILEOP_ABORT = 0,
                FILEOP_DOIT, // Extract
                FILEOP_SKIP
            }

            private enum SetupIterateCabinetAction : uint
            {
                Iterate = 0, Extract
            }
            #endregion

            private string OutputDirectory;
            Dictionary<string, int> FileList;
            
            int TotalSize;
            int ExtractedSize;
            
            string Source;
            string Dest;

            private bool IsCancelled = false;
            
            #region Events
            #region EventArgs
            public class ExtractProgressEventArgs : EventArgs
            {
                public readonly string File;
                public readonly string Path;
                public readonly long TotalBytes;
                public readonly int BytesRead;
                public readonly int PercentRead;
                public readonly int BytesPerSecond;
                public bool Cancel;

                public ExtractProgressEventArgs(string file, string path, long totalBytes, int bytesRead, int percentRead, int bytesPerSecond)
                {
                    this.File = file;
                    this.Path = path;
                    this.TotalBytes = totalBytes;
                    this.BytesRead = bytesRead;
                    this.PercentRead = percentRead;
                    this.BytesPerSecond = bytesPerSecond;
                }
            }

            #endregion

            // Delegate declarations.
            public delegate void ExtractProgressEventHandler(object sender, ExtractProgressEventArgs e);
            public delegate void CancelledEventHandler(object sender, EventArgs e);
            
            // Events
            public event ExtractProgressEventHandler ExtractProgress;
            public event CancelledEventHandler Cancelled;
            
            #region Invoker Methods
            protected virtual void InvokeExtractProgress(ExtractProgressEventArgs e)
            {

                ExtractProgressEventHandler h = ExtractProgress;
                if (h != null)
                {
                    h(this, e);
                }
            }
            
            protected virtual void InvokeCancelled(EventArgs e)
            {

                CancelledEventHandler h = Cancelled;
                if (h != null)
                {
                    h(this, e);
                }
            }
            #endregion
            #endregion            

            public MSCabinet()
            {
            }

            /// <summary>
            /// The FileCallback callback function is used by a number of the setup functions. The PSP_FILE_CALLBACK type defines a pointer to this callback function. FileCallback is a placeholder for the application-defined function name.
            /// Platform SDK: Setup API
            /// </summary>
            private uint CallBack(uint context, uint notification, IntPtr param1, IntPtr param2)
            {
                uint rtnValue = NO_ERROR;

                switch (notification)
                {
                    case SPFILENOTIFY_FILEINCABINET:
                        if (this.IsCancelled)
                        {
                            this.InvokeCancelled(new EventArgs());
                            
                            rtnValue = (uint)FILEOP.FILEOP_ABORT;
                            break;
                        }
                        
                        rtnValue = OnFileFound(context, notification, param1, param2);
                        break;
                    case SPFILENOTIFY_FILEEXTRACTED:
                        rtnValue = NO_ERROR; // OnFileExtractComplete(param1);
                        break;
                    case SPFILENOTIFY_NEEDNEWCABINET:
                        rtnValue = NO_ERROR;
                        break;
                }
                return rtnValue;
            }

            protected virtual uint OnFileFound(uint context, uint notification, IntPtr param1, IntPtr param2)
            {
                uint fileOperation = NO_ERROR;

                FILE_IN_CABINET_INFO fileInCabinetInfo = (FILE_IN_CABINET_INFO)Marshal.PtrToStructure(param1, typeof(FILE_IN_CABINET_INFO));

                switch (context)
                {
                    case (uint)SetupIterateCabinetAction.Iterate:
                        fileOperation = (uint)FILEOP.FILEOP_SKIP;
                        FileList.Add(Source +"\\"+ fileInCabinetInfo.NameInCabinet, (int)fileInCabinetInfo.FileSize);
                        TotalSize += (int)fileInCabinetInfo.FileSize;

                        break;

                    case (uint)SetupIterateCabinetAction.Extract:
                        fileOperation = (uint)FILEOP.FILEOP_DOIT;

                        string targetName = Path.Combine(this.OutputDirectory, fileInCabinetInfo.NameInCabinet);
                        fileInCabinetInfo.FullTargetName = targetName;

                        string destFolder = Path.GetDirectoryName(targetName);
                        if (!Directory.Exists(destFolder + "\\"))
                        {
                            Directory.CreateDirectory(destFolder + "\\");
                        }

                        Marshal.StructureToPtr(fileInCabinetInfo, param1, true);

                        ExtractedSize += (int)fileInCabinetInfo.FileSize;
                        InvokeExtractProgress(new ExtractProgressEventArgs(targetName, destFolder,
                            TotalSize, ExtractedSize, (ExtractedSize / TotalSize)*100, 0));

                        break;
                }

                return fileOperation;
            }

            public void Extract(string file, string path)
            {
                if (this.TotalSize == 0)
                    this.Iterate(file, new Dictionary<string, int>(), out this.TotalSize);

                IsCancelled = false;

                Source = file;
                Dest = path;

                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                OutputDirectory = path;
   
                PSP_FILE_CALLBACK cb = new PSP_FILE_CALLBACK(this.CallBack);

                if (!SetupIterateCabinet(file, 0, cb, (uint)SetupIterateCabinetAction.Extract))
                {
                    string errMsg = new System.ComponentModel.Win32Exception((int)GetLastError()).Message;
                    System.Windows.Forms.MessageBox.Show(errMsg);
                }
            }

            public Dictionary<string, int> Iterate(string file, Dictionary<string, int> fileList, out int totalBytes) //, bool fakeSynchronous = false, Delegate callback = null)
            {
                FileList = fileList;
                this.TotalSize = 0;
                this.Source = file;

                PSP_FILE_CALLBACK cb = new PSP_FILE_CALLBACK(this.CallBack);

                if (!SetupIterateCabinet(file, 0, cb, (uint)SetupIterateCabinetAction.Iterate))
                {
                    string errMsg = new System.ComponentModel.Win32Exception((int)GetLastError()).Message;
                    System.Windows.Forms.MessageBox.Show(errMsg);
                }

                totalBytes = this.TotalSize;
                return FileList;
            }

            internal void Cancel()
            {
                this.IsCancelled = true;
            }
        }
        #endregion

        #region Shell32Zip
        public class Shell32Zip
        {
            //private string OutputDirectory;
            int ExtractedSize = 0;
            int TotalSize = 0;

            string Source;
            string Dest;

            private bool IsCancelled = false;

            #region Events
            #region EventArgs
            public class ExtractProgressEventArgs : EventArgs
            {
                public readonly string File;
                public readonly string Path;
                public readonly long TotalBytes;
                public readonly int BytesRead;
                public readonly int PercentRead;
                public readonly int BytesPerSecond;
                public bool Cancel;

                public ExtractProgressEventArgs(string file, string path, long totalBytes, int bytesRead, int percentRead, int bytesPerSecond)
                {
                    this.File = file;
                    this.Path = path;
                    this.TotalBytes = totalBytes;
                    this.BytesRead = bytesRead;
                    this.PercentRead = percentRead;
                    this.BytesPerSecond = bytesPerSecond;
                }
            }

            #endregion

            // Delegate declarations.
            public delegate void ExtractProgressEventHandler(object sender, ExtractProgressEventArgs e);
            public delegate void CancelledEventHandler(object sender, EventArgs e);

            // Events
            public event ExtractProgressEventHandler ExtractProgress;
            public event CancelledEventHandler Cancelled;

            #region Invoker Methods
            protected virtual void InvokeExtractProgress(ExtractProgressEventArgs e)
            {

                ExtractProgressEventHandler h = ExtractProgress;
                if (h != null)
                {
                    h(this, e);
                }
            }

            protected virtual void InvokeCancelled(EventArgs e)
            {

                CancelledEventHandler h = Cancelled;
                if (h != null)
                {
                    h(this, e);
                }
            }
            #endregion
            #endregion            

            public Shell32Zip()
            {
            }

            public void Extract(string file, string path)
            {
                if (this.TotalSize == 0)
                    this.Iterate(file, new Dictionary<string, int>(), out this.TotalSize);

                this.IsCancelled = false;

                Helpers.DeleteDirectory(path, true);
                Directory.CreateDirectory(path);

                this.Source = file;
                this.Dest = path;

                Type ShellAppType = Type.GetTypeFromProgID("Shell.Application");
                Shell sh = (Shell)Activator.CreateInstance(ShellAppType);
                
                Folder fldr = sh.NameSpace(file);
                Folder dest = sh.NameSpace(path);

                foreach (FolderItem fi in fldr.Items())
                {
                    if (IsCancelled)
                    {
                        this.InvokeCancelled(new EventArgs());
                        return;
                    }

                    if (fi.IsFolder)
                    {
                        string f = fi.Path.Substring(fi.Path.IndexOf(".zip", StringComparison.InvariantCultureIgnoreCase)
                            + 4).TrimStart("\\".ToCharArray());
                        if (f.Contains("\\"))
                        f = f.Substring(f.IndexOf("\\") + 1);

                        string dir = Path.Combine(path, f);

                        Extract(fi.Path, dir.ToString());
                    }
                    else
                    {
                        //fldr.CopyHere(sh.NameSpace(gadgetPath).Items(), 4);
                        dest.CopyHere(fi, 4); //sh.NameSpace(gadgetPath).Items(), 4);
                        
                        ExtractedSize += fi.Size;
                        InvokeExtractProgress(new ExtractProgressEventArgs(file, path, TotalSize,
                            ExtractedSize, (ExtractedSize / TotalSize) * 100, 0));
                    }
                }

                Marshal.ReleaseComObject(sh);
            }

            public Dictionary<string, int> Iterate(string file, Dictionary<string, int> fileList, out int totalBytes)
            {
                Type ShellAppType = Type.GetTypeFromProgID("Shell.Application");
                Shell sh = (Shell)Activator.CreateInstance(ShellAppType);
                
                Folder fldr = sh.NameSpace(file.ToString());

                foreach (FolderItem fi in fldr.Items())
                {
                    if (fi.IsFolder)
                    {
                        Iterate(fi.Path, fileList, out totalBytes);
                    }
                    else
                    {
                        fileList.Add(fi.Path.ToString(), fi.Size);
                        this.TotalSize += fi.Size;
                    }
                }

                totalBytes = this.TotalSize;

                Marshal.ReleaseComObject(sh);
                return fileList;
            }

            internal void Cancel()
            {
                this.IsCancelled = true;
            }
        }
        #endregion

        #region Events
        #region EventArgs
        public class ExtractProgressEventArgs : EventArgs
        {
            public readonly string File;
            public readonly string Path;
            public readonly long TotalBytes;
            public readonly int BytesRead;
            public readonly int PercentRead;
            public readonly int BytesPerSecond;
            public bool Cancel;

            public ExtractProgressEventArgs(string file, string path, long totalBytes, int bytesRead, int percentRead, int bytesPerSecond)
            {
                this.File = file;
                this.Path = path;
                this.TotalBytes = totalBytes;
                this.BytesRead = bytesRead;
                this.PercentRead = percentRead;
                this.BytesPerSecond = bytesPerSecond;
            }
        }

        public class ExtractFailedEventArgs : EventArgs
        {
            public readonly string File;
            public readonly string Path;
            public readonly Exception Error;
            public bool Retry;

            public ExtractFailedEventArgs(string file, string path, Exception error)
            {
                this.File = file;
                this.Path = path;
                this.Error = error;
            }
        }

        public class ExtractCompleteEventArgs : EventArgs
        {
            public readonly string File;
            public readonly string Path;

            public ExtractCompleteEventArgs(string file, string path)
            {
                this.File = file;
                this.Path = path;
            }
        }
        #endregion

        // Delegate declarations.
        public delegate void ExtractProgressEventHandler(object sender, ExtractProgressEventArgs e);
        public delegate void ExtractCompleteEventHandler(object sender, ExtractCompleteEventArgs e);
        public delegate void ExtractFailedEventHandler(object sender, ExtractFailedEventArgs e);
        public delegate void ExtractCancelledEventHandler(object sender, EventArgs e);

        // Events
        public event ExtractProgressEventHandler ExtractProgress;
        public event ExtractCompleteEventHandler ExtractComplete;
        public event ExtractFailedEventHandler ExtractFailed;
        public event ExtractCancelledEventHandler ExtractCancelled;

        #region Invoker Methods
        protected virtual void InvokeExtractProgress(ExtractProgressEventArgs e)
        {

            ExtractProgressEventHandler h = ExtractProgress;
            if (h != null)
            {
                h(this, e);
            }
        }

        protected virtual void InvokeExtractComplete(ExtractCompleteEventArgs e)
        {
            ExtractCompleteEventHandler h = ExtractComplete;
            if (h != null)
            {
                h(this, e);
            }
        }

        protected virtual void InvokeExtractFailed(ExtractFailedEventArgs e)
        {
            ExtractFailedEventHandler h = ExtractFailed;
            if (h != null)
            {
                h(this, e);
            }
        }
        
        protected virtual void InvokeExtractCancelled(EventArgs e)
        {
            ExtractCancelledEventHandler h = ExtractCancelled;
            if (h != null)
            {
                h(this, e);
            }
        }
        #endregion
        #endregion

        private bool IsCancelled = false;

        public enum Compressions
        {
            Zip = 0,
            Cab = 1,
            Unknown = 3
        }

        public class ArchiveInfo
        {
            public readonly int CompressedSize;
            public readonly int UncompressedSize;
            public readonly bool HasManifest;
            public readonly Compressions Compression;
            public readonly Dictionary<string, int> Files;
        }

        public AsyncGadgetExtractor()
        {
        }

        public void ExtractFileAsync(string file, string path = "")
        {
            this.IsCancelled = false;

            if (File.Exists(file))
            {
                object[] args = new object[] { file, path };

                System.Threading.Thread AsyncThread = new System.Threading.Thread(ThreadedExtract);
                AsyncThread.IsBackground = true;
                AsyncThread.SetApartmentState(System.Threading.ApartmentState.STA);
                AsyncThread.Start(args);
            }
            else
            {
                ExtractFailedEventArgs ef = new ExtractFailedEventArgs(file, path, new Exception("The source file is missing", new Exception("File Not Found")));
                InvokeExtractFailed(ef);
                if (ef.Retry)
                    ExtractFileAsync(file, path);
            }
        }

        public void ExtractFile(string file, string path = "")
        {
            this.IsCancelled = false;

            if (File.Exists(file))
            {
                object[] args = new object[] { file, path };
                ThreadedExtract(args);
            }
            else
            {
                ExtractFailedEventArgs ef = new ExtractFailedEventArgs(file, path, new Exception("The source file is missing", new Exception("File Not Found")));
                InvokeExtractFailed(ef);
                if (ef.Retry)
                    ExtractFile(file, path);
            }
        }

        public void Cancel()
        {
            this.IsCancelled = true;
        }

        [STAThread]
        private void ThreadedExtract(object args)
        {
            object[] argsArray = (object[])args;
            string file = ((string)argsArray[0]).Replace("/", "\\");
            string path = ((string)argsArray[1]).Replace("/", "\\");
            string filename = Path.GetFileName(file); // file.Replace("/", "\\").Substring(file.Length - (file.LastIndexOf("\\") + 1));
            string tempFile;
            string tempPath;
            int uncompressedSize;
            int compressedSize;
            //int bytesRead = 0;
            //int percentRead = 0;
            
            Dictionary<string, int> fileList = new Dictionary<string, int>();
            Compressions compression;
            
            if (path == "")
            {
                path = Helpers.Gadg8GadgetsPath;
            }
            else if (!path.Replace("/", "\\").EndsWith("\\"))
            {
                path += "\\";
            }

            compressedSize = (int)new FileInfo(file).Length;
            compression = GetCompression(file);

            if (compression == Compressions.Cab)
            {
                tempPath = Helpers.TempPath + Path.GetFileNameWithoutExtension(file);
                tempFile = tempPath + ".cab";

                if (!Directory.Exists(Helpers.TempPath))
                    Directory.CreateDirectory(Helpers.TempPath);

                File.Copy(file, tempFile, true);

                MSCabinet cab = new MSCabinet();
                fileList = cab.Iterate(tempFile, fileList, out uncompressedSize);

                cab.Cancelled += (sender, e) =>
                {
                    this.InvokeExtractCancelled(new EventArgs());
                };

                cab.ExtractProgress += (sender, e) =>
                {
                    if (this.IsCancelled)
                    {
                        cab.Cancel();
                    }

                    this.InvokeExtractProgress(new ExtractProgressEventArgs(e.File, e.Path,
                    e.TotalBytes, e.BytesRead, e.PercentRead, 0)); 
                };

                if (!FindGadgetManifest(fileList))
                {
                    ExtractFailedEventArgs ef = new ExtractFailedEventArgs(file, path, new Exception("This .gadget package does not contain a valid gadget manifest file", new Exception("Manifest Not Found")));
                    InvokeExtractFailed(ef);
                    if (ef.Retry)
                        ExtractFileAsync(file, path);
                    return;
                }

                cab.Extract(tempFile, tempPath);
            }
            else if (compression == Compressions.Zip)
            {
                tempPath = Helpers.TempPath + Path.GetFileNameWithoutExtension(file);
                tempFile = tempPath + ".zip";

                if (!Directory.Exists(Helpers.TempPath))
                    Directory.CreateDirectory(Helpers.TempPath);

                File.Copy(file, tempFile, true);

                Shell32Zip zip = new Shell32Zip();
                
                fileList = zip.Iterate(tempFile, fileList, out uncompressedSize);

                zip.Cancelled += (sender, e) =>
                {
                    this.InvokeExtractCancelled(new EventArgs());
                };

                zip.ExtractProgress += (sender, e) =>
                {
                    if (this.IsCancelled)
                    {
                        zip.Cancel();
                    }

                    this.InvokeExtractProgress(new ExtractProgressEventArgs(e.File, e.Path,
                        e.TotalBytes, e.BytesRead, e.PercentRead, 0));
                };

                if (!FindGadgetManifest(fileList))
                {
                    ExtractFailedEventArgs ef = new ExtractFailedEventArgs(file, path, new Exception("This .gadget package does not contain a valid gadget manifest file", new Exception("Manifest Not Found")));
                    InvokeExtractFailed(ef);
                    if (ef.Retry)
                        ExtractFileAsync(file, path);
                    return;
                }

                zip.Extract(tempFile, tempPath);
            }
            else
            {
                ExtractFailedEventArgs ef = new ExtractFailedEventArgs(file, path, new Exception("An unknown archive compression is used for this package. Compression must be Zip or Cab", new Exception("Unknown Compression")));
                InvokeExtractFailed(ef);
                if (ef.Retry)
                    ExtractFileAsync(file, path);
                return;
            }

            if (IsCancelled)
            {
                File.Delete(tempFile);
                Directory.Delete(tempPath, true);
                return;
            }

            Helpers.CopyDirectory(tempPath, path + filename + "\\", true, false, true);
            File.Delete(tempFile);
            InvokeExtractComplete(new ExtractCompleteEventArgs(file, path + filename + "\\"));
        }

        private bool FindGadgetManifest(Dictionary<string, int> fileList)
        {
            foreach (string s in fileList.Keys)
            {
                if (s.EndsWith("gadget.xml", StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        public Compressions GetCompression(string file)
        {
            if (!System.IO.File.Exists(file))
                throw new FileNotFoundException(file + " not found");

            int MaxContent = 4;
            FileStream fs = File.OpenRead(file);


            byte[] buf = new byte[MaxContent];
            fs.Read(buf, 0, MaxContent);
            fs.Close();

            if (CompareBytes(buf, new byte[] { 0x50, 0x4B, 0x03, 0x04 }))
            {
                return Compressions.Zip;
            }
            else if (CompareBytes(buf, new byte[] { 0x4D, 0x53, 0x43, 0x46 }))
            {
                return Compressions.Cab;
            }

            return Compressions.Unknown;
        }

        public bool CompareBytes(byte[] byte1, byte[] byte2)
        {
            if (byte1.Length != byte2.Length)
                return false;

            for (int i = 0; i < byte1.Length; i++)
            {
                if (byte1[i] != byte2[i])
                    return false;
            }
            return true;
        }
    }
}
