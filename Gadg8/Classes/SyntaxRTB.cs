﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace Gadg8
{
    public class SyntaxRTB : RichTextBox
    {
        private int oldLineCount = 0;
        private int oldLine = -1;
        private int oldCharCount = 0;
        private int lineNumberWidth;
        private bool processing;

        public Brush LineNumbersSeperator = new SolidBrush(SystemColors.ControlDark);
        public Brush LineNumbersForeground = new SolidBrush(SystemColors.ControlText);
        public Brush LineNumbersBackground = new SolidBrush(SystemColors.Control);
        public Brush LineNumbersHiliteBackground = new SolidBrush(Color.DarkRed);
        public Brush LineNumbersHiliteForeground = new SolidBrush(Color.White);
        public Color DefaultTextColor = Color.Black;
        public Color SpecialsColor = Color.DarkCyan;
        public Color TagsColor = Color.Brown;
        public Color KeywordsColor = Color.Blue;
        public Color NumbersColor = Color.Red;
        public Color CommentsColor = Color.Green;
        public Color HtmlCommentsColor = Color.Gray;
        public Color StringsColor = Color.DarkRed;

        private string SpecialWords = "";
        private string Keywords = "";
        private List<int> HighlightLines = new List<int>();

        private bool AllowPaint = true;

        public SyntaxRTB()
            : base()
        {
            this.DoubleBuffered = true;

            this.Font = new Font("Consolas", 10, FontStyle.Regular,
                GraphicsUnit.Point, ((byte)(0)));
            if (this.Font.Name != "Consolas")
                this.Font = new Font("Courier New", 10, FontStyle.Regular,
                GraphicsUnit.Point, ((byte)(0)));

            InitializeRegex();
        }

        protected override void OnLinkClicked(LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.LinkText);
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                //case Native.WM_SETFOCUS:
                //    return;

                case (int)Gadg8Gadget.Native.WindowsMessageFlags.WM_PAINT:
                    if (AllowPaint)
                    {
                        base.WndProc(ref m);

                        Graphics g = Graphics.FromHwnd(this.Handle);
                        this.PaintLineNumbers(g);
                        g.Dispose();
                    }
                    else
                    {
                        m.Result = new IntPtr(0);
                    }
                    break;

                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                case Keys.PageUp:
                    if (this.GetLineFromCharIndex(this.SelectionStart) == 0)
                    {
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                    }
                    break;

                case Keys.Left:
                    if (this.SelectionStart == 0)
                    {
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                    }
                    break;

                case Keys.Back:
                    if (this.SelectionStart + this.SelectionLength == 0)
                    {
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                    }
                    break;

                case Keys.Down:
                case Keys.PageDown:
                    if (this.GetLineFromCharIndex(this.SelectionStart + this.SelectionLength) == this.Lines.Length - 1)
                    {
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                    }
                    break;

                case Keys.Right:
                    if (this.SelectionStart + this.SelectionLength == this.Text.Length)
                    {
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                    }
                    break;

                case Keys.Delete:
                    if (this.SelectionStart == this.Text.Length)
                    {
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                    }
                    break;

                case Keys.Home:
                    if (this.SelectionStart == this.GetFirstCharIndexOfCurrentLine())
                    {
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                    }
                    break;
                
                case Keys.End:
                    if (this.SelectionStart + this.SelectionLength == this.Text.IndexOf(Environment.NewLine, this.GetFirstCharIndexOfCurrentLine()) ||
                        this.SelectionStart + this.SelectionLength == this.Text.Length)
                    {
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                    }
                    break;

                default:
                    break;
            }
            
            base.OnKeyDown(e);
        }

        protected override void OnTextChanged(EventArgs e)
        {
            
            if (this.Text.Length == 0)
            {
                oldLineCount = 0;
                oldLine = -1;
                oldCharCount = 0;
            }
            else
            {
                    int currentSelectionStart = this.SelectionStart;
                    int currentSelectionLength = this.SelectionLength;

                    if (this.oldLine != this.GetLineFromCharIndex(this.SelectionStart) &&
                        (this.Text.Length > this.oldCharCount + 1 || this.Text.Length < this.oldLineCount - 1))
                    {
                        System.Threading.Thread thread = new System.Threading.Thread((x) =>
                        {
                            if (processing == false)
                            {
                                processing = true;
                                ProcessAllLines();

                                // Process multi-line comments
                                ProcessMultilineComments(CommentsColor);

                                // Process HTML comments
                                ProcessHtmlComments(HtmlCommentsColor);
                                
                                processing = false;
                            }
                        });
                        thread.Start();
                    }
                    else
                    {
                        // Find the start of the current line.
                        int lineStart = currentSelectionStart;
                        while ((lineStart > 0) && (Text[lineStart - 1] != '\n'))
                            lineStart--;

                        // Find the end of the current line.
                        int lineEnd = currentSelectionStart;
                        while ((lineEnd < Text.Length) && (Text[lineEnd] != '\n'))
                            lineEnd++;

                        // Calculate the length of the line.
                        int lineLength = lineEnd - lineStart;

                        System.Threading.Thread thread = new System.Threading.Thread((x) =>
                        {
                            if (processing == false)
                            {
                                processing = true;

                                // Process current line.
                                ProcessLine(lineStart, lineLength);

                                // Process multi-line comments
                                ProcessMultilineComments(CommentsColor);

                                // Process HTML comments
                                ProcessHtmlComments(HtmlCommentsColor);

                                processing = false;
                            }
                        });
                        thread.Start();

                    }

                    //this.SelectionStart = currentSelectionStart;
                    //this.SelectionLength = currentSelectionLength;
                    //this.SelectionColor = DefaultTextColor;

                if (this.Lines.Length != oldLineCount)
                {
                    _CharIndexForTextLine = null;
                    Native.SendMessage(this.Handle, (int)Gadg8Gadget.Native.WindowsMessageFlags.WM_PAINT, IntPtr.Zero, IntPtr.Zero);
                    oldLineCount = this.Lines.Length;
                }

                oldLine = this.GetLineFromCharIndex(this.SelectionStart);
                oldCharCount = this.Text.Length;
            }

            base.OnTextChanged(e);
        }
        
        private void ProcessLine(int lineStart, int lineLength, string lineText = null)
        {
            //int currentSelectionStart = 0;
            //int currentSelectionLength = 0;

            Point pt = new Point();

            this.Invoke(new MethodInvoker(() =>
            {
                Native.SendMessage(this.Handle, Native.EM_GETSCROLLPOS, IntPtr.Zero, ref pt);
                
                Native.LockWindow(this.Handle);
                AllowPaint = false;
                Native.SendMessage(this.Handle, (int)Gadg8Gadget.Native.WindowsMessageFlags.WM_PAINT, false, 0);
                //currentSelectionStart = this.SelectionStart;
                //currentSelectionLength = this.SelectionLength;

                // Get the current line.
                if (string.IsNullOrEmpty(lineText))
                    lineText = Text.Substring(lineStart, lineLength);
            }));
            
            if (lineText.Length <= 500)
            {
                // Make line default color before processing
                SetSelectionColor(lineStart, lineStart + lineLength, DefaultTextColor);
                
                // Process keywords and specials
                ProcessRegex(RegexKeywords, KeywordsColor, lineStart, lineLength, lineText);
                ProcessRegex(RegexSpecials, SpecialsColor, lineStart, lineLength, lineText);

                // Process numbers
                ProcessRegex(RegexIntegers, NumbersColor, lineStart, lineLength, lineText);

                // Process strings
                ProcessRegex(RegexStrings, StringsColor, lineStart, lineLength, lineText);

                // Process single-line comments
                ProcessRegex(RegexSingleLineComments, CommentsColor, lineStart, lineLength, lineText);
            }

            this.Invoke(new MethodInvoker(() =>
            {
                Native.SendMessage(this.Handle, Native.EM_SETSCROLLPOS, IntPtr.Zero, ref pt);
                
                Native.LockWindow(IntPtr.Zero);
                AllowPaint = true;
                Native.SendMessage(this.Handle, (int)Gadg8Gadget.Native.WindowsMessageFlags.WM_PAINT, true, 0);
                Native.SendMessage(this.Handle, (int)Gadg8Gadget.Native.WindowsMessageFlags.WM_PAINT, IntPtr.Zero, IntPtr.Zero);
            }));

            //SetSelection(currentSelectionStart, currentSelectionStart + currentSelectionLength);
            //Application.DoEvents();
        }

        private Regex RegexKeywords;
        private Regex RegexSpecials;
        private Regex RegexStrings;
        private Regex RegexSingleLineComments;
        private Regex RegexMultilineComments;
        private Regex RegexIntegers;
        private Regex RegexHtmlComments;

        public void InitializeRegex()
        {
            if (RegexSpecials == null)
            {
                RegexSpecials = new Regex(SpecialWords, RegexOptions.Singleline | RegexOptions.Compiled);
                RegexSpecials.Match("initialize");
            }

            if (RegexKeywords == null)
            {
                RegexKeywords = new Regex(Keywords, RegexOptions.Singleline | RegexOptions.Compiled);
                RegexKeywords.Match("initialize");
            }

            RegexStrings = new Regex("([\"'])[^\\1\\\\\\r\\n]*(?:\\\\.[^\\1\\\\\\r\\n]*)*\\1", RegexOptions.Singleline | RegexOptions.Compiled);
            RegexStrings.Match("initialize");

            RegexSingleLineComments = new Regex("//.*$", RegexOptions.Singleline | RegexOptions.Compiled);
            RegexSingleLineComments.Match("initialize");

            RegexMultilineComments = new Regex("(/\\*.*?\\*/)|(/\\*.*)", RegexOptions.Singleline | RegexOptions.Compiled);
            RegexMultilineComments.Match("initialize");

            RegexIntegers = new Regex("\\b(?:[0-9]*\\.)?[0-9]+\\b", RegexOptions.Singleline | RegexOptions.Compiled);
            RegexIntegers.Match("initialize");

            RegexHtmlComments = new Regex("(\\<\\!\\-\\-.*?\\-\\-\\>)", RegexOptions.Singleline | RegexOptions.Compiled);
            RegexHtmlComments.Match("initialize");
        }

        public void ProcessAllLines()
		{
            int linesLength = 0;

            this.Invoke(new MethodInvoker(() =>
            {
                linesLength = this.Lines.Length;
            }));

			int start = 0;
			int i = 0;
			
            while (i < linesLength)
			{
                string lineText = "";

                this.Invoke(new MethodInvoker(() =>
                {
                    lineText = this.Lines[i];
                }));

				int lineStart = start;

				ProcessLine(lineStart, lineText.Length, lineText);

				start += lineText.Length + 1;
                i++;
			}

            //this.Invoke(new MethodInvoker(() =>
            //{
            //    this.SelectionStart = currentSelectionStart;
            //    this.SelectionLength = currentSelectionLength;
            //    this.SelectionColor = DefaultTextColor;
            //}));
		}

        private void ProcessRegex(Regex regex, Color color, int lineStart, int lineLength, string lineText)
        {
            Match regMatch;

            for (regMatch = regex.Match(lineText); regMatch.Success; regMatch = regMatch.NextMatch())
            {
                int start = lineStart + regMatch.Index;
                int length = regMatch.Length;
                SetSelectionColor(start, start + length, color);
            }
        }

        private void ProcessHtmlComments(Color color)
        {
            string text = "";

            this.Invoke(new MethodInvoker(() =>
            {
                text = this.Text;
            }));

            Match regMatch;

            for (regMatch = RegexHtmlComments.Match(text); regMatch.Success; regMatch = regMatch.NextMatch())
            {
                int start = regMatch.Index;
                int length = regMatch.Length;
                SetSelectionColor(start, start + length, color);
            }
        }

        private void ProcessMultilineComments(Color color)
        {
            string text = "";

            this.Invoke(new MethodInvoker(() =>
            {
                text = this.Text;
            }));

            Match regMatch;

            for (regMatch = RegexMultilineComments.Match(text); regMatch.Success; regMatch = regMatch.NextMatch())
            {
                int start = regMatch.Index;
                int length = regMatch.Length;
                SetSelectionColor(start, start + length, color);
            }
        }

        private void PaintLineNumbers(Graphics g)
        {
            SizeF strSize = g.MeasureString(this.Lines.Length.ToString(), this.Font);
            int width = (int)strSize.Width + 3;
            int height = (int)strSize.Height;

            if (width != lineNumberWidth)
            {
                this.lineNumberWidth = width;
                Native.SendMessage(this.Handle, Native.EM_SETMARGINS, (IntPtr)Native.EC_LEFTMARGIN, (IntPtr)(width + this.Margin.Left));
                Native.SendMessage(this.Handle, (int)Gadg8Gadget.Native.WindowsMessageFlags.WM_PAINT, IntPtr.Zero, IntPtr.Zero);
                return;
            }

            g.FillRectangle(LineNumbersSeperator, new Rectangle(0, 0, width - 1, this.Bounds.Height));
            g.FillRectangle(LineNumbersBackground, new Rectangle(0, 0, width - 2, this.Bounds.Height));

            int n = NumberOfVisibleDisplayLines;
            int first = FirstVisibleDisplayLine;

            int py = 0;
            
            for (int i = 0; i <= n; i++)
            {
                int ix = first + i;
                int c = GetCharIndexForDisplayLine(ix) - 1;

                var p = GetPosFromCharIndex(c + 1);

                Rectangle r4 = Rectangle.Empty;

                if (i == n)
                {
                    if (this.Bounds.Height <= py) continue;
                    r4 = new Rectangle(0, py, width, this.Bounds.Height - py);
                }
                else
                {
                    if (p.Y == py) continue;
                    r4 = new Rectangle(0, py, width, p.Y - py);
                }

                var s = String.Format("{0:D" + this.Lines.Length.ToString().Length + "}", ix);

                if (HighlightLines.Contains(ix))
                {
                    g.FillRectangle(LineNumbersHiliteBackground, new Rectangle(r4.X + 1, r4.Top + 1, width - 4, Math.Min(r4.Height - 1, height - 3)));
                    g.DrawString(s, this.Font, LineNumbersHiliteForeground, r4, null);
                }
                else
                {
                    g.DrawString(s, this.Font, LineNumbersForeground, r4, null);
                }

                py = p.Y;

                ix++;
            }
        }

        private int GetCharIndexForDisplayLine(int line)
        {
            return Native.SendMessage(this.Handle, Native.EM_LINEINDEX, (IntPtr)line, IntPtr.Zero);
        }

        private int NumberOfVisibleDisplayLines
        {
            get
            {
                int topIndex = this.GetCharIndexFromPosition(new System.Drawing.Point(1, 1));
                int bottomIndex = this.GetCharIndexFromPosition(new System.Drawing.Point(1, this.Height - 1));
                int topLine = this.GetLineFromCharIndex(topIndex);
                int bottomLine = this.GetLineFromCharIndex(bottomIndex);
                int n = bottomLine - topLine + 1;
                return n;
            }
        }

        private int FirstVisibleDisplayLine
        {
            get
            {
                return Native.SendMessage(this.Handle, Native.EM_GETFIRSTVISIBLELINE, IntPtr.Zero, IntPtr.Zero);
            }
            set
            {
                int current = FirstVisibleDisplayLine;
                int line = value - current;
                ScrollToLine(line);
            }
        }

        private int GetCharIndexForTextLine(int ix)
        {
            if (ix >= CharIndexForTextLine.Length) return 0;
            if (ix < 0) return 0;
            return CharIndexForTextLine[ix];
        }

        private int GetCharIndexFromPos(int x, int y)
        {
            var p = new Native.POINTL { X = x, Y = y };
            int rawSize = Marshal.SizeOf(typeof(Native.POINTL));
            IntPtr lParam = Marshal.AllocHGlobal(rawSize);
            Marshal.StructureToPtr(p, lParam, false);
            int r = Native.SendMessage(this.Handle, (int)Native.EM_CHARFROMPOS, IntPtr.Zero, lParam);
            Marshal.FreeHGlobal(lParam);
            return r;
        }

        private Point GetPosFromCharIndex(int ix)
        {
            int rawSize = Marshal.SizeOf(typeof(Native.POINTL));
            IntPtr wParam = Marshal.AllocHGlobal(rawSize);
            int r = Native.SendMessage(this.Handle, (int)Native.EM_POSFROMCHAR, (IntPtr)wParam, (IntPtr)ix);

            Native.POINTL p1 = (Native.POINTL)Marshal.PtrToStructure(wParam, typeof(Native.POINTL));

            Marshal.FreeHGlobal(wParam);
            var p = new Point { X = p1.X, Y = p1.Y };
            return p;
        }

        private int[] _CharIndexForTextLine;
        private int[] CharIndexForTextLine
        {
            get
            {
                if (_CharIndexForTextLine == null)
                {
                    var list = new List<int>();
                    int ix = 0;

                    foreach (var c in this.Text)
                    {
                        if (c == '\n') list.Add(ix);
                        ix++;
                    }
                    _CharIndexForTextLine = list.ToArray();
                }
                return _CharIndexForTextLine;
            }

        }

        public void ScrollToLine(int lineNumber)
        {
            Native.SendMessage(this.Handle, Native.EM_LINESCROLL, IntPtr.Zero, (IntPtr)(lineNumber-1));
        }

        public void ClearHighlightedLines()
        {
            if (HighlightLines.Count > 0)
            {
                HighlightLines.Clear();
                Native.SendMessage(this.Handle, (int)Gadg8Gadget.Native.WindowsMessageFlags.WM_PAINT, IntPtr.Zero, IntPtr.Zero);
            }
        }

        public void HighlightLine(int lineNumber, bool hilite = true)
        {
            if (hilite)
            {
                if (!HighlightLines.Contains(lineNumber))
                {
                    HighlightLines.Add(lineNumber);
                    Native.SendMessage(this.Handle, (int)Gadg8Gadget.Native.WindowsMessageFlags.WM_PAINT, IntPtr.Zero, IntPtr.Zero);
                }
            }
            else
            {
                if (HighlightLines.Contains(lineNumber))
                {
                    HighlightLines.Remove(lineNumber);
                    Native.SendMessage(this.Handle, (int)Gadg8Gadget.Native.WindowsMessageFlags.WM_PAINT, IntPtr.Zero, IntPtr.Zero);
                }
            }
        }

        /// <summary>
        /// Compiles the keywords as a regular expression.
        /// </summary>
        public void CompileKeywords(string[] words)
        {
            Keywords = "";

            for (int i = 0; i < words.Length; i++)
            {
                string strWord = words[i];

                if (i == words.Length - 1)
                    Keywords += "\\b" + strWord + "\\b";
                else
                    Keywords += "\\b" + strWord +"\\b|";
            }

            RegexKeywords = new Regex(Keywords, RegexOptions.Singleline | RegexOptions.Compiled);
            RegexKeywords.Match("initialize");
        }

        /// <summary>
        /// Compiles the special words as a regular expression.
        /// </summary>
        public void CompileSpecialWords(string[] words)
        {
            SpecialWords = "";

            for (int i = 0; i < words.Length; i++)
            {
                string strWord = words[i];

                if (i == words.Length - 1)
                    SpecialWords += "\\b" + strWord + "\\b";
                else
                    SpecialWords += "\\b" + strWord + "\\b|";
            }

            RegexSpecials = new Regex(SpecialWords, RegexOptions.Singleline | RegexOptions.Compiled);
            RegexSpecials.Match("initialize");
        }

        public void SetSelectionColor(int start, int end, System.Drawing.Color color)
        {
            this.Invoke(new MethodInvoker(() =>
            {
                Native.CHARFORMAT charFormat = new Native.CHARFORMAT();

                charFormat.cbSize = Marshal.SizeOf(typeof(Native.CHARFORMAT));
                charFormat.szFaceName = new char[32];
                charFormat.dwMask = 0x40000000;
                charFormat.dwEffects = 0;
                charFormat.crTextColor = System.Drawing.ColorTranslator.ToWin32(color);

                IntPtr lParam = Marshal.AllocCoTaskMem(charFormat.cbSize);
                Marshal.StructureToPtr(charFormat, lParam, false);

                IntPtr oldStart;
                IntPtr oldEnd;

                Native.SendMessage(this.Handle, Native.EM_GETSEL, out oldStart, out oldEnd);
                Native.SendMessage(this.Handle, Native.EM_SETSEL, (IntPtr)start, (IntPtr)end);
                Native.SendMessage(this.Handle, Native.EM_SETCHARFORMAT, (IntPtr)Native.SCF_SELECTION, lParam);
                Native.SendMessage(this.Handle, Native.EM_SETSEL, oldStart, oldEnd);
                
                Marshal.FreeCoTaskMem(lParam);
            }));
        }

        public void SetSelection(int start, int end)
        {
            this.Invoke(new MethodInvoker(() =>
            {
                Native.SendMessage(this.Handle, Native.EM_SETSEL, (IntPtr)start, (IntPtr)end);
            }));
        }
    }
}
