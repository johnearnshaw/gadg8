﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Reflection;

namespace Gadg8
{
    public class FileAssociation
    {
        public static bool CheckFileAssociation()
        {
            string progid = "gadg8_auto_file";
            return IsAssociated(".gadget", Assembly.GetExecutingAssembly().Location, progid);
        }

        public static void AssociateGadgetFiles()
        {
            string progid = "gadg8_auto_file";
            string icon = "\"" + Helpers.AppPath + "gadg8package.ico\"";
            string app = Assembly.GetExecutingAssembly().Location; 

            Associate(".gadget", progid, "Desktop Gadget Package", icon, app);
        }

        // Associate file extension with progID, description, icon and application
        private static void Associate(string extension, string progId, string description, string icon, string application)
        {
            application = "\"" + Helpers.ToShortPathName(application) + "\" \"%1\"";

            RegistryKey regkey = Registry.CurrentUser.OpenSubKey(@"Software\Classes\" + extension, true);
            if (regkey == null)
                regkey = Registry.CurrentUser.CreateSubKey(@"Software\Classes\" + extension);

            regkey.SetValue("", progId);

            if (progId != null && progId.Length > 0)
            {
                using (RegistryKey key = Registry.CurrentUser.CreateSubKey(@"Software\Classes\" + progId))
                {
                    if (description != null)
                        key.SetValue("", description);
                    if (icon != null)
                        key.CreateSubKey("DefaultIcon").SetValue("", icon);
                    if (application != null)
                        key.CreateSubKey(@"Shell\Open\Command").SetValue("", application);
                }
                // Tell explorer.exe to refresh
                Native.SHChangeNotify(Native.SHCNE_ASSOCCHANGED, Native.SHCNF_IDLIST, IntPtr.Zero, IntPtr.Zero);
            }
        }

        // Return true if extension already associated in registry
        private static bool IsAssociated(string extension, string application, string progId)
        {
            application = "\"" + Helpers.ToShortPathName(application) + "\" \"%1\""; 

            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Classes\" + extension);
            if (key == null)
                return false;

            if (key.GetValue("").ToString() == "gadg8_auto_file")
            {
                key.Close();
                key = Registry.CurrentUser.OpenSubKey(@"Software\Classes\" + progId);
                if (key == null)
                    return false;

                string currentApp = key.OpenSubKey(@"Shell\Open\Command").GetValue("").ToString();
                if (currentApp == application)
                {
                    key.Close();
                    return true;
                }
                else
                {
                    key.Close();
                    return false;
                }
            }
            else
            {
                key.Close();
                return false;
            }
        }
    }
}
