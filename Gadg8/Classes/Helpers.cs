﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Reflection;
using System.Drawing;
using System.Windows.Forms;

namespace Gadg8
{
    public class Helpers
    {
        public static string AppPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\";
        public static string Gadg8GadgetsPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Gadg8\\Desktop Gadgets\\");
        public static string TempPath = Path.Combine(System.IO.Path.GetTempPath(), "Gadg8\\");

        public static bool CopyDirectory(string sourcePath, string destinationPath, bool move = false, bool merge = false, bool overwrite = true)
        {
            if (sourcePath.ToLower() == destinationPath.ToLower())
            {
                return false;
            }

            // Check if the target directory exists, if not, create it.
            if (!Directory.Exists(destinationPath))
            {
                Directory.CreateDirectory(destinationPath);
            }
            else
            {
                if (!merge)
                {
                    DeleteDirectory(destinationPath, true);
                    Directory.CreateDirectory(destinationPath);
                }
            }

            try
            {
                // Copy each file into it's new directory.
                DirectoryInfo sdi = new DirectoryInfo(sourcePath);
                foreach (FileInfo fi in sdi.GetFiles())
                {
                    fi.CopyTo(Path.Combine(destinationPath, fi.Name), overwrite);
                }

                // Copy each subdirectory using recursion.
                DirectoryInfo ddi = new DirectoryInfo(destinationPath);
                foreach (DirectoryInfo subDir in sdi.GetDirectories())
                {
                    if (!CopyDirectory(subDir.FullName, Path.Combine(destinationPath, subDir.Name), move, merge, overwrite))
                    {
                        return false;
                    }
                }

                if (move)
                    Directory.Delete(sourcePath, true);

                return true;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public static bool IsFileInUse(string path)
        {
            FileStream stream = null;
            
            try
            {
                stream = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return false;
        }

        public static bool IsGadgetInUse(string path)
        {
            bool ret = false;
            DirectoryInfo folder = new DirectoryInfo(path);

            foreach (DirectoryInfo di in folder.GetDirectories())
            {
                if (IsGadgetInUse(di.FullName))
                    ret = true;
            }

            foreach (FileInfo fi in folder.GetFiles())
            {
                if (IsFileInUse(fi.FullName))
                    ret = true;
            }

            return ret;
        }

        public static void ForceDeleteReadOnly(string pFolderPath)
        {
            foreach (string Folder in Directory.GetDirectories(pFolderPath))
            {
                ForceDeleteReadOnly(Folder);
            }

            foreach (string file in Directory.GetFiles(pFolderPath))
            {
                var pPath = Path.Combine(pFolderPath, file);
                FileInfo fi = new FileInfo(pPath);
                File.SetAttributes(pPath, FileAttributes.Normal);
                File.Delete(file);
            }

            Directory.Delete(pFolderPath);
        }

        public static bool DeleteDirectory(string path, bool recursive, string quarantinePath = "")
        {
            if (Directory.Exists(path))
            {
                try
                {
                    Directory.Delete(path, recursive);
                    while (Directory.Exists(path)) // we loop a while incase it takes time to delete the folder. Seems to fix a rare issue.
                    {
                        Application.DoEvents();
                    }
                    return true;
                }
                catch (IOException)
                {
                    System.Threading.Thread.Sleep(0); // we sleep once and try again just incase the folder was being used by another proccess.
                    return DeleteDirectory(path, recursive, quarantinePath);
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool SaveBinaryData(string fileName, byte[] data)
        {
            string path = Path.GetDirectoryName(fileName);

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            BinaryWriter Writer = null;
            try
            {
                // use a stream to write the bytes to file
                Writer = new BinaryWriter(File.Create(fileName)); //File.OpenWrite(FileName));

                Writer.Write(data);
                Writer.Flush();
                Writer.Close();
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static List<string> GetGadgetFolders()
        {
            List<string> l = new List<string>() { Gadg8GadgetsPath };

            string msPath1 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "Windows Sidebar\\Gadgets\\");
            string msPath2 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Microsoft\\Windows Sidebar\\Gadgets\\");

            if (Directory.Exists(msPath1))
                l.Add(msPath1);

            if (Directory.Exists(msPath2))
                l.Add(msPath2);

                return l;
        }

        public static List<string> GetGadgets()
        {
            List<string> folders = new List<string>();

            foreach (string folder in GetGadgetFolders())
            {
                foreach (string dir in Directory.GetDirectories(folder))
                {
                    if (dir.EndsWith(".gadget", StringComparison.InvariantCultureIgnoreCase))
                    {
                        folders.Add(dir + "\\");
                    }
                }
            }

            return folders;
        }

        public static int MakeLong(short lowPart, short highPart)
        {
            return (int)(((ushort)lowPart) | (uint)(highPart << 16));
        }

        // Return short path format of a file name
        public static string ToShortPathName(string path)
        {
            StringBuilder s = new StringBuilder(1000);
            uint iSize = (uint)s.Capacity;
            uint iRet = Native.GetShortPathName(path, s, iSize);
            return s.ToString();
        }

        public static Bitmap ToSquareBitmap(Image img, int size = 64)
        {
            Bitmap bmp = new Bitmap(size, size);
            
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.Clear(Color.Transparent /*SystemColors.Window*/);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

                // Figure out the ratio
                double ratioX = (double)bmp.Width / (double)img.Width;
                double ratioY = (double)bmp.Height / (double)img.Height;
                // use whichever multiplier is smaller
                double ratio = ratioX < ratioY ? ratioX : ratioY;

                // now we can get the new height and width
                int newHeight = Convert.ToInt32(img.Height * ratio);
                int newWidth = Convert.ToInt32(img.Width * ratio);

                // Now calculate the X,Y position of the upper-left corner 
                // (one of these will always be zero)
                int posX = Convert.ToInt32((bmp.Width - (img.Width * ratio)) / 2);
                int posY = Convert.ToInt32((bmp.Height - (img.Height * ratio)) / 2);

                g.DrawImage(img, posX, posY, newWidth, newHeight);
            }
            
            return bmp;
        }
    }
}
