﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Gadg8
{
    public class InstanceNotifier : NativeWindow
    {
        private string Arg;

        public InstanceNotifier(string arg)
        {
            this.Arg = arg;

            CreateParams cp = new CreateParams();
            this.CreateHandle(cp);
            
            // Send a global Win32 message to notify the running instance to identify itself using HWND_BROADCAST instead of
            // finding the the current instance's main window by name or something... I prefer this to using native FindWindow
            // methods.
            Native.SendMessage((IntPtr)Native.HWND_BROADCAST, Native.WM_FINDGADG8SIDEBAR, IntPtr.Zero, this.Handle);
        }

        protected override void WndProc(ref Message m)
        {
            // We receive this message back from a running instance of Gadg8's sidebar window which contains it's form handle
            // so we can then send it the arg. Once sent we just destroy the handle of insance notifier as it's not needed now.
            if (m.Msg == Native.WM_SENDGADG8SIDEBARHANDLE)
            {
                SendArg(m.LParam, this.Arg);
                this.DestroyHandle();
            }

            base.WndProc(ref m);
        }

        public static void SendArg(IntPtr handle, string arg)
        {
            // We use a COPYDATASTRUCT and WM_COPYDATA to send arg back to the running instance of our sidebar window. This
            // automatically manages the inter-process memory sharing (or copying).
            Native.COPYDATASTRUCT cds = new Native.COPYDATASTRUCT();
            byte[] sarr = System.Text.Encoding.Default.GetBytes(arg);
            int len = sarr.Length;
            cds.dwData = IntPtr.Zero;
            cds.lpData = arg;
            cds.cbData = len + 1;

            IntPtr lpStruct = Marshal.AllocHGlobal(Marshal.SizeOf(cds));
            Marshal.StructureToPtr(cds, lpStruct, false);

            Native.SendMessage(handle, Native.WM_COPYDATA, IntPtr.Zero, ref cds);
            Marshal.Release(lpStruct);
        }
    }
}
