﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Reflection;
using System.IO;
using System.Xml;
using System.Security.Permissions;
using System.Xml.Serialization;
using System.Security.Cryptography.X509Certificates;

namespace Gadg8
{
    public class X509Certificate
    {
        static public X509CertificateDetails CheckFileCertificate(string filename)
        {
            try
            {
                X509Certificate2 cert = new X509Certificate2(filename);
                string publisher = cert.GetNameInfo(X509NameType.SimpleName, false);
                string url = cert.GetNameInfo(X509NameType.DnsName, false);

                bool verified = WinTrust.VerifyEmbeddedSignature(filename);
                DateTime timestamp = WinCrypt.GetUTCTimestamp(filename);

                return new X509CertificateDetails(publisher, verified, timestamp, cert);
            }
            catch
            {
                return new X509CertificateDetails("", false, new DateTime(), null);
            }
        }

        public class X509CertificateDetails
        {
            public X509Certificate2 Certificate { get; set; }
            public string Publisher { get; private set; }
            public bool Verified { get; private set; }
            public DateTime Timestamp { get; private set; }

            public X509CertificateDetails(string publisher, bool verified, DateTime timestamp, X509Certificate2 cert)
            {
                this.Publisher = publisher;
                this.Verified = verified;
                this.Timestamp = timestamp;
                this.Certificate = cert;
            }

            public void ShowCertificate()
            {
                if (Certificate != null)
                    X509Certificate2UI.DisplayCertificate(Certificate);
            }
        }
    }
}
