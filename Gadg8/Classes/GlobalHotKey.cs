﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;

namespace Gadg8
{
    public class GlobalHotKey : NativeWindow
    {
	    [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
	    private static extern int RegisterHotKey(IntPtr hwnd, int id, int fsModifiers, int vk);

	    [DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
	    private static extern int UnregisterHotKey(IntPtr hwnd, int id);

	    [DllImport("kernel32", EntryPoint = "GlobalAddAtomA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
	    private static extern short GlobalAddAtom(string lpString);

	    [DllImport("kernel32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
	    private static extern short GlobalDeleteAtom(short nAtom);

	    private const int MOD_ALT = 1;
	    private const int MOD_CONTROL = 2;
	    private const int MOD_SHIFT = 4;
	    private const int MOD_WIN = 8;
	    private const int WM_HOTKEY = 0x312;
        
        private short hotkeyID;
        private string Name;

        public event EventHandler HotKeyPressed;
        
        public GlobalHotKey()
        {
            Random rnd = new Random();
            Name = "Gadg8HotKey_" + rnd.Next();

            CreateParams cp = new CreateParams();

            // Fill in the CreateParams details.
            cp.Caption = "";
            cp.ClassName = "STATIC";

            // Set the position on the form
            cp.X = 100;
            cp.Y = 100;
            cp.Height = 100;
            cp.Width = 100;

            this.CreateHandle(cp);
            //Debug.WriteLine(Me.Handle)
        }

        ~GlobalHotKey()
        {
            UnregisterHotKey();
		    this.ReleaseHandle();
		    this.DestroyHandle();
        }

	    protected override void WndProc(ref System.Windows.Forms.Message m)
	    {
		    if (m.Msg == WM_HOTKEY)
            {
			    if (HotKeyPressed != null)
                {
				    HotKeyPressed(this, new EventArgs());
			    }
		    }
            
            DefWndProc(ref m);
	    }

        public bool RegisterHotKey(Keys keycode, bool windowsKey = false, bool altKey = false, bool shiftKey = false,
            bool controlKey = false)
	    {
            UnregisterHotKey();

		    int modkeys = 0;

            if (windowsKey)
                modkeys += MOD_WIN;
            
            if (altKey)
			    modkeys += MOD_ALT;
            
		    if (shiftKey)
                modkeys += MOD_SHIFT;
            
            if (controlKey)
			    modkeys += MOD_CONTROL;

            try
            {
                string atomName = Process.GetCurrentProcess().Id.ToString("X8") + this.Name;
                hotkeyID = GlobalAddAtom(atomName);
                if (hotkeyID == 0)
                {
                    return false;
                }

                return (RegisterHotKey(this.Handle, hotkeyID, modkeys, Convert.ToInt32(keycode)) == 0);
            }
            catch
            {
                UnregisterHotKey();
                return false;
            }
	    }

        public void UnregisterHotKey()
	    {
            if (this.hotkeyID != 0)
            {
                UnregisterHotKey(this.Handle, hotkeyID);
                GlobalDeleteAtom(hotkeyID);
                hotkeyID = 0;
            }
	    }
    }
}
