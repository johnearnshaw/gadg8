﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.Win32;

// pinvoke methods
internal class Native
{
    [DllImport("USER32.DLL")]
    public static extern bool SetForegroundWindow(IntPtr hWnd);

    [DllImport("uxtheme.dll")]
    public extern static int SetWindowTheme(IntPtr hWnd, [MarshalAs(UnmanagedType.LPWStr)] string pszSubAppName,
        [MarshalAs(UnmanagedType.LPWStr)] string pszSubIdList);

    [DllImport("Kernel32.dll")]
    public static extern uint GetShortPathName(string lpszLongPath, [Out] StringBuilder lpszShortPath, uint cchBuffer);

    [DllImport("shell32.dll")]
    public static extern void SHChangeNotify(int wEventId, int uFlags, IntPtr dwItem1, IntPtr dwItem2);

    public const int SHCNE_ASSOCCHANGED = 0x08000000;
    public const int SHCNF_IDLIST = 0x0000;

    [DllImport("oleaut32.dll")]
    public static extern int RegisterActiveObject([MarshalAs(UnmanagedType.IDispatch)] object punk, ref Guid rclsid, uint dwFlags, out uint pdwRegister);
    
    [DllImport("oleaut32.dll")]
    public static extern int GetActiveObject(ref Guid rclsid, IntPtr pvReserved, [MarshalAs(UnmanagedType.IDispatch)] out object ppunk);

    [DllImport("oleaut32.dll")]
    public static extern int RevokeActiveObject(uint register, IntPtr reserved);

    #region used by single instance mutex for installing new gadgets etc...
    public const int HWND_BROADCAST = 0xffff;
    public static int WM_FINDGADG8SIDEBAR = RegisterWindowMessage("WM_FINDGADG8SIDEBAR");
    public static int WM_SENDGADG8SIDEBARHANDLE = RegisterWindowMessage("WM_SENDGADG8SIDEBARHANDLE");
    public static int WM_INSTALLGADG8GADGET = RegisterWindowMessage("WM_INSTALLGADG8GADGET");

    public const int WM_USER = 0x400;
    public const int WM_COPYDATA = 0x4A;

    //Used for WM_COPYDATA for string messages
    public struct COPYDATASTRUCT
    {
        public IntPtr dwData;
        public int cbData;
        [MarshalAs(UnmanagedType.LPStr)]
        public string lpData;
    }
    [System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SendMessage", ExactSpelling = false, CharSet = CharSet.Auto, SetLastError = true)]
    public static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

    [System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SendMessage", ExactSpelling = false, CharSet = CharSet.Auto, SetLastError = true)]
    public static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, ref COPYDATASTRUCT lParam);

    [System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SendMessage", ExactSpelling = false, CharSet = CharSet.Auto, SetLastError = true)]
    public static extern int SendMessage(IntPtr hWnd, int Msg, out IntPtr wParam, out IntPtr lParam);

    [System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SendMessage", ExactSpelling = false, CharSet = CharSet.Auto, SetLastError = true)]
    public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);

    [System.Runtime.InteropServices.DllImport("user32", EntryPoint = "SendMessage", ExactSpelling = false, CharSet = CharSet.Auto, SetLastError = true)]
    public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, IntPtr wParam, ref System.Drawing.Point lParam);

    [DllImport("User32.dll", EntryPoint = "PostMessage")]
    public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);
    
    [DllImport("user32")]
    public static extern int RegisterWindowMessage(string message);

    [DllImport("user32.dll", EntryPoint = "LockWindowUpdate", SetLastError = true, CharSet = CharSet.Auto)]
    public static extern IntPtr LockWindow(IntPtr Handle);
    #endregion

    [DllImport("User32.dll", EntryPoint = "FindWindow")]
    public static extern IntPtr FindWindow(String lpClassName, String lpWindowName);

    #region Used for checking if .gadget file is zip or cab (currently unused)

    [DllImport("urlmon.dll", CharSet = CharSet.Unicode, ExactSpelling = true, SetLastError = false)]
    public static extern int FindMimeFromData(IntPtr pBC,
          [MarshalAs(UnmanagedType.LPWStr)] string pwzUrl,
         [MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.I1, SizeParamIndex = 3)] 
        byte[] pBuffer,
          int cbSize,
             [MarshalAs(UnmanagedType.LPWStr)]  string pwzMimeProposed,
          int dwMimeFlags,
          out IntPtr ppwzMimeOut,
          int dwReserved);

    #endregion

    #region Used to override OS imposed min form width and height
    public const int WM_WINDOWPOSCHANGING = 0x0046;
    public const int WM_GETMINMAXINFO = 0x0024;

    public struct WindowPos
    {
        public IntPtr hwnd;
        public IntPtr hwndInsertAfter;
        public int x;
        public int y;
        public int width;
        public int height;
        public uint flags;
    }

    public struct POINT
    {
        public int x;
        public int y;
    }

    public struct MinMaxInfo
    {
        public POINT ptReserved;
        public POINT ptMaxSize;
        public POINT ptMaxPosition;
        public POINT ptMinTrackSize;
        public POINT ptMaxTrackSize;
    }
    #endregion

    #region used to show forms without them taking focus
    public const int SW_SHOWNOACTIVATE = 4;
    public const int HWND_TOPMOST = -1;
    public const int HWND_NOTOPMOST = -2;
    public const int HWND_TOP = 0;
    public const int HWND_BOTTOM = 1;
    public const uint SWP_NOACTIVATE = 0x0010;

    [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
    static extern bool SetWindowPos(
         IntPtr hWnd,             // Window handle
         int hWndInsertAfter,  // Placement-order handle
         int X,                // Horizontal position
         int Y,                // Vertical position
         int cx,               // Width
         int cy,               // Height
         uint uFlags);         // Window positioning flags

    [DllImport("user32.dll")]
    static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

    static void ShowInactiveTopmost(System.Windows.Forms.Form frm)
    {
        ShowWindow(frm.Handle, SW_SHOWNOACTIVATE);
        SetWindowPos(frm.Handle, HWND_TOPMOST,
        frm.Left, frm.Top, frm.Width, frm.Height,
        SWP_NOACTIVATE);
    }
    #endregion

    [DllImport("SetupApi.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern bool SetupIterateCabinet(string cabinetFile, uint
    reserved, PSP_FILE_CALLBACK callBack, IntPtr context);

    public const uint SPFILENOTIFY_FILEINCABINET = 0x00000011;
    public const uint SPFILENOTIFY_NEEDNEWCABINET = 0x00000012;
    public const uint SPFILENOTIFY_FILEEXTRACTED = 0x00000013;
    public const uint NO_ERROR = 0;

    public const int LVM_FIRST = 0x1000;
    public const int LVM_SETICONSPACING = LVM_FIRST + 53;
    public const int LVM_SETTILEWIDTH = LVM_FIRST + 141;

    public const int EM_SETMARGINS = 0x00D3;
    public const int EM_GETSEL = 0x00B0;
    public const int EM_SETSEL = 0x00B1;
    public const int EM_GETRECT = 0x00B2;
    public const int EM_LINESCROLL = 0x00B6;
    public const int EM_GETLINECOUNT = 0x00BA;
    public const int EM_LINEINDEX = 0x00BB;
    public const int EM_LINEFROMCHAR = 0x00C9;
    public const int EM_GETFIRSTVISIBLELINE = 0x00CE;
    public const int EM_GETLIMITTEXT = 0x00D5;
    public const int EM_POSFROMCHAR = 0x00D6;
    public const int EM_CHARFROMPOS = 0x00D7;
    public const int EM_SETCHARFORMAT = 0x0444;
    public const int EM_GETSCROLLPOS = 0x0400 + 221;
    public const int EM_SETSCROLLPOS = 0x0400 + 222;

    public const int SCF_SELECTION = 0x01;

    public const int EC_LEFTMARGIN = 0x0001;
    public const int EC_RIGHTMARGIN = 0x0002;

    public const int WM_SETREDRAW = 11;
    public const int WM_SETFOCUS = 0x0007;

    public struct POINTL
    {
        public Int32 X;
        public Int32 Y;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct CHARFORMAT
    {
        public int cbSize;
        public UInt32 dwMask;
        public UInt32 dwEffects;
        public Int32 yHeight;
        public Int32 yOffset;
        public Int32 crTextColor;
        public byte bCharSet;
        public byte bPitchAndFamily;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public char[] szFaceName;
    }

    public delegate uint PSP_FILE_CALLBACK(IntPtr context, uint notification, IntPtr param1, IntPtr param2);

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public class FILE_IN_CABINET_INFO
    {
        public string NameInCabinet;
        public uint FileSize;
        public uint Win32Error;
        public ushort DosDate;
        public ushort DosTime;
        public ushort DosAttribs;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
        public System.String FullTargetName;
    }

    private const int LWA_ALPHA = 0x2;
    
    [DllImport("user32.dll")]
    private static extern int SetLayeredWindowAttributes(IntPtr hWnd, int crKey, byte bAlpha, int dwFlags);

    public static bool SetOpacity(IntPtr handle, byte pbytAlpha)
    {
        return SetLayeredWindowAttributes(handle, 0, pbytAlpha, LWA_ALPHA) != 0;
    }
    
    public static bool IsRunningOnWin64
    {
        get {
            return (IntPtr.Size == 8);
        }
    }
    
    private const int GWL_HWNDPARENT = -8;
    
    private const long GWLP_HWNDPARENT = -8;
    
    [DllImport("user32.dll", EntryPoint="GetWindowLongA")]
    private static extern int GetWindowLong(int hWnd, int nIndex);
    
    [DllImport("user32.dll", EntryPoint="SetWindowLongA")]
    private static extern int SetWindowLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong);
    
    [DllImport("user32.dll", EntryPoint="SetWindowLongPtrA")]
    private static extern long SetWindowLongPtr(IntPtr hWnd, long nIndex, IntPtr dwNewLong);
}
