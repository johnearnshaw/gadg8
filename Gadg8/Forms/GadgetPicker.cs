﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Gadg8Gadget;

namespace Gadg8
{
    public partial class GadgetPicker : Form
    {
        private Sidebar SidebarWindow;
        private int SelectedIndex = 0;
        private bool cancel = false;

        private List<GadgetManifest> GadgetXmlList;
        private List<string> GadgetXmlFolderList;
        private List<string> GadgetFolderList;

        private List<FileSystemWatcher> FSWatcherList;

        private ContextMenu GadgetMenu;

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            Native.SetForegroundWindow(this.Handle);
        }

        public GadgetPicker(Sidebar sidebarWindow)
        {
            InitializeComponent();

            this.SidebarWindow = sidebarWindow;
            this.Icon = sidebarWindow.Icon;

            this.DescriptionText.Text = "";
            this.CopywrightText.Text = "";
            this.AuthorText.Text = "";
            this.VersionText.Text = "";

            this.FormClosing += GadgetPicker_FormClosing;

            CreateFileSystemWatchers();

            GetMoreGadgetsLink.SizeChanged += (sender, e) =>
            {
                GetMoreGadgetsLink.Left = (this.ClientRectangle.Width - GetMoreGadgetsLink.Width) - 10;
            };

            GetMoreGadgetsLink.Click += (sender, e) =>
            {
                Gadg8Gadget.Helpers.OpenLink("http://gadg8.org/get-more-gadgets/");
            };

            GetMoreGadgetsLink.Text = Localize.Strings.GetMoreGadgets;

            Native.SetWindowTheme(GadgetList.Handle, "explorer", null);
            //Native.SendMessage(GadgetList.Handle, Native.LVM_SETICONSPACING, IntPtr.Zero, (IntPtr)Helpers.MakeLong(110, 110));
            //Native.SendMessage(GadgetList.Handle, Native.LVM_SETTILEWIDTH, IntPtr.Zero, (IntPtr)100);

            GadgetList.MultiSelect = false;
            GadgetList.View = View.LargeIcon;
            GadgetList.Sorting = SortOrder.Ascending;

            GadgetList.Groups.Add(new ListViewGroup(Localize.Strings.Gadg8DesktopGadgets));
            GadgetList.Groups.Add(new ListViewGroup(Localize.Strings.WindowsDesktopGadgets));
            GadgetList.ShowGroups = true;

            GadgetList.ItemDrag += GadgetListItemDrag;
            GadgetList.ItemSelectionChanged += GadgetListItemSelectionChanged;
            GadgetList.SelectedIndexChanged += GadgetListSelectedIndexChanged;
            GadgetList.MouseDoubleClick += GadgetListMouseDoubleClick;
            GadgetList.KeyUp += GadgetListKeyUp;
            
            GadgetMenu = new ContextMenu();
            GadgetMenu.Popup += GadgetMenuPopup;

            GadgetList.ContextMenu = GadgetMenu;

            RefreshGadgets();
        }

        private void GadgetPicker_FormClosing(object sender, FormClosingEventArgs e)
        {
            cancel = true;
        }

        private void CreateFileSystemWatchers()
        {
            FSWatcherList = new List<FileSystemWatcher>();

            if (!Directory.Exists(Helpers.Gadg8GadgetsPath))
                Directory.CreateDirectory(Helpers.Gadg8GadgetsPath);

            foreach (string g in Helpers.GetGadgetFolders())
            {
                FileSystemWatcher watcher = new FileSystemWatcher(g.TrimEnd('\\'));

                watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName;

                watcher.Created += (sender, e) =>
                {
                    System.Threading.Timer t = new System.Threading.Timer(new System.Threading.TimerCallback((x) =>
                    {
                        this.Invoke(new MethodInvoker(() =>
                        {
                            string path = e.FullPath.ToLower() + "\\";
                            AddGadget(path);

                            SetSelectedIndex();
                        }));
                    }), null, 2500, System.Threading.Timeout.Infinite);
                };
                
                watcher.Deleted += (sender, e) =>
                {
                    this.Invoke(new MethodInvoker(() =>
                    {
                        string path = e.FullPath.ToLower() + "\\";

                        if (GadgetFolderList.Contains(path))
                        {
                            foreach (ListViewItem i in GadgetList.Items)
                            {
                                ListViewItem.ListViewSubItem si = i.SubItems[1];
                                if (si.Text.ToLower() == path)
                                {
                                    GadgetList.Items.Remove(i);
                                    break;
                                }
                            }
                            
                            SetSelectedIndex();
                        }
                    }));
                };
                
                watcher.Renamed += (sender, e) =>
                {
                    this.Invoke(new MethodInvoker(() =>
                    {
                        string old = e.OldFullPath.ToLower() + "\\";
                        string path = e.FullPath.ToLower() + "\\";

                        if (GadgetFolderList.Contains(old))
                        {
                            foreach (ListViewItem i in GadgetList.Items)
                            {
                                ListViewItem.ListViewSubItem si = i.SubItems[1];
                                if (si.Text.ToLower() == old)
                                {
                                    si.Text = path;
                                    GadgetXmlFolderList[i.ImageIndex] = GadgetXmlFolderList[i.ImageIndex].Replace(old, path);
                                    break;
                                }
                            }
                        }
                    }));
                };

                watcher.EnableRaisingEvents = true;
                FSWatcherList.Add(watcher);
            }
        }

        private void GadgetMenuPopup(object sender, EventArgs e)
        {
            if (GadgetList.SelectedItems.Count > 0)
            {
                ListViewItem item = GadgetList.SelectedItems[0];
                string path = item.SubItems[1].Text;

                GadgetMenu.MenuItems.Clear();
                GadgetMenu.MenuItems.Add(new MenuItem(Localize.Strings.RunGadget, (s, ea) =>
                {
                    this.SidebarWindow.RunGadget(path);
                }));

                GadgetMenu.MenuItems.Add(new MenuItem(Localize.Strings.GetMoreGadgets, (s, ea) =>
                {
                    Gadg8Gadget.Helpers.OpenLink("http://gadg8.org/get-more-gadgets/");
                }));

                if (path.StartsWith(Helpers.Gadg8GadgetsPath, StringComparison.InvariantCultureIgnoreCase))
                {
                    GadgetMenu.MenuItems.Add("-");
                    GadgetMenu.MenuItems.Add(new MenuItem(Localize.Strings.UninstallGadget, (s, ea) =>
                    {
                        if (this.SidebarWindow.UninstallGadget(path, GadgetXmlList[item.ImageIndex].Name))
                        {
                            GadgetList.Items.Remove(item);
                            SetSelectedIndex();
                        }
                    }));
                }
            }
            else
            {
                // Clear items to stop menu showing.
                GadgetMenu.MenuItems.Clear();
            }
        }

        private void GadgetListKeyUp(object sender, KeyEventArgs e)
        {
            if (GadgetList.SelectedItems.Count == 1 && e.KeyCode == Keys.Enter)
            {
                ListViewItem item = GadgetList.SelectedItems[0];
                this.SidebarWindow.RunGadget(item.SubItems[1].Text);
            }
        }

        private void GadgetListMouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (GadgetList.SelectedItems.Count == 1 && e.Button == MouseButtons.Left)
            {
                ListViewItem item = GadgetList.SelectedItems[0];
                this.SidebarWindow.RunGadget(item.SubItems[1].Text);
            }
        }

        private void GadgetListSelectedIndexChanged(object sender, EventArgs e)
        {
            //if (GadgetList.Items.Count> 0 && GadgetList.FocusedItem == null)
            //{
            //    GadgetList.SelectedIndices.Add(SelectedIndex);
            //}
        }

        private void GadgetListItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                SelectedIndex = GadgetList.SelectedItems[0].ImageIndex;
                FillGadgetInfo(SelectedIndex);
            }
        }

        private void FillGadgetInfo(int index)
        {
            GadgetManifest gadgetXml = GadgetXmlList[index];

            VersionText.Text = Localize.Strings.Version + " " + gadgetXml.Version.Trim();
            DescriptionText.Text = gadgetXml.description;

            AuthorText.Text = gadgetXml.Author.Name;

            CopywrightText.Text = gadgetXml.Copyright;

            if (gadgetXml.Author != null && gadgetXml.Author.Logo != null && !string.IsNullOrEmpty(gadgetXml.Author.Logo.Src))
            {
                if (gadgetXml.Author.Logo.Src.StartsWith("../"))
                {
                    string f = GadgetXmlFolderList[index];
                    string src = gadgetXml.Author.Logo.Src;

                    if (f.EndsWith(".gadget", StringComparison.InvariantCultureIgnoreCase))
                        src = src.Substring(src.LastIndexOf("../") + 3);

                    if (File.Exists(f + "\\" + src))
                    {
                        AuthorImage.ImageLocation = f + "\\" + src;
                    }
                    else
                    {
                        AuthorImage.ImageLocation = "";
                    }
                }
                else
                {
                    string f = GadgetFolderList[index];
                    string src = gadgetXml.Author.Logo.Src;

                    if (File.Exists(f + "\\" + src))
                    {
                        AuthorImage.ImageLocation = f + src;
                    }
                    else
                    {
                        AuthorImage.ImageLocation = "";
                    }
                }
            }
            else
            {
                AuthorImage.ImageLocation = "";
            }
        }

        private void GadgetListItemDrag(object sender, ItemDragEventArgs e)
        {
            ListViewItem item = (ListViewItem)e.Item;
            DoDragDrop(item, DragDropEffects.Move);
            //MessageBox.Show("dragged " + item);
        }

        private void RefreshGadgets()
        {
            System.Threading.Thread updateThread = new System.Threading.Thread(() =>
            {
                if (GadgetXmlList == null)
                    GadgetXmlList = new List<GadgetManifest>();

                if (GadgetXmlFolderList == null)
                    GadgetXmlFolderList = new List<string>();

                if (GadgetFolderList == null)
                    GadgetFolderList = new List<string>();


                GadgetXmlList.Clear();
                GadgetXmlFolderList.Clear();
                GadgetFolderList.Clear();

                ListViewItem.ListViewSubItem subItem;
                ListViewItem item = null;
                ImageList imageList = new ImageList();
                imageList.ImageSize = new Size(64, 64);

                GadgetList.Invoke(new MethodInvoker(() =>
                {
                    GadgetList.Items.Clear();
                    GadgetList.LargeImageList = imageList;
                }));

                int counter = 0;

                foreach (string path in Helpers.GetGadgets())
                {
                    if (cancel)
                    {
                        System.Threading.Thread.CurrentThread.Abort();
                        return;
                    }

                    string xmlFolder;
                    GadgetManifest gadgetXml = Gadg8Gadget.Helpers.GetGadgetXML(path, out xmlFolder);

                    if (gadgetXml != null)
                    {
                        GadgetXmlList.Add(gadgetXml);
                        GadgetXmlFolderList.Add(xmlFolder.ToLower());
                        GadgetFolderList.Add(path.ToLower());

                        Bitmap icon;

                        if (File.Exists(path + gadgetXml.Icons[0].Src))
                        {
                            icon = (Bitmap)Image.FromFile(path + gadgetXml.Icons[0].Src);
                        }
                        else
                        {
                            icon = Properties.Resources.gadg8package;
                        }

                        Bitmap bmp = Helpers.ToSquareBitmap(icon);
                        
                        icon.Dispose();

                        try
                        {
                            GadgetList.Invoke(new MethodInvoker(() =>
                            {
                                imageList.Images.Add(bmp);
                            }));
                        }
                        catch { }

                        ListViewGroup group;

                        if (path.StartsWith(Helpers.Gadg8GadgetsPath, StringComparison.InvariantCultureIgnoreCase))
                        {
                            group = GadgetList.Groups[0];
                        }
                        else
                        {
                            group = GadgetList.Groups[1];
                        }

                        item = new ListViewItem(gadgetXml.Name, counter, group);

                        subItem = new ListViewItem.ListViewSubItem(item, path);

                        item.SubItems.Add(subItem);

                        try
                        {
                            GadgetList.Invoke(new MethodInvoker(() =>
                            {
                                GadgetList.Items.Add(item);
                                GadgetList.Sort();
                            }));
                        }
                        catch { }

                        counter++;
                    }
                }

                //SetSelectedIndex();
            });

            updateThread.Start();
        }

        private void SetSelectedIndex()
        {
            GadgetList.Invoke(new MethodInvoker(() =>
            {
                if (GadgetList.Items.Count > 0)
                {
                    if (GadgetList.Items.Count > SelectedIndex)
                    {
                        GadgetList.SelectedIndices.Add(SelectedIndex);
                    }
                    else
                    {
                        GadgetList.SelectedIndices.Add(GadgetList.Items.Count - 1);
                    }
                }
            }));
        }

        private void AddGadget(string path)
        {
            
            ListViewItem.ListViewSubItem subItem;
            ListViewItem item = null;

            string xmlFolder;
            GadgetManifest gadgetXml = Gadg8Gadget.Helpers.GetGadgetXML(path, out xmlFolder);

            if (gadgetXml != null)
            {
                GadgetXmlList.Add(gadgetXml);
                GadgetXmlFolderList.Add(xmlFolder.ToLower());
                GadgetFolderList.Add(path.ToLower());

                Bitmap icon;

                if (File.Exists(path + gadgetXml.Icons[0].Src))
                {
                    icon = (Bitmap)Image.FromFile(path + gadgetXml.Icons[0].Src);
                }
                else
                {
                    icon = Properties.Resources.gadg8package;
                }

                Bitmap bmp = Helpers.ToSquareBitmap(icon);

                icon.Dispose();

                GadgetList.Invoke(new MethodInvoker(() =>
                {
                    GadgetList.LargeImageList.Images.Add(bmp);
                }));

                ListViewGroup group;

                if (path.StartsWith(Helpers.Gadg8GadgetsPath, StringComparison.InvariantCultureIgnoreCase))
                {
                    group = GadgetList.Groups[0];
                }
                else
                {
                    group = GadgetList.Groups[1];
                }

                item = new ListViewItem(gadgetXml.Name, GadgetList.LargeImageList.Images.Count - 1, group);

                subItem = new ListViewItem.ListViewSubItem(item, path);

                item.SubItems.Add(subItem);

                GadgetList.Invoke(new MethodInvoker(() =>
                {
                    GadgetList.Items.Add(item);
                    GadgetList.Sort();
                }));
            }
        }
    }
}
