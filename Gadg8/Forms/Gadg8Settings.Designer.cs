﻿namespace Gadg8
{
    partial class Gadg8Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.ButtonOK = new System.Windows.Forms.Button();
            this.SettingsTabControl = new System.Windows.Forms.TabControl();
            this.GeneralSettings = new System.Windows.Forms.TabPage();
            this.DesktopMenuCheckbox = new System.Windows.Forms.CheckBox();
            this.FileAssociationCheckbox = new System.Windows.Forms.CheckBox();
            this.RunOnStartupCheckbox = new System.Windows.Forms.CheckBox();
            this.DevSettings = new System.Windows.Forms.TabPage();
            this.Gadg8LogCheckbox = new System.Windows.Forms.CheckBox();
            this.OutputWindowCheckbox = new System.Windows.Forms.CheckBox();
            this.ShowDevCheckbox = new System.Windows.Forms.CheckBox();
            this.ResetAllSettingsButton = new System.Windows.Forms.Button();
            this.AboutSettings = new System.Windows.Forms.TabPage();
            this.WebLinkText = new System.Windows.Forms.LinkLabel();
            this.DescriptionText = new System.Windows.Forms.Label();
            this.VersionText = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.AdvancedSettings = new System.Windows.Forms.TabPage();
            this.panel1.SuspendLayout();
            this.SettingsTabControl.SuspendLayout();
            this.GeneralSettings.SuspendLayout();
            this.DevSettings.SuspendLayout();
            this.AboutSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ButtonCancel);
            this.panel1.Controls.Add(this.ButtonOK);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 313);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(334, 38);
            this.panel1.TabIndex = 0;
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Location = new System.Drawing.Point(254, 8);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.ButtonCancel.TabIndex = 1;
            this.ButtonCancel.Text = "Cancel";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            // 
            // ButtonOK
            // 
            this.ButtonOK.Location = new System.Drawing.Point(173, 8);
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.Size = new System.Drawing.Size(75, 23);
            this.ButtonOK.TabIndex = 0;
            this.ButtonOK.Text = "OK";
            this.ButtonOK.UseVisualStyleBackColor = true;
            // 
            // SettingsTabControl
            // 
            this.SettingsTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SettingsTabControl.Controls.Add(this.GeneralSettings);
            this.SettingsTabControl.Controls.Add(this.AdvancedSettings);
            this.SettingsTabControl.Controls.Add(this.DevSettings);
            this.SettingsTabControl.Controls.Add(this.AboutSettings);
            this.SettingsTabControl.Location = new System.Drawing.Point(6, 6);
            this.SettingsTabControl.Name = "SettingsTabControl";
            this.SettingsTabControl.SelectedIndex = 0;
            this.SettingsTabControl.Size = new System.Drawing.Size(324, 309);
            this.SettingsTabControl.TabIndex = 1;
            // 
            // GeneralSettings
            // 
            this.GeneralSettings.Controls.Add(this.DesktopMenuCheckbox);
            this.GeneralSettings.Controls.Add(this.FileAssociationCheckbox);
            this.GeneralSettings.Controls.Add(this.RunOnStartupCheckbox);
            this.GeneralSettings.Location = new System.Drawing.Point(4, 22);
            this.GeneralSettings.Name = "GeneralSettings";
            this.GeneralSettings.Padding = new System.Windows.Forms.Padding(3);
            this.GeneralSettings.Size = new System.Drawing.Size(316, 283);
            this.GeneralSettings.TabIndex = 0;
            this.GeneralSettings.Text = "General";
            this.GeneralSettings.UseVisualStyleBackColor = true;
            // 
            // DesktopMenuCheckbox
            // 
            this.DesktopMenuCheckbox.AutoSize = true;
            this.DesktopMenuCheckbox.Location = new System.Drawing.Point(14, 63);
            this.DesktopMenuCheckbox.Name = "DesktopMenuCheckbox";
            this.DesktopMenuCheckbox.Size = new System.Drawing.Size(145, 17);
            this.DesktopMenuCheckbox.TabIndex = 2;
            this.DesktopMenuCheckbox.Text = "desktop menu integration";
            this.DesktopMenuCheckbox.UseVisualStyleBackColor = true;
            // 
            // FileAssociationCheckbox
            // 
            this.FileAssociationCheckbox.AutoSize = true;
            this.FileAssociationCheckbox.Location = new System.Drawing.Point(14, 39);
            this.FileAssociationCheckbox.Name = "FileAssociationCheckbox";
            this.FileAssociationCheckbox.Size = new System.Drawing.Size(95, 17);
            this.FileAssociationCheckbox.TabIndex = 1;
            this.FileAssociationCheckbox.Text = "file association";
            this.FileAssociationCheckbox.UseVisualStyleBackColor = true;
            // 
            // RunOnStartupCheckbox
            // 
            this.RunOnStartupCheckbox.AutoSize = true;
            this.RunOnStartupCheckbox.Location = new System.Drawing.Point(14, 16);
            this.RunOnStartupCheckbox.Name = "RunOnStartupCheckbox";
            this.RunOnStartupCheckbox.Size = new System.Drawing.Size(91, 17);
            this.RunOnStartupCheckbox.TabIndex = 0;
            this.RunOnStartupCheckbox.Text = "run on startup";
            this.RunOnStartupCheckbox.UseVisualStyleBackColor = true;
            // 
            // DevSettings
            // 
            this.DevSettings.Controls.Add(this.Gadg8LogCheckbox);
            this.DevSettings.Controls.Add(this.OutputWindowCheckbox);
            this.DevSettings.Controls.Add(this.ShowDevCheckbox);
            this.DevSettings.Controls.Add(this.ResetAllSettingsButton);
            this.DevSettings.Location = new System.Drawing.Point(4, 22);
            this.DevSettings.Name = "DevSettings";
            this.DevSettings.Padding = new System.Windows.Forms.Padding(3);
            this.DevSettings.Size = new System.Drawing.Size(316, 283);
            this.DevSettings.TabIndex = 1;
            this.DevSettings.Text = "Dev & Debug";
            this.DevSettings.UseVisualStyleBackColor = true;
            // 
            // Gadg8LogCheckbox
            // 
            this.Gadg8LogCheckbox.AutoSize = true;
            this.Gadg8LogCheckbox.Location = new System.Drawing.Point(14, 62);
            this.Gadg8LogCheckbox.Name = "Gadg8LogCheckbox";
            this.Gadg8LogCheckbox.Size = new System.Drawing.Size(108, 17);
            this.Gadg8LogCheckbox.TabIndex = 6;
            this.Gadg8LogCheckbox.Text = "enable gadg8 log";
            this.Gadg8LogCheckbox.UseVisualStyleBackColor = true;
            // 
            // OutputWindowCheckbox
            // 
            this.OutputWindowCheckbox.AutoSize = true;
            this.OutputWindowCheckbox.Location = new System.Drawing.Point(14, 39);
            this.OutputWindowCheckbox.Name = "OutputWindowCheckbox";
            this.OutputWindowCheckbox.Size = new System.Drawing.Size(159, 17);
            this.OutputWindowCheckbox.TabIndex = 5;
            this.OutputWindowCheckbox.Text = "show gadget output window";
            this.OutputWindowCheckbox.UseVisualStyleBackColor = true;
            // 
            // ShowDevCheckbox
            // 
            this.ShowDevCheckbox.AutoSize = true;
            this.ShowDevCheckbox.Location = new System.Drawing.Point(14, 16);
            this.ShowDevCheckbox.Name = "ShowDevCheckbox";
            this.ShowDevCheckbox.Size = new System.Drawing.Size(175, 17);
            this.ShowDevCheckbox.TabIndex = 4;
            this.ShowDevCheckbox.Text = "enable gadget developer otions";
            this.ShowDevCheckbox.UseVisualStyleBackColor = true;
            // 
            // ResetAllSettingsButton
            // 
            this.ResetAllSettingsButton.Location = new System.Drawing.Point(98, 247);
            this.ResetAllSettingsButton.Name = "ResetAllSettingsButton";
            this.ResetAllSettingsButton.Size = new System.Drawing.Size(121, 23);
            this.ResetAllSettingsButton.TabIndex = 3;
            this.ResetAllSettingsButton.Text = "reset all settings";
            this.ResetAllSettingsButton.UseVisualStyleBackColor = true;
            // 
            // AboutSettings
            // 
            this.AboutSettings.Controls.Add(this.WebLinkText);
            this.AboutSettings.Controls.Add(this.DescriptionText);
            this.AboutSettings.Controls.Add(this.VersionText);
            this.AboutSettings.Controls.Add(this.pictureBox1);
            this.AboutSettings.Location = new System.Drawing.Point(4, 22);
            this.AboutSettings.Name = "AboutSettings";
            this.AboutSettings.Size = new System.Drawing.Size(316, 283);
            this.AboutSettings.TabIndex = 2;
            this.AboutSettings.Text = "About";
            this.AboutSettings.UseVisualStyleBackColor = true;
            // 
            // WebLinkText
            // 
            this.WebLinkText.ActiveLinkColor = System.Drawing.Color.SteelBlue;
            this.WebLinkText.AutoSize = true;
            this.WebLinkText.Cursor = System.Windows.Forms.Cursors.Hand;
            this.WebLinkText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WebLinkText.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.WebLinkText.LinkColor = System.Drawing.Color.DodgerBlue;
            this.WebLinkText.Location = new System.Drawing.Point(491, 12);
            this.WebLinkText.Name = "WebLinkText";
            this.WebLinkText.Size = new System.Drawing.Size(62, 13);
            this.WebLinkText.TabIndex = 0;
            this.WebLinkText.TabStop = true;
            this.WebLinkText.Text = "website link";
            this.WebLinkText.VisitedLinkColor = System.Drawing.Color.SteelBlue;
            // 
            // DescriptionText
            // 
            this.DescriptionText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DescriptionText.Location = new System.Drawing.Point(6, 137);
            this.DescriptionText.Name = "DescriptionText";
            this.DescriptionText.Size = new System.Drawing.Size(304, 126);
            this.DescriptionText.TabIndex = 6;
            this.DescriptionText.Text = "description";
            // 
            // VersionText
            // 
            this.VersionText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VersionText.Location = new System.Drawing.Point(203, 3);
            this.VersionText.Name = "VersionText";
            this.VersionText.Size = new System.Drawing.Size(107, 18);
            this.VersionText.TabIndex = 5;
            this.VersionText.Text = "version";
            this.VersionText.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Gadg8.Properties.Resources.splash;
            this.pictureBox1.Location = new System.Drawing.Point(6, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(304, 105);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // AdvancedSettings
            // 
            this.AdvancedSettings.Location = new System.Drawing.Point(4, 22);
            this.AdvancedSettings.Name = "AdvancedSettings";
            this.AdvancedSettings.Size = new System.Drawing.Size(316, 283);
            this.AdvancedSettings.TabIndex = 3;
            this.AdvancedSettings.Text = "Advanced";
            this.AdvancedSettings.UseVisualStyleBackColor = true;
            // 
            // Gadg8Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 351);
            this.Controls.Add(this.SettingsTabControl);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Gadg8Settings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gadg8 Properties";
            this.panel1.ResumeLayout(false);
            this.SettingsTabControl.ResumeLayout(false);
            this.GeneralSettings.ResumeLayout(false);
            this.GeneralSettings.PerformLayout();
            this.DevSettings.ResumeLayout(false);
            this.DevSettings.PerformLayout();
            this.AboutSettings.ResumeLayout(false);
            this.AboutSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl SettingsTabControl;
        private System.Windows.Forms.TabPage GeneralSettings;
        private System.Windows.Forms.TabPage DevSettings;
        private System.Windows.Forms.Button ButtonCancel;
        private System.Windows.Forms.Button ButtonOK;
        private System.Windows.Forms.TabPage AboutSettings;
        private System.Windows.Forms.LinkLabel WebLinkText;
        private System.Windows.Forms.Label DescriptionText;
        private System.Windows.Forms.Label VersionText;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox FileAssociationCheckbox;
        private System.Windows.Forms.CheckBox RunOnStartupCheckbox;
        private System.Windows.Forms.CheckBox ShowDevCheckbox;
        private System.Windows.Forms.Button ResetAllSettingsButton;
        private System.Windows.Forms.CheckBox OutputWindowCheckbox;
        private System.Windows.Forms.CheckBox DesktopMenuCheckbox;
        private System.Windows.Forms.CheckBox Gadg8LogCheckbox;
        private System.Windows.Forms.TabPage AdvancedSettings;

    }
}