﻿namespace Gadg8
{
    partial class InstallGadgetDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.WarningMessage = new System.Windows.Forms.Label();
            this.ShieldIconBox = new System.Windows.Forms.PictureBox();
            this.FileName = new System.Windows.Forms.Label();
            this.Publisher = new System.Windows.Forms.Label();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.ButtonOK = new System.Windows.Forms.Button();
            this.ButtonDetails = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShieldIconBox)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Controls.Add(this.WarningMessage);
            this.panel1.Controls.Add(this.ShieldIconBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 100);
            this.panel1.TabIndex = 0;
            // 
            // WarningMessage
            // 
            this.WarningMessage.Location = new System.Drawing.Point(92, 15);
            this.WarningMessage.Name = "WarningMessage";
            this.WarningMessage.Size = new System.Drawing.Size(280, 70);
            this.WarningMessage.TabIndex = 4;
            this.WarningMessage.Text = "warning message";
            // 
            // ShieldIconBox
            // 
            this.ShieldIconBox.Location = new System.Drawing.Point(12, 15);
            this.ShieldIconBox.Name = "ShieldIconBox";
            this.ShieldIconBox.Size = new System.Drawing.Size(70, 70);
            this.ShieldIconBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ShieldIconBox.TabIndex = 0;
            this.ShieldIconBox.TabStop = false;
            // 
            // FileName
            // 
            this.FileName.AutoSize = true;
            this.FileName.Location = new System.Drawing.Point(9, 108);
            this.FileName.Name = "FileName";
            this.FileName.Size = new System.Drawing.Size(49, 13);
            this.FileName.TabIndex = 5;
            this.FileName.Text = "filename:";
            // 
            // Publisher
            // 
            this.Publisher.AutoSize = true;
            this.Publisher.Location = new System.Drawing.Point(9, 128);
            this.Publisher.Name = "Publisher";
            this.Publisher.Size = new System.Drawing.Size(52, 13);
            this.Publisher.TabIndex = 6;
            this.Publisher.Text = "publisher:";
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonCancel.Location = new System.Drawing.Point(297, 151);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.ButtonCancel.TabIndex = 3;
            this.ButtonCancel.Text = "no";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            // 
            // ButtonOK
            // 
            this.ButtonOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonOK.Location = new System.Drawing.Point(216, 151);
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.Size = new System.Drawing.Size(75, 23);
            this.ButtonOK.TabIndex = 2;
            this.ButtonOK.Text = "yes";
            this.ButtonOK.UseVisualStyleBackColor = true;
            // 
            // ButtonDetails
            // 
            this.ButtonDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.ButtonDetails.Location = new System.Drawing.Point(12, 151);
            this.ButtonDetails.Name = "ButtonDetails";
            this.ButtonDetails.Size = new System.Drawing.Size(164, 23);
            this.ButtonDetails.TabIndex = 1;
            this.ButtonDetails.Text = "certificate details...";
            this.ButtonDetails.UseVisualStyleBackColor = true;
            // 
            // InstallGadgetDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 186);
            this.Controls.Add(this.ButtonDetails);
            this.Controls.Add(this.ButtonOK);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.Publisher);
            this.Controls.Add(this.FileName);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InstallGadgetDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Install Gadget";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ShieldIconBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label WarningMessage;
        private System.Windows.Forms.PictureBox ShieldIconBox;
        private System.Windows.Forms.Label FileName;
        private System.Windows.Forms.Label Publisher;
        private System.Windows.Forms.Button ButtonCancel;
        private System.Windows.Forms.Button ButtonOK;
        private System.Windows.Forms.Button ButtonDetails;
    }
}