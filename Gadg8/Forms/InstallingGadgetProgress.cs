﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Gadg8
{
    public partial class InstallingGadgetProgress : Form
    {
        private bool IsCancelled = false;

        public delegate void CancelEventHandler(object sender, EventArgs e);
        public event CancelEventHandler Cancel;
        protected virtual void InvokeCancel(EventArgs e)
        {
            CancelEventHandler h = Cancel;
            if (h != null)
            {
                h(this, e);
            }
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            Native.SetForegroundWindow(this.Handle);
        }

        public InstallingGadgetProgress(string path)
        {
            InitializeComponent();

            this.Text = Localize.Strings.InstallingGadget;
            this.Filename.Text = Localize.Strings.Filename + " " + Path.GetFileName(path);

            this.Status.Text = Localize.Strings.Installing;
            this.ButtonCancel.Text = Localize.Strings.Cancel;
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            if (!this.IsCancelled)
            {
                this.Status.Text = Localize.Strings.Cancelling;
                this.InvokeCancel(new EventArgs());
                this.ButtonCancel.Enabled = false;
                this.IsCancelled = true;
            }
        }

        private delegate void dUpdateProgress(int p, string f);
        internal void UpdateProgress(int p, string f)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new dUpdateProgress(UpdateProgress), new object[] { p, f });
            }
            else
            {
                if (p < 100)
                {
                    this.Status.Text = Path.GetFileName(f);
                }

                this.Progress.Value = p;
            }
        }

        internal void InvokeClose()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(InvokeClose));
            }
            else
            {
                this.Close();
            }
        }
    }
}
