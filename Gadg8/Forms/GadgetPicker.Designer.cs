﻿namespace Gadg8
{
    partial class GadgetPicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.GetMoreGadgetsLink = new System.Windows.Forms.LinkLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CopywrightText = new System.Windows.Forms.Label();
            this.AuthorText = new System.Windows.Forms.Label();
            this.AuthorImage = new System.Windows.Forms.PictureBox();
            this.DescriptionText = new System.Windows.Forms.Label();
            this.VersionText = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.GadgetList = new Gadg8.FlickerFreeListView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AuthorImage)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.GetMoreGadgetsLink);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(570, 38);
            this.panel1.TabIndex = 0;
            // 
            // GetMoreGadgetsLink
            // 
            this.GetMoreGadgetsLink.ActiveLinkColor = System.Drawing.Color.SteelBlue;
            this.GetMoreGadgetsLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.GetMoreGadgetsLink.AutoSize = true;
            this.GetMoreGadgetsLink.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GetMoreGadgetsLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GetMoreGadgetsLink.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.GetMoreGadgetsLink.LinkColor = System.Drawing.Color.DodgerBlue;
            this.GetMoreGadgetsLink.Location = new System.Drawing.Point(477, 12);
            this.GetMoreGadgetsLink.Name = "GetMoreGadgetsLink";
            this.GetMoreGadgetsLink.Size = new System.Drawing.Size(89, 13);
            this.GetMoreGadgetsLink.TabIndex = 0;
            this.GetMoreGadgetsLink.TabStop = true;
            this.GetMoreGadgetsLink.Text = "get more gadgets";
            this.GetMoreGadgetsLink.VisitedLinkColor = System.Drawing.Color.SteelBlue;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel2.Controls.Add(this.CopywrightText);
            this.panel2.Controls.Add(this.AuthorText);
            this.panel2.Controls.Add(this.AuthorImage);
            this.panel2.Controls.Add(this.DescriptionText);
            this.panel2.Controls.Add(this.VersionText);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 265);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(570, 95);
            this.panel2.TabIndex = 1;
            // 
            // CopywrightText
            // 
            this.CopywrightText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CopywrightText.Location = new System.Drawing.Point(392, 76);
            this.CopywrightText.Name = "CopywrightText";
            this.CopywrightText.Size = new System.Drawing.Size(174, 15);
            this.CopywrightText.TabIndex = 4;
            this.CopywrightText.Text = "copyright";
            this.CopywrightText.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // AuthorText
            // 
            this.AuthorText.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AuthorText.Location = new System.Drawing.Point(391, 59);
            this.AuthorText.Name = "AuthorText";
            this.AuthorText.Size = new System.Drawing.Size(175, 15);
            this.AuthorText.TabIndex = 3;
            this.AuthorText.Text = "author";
            this.AuthorText.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // AuthorImage
            // 
            this.AuthorImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AuthorImage.Location = new System.Drawing.Point(513, 6);
            this.AuthorImage.Name = "AuthorImage";
            this.AuthorImage.Size = new System.Drawing.Size(50, 50);
            this.AuthorImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AuthorImage.TabIndex = 2;
            this.AuthorImage.TabStop = false;
            // 
            // DescriptionText
            // 
            this.DescriptionText.ForeColor = System.Drawing.SystemColors.GrayText;
            this.DescriptionText.Location = new System.Drawing.Point(3, 25);
            this.DescriptionText.Name = "DescriptionText";
            this.DescriptionText.Size = new System.Drawing.Size(320, 66);
            this.DescriptionText.TabIndex = 1;
            this.DescriptionText.Text = "description";
            // 
            // VersionText
            // 
            this.VersionText.AutoSize = true;
            this.VersionText.Location = new System.Drawing.Point(3, 6);
            this.VersionText.Name = "VersionText";
            this.VersionText.Size = new System.Drawing.Size(41, 13);
            this.VersionText.TabIndex = 0;
            this.VersionText.Text = "version";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Window;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 38);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(145, 227);
            this.panel3.TabIndex = 2;
            this.panel3.Visible = false;
            // 
            // GadgetList
            // 
            this.GadgetList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GadgetList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GadgetList.Location = new System.Drawing.Point(145, 38);
            this.GadgetList.Name = "GadgetList";
            this.GadgetList.Size = new System.Drawing.Size(425, 227);
            this.GadgetList.TabIndex = 3;
            this.GadgetList.UseCompatibleStateImageBehavior = false;
            // 
            // GadgetPicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 360);
            this.Controls.Add(this.GadgetList);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(400, 300);
            this.Name = "GadgetPicker";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Gadgets";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AuthorImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private Gadg8.FlickerFreeListView GadgetList;
        private System.Windows.Forms.Label VersionText;
        private System.Windows.Forms.Label DescriptionText;
        private System.Windows.Forms.Label CopywrightText;
        private System.Windows.Forms.Label AuthorText;
        private System.Windows.Forms.PictureBox AuthorImage;
        private System.Windows.Forms.LinkLabel GetMoreGadgetsLink;

    }
}