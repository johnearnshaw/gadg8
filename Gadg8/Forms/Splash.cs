﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Gadg8
{
    public partial class frmSplash : Form
    {
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= (int)Gadg8Gadget.Native.ExStyleFlags.WS_EX_TOOLWINDOW;
                return cp;
            }
        }

        public frmSplash()
        {
            InitializeComponent();
            SetTopLevel(true);
        }

        private delegate void dUpdateStatus(string text);
        public void UpdateStatus(string text)
        {
            if (InvokeRequired)
            {
                this.Invoke(new dUpdateStatus(UpdateStatus), new object[] { text });
            }
            else
            {
                StatusText.Text = text;
            }
        }

        private delegate void dUpdateProgress(int val);
        public void UpdateProgress(int val)
        {
            if (InvokeRequired)
            {
                this.Invoke(new dUpdateProgress(UpdateProgress), new object[] { val });
            }
            else
            {
                if (val == 0)
                {
                    Progress.Visible = false;
                    Progress.Value = 0;
                }
                else
                {
                    Progress.Visible = true;
                    Progress.Value = val;
                }
            }
        }
    }
}
