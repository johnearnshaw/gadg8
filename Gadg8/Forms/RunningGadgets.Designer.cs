﻿namespace Gadg8
{
    partial class RunningGadgets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RunningGadgetsListBox = new System.Windows.Forms.ListBox();
            this.CloseGadgetButton = new System.Windows.Forms.Button();
            this.ShowGadgetButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RunningGadgetsListBox
            // 
            this.RunningGadgetsListBox.FormattingEnabled = true;
            this.RunningGadgetsListBox.Location = new System.Drawing.Point(13, 13);
            this.RunningGadgetsListBox.Name = "RunningGadgetsListBox";
            this.RunningGadgetsListBox.Size = new System.Drawing.Size(259, 199);
            this.RunningGadgetsListBox.TabIndex = 0;
            // 
            // CloseGadgetButton
            // 
            this.CloseGadgetButton.Location = new System.Drawing.Point(146, 226);
            this.CloseGadgetButton.Name = "CloseGadgetButton";
            this.CloseGadgetButton.Size = new System.Drawing.Size(126, 23);
            this.CloseGadgetButton.TabIndex = 1;
            this.CloseGadgetButton.Text = "close gadget";
            this.CloseGadgetButton.UseVisualStyleBackColor = true;
            // 
            // ShowGadgetButton
            // 
            this.ShowGadgetButton.Location = new System.Drawing.Point(13, 226);
            this.ShowGadgetButton.Name = "ShowGadgetButton";
            this.ShowGadgetButton.Size = new System.Drawing.Size(127, 23);
            this.ShowGadgetButton.TabIndex = 2;
            this.ShowGadgetButton.Text = "show gadget";
            this.ShowGadgetButton.UseVisualStyleBackColor = true;
            // 
            // RunningGadgets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.ShowGadgetButton);
            this.Controls.Add(this.CloseGadgetButton);
            this.Controls.Add(this.RunningGadgetsListBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RunningGadgets";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Running Gadgets";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox RunningGadgetsListBox;
        private System.Windows.Forms.Button CloseGadgetButton;
        private System.Windows.Forms.Button ShowGadgetButton;
    }
}