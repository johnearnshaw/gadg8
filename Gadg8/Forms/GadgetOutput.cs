﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Gadg8
{
    public partial class GadgetOutput : Form
    {
        int oldWidth;

        string[] Specials = { "String", "Date", "Math", "Array", "Boolean", "NaN", "Number", "Object", "E", "RegExp", "System" };
        //string[] Specials = { "Anchor", "Applet", "Area", "Array", "Boolean", "Button", "Checkbox", "Date", "E", "FileUpload" }, 
                                //"Form", "Frame", "Function", "Hidden", "History", "Image", "Infinity", "JavaArray", "JavaClass",
                                //"JavaObject", "JavaPackage", "LN10", "LN2", "LOG10E", "LOG2E", "Layer", "Link", "Location" },
                                //"MAX_VALUE", "MIN_VALUE", "Math", "MimeType", "NEGATIVE_INFINITY", "NaN", "Number", "Object",
                                // "Option", "PI", "POSITIVE_INFINITY", "Packages", "Password", "Plugin", "Radio", "RegExp", "Reset",
                                // "SQRT1_2", "SQRT2", "Select", "String", "Style", "Submit", "Text", "Textarea", "URL",
                                // "URLUnencoded", "UTC", "XMLDocument", "XSLDocument" };

        string[] Keywords = { "var", "function", "new", "for", "each", "in", "string", "boolean", "int", "float", "enum", "eval", "do",
                            "while", "try", "catch", "throw", "break", "single", "double", "if", "else" };
        //string[] Keywords = { "_content", "aLinkcolor", "above", "abs", "acos", "action", "alert", "align", "all", "anchor" },
                                //"anchors", "appCodeName", "appCore", "appMinorVersion", "appName", "appVersion", "applets", "apply",
                                //"arguments", "arguments.callee", "arguments.caller", "arguments.length", "arity", "asin", "atan" },
                                //"atan2", "atob", "attachEvent", "attributes", "availHeight", "availLeft", "availTop", "availWidth",
                                //"back", "background", "backgroundColor", "backgroundImage", "below", "bgColor", "big", "blink", "blur",
                                //"body", "bold", "bool", "boolean", "border", "borderBottomWidth", "borderColor", "borderLeftWidth",
                                //"borderRightWidth", "borderStyle", "borderTopWidth", "borderWidths", "bottom", "break", "btoa" },
        //                        "bufferDepth", "byte", "call", "captureEvents", "case", "catch", "ceil", "char", "charAt", "charCodeAt",
        //                        "characterSet", "checked", "childNodes", "class", "className", "classes", "clear", "clearInterval",
        //                        "clearTimeout", "click", "clientInformation", "clip", "clipboardData", "close", "closed", "colorDepth",
        //                        "compile", "complete", "components", "concat", "confirm", "const", "constructor", "contextual",
        //                        "continue", "controllers", "cookie", "cookieEnabled", "cos", "cpuClass", "createElement",
        //                        "createEventObject", "createPopup", "createStyleSheet", "createTextNode", "crypto", "current", "data",
        //                        "debugger", "default", "defaultCharset", "defaultChecked", "defaultStatus", "defaultValue", "defaultView",
        //                        "delete", "description", "detachEvent", "dialogArguments", "dialogHeight", "dialogLeft", "dialogTop",
        //                        "dialogWidth", "dir", "directories", "disableExternalCapture", "display", "do", "doctype", "document",
        //                        "documentElement", "domain", "double", "dump", "elementFromPoint", "elements", "else", "embeds",
        //                        "enableExternalCapture", "enabledPlugin", "encoding", "enum", "escape", "eval", "event", "exec",
        //                        "execCommand", "execScript", "exp", "expando", "export", "extends", "external", "false", "fgColor",
        //                        "fileCreatedDate", "fileModifiedDate", "fileSize", "fileUpdatedDate", "filename", "final", "finally",
        //                        "find", "firstChild", "fixed", "float", "floor", "focus", "fontFamily", "fontSize", "fontWeight",
        //                        "fontcolor", "fontsize", "for", "form", "formName", "forms", "forward", "frameElement", "frames",
        //                        "fromCharCode", "function", "getAttention", "getDate", "getDay", "getElementById", "getElementsByName",
        //                        "getElementsByTagName", "getFullYear", "getHours", "getMilliseconds", "getMinutes", "getMonth",
        //                        "getSeconds", "getSelection", "getTime", "getTimezoneOffset", "getUTCDate", "getUTCDay", "getUTCFullYear",
        //                        "getUTCHours", "getUTCMilliseconds", "getUTCMinutes", "getUTCMonth", "getUTCSeconds", "getYear", "global",
        //                        "go", "goto", "handleEvent", "hasFocus", "hash", "height", "history", "home", "host", "hostname", "href",
        //                        "hspace", "ids", "if", "ignoreCase", "images", "implementation", "implements", "import", "in", "index",
        //                        "indexOf", "innerHeight", "innerWidth", "input", "instanceof", "int", "interface", "isFinite", "isNaN",
        //                        "italics", "java", "javaEnabled", "join", "language", "lastChild", "lastIndex", "lastIndexOf", "lastMatch",
        //                        "lastModified", "lastParen", "layerX", "layers", "left", "leftContext", "length", "lineHeight", "link",
        //                        "linkColor", "links", "listStyleType", "load", "localName", "location", "locationbar", "log", "long",
        //                        "lowsrc", "marginBottom", "marginLeft", "marginRight", "marginTop", "margins", "match", "max", "media",
        //                        "menubar", "mergeAttributes", "method", "mimeTypes", "min", "moveAbove", "moveBelow", "moveBy", "moveTo",
        //                        "moveToAbsolute", "multiline", "name", "nameProp", "namespaceURI", "namespaces", "native", "navigate",
        //                        "navigator", "netscape", "new", "next", "nextSibling", "nodeName", "nodeType", "nodeValue", "null",
        //                        "offscreenBuffering", "onAbort", "onActivate", "onAfterprint", "onAfterupdate", "onBeforeactivate",
        //                        "onBeforecut", "onBeforedeactivate", "onBeforeeditfocus", "onBeforepaste", "onBeforeprint",
        //                        "onBeforeunload", "onBeforeupdate", "onBlur", "onCellchange", "onChange", "onClick", "onClose",
        //                        "onContextmenu", "onControlselect", "onCut", "onDataavailable", "onDatasetchanged", "onDatasetcomplete",
        //                        "onDblclick", "onDeactivate", "onDrag", "onDragdrop", "onDragend", "onDragenter", "onDragleave",
        //                        "onDragover", "onDragstart", "onDrop", "onError", "onErrorupdate", "onFocus", "onHelp", "onKeydown",
        //                        "onKeypress", "onKeyup", "onLine", "onLoad", "onMousedown", "onMousemove", "onMouseout", "onMouseover",
        //                        "onMouseup", "onPaste", "onPropertychange", "onReadystatechange", "onReset", "onResize", "onResizeend",
        //                        "onResizestart", "onRowenter", "onRowexit", "onRowsdelete", "onRowsinserted", "onScroll", "onSelect",
        //                        "onSelectionchange", "onSelectstart", "onStop", "onSubmit", "onUnload", "open", "opener", "opsProfile",
        //                        "options", "oscpu", "outerHeight", "outerWidth", "ownerDocument", "package", "paddingBottom",
        //                        "paddingLeft", "paddingRight", "paddingTop", "paddings", "pageX", "pageXOffset", "pageY", "pageYOffset",
        //                        "parent", "parentLayer", "parentNode", "parentWindow", "parse", "parseFloat", "parseInt", "pathname",
        //                        "personalbar", "pixelDepth", "pkcs11", "platform", "plugins", "plugins.refresh", "pop", "port", "pow",
        //                        "preference", "prefix", "previous", "previousSibling", "print", "private", "product", "productSub",
        //                        "prompt", "prompter", "protected", "protocol", "prototype", "public", "push", "queryCommandEnabled",
        //                        "queryCommandIndeterm", "queryCommandState", "queryCommandValue", "random", "readyState", "recalc",
        //                        "referrer", "releaseCapture", "releaseEvents", "reload", "replace", "reset", "resizeBy", "resizeTo",
        //                        "return", "returnValue", "reverse", "right", "rightContext", "round", "routeEvents", "savePreferences",
        //                        "screen", "screenLeft", "screenTop", "screenX", "screenY", "scripts", "scroll", "scrollBy",
        //                        "scrollByLines", "scrollByPages", "scrollTo", "scrollX", "scrollY", "scrollbars", "search", "security",
        //                        "securityPolicy", "select", "selected", "selectedIndex", "selection", "self", "setActive", "setCursor",
        //                        "setDate", "setFullYear", "setHotKeys", "setHours", "setInterval", "setMilliseconds", "setMinutes",
        //                        "setMonth", "setResizable", "setSeconds", "setTime", "setTimeout", "setUTCDate", "setUTCFullYear",
        //                        "setUTCHours", "setUTCMilliseconds", "setUTCMinutes", "setUTCMonth", "setUTCSeconds", "setYear",
        //                        "setZOptions", "shift", "short", "showHelp", "showModalDialog", "showModelessDialog", "siblingAbove",
        //                        "siblingBelow", "sidebar", "signText", "sin", "sizeToContent", "slice", "small", "sort", "source",
        //                        "splice", "split", "sqrt", "src", "static", "status", "statusbar", "stop", "strike", "string",
        //                        "styleSheets", "sub", "submit", "substr", "substring", "suffixes", "sun", "sup", "super", "switch",
        //                        "synchronized", "systemLanguage", "tags", "taint", "taintEnabled", "tan", "target", "test", "text",
        //                        "textAlign", "textDecoration", "textIndent", "textTransform", "this", "throw", "throws", "title",
        //                        "toGMTString", "toLocaleString", "toLowerCase", "toSource", "toString", "toUTCString", "toUpperCase",
        //                        "toolbar", "top", "transient", "true", "try", "type", "typeof", "undefined", "unescape", "uniqueID",
        //                        "unshift", "untaint", "unwatch", "updateCommands", "updateInterval", "userAgent", "userLanguage",
        //                        "userProfile", "vLinkcolor", "value", "valueOf", "var", "vendor", "vendorSub", "visibility", "void",
        //                        "volatile", "vspace", "watch", "while", "whiteSpace", "width", "window", "with", "write", "writeln",
        //                        "x", "y", "zIndex" };

        public GadgetOutput()
        {
            InitializeComponent();

            this.MessageList.DoubleBuffer();

            //this.CodeRichTextBox.ReadOnly = true;

            this.CodeRichTextBox.CompileKeywords(Keywords);
            this.CodeRichTextBox.CompileSpecialWords(Specials);

            this.Resize += GadgetOutputResize;
            this.MessageList.ItemSelectionChanged += ListViewItemSelectionChanged;

            oldWidth = (this.MessageList.Width - SystemInformation.VerticalScrollBarWidth) - 1;
        }

        private void GadgetOutputResize(object sender, EventArgs e)
        {
            this.ResizeColumnHeaders();

            oldWidth = (this.MessageList.Width - SystemInformation.VerticalScrollBarWidth) - 1;
        }

        private void ListViewItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            CodeRichTextBox.Text = "";
            CodeRichTextBox.ClearHighlightedLines();
            splitContainer1.Panel2Collapsed = true;
            
            try
            {
                string file = MessageList.SelectedItems[0].SubItems[4].Text;
                int line = int.Parse(MessageList.SelectedItems[0].SubItems[2].Text);

                string text = File.ReadAllText(file);
                CodeRichTextBox.Text = text;

                if (line > 0)
                    CodeRichTextBox.HighlightLine(line);

                if (line > 3)
                    CodeRichTextBox.ScrollToLine(line - 3);

                splitContainer1.Panel2Collapsed = false;
            }
            catch
            {
                CodeRichTextBox.Text = "";
                splitContainer1.Panel2Collapsed = true;
            }
        }

        private void ResizeColumnHeaders()
        {
            //for (int i = 0; i < this.MessageList.Columns.Count - 1; i++) this.MessageList.AutoResizeColumn(i, ColumnHeaderAutoResizeStyle.ColumnContent);
            //this.MessageList.Columns[this.MessageList.Columns.Count - 1].Width = -2;

            int width = (this.MessageList.Width - SystemInformation.VerticalScrollBarWidth) - 1;

            foreach (ColumnHeader col in this.MessageList.Columns)
            {
                float percWidth = (float)col.Width / this.oldWidth;
                col.Width = (int)Math.Ceiling(percWidth * width);
            }
        }

        public void OutputMessage(string message, string filePath, int lineNumber, bool javascriptError)
        {
            string filename = "";

            if (!string.IsNullOrEmpty(filePath))
                filename = filePath.Substring(filePath.LastIndexOf("\\") + 1);
            
            ListViewItem item;

            if (javascriptError)
            {
                item = new ListViewItem("Error");
            }
            else
            {
                item = new ListViewItem("Debug");
            }

            item.SubItems.Add(message);
            item.SubItems.Add((lineNumber > 0 ? lineNumber.ToString() : ""));
            item.SubItems.Add(filename);
            item.SubItems.Add(filePath);
            
            this.MessageList.Items.Add(item);
            this.MessageList.Items[MessageList.Items.Count - 1].Selected = true;
        }
    }
}
