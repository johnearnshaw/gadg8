﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Resources;
using System.Globalization;
using Gadg8Gadget;

namespace Gadg8
{
    [Guid("b5980ccc-f807-436d-b456-89ab1bb03bbb")]
    [ComVisible(true)]
    public partial class Sidebar : Form, IGadgetHost
    {
        private bool devOptions;
        private IntPtr hWnd = IntPtr.Zero;

        GadgetServer Server;
        
        string Gadg8GadgetPath = Application.StartupPath + "\\Gadg8 Gadget.exe";
        int PortNumber = 0;

        string[] GadgetInstallList;
        public Dictionary<string, RunningGadget> RunningGadgetsList;
        System.Threading.Timer ProcessCheckTimer;

        GadgetPicker GadgetPickerWindow;
        Gadg8Settings Gadg8SettingsWindow;
        RunningGadgets RunningGadgetsWindow;
        GadgetOutput GadgetOutputWindow;
        NotifyIcon TrayIcon;
        GlobalHotKey HotKey;

        // Stop garbage collector disposing of this IGadgetHost object.
        public override object InitializeLifetimeService()
        {
            return null;
        }

        // Hide sidebar window from Alt+Tab by setting ExStyle.
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= (int)Gadg8Gadget.Native.ExStyleFlags.WS_EX_TOOLWINDOW;
                return cp;
            }
        }

        public Sidebar(string arg = null)
        {
            InitializeComponent();
            this.HandleCreated += (sender, e) => { this.hWnd = this.Handle; };

            // Setup some dummy location for our sidebar window until we are ready to implement it.
            // We'll set the opacity to 0 for now instead of hiding the window.
            this.Width = 180;
            this.Height = Screen.PrimaryScreen.WorkingArea.Height;
            this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
            this.Top = Screen.PrimaryScreen.WorkingArea.Top;
            this.Opacity = 0;

            // Set form icon to application icon.
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            
            // Hook up our form closing event to close running gadgets
            this.FormClosing += (sender, e) =>
            {
                CloseGadgets();

                Settings.SaveSettings();

                // Hide the tray icon or it doesn't disappear until you roll the mouse over it.
                this.TrayIcon.Visible = false;
            };

            // Create a new gadget server and create a channel for IPC communication.
            Server = new GadgetServer();
            PortNumber = Server.CreateChannel();
            
            if (PortNumber == 0)
            {
                MessageBox.Show("Unable to open gadget communication port.");
                Application.Exit();
            }

            // If we have a IPC port number we add to the Windows running object table.
            // Even though our application is single instance, there could be other applications
            // implementing GadgetHost so we use the port number as a unique identifier.
            RunningObjectTable rot = new RunningObjectTable();
            rot.AddToROT<IGadgetHost>(this, "Gadg8.GadgetHost." + PortNumber);

            CreateContextMenu();
            CreateSysemTrayIcon();

            // If our sidebar is not running and a .gadget file is opened or Gadg8 Gadgets is chosen from the desktop
            // program.cs launches frmSidebar with the file path of the .gadget to install.
            if (!string.IsNullOrEmpty(arg))
            {
                // If arg is an integer value of 1 it means our desktop context menu item has been clicked and we just
                // want to show our gadget picker dialog. Otherwise we presume it is the path to a .gadget file and send
                // it off to our InstallGadget method.
                int showGadgetPicker = 0;
                if (int.TryParse(arg, out showGadgetPicker) && showGadgetPicker == 1)
                {
                    ShowGadgetPicker();
                }
                else
                {
                    InstallGadget(arg);
                }
            }

            // Check if first run and if true, install some gadgets.
            if (Settings.Read("firstrun", true))
            {
                GadgetInstallList = new string[] { "supersearch", "auctionsidebartool" };
                InstallDefaultGadgets();
                
                Settings.Write("firstrun", false, "", true);
                Settings.SaveSettings();
            }

            // Check settings and set some default settings
            Setup();

            // Finally start the currently running gadgets
            StartRunningGadgets();

            // Start a threading timer to periodically check on gadget processes incase they stop responding or
            // terminate unexpectedly.
            ProcessCheckTimer = new System.Threading.Timer(new TimerCallback(CheckRunningGadgets), null, 1000, 1000);

            HotKey = new GlobalHotKey();
            HotKey.RegisterHotKey(Keys.G, true);
            HotKey.HotKeyPressed += (sender, e) =>
            {
                ShowGadgets();
            };
        }

        protected override void WndProc(ref Message m)
        {
            // If we receive our custom global WndProc message WM_FINDGADG8SIDEBAR it means that a new instance of Gadg8
            // has been launched and is trying to pass arguments (or argument, as we only use the first one) to the current
            // running instance.
            if (m.Msg == Native.WM_FINDGADG8SIDEBAR)
            {
                // We send a message back to the new instance with the handle of our sidebar window so the new instance
                // can send a WM_COPYDATA back to us with the arg
                Native.SendMessage(m.LParam, Native.WM_SENDGADG8SIDEBARHANDLE, IntPtr.Zero, this.Handle);
            }
            else if (m.Msg == Native.WM_COPYDATA)
            {
                // Once we receive the args structure we get the lpData which contains our arg and see what's inside.
                Native.COPYDATASTRUCT cds = (Native.COPYDATASTRUCT)m.GetLParam(typeof(Native.COPYDATASTRUCT));
                string arg = cds.lpData;

                if (!string.IsNullOrEmpty(arg))
                {
                    // If arg is an integer value of 1 it means our desktop context menu item has been clicked and we just
                    // want to show our gadget picker dialog. Otherwise we presume it is the path to a .gadget file and send
                    // it off to our InstallGadget method.
                    int showGadgetPicker = 0;
                    if (int.TryParse(arg, out showGadgetPicker) && showGadgetPicker == 1)
                    {
                        ShowGadgetPicker();
                    }
                    else
                    {
                        InstallGadget(arg);
                    }
                }
            }

            base.WndProc(ref m);
        }

        public void CloseGadgets()
        {
            foreach (RunningGadget g in RunningGadgetsList.Values)
            {
                try
                {
                    // If the process is still running sends WM_CLOSE message to gadget window causing the process to exit.
                    CloseGadget(g);
                }
                finally
                { }
            }
        }

        private void CreateContextMenu()
        {
            int i;
            MenuItem[] menuItems = new MenuItem[8];

            i = 0;

            menuItems[i] = new MenuItem(Localize.Strings.BringGadgetsToFront, (sender, e) => { this.ShowGadgets(); });
            menuItems[++i] = new MenuItem(Localize.Strings.AddGadgets, (sender, e) => { this.AddGadgets(); });
            menuItems[++i] = new MenuItem("-");
            menuItems[++i] = new MenuItem(Localize.Strings.Properties, (sender, e) => { this.ShowGadg8Settings(); });
            menuItems[++i] = new MenuItem(Localize.Strings.ShowRunningGadgetList, (sender, e) => { this.ShowRunningGadgetsWindow(); });
            menuItems[++i] = new MenuItem(Localize.Strings.Help, (sender, e) => { this.ShowHelp(); });
            menuItems[++i] = new MenuItem("-");
            menuItems[++i] = new MenuItem(Localize.Strings.Quit, (sender, e) => { this.ExitApplication(); });

            this.ContextMenu = new ContextMenu(menuItems);
        }

        private void ExitApplication()
        {
            this.Close();
        }

        private void ShowHelp()
        {
            throw new NotImplementedException();
        }

        private void CreateSysemTrayIcon()
        {
            TrayIcon = new NotifyIcon();
            TrayIcon.Icon = this.Icon;
            TrayIcon.Text = Localize.Strings.Gadg8DesktopGadgets;
            TrayIcon.Visible = true;
            TrayIcon.DoubleClick += (sender, e) =>
            {
                if (this.RunningGadgetsList.Count > 0)
                {
                    this.ShowGadgets();
                }
                else
                {
                    this.ShowGadgetPicker();
                }
            };

            TrayIcon.ContextMenu = this.ContextMenu;
        }

        private void InstallDefaultGadgets()
        {
            string SplashText = "";
            int SplashProgress = 0;
            bool InstallComplete = false;
            
            string temp;

            AsyncGadgetExtractor extractor = new AsyncGadgetExtractor();
            extractor.ExtractProgress += (sender, e) => { SplashProgress = e.PercentRead; };

            ThreadPool.QueueUserWorkItem((x) =>
            {    
                using (var splash = new frmSplash())
                {
                    splash.Show();
                    //SplashText = splash.StatusText.Text;
                    
                    while (!InstallComplete)
                    {
                        if (splash.StatusText.Text != SplashText)
                            splash.UpdateStatus(SplashText);

                        splash.UpdateProgress(SplashProgress);

                        Application.DoEvents();
                    }
                    
                    splash.Close();
                    SplashText = "";

                    // Show gadget picker dialog after gadgets have installed.
                    ShowGadgetPicker();
                }
            });
            
            ResourceSet resourceSet = Properties.Resources.ResourceManager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            foreach (DictionaryEntry entry in resourceSet)
            {
                foreach (string name in GadgetInstallList)
                {
                    if (!Directory.Exists(Helpers.Gadg8GadgetsPath + name + ".gadget"))
                    {
                        if (entry.Key.ToString() == name)
                        {
                            if (!Directory.Exists(Helpers.TempPath))
                                Directory.CreateDirectory(Helpers.TempPath);

                            SplashText = "Copying " + name + "...";
                            temp = Helpers.TempPath + name + ".gadget";
                            Helpers.SaveBinaryData(temp, (byte[])entry.Value);

                            extractor.ExtractComplete += (sender, e) =>
                            {
                                SplashText = "Deleting temp files...";
                                File.Delete(temp);
                            };

                            SplashText = "Extracting " + name + "...";
                            extractor.ExtractFile(temp, Helpers.Gadg8GadgetsPath);
                        }
                    }
                }
            }
            
            InstallComplete = true;
            SplashProgress = 0;
        }

        private void ShowGadgetPicker()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(ShowGadgetPicker));
                return;
            }

            if (this.GadgetPickerWindow == null)
            {
                this.GadgetPickerWindow = new GadgetPicker(this);
                this.GadgetPickerWindow.FormClosed += (sender, e) =>
                {
                    this.GadgetPickerWindow = null;
                };

                this.GadgetPickerWindow.Show(this);
            }
            else
            {
                this.GadgetPickerWindow.Activate();
            }
        }

        private void ShowGadg8Settings()
        {
            if (this.Gadg8SettingsWindow == null)
            {
                this.Gadg8SettingsWindow = new Gadg8Settings(this);
                this.Gadg8SettingsWindow.FormClosed += (sender, e) =>
                {
                    this.Gadg8SettingsWindow = null;
                };
            }
            this.Gadg8SettingsWindow.Show(this);
        }

        private void ShowRunningGadgetsWindow()
        {
            if (this.RunningGadgetsWindow == null)
            {
                this.RunningGadgetsWindow = new RunningGadgets(this);
                this.RunningGadgetsWindow.FormClosed += (sender, e) =>
                {
                    this.RunningGadgetsWindow = null;
                };
            }
            this.RunningGadgetsWindow.Show(this);
        }

        private void StartRunningGadgets()
        {
            RunningGadgetsList = ReadRunningGadgetsList();

            foreach (KeyValuePair<string, RunningGadget> g in this.RunningGadgetsList)
            {
                string path = Path.GetDirectoryName(g.Value.Path) + "\\";
                string uniqueId = g.Key;

                if (!Directory.Exists(path))
                {
                    RemoveRunningGadget(uniqueId);
                }
                else
                {
                    RunGadget(path, uniqueId);
                }
            }

            this.WriteRunningGadgetsList();
        }

        private Dictionary<string, RunningGadget> ReadRunningGadgetsList()
        {
            Dictionary<string, RunningGadget> d = new Dictionary<string, RunningGadget>();

            string[] list = Settings.Read("runninggadgets", "").Split(new string[] {","},
                StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < list.Length; i++)
            {
                string[] g = list[i].Split(new string[] {"="},  StringSplitOptions.None);
                d.Add(g[0], new RunningGadget() { Path = g[1] });
            }            

            return d;
        }

        private void WriteRunningGadgetsList()
        {
            string list = "";

            foreach (KeyValuePair<string, RunningGadget> g in RunningGadgetsList)
            {
                list += g.Key + "=" + g.Value.Path + ",";
            }
            
            Settings.Write("runninggadgets", list, "", true);
            Settings.SaveSettings();
        }

        private void CheckRunningGadgets(object state)
        {
            Dictionary<string, RunningGadget> rg = new Dictionary<string,RunningGadget>(RunningGadgetsList);

            foreach (KeyValuePair<string, RunningGadget> g in rg)
            {
                if (g.Value.UserClosed)
                {
                    RemoveRunningGadget(g.Key);
                    continue;
                }

                if (g.Value.GadgetProcess != null)
                {
                    if (g.Value.GadgetProcess.HasExited || !g.Value.GadgetProcess.Responding)
                    {
                        if (g.Value.Strikes == 3)
                        {
                            if (g.Value.GadgetProcess.HasExited)
                            {
                                string name = "Gadget";
                                if (g.Value.GadgetXml != null)
                                    name = g.Value.GadgetXml.Name;

                                TrayIcon.ShowBalloonTip(10000, Localize.Strings.GadgetUnexpectedExit,
                                    Localize.Strings.GadgetUnexpectedExitMessage.Replace("{gadgetname}",
                                    name), ToolTipIcon.None);

                                g.Value.Strikes = 0;
                                RemoveRunningGadget(g.Key);
                            }
                            else if (!g.Value.GadgetProcess.Responding)
                            {
                                string name = "Gadget";
                                if (g.Value.GadgetXml != null)
                                    name = g.Value.GadgetXml.Name;

                                TrayIcon.ShowBalloonTip(10000, Localize.Strings.GadgetNotResponding,
                                    Localize.Strings.GadgetNotRespondingMessage.Replace("{gadgetname}",
                                    name), ToolTipIcon.None);

                                g.Value.Strikes = 0;
                            }
                        }
                        else
                        {
                            g.Value.Strikes++;
                        }
                    }
                    else
                    {
                        g.Value.Strikes = 0;
                    }
                }
            }
        }

        public void Setup()
        {
            if (Settings.Read<bool>("showoutputwindow", false))
            {
                ShowGadgetOutputWindow();
            }
            else
            {
                if (this.GadgetOutputWindow != null)
                {
                    this.GadgetOutputWindow.Close();
                }
            }

            this.devOptions = Settings.Read<bool>("showdevoptions", false);

            // Make sure the icon for .gadget files is in the app directory and if not then create it.
            if (!File.Exists(Helpers.AppPath + "gadg8package.ico"))
                Helpers.SaveBinaryData(Helpers.AppPath + "gadg8package.ico", Properties.Resources.gadg8packageicon);

            // Make sure Gadg8 runs on Windows startup if it enabled in the settings.
            if (Settings.Read("runonstartup", true))
            {
                Startup.AddToStartup();
            }
            else
            {
                Startup.RemoveFromStartup();
            }

            
            // Add Windows desktop right click menu entry for adding gadgets.
            if (Settings.Read("desktopmenuentry", true))
            {
                if (!DesktopMenuEntry.CheckContextMenuEntry())
                    DesktopMenuEntry.AddContextMenuEntry();
            }
            else
            {
                if (DesktopMenuEntry.CheckContextMenuEntry())
                    DesktopMenuEntry.RemoveContextMenuEntry();
            }

            // Check file association for .gadget files.
            bool fa = FileAssociation.CheckFileAssociation();
            if (Settings.Read("showfileassociationdialog", true) && !fa)
            {
                bool check = false;
                DialogResult result = CheckboxDialog.Show(this, Localize.Strings.AssociateGadgetFilesMessage,
                    Localize.Strings.DontShowAgain, ref check, Localize.Strings.AssociateGadgetFiles);

                if (check == true)
                {
                    Settings.Write("showfileassociationdialog", false, "", true);
                    Settings.SaveSettings();
                }

                if (result == DialogResult.Yes)
                    FileAssociation.AssociateGadgetFiles();
            }
            else if (Settings.Read("checkfileassociation", true) && !fa)
            {
                FileAssociation.AssociateGadgetFiles();
            }
        }

        private void ShowGadgetOutputWindow()
        {
            if (this.GadgetOutputWindow == null)
            {
                this.GadgetOutputWindow = new GadgetOutput();
                this.GadgetOutputWindow.FormClosed += (sender, e) =>
                {
                    this.GadgetOutputWindow = null;
                    Settings.Write("showoutputwindow", false, "", true);
                    Settings.SaveSettings();
                };
            }
            this.GadgetOutputWindow.Show();
        }

        protected override bool ShowWithoutActivation
        {
            get { return true; }
        }
        
        private void ShowGadgets()
        {
            if (Properties.Settings.Default.SidebarVisible)
            {
                if (this.WindowState != FormWindowState.Minimized)
                {
                    this.Show();
                }

                foreach (RunningGadget g in RunningGadgetsList.Values)
                {
                    if (g.GadgetHandle != IntPtr.Zero)
                        Native.SetForegroundWindow(g.GadgetHandle);
                }
                
            }
        }

        public void ShowGadget(RunningGadget g)
        {
            if (g.GadgetHandle != IntPtr.Zero)
                Native.SetForegroundWindow(g.GadgetHandle);
        }

        [ComVisible(true)]
        public void InstallGadget(string file)
        {
            if (this.WindowState != FormWindowState.Minimized)
            {
                this.Focus();
            }

            X509Certificate.X509CertificateDetails cert = X509Certificate.CheckFileCertificate(file);

            InstallGadgetDialog installDialog = new InstallGadgetDialog(file, cert);
            if (installDialog.ShowDialog(this) == DialogResult.Yes)
            {
                AsyncGadgetExtractor extractor = new AsyncGadgetExtractor();

                InstallingGadgetProgress installingDialog = new InstallingGadgetProgress(file);
                installingDialog.Cancel += (sender, e) => { extractor.Cancel(); };
                
                extractor.ExtractCancelled += (sender, e) =>
                {
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(() => { installingDialog.Close(); }));
                    }
                    else
                    {
                        installingDialog.Close();
                    }
                    return;
                };

                extractor.ExtractFailed += (sender, e) =>
                {
                    MessageBox.Show(this, e.Error.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    installingDialog.InvokeClose();
                    return;
                };

                extractor.ExtractProgress += (sender, e) => { installingDialog.UpdateProgress(e.PercentRead, e.File); };
                extractor.ExtractComplete += (sender, e) =>
                {
                    installingDialog.InvokeClose();
                    RunGadget(e.Path);
                };

                installingDialog.Show(this);
                extractor.ExtractFileAsync(file);
            }
        }

        public void RunGadget(string path, string uniqueId = null)
        {
            int top = 0;
            int left = 0;

            // If uniqueId is null then the gadget is not loaded from previously running gadgets list and is a new gadget instance.
            // We create a new unique id and calculate the starting point of the new gadget here.
            if (string.IsNullOrEmpty(uniqueId))
            {
                uniqueId = Guid.NewGuid().ToString();

                CalculateStartLocation(ref top, ref left);
            }

            // Replace spaces in path with *, we do this because arguments are seperated by spaces so if gadget path contains
            // spaces it would be split wrong. We swap * back to spaces in Gadg8Gadget process.
            string gadgetPath = path.Replace(" ", "*");
            
            int hostIpcPort = this.PortNumber;
            string args = gadgetPath + " " + left.ToString() + " " + top.ToString() + " " + uniqueId + " " + hostIpcPort.ToString();

            Process process = new Process();

            // Configure the process using the StartInfo properties.
            process.StartInfo.FileName = Gadg8GadgetPath;
            process.StartInfo.Arguments = args;
            process.Start();

#if DEBUG
            if (Debugger.IsAttached)
            {
                Debugging.TryAttachDebugger(process.Id);
            }
#endif

            if (RunningGadgetsList.ContainsKey(uniqueId))
            {
                RunningGadgetsList[uniqueId].GadgetProcess = process;
            }
            else
            {
                RunningGadgetsList.Add(uniqueId, new RunningGadget() { Path = path, GadgetProcess = process, UniqueId = uniqueId });
                WriteRunningGadgetsList();
            }

            if (RunningGadgetsWindow != null)
                RunningGadgetsWindow.LoadGadgetsList();
        }

        private void CalculateStartLocation(ref int top, ref int left)
        {
            int padding = 25;
            // Guess at gadget average width/height
            int gadgetWidth = 140;
            int gadgetHeight = 240;

            Rectangle screen = Screen.PrimaryScreen.WorkingArea;
            Rectangle pos = new Rectangle(screen.Right - (gadgetWidth + padding), screen.Top + padding,
                gadgetWidth + padding, gadgetHeight + padding);

        loop:

            foreach (RunningGadget gadget in RunningGadgetsList.Values)
            {
                if (gadget.GadgetLocation.IntersectsWith(pos))
                {
                    pos.Y = gadget.GadgetLocation.Bottom + padding;

                    if (pos.Y > screen.Bottom - gadgetHeight)
                    {
                        pos.X -= (gadgetWidth + padding);
                        pos.Y = screen.Top + padding;
                    }

                    goto loop;
                }
            }

            if (pos.X < 0)
            {
                pos.Y = 0 + padding;
                pos.X = 0 + padding;
            }

            top = pos.Y;
            left = pos.X;
        }

        public void CloseGadget(RunningGadget g)
        {
            if (g.GadgetHandle != IntPtr.Zero)
                Gadg8Gadget.Native.SendMessage(g.GadgetHandle, (int)Gadg8Gadget.Native.WindowsMessageFlags.WM_CLOSE, 0, 0);
        }

        private void ResetSettings()
        {
            Settings.ClearSettings();
            Settings.SaveSettings();
        }

        public void RemoveRunningGadget(string uniqueId)
        {
            if (uniqueId != null)
            {
                if (RunningGadgetsList.ContainsKey(uniqueId))
                {
                    if (RunningGadgetsList[uniqueId].GadgetProcess != null)
                    {
                        if (!RunningGadgetsList[uniqueId].GadgetProcess.HasExited)
                        {
                            try
                            {
                                RunningGadgetsList[uniqueId].GadgetProcess.Kill();
                            }
                            finally
                            { }
                        }

                        try
                        {
                            RunningGadgetsList[uniqueId].GadgetProcess.Dispose();
                        }
                        finally
                        { }
                    }

                    RunningGadgetsList.Remove(uniqueId);
                }

                Settings.DeleteAllSettingsById(uniqueId);
                WriteRunningGadgetsList();

                if (RunningGadgetsWindow != null)
                    RunningGadgetsWindow.LoadGadgetsList();
            }
        }

        private void RestartRunningGadget(string uniqueId)
        {
            if (RunningGadgetsList.ContainsKey(uniqueId))
            {
                if (RunningGadgetsList[uniqueId].GadgetProcess != null)
                {
                    if (!RunningGadgetsList[uniqueId].GadgetProcess.HasExited)
                        RunningGadgetsList[uniqueId].GadgetProcess.Kill();

                    RunningGadgetsList[uniqueId].GadgetProcess.Start();
                }
            }
        }

        #region IGadgetHost
        public bool SettingsChanged
        {
            get { return Settings.SettingsChanged; }
        }

        public void SaveSettings()
        {
            Settings.SaveSettings();
        }

        public t ReadSetting<t>(string name, t defaultValue, string uniqueId)
        {
            return Settings.Read<t>(name, defaultValue, uniqueId);
        }

        public void WriteSetting(string name, object value, string uniqueId, bool privateSetting)
        {
            Settings.Write(name, value, uniqueId, privateSetting);
        }

        public void Handshake(IntPtr handle, string uniqueId, GadgetManifest gadgetManifest)
        {
            if (RunningGadgetsList.ContainsKey(uniqueId))
            {
                RunningGadgetsList[uniqueId].GadgetHandle = handle;
                RunningGadgetsList[uniqueId].GadgetXml = gadgetManifest;
            }
            else
            {
                Process proc = null;
                foreach(var p in Process.GetProcesses())
                {
                    if (p.MainWindowHandle == handle)
                    {
                        proc = p;
                    }
                }

                RunningGadgetsList.Add(uniqueId, new RunningGadget
                {
                    GadgetHandle = handle,
                    GadgetProcess = proc,
                    GadgetXml = gadgetManifest,
                    GadgetLocation = new Rectangle(0, 0, 0, 0),
                    Path = "",
                    Strikes = 0,
                    UniqueId = uniqueId,
                    UserClosed = false,
                });
            }

            if (RunningGadgetsWindow != null)
                RunningGadgetsWindow.LoadGadgetsList();
        }

        public bool GadgetClosing(IntPtr handle, string uniqueId)
        {
            if (Settings.Read("showclosegadgetdialog", true))
            {
                if (this.hWnd != IntPtr.Zero)
                    Native.SetForegroundWindow(this.hWnd);

                bool check = false;
                DialogResult result = System.Windows.Forms.DialogResult.Yes;

                // Invoke the dialog on the current process and not the calling process or setting the owner
                // of the dialog fails.
                this.Invoke(new MethodInvoker(() =>
                {
                    if (this.WindowState != FormWindowState.Minimized)
                    {
                        this.Show();
                    }
                    result = CheckboxDialog.Show(this, Localize.Strings.ClosingGadgetsMessage,
                    Localize.Strings.DontShowAgain, ref check, Localize.Strings.ClosingGadgets);
                }));

                if (check == true)
                {
                    Settings.Write("showclosegadgetdialog", false, "", true);
                    Settings.SaveSettings();
                }

                if (result != DialogResult.Yes)
                    return false;
            }

            if (RunningGadgetsList.ContainsKey(uniqueId))
            {
                RunningGadgetsList[uniqueId].UserClosed = true;
            }
            return true;
        }

        public void OutputString(string message, bool javascriptError, string filePath = "", int lineNumber = 0)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(() =>
                {
                    this.OutputString(message, javascriptError, filePath, lineNumber);
                }));

                return;
            }

            if (this.GadgetOutputWindow != null)
                this.GadgetOutputWindow.OutputMessage(message, filePath, lineNumber, javascriptError);

            System.Diagnostics.Debug.WriteLine(message);
        }

        public void WriteLog(string text)
        {
            Log.Write(text, Settings.Read("gadg8logpath", ""));
        }

        public Point GadgetMoved(Rectangle gadgetRect, string uniqueId)
        {
            if (RunningGadgetsList.ContainsKey(uniqueId))
            {
                RunningGadgetsList[uniqueId].GadgetLocation = gadgetRect;
            }

            return new Point(0, 0);
        }

        public void AddGadgets()
        {
            ShowGadgetPicker();
        }
        #endregion

        #region screen snapping stuff
        private const int SnapDist = 100;

        private bool DoSnap(int pos, int edge)
        {
            int delta = pos - edge;
            return delta > 0 && delta <= SnapDist;
        }

        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);
            Screen scn = Screen.FromPoint(this.Location);
            if (DoSnap(this.Left, scn.WorkingArea.Left)) this.Left = scn.WorkingArea.Left;
            if (DoSnap(this.Top, scn.WorkingArea.Top)) this.Top = scn.WorkingArea.Top;
            if (DoSnap(scn.WorkingArea.Right, this.Right)) this.Left = scn.WorkingArea.Right - this.Width;
            if (DoSnap(scn.WorkingArea.Bottom, this.Bottom)) this.Top = scn.WorkingArea.Bottom - this.Height;
        }
        #endregion

        internal bool UninstallGadget(string path, string name, bool noMessage = false)
        {
            if (!noMessage)
            {
                if (MessageBox.Show(Localize.Strings.UninstallGadgetMessage.Replace("{gadget}", name),
                    Localize.Strings.UninstallGadget, MessageBoxButtons.YesNo) != System.Windows.Forms.DialogResult.Yes)
                {
                    return false;
                }
            }

            if (Helpers.IsGadgetInUse(path))
            {
                if (MessageBox.Show(Localize.Strings.UninstallGadgetInUseMessage.Replace("{gadget}", name),
                    Localize.Strings.UninstallGadget, MessageBoxButtons.RetryCancel) == System.Windows.Forms.DialogResult.Retry)
                {
                    return this.UninstallGadget(path, name, true);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                Helpers.ForceDeleteReadOnly(path);
                return true;
            }
        }
    }
}
