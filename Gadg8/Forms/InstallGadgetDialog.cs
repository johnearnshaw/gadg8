﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Gadg8
{
    public partial class InstallGadgetDialog : Form
    {
        X509Certificate.X509CertificateDetails Cert;

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            Native.SetForegroundWindow(this.Handle);
        }

        public InstallGadgetDialog(string path, X509Certificate.X509CertificateDetails cert)
        {
            InitializeComponent();

            this.Cert = cert;

            // Set form icon to application icon.
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);

            this.Publisher.Text = Localize.Strings.Publisher + " " + (!string.IsNullOrEmpty(cert.Publisher) ?
                    cert.Publisher : Localize.Strings.Unknown);

            if (!this.Cert.Verified)
            {
                this.ShieldIconBox.Image = Properties.Resources.Firewall_10603;
                this.WarningMessage.Text = Localize.Strings.InstallGadgetUnverifiedMessage;
                this.ButtonDetails.Enabled = false;
                
            }
            else
            {
                DateTime starts = DateTime.Parse(cert.Certificate.GetEffectiveDateString());
                DateTime expires = DateTime.Parse(cert.Certificate.GetExpirationDateString());

                if (cert.Timestamp > starts && cert.Timestamp < expires)
                {
                    this.ShieldIconBox.Image = Properties.Resources.Firewall_10602;
                    this.WarningMessage.Text = Localize.Strings.InstallGadgetVerifiedMessage;
                }
                else
                {
                    this.ShieldIconBox.Image = Properties.Resources.Firewall_10604;
                    this.WarningMessage.Text = Localize.Strings.InstallGadgetExpiredMessage;
                }
            }

            this.FileName.Text = Localize.Strings.Filename + " " + Path.GetFileName(path);

            this.ButtonDetails.Text = Localize.Strings.CertificateDetails;
            this.ButtonDetails.Click += ButtonDetailsClick;

            this.AcceptButton = ButtonOK;
            this.CancelButton = ButtonCancel;

            this.ButtonOK.DialogResult = DialogResult.Yes;
            this.ButtonOK.Text = Localize.Strings.Yes;

            this.ButtonCancel.DialogResult = DialogResult.No;
            this.ButtonCancel.Text = Localize.Strings.No;
        }

        private void ButtonDetailsClick(object sender, EventArgs e)
        {
            this.Cert.ShowCertificate();
        }
    }
}
