﻿namespace Gadg8
{
    partial class GadgetOutput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.splitContainer1 = new Gadg8.BlurSplitContainer();
            this.MessageList = new System.Windows.Forms.ListView();
            this.lvMessageType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvMessage = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvLineNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvFileName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvFilePath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.CodeRichTextBox = new Gadg8.SyntaxRTB();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 339);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(674, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Cursor = System.Windows.Forms.Cursors.SizeNS;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.MessageList);
            this.splitContainer1.Panel1MinSize = 0;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.CodeRichTextBox);
            this.splitContainer1.Panel2MinSize = 0;
            this.splitContainer1.Size = new System.Drawing.Size(674, 339);
            this.splitContainer1.SplitterDistance = 168;
            this.splitContainer1.TabIndex = 1;
            // 
            // MessageList
            // 
            this.MessageList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.MessageList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvMessageType,
            this.lvMessage,
            this.lvLineNumber,
            this.lvFileName,
            this.lvFilePath});
            this.MessageList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MessageList.FullRowSelect = true;
            this.MessageList.GridLines = true;
            this.MessageList.HideSelection = false;
            this.MessageList.Location = new System.Drawing.Point(0, 0);
            this.MessageList.MultiSelect = false;
            this.MessageList.Name = "MessageList";
            this.MessageList.Size = new System.Drawing.Size(672, 166);
            this.MessageList.TabIndex = 4;
            this.MessageList.UseCompatibleStateImageBehavior = false;
            this.MessageList.View = System.Windows.Forms.View.Details;
            // 
            // lvMessageType
            // 
            this.lvMessageType.Text = "Message Type";
            this.lvMessageType.Width = 89;
            // 
            // lvMessage
            // 
            this.lvMessage.Text = "Message";
            this.lvMessage.Width = 264;
            // 
            // lvLineNumber
            // 
            this.lvLineNumber.Text = "Line No.";
            this.lvLineNumber.Width = 53;
            // 
            // lvFileName
            // 
            this.lvFileName.Text = "File Name";
            this.lvFileName.Width = 80;
            // 
            // lvFilePath
            // 
            this.lvFilePath.Text = "File Path";
            this.lvFilePath.Width = 146;
            // 
            // CodeRichTextBox
            // 
            this.CodeRichTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.CodeRichTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CodeRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CodeRichTextBox.Font = new System.Drawing.Font("Consolas", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CodeRichTextBox.HideSelection = false;
            this.CodeRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.CodeRichTextBox.Name = "CodeRichTextBox";
            this.CodeRichTextBox.Size = new System.Drawing.Size(672, 165);
            this.CodeRichTextBox.TabIndex = 1;
            this.CodeRichTextBox.Text = "";
            this.CodeRichTextBox.WordWrap = false;
            // 
            // GadgetOutput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 361);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "GadgetOutput";
            this.Text = "GadgetOutputWindow";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private Gadg8.BlurSplitContainer splitContainer1;
        internal Gadg8.SyntaxRTB CodeRichTextBox;
        internal System.Windows.Forms.ListView MessageList;
        private System.Windows.Forms.ColumnHeader lvMessageType;
        internal System.Windows.Forms.ColumnHeader lvMessage;
        internal System.Windows.Forms.ColumnHeader lvLineNumber;
        internal System.Windows.Forms.ColumnHeader lvFileName;
        internal System.Windows.Forms.ColumnHeader lvFilePath;


    }
}