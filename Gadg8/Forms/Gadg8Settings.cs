﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace Gadg8
{
    public partial class Gadg8Settings : Form
    {
        Sidebar SidebarWindow;

        public Gadg8Settings(Sidebar sidebarWindow)
        {
            InitializeComponent();

            this.SidebarWindow = sidebarWindow;

            // Set form icon to application icon.
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);

            this.VersionText.Text = Localize.Strings.Version + " " + Assembly.GetExecutingAssembly().GetName().Version;
            this.DescriptionText.Text = Localize.Strings.AboutText;
            
            this.WebLinkText.SizeChanged += WebLinkTextSizeChanged;
            this.WebLinkText.Text = Localize.Strings.Gadg8Website;
            this.WebLinkText.Click += WebLinkTextClick;

            this.AcceptButton = ButtonOK;
            this.ButtonOK.Click += ButtonOKClick;

            this.CancelButton = ButtonCancel;
            this.ButtonCancel.Click += ButtonCancelClick;

            this.RunOnStartupCheckbox.Text = Localize.Strings.RunOnWindowsStartup;
            this.RunOnStartupCheckbox.Checked = Settings.Read<bool>("runonstartup",
                Startup.CheckStartup()) & Startup.CheckStartup();

            this.FileAssociationCheckbox.Text = Localize.Strings.CheckFileAssociation;
            this.FileAssociationCheckbox.Checked = Settings.Read<bool>("checkfileassociation",
                FileAssociation.CheckFileAssociation()) & FileAssociation.CheckFileAssociation();

            this.DesktopMenuCheckbox.Text = Localize.Strings.DesktopMenuIntegration;
            this.DesktopMenuCheckbox.Checked = Settings.Read<bool>("desktopmenuentry",
                DesktopMenuEntry.CheckContextMenuEntry()) & DesktopMenuEntry.CheckContextMenuEntry();

            this.OutputWindowCheckbox.Text = Localize.Strings.ShowOutputWindow;
            this.OutputWindowCheckbox.Checked = Settings.Read<bool>("showoutputwindow", false);

            this.ShowDevCheckbox.Text = Localize.Strings.EnableDeveloperOptions;
            this.ShowDevCheckbox.Checked = Settings.Read<bool>("showdevoptions", false);

            this.Gadg8LogCheckbox.Text = Localize.Strings.EnableGadg8Log;
            this.Gadg8LogCheckbox.Checked = Settings.Read<bool>("writegadg8log", false);

            this.ResetAllSettingsButton.Text = Localize.Strings.ResetAllSettings;
            this.ResetAllSettingsButton.Left = (this.SettingsTabControl.Width / 2) - (this.ResetAllSettingsButton.Width / 2);
            this.ResetAllSettingsButton.Click += ResetAllSettingsButtonClick;
        }

        private void ResetAllSettingsButtonClick(object sender, EventArgs e)
        {
            if (MessageBox.Show(Localize.Strings.ResetSettingsMessage, Localize.Strings.ResetAllSettings,
                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                SidebarWindow.CloseGadgets();
                Settings.ClearSettings();
                Settings.SaveSettings();
            }
        }

        private void ButtonCancelClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ButtonOKClick(object sender, EventArgs e)
        {
            ApplySettings();
            this.Close();
        }

        private void WebLinkTextClick(object sender, EventArgs e)
        {
            Gadg8Gadget.Helpers.OpenLink("http://gadg8.org/");
        }

        private void WebLinkTextSizeChanged(object sender, EventArgs e)
        {
            WebLinkText.Location = new Point((this.SettingsTabControl.Width - WebLinkText.Width) - 10, WebLinkText.Top);
        }

        private void ApplySettings()
        {
            Settings.Write("runonstartup", this.RunOnStartupCheckbox.Checked, "", true);
            Settings.Write("checkfileassociation", this.FileAssociationCheckbox.Checked, "", true);
            Settings.Write("desktopmenuentry", this.DesktopMenuCheckbox.Checked, "", true);
            Settings.Write("showoutputwindow", this.OutputWindowCheckbox.Checked, "", true);
            Settings.Write("showdevoptions", this.ShowDevCheckbox.Checked, "", true);
            Settings.Write("writegadg8log", this.Gadg8LogCheckbox.Checked, "", true);
            Settings.SaveSettings();

            SidebarWindow.Setup();
        }
    }
}
