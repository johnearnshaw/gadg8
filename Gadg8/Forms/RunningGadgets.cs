﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Gadg8
{
    public partial class RunningGadgets : Form
    {
        Sidebar SidebarWindow;
        List<RunningGadget> RunningGadgetList;

        int index = -1;

        public RunningGadgets(Sidebar sidebarWindow)
        {
            InitializeComponent();

            this.SidebarWindow = sidebarWindow;

            // Set form icon to application icon.
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);

            this.ShowGadgetButton.Text = Localize.Strings.ShowGadget;
            this.CloseGadgetButton.Text = Localize.Strings.CloseGadget;

            RunningGadgetList = new List<RunningGadget>();
            LoadGadgetsList();

            RunningGadgetsListBox.SelectedIndexChanged += RunningGadgetsListBox_SelectedIndexChanged;
            
            ShowGadgetButton.Click += ShowGadgetButton_Click;
            CloseGadgetButton.Click += CloseGadgetButton_Click;

            if (RunningGadgetsListBox.SelectedItem == null)
            {
                this.CloseGadgetButton.Enabled = false;
                this.ShowGadgetButton.Enabled = false;
            }
        }

        private void ShowGadgetButton_Click(object sender, EventArgs e)
        {
            SidebarWindow.ShowGadget(RunningGadgetList[RunningGadgetsListBox.SelectedIndex]);
        }

        private void CloseGadgetButton_Click(object sender, EventArgs e)
        {
            if (RunningGadgetsListBox.SelectedIndex > -1)
            {
                SidebarWindow.CloseGadget(RunningGadgetList[RunningGadgetsListBox.SelectedIndex]);
                SidebarWindow.RemoveRunningGadget(RunningGadgetList[RunningGadgetsListBox.SelectedIndex].UniqueId);
            }
        }

        private void RunningGadgetsListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RunningGadgetsListBox.SelectedItem == null)
            {
                this.CloseGadgetButton.Enabled = false;
                this.ShowGadgetButton.Enabled = false;
            }
            else
            {
                index = RunningGadgetsListBox.SelectedIndex;

                this.CloseGadgetButton.Enabled = true;
                this.ShowGadgetButton.Enabled = true;
            }
        }

        public void LoadGadgetsList()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(LoadGadgetsList));
                return;
            }

            this.CloseGadgetButton.Enabled = false;
            this.ShowGadgetButton.Enabled = false;

            RunningGadgetList.Clear();
            RunningGadgetsListBox.Items.Clear();

            foreach (RunningGadget g in SidebarWindow.RunningGadgetsList.Values)
            {
                RunningGadgetList.Add(g);
                if (g.GadgetXml != null)
                {
                    RunningGadgetsListBox.Items.Add(g.GadgetXml.Name);
                }
                else
                {
                    RunningGadgetsListBox.Items.Add(Localize.Strings.UnknownGadget);
                }
            }

            if (RunningGadgetsListBox.Items.Count - 1 < index)
            {
                RunningGadgetsListBox.SelectedIndex = RunningGadgetsListBox.Items.Count - 1;
            }
            else
            {
                RunningGadgetsListBox.SelectedIndex = index;
            }

            if (RunningGadgetsListBox.SelectedItem != null)
            {
                this.CloseGadgetButton.Enabled = true;
                this.ShowGadgetButton.Enabled = true;
            }
        }
    }
}
