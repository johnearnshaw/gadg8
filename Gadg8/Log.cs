﻿using System; 
using System.Collections.Generic; 
using System.IO; 
using System.Reflection; 
using System.Text; 
using System.Windows.Forms;
using System.Xml; 
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

namespace Gadg8
{
    public static class Log
    {
        public static string LogPath = Application.StartupPath.ToString() + "\\log.txt";

        public static void Write(string text, string file = "")
        {
            if (Settings.Read("writegadg8log", false))
            {
                if (string.IsNullOrEmpty(file))
                {
                    File.AppendAllText(LogPath, DateTime.Now.ToString() + "\r\n" + text + "\r\n\r\n", Encoding.UTF8);
                }
                else
                {
                    File.AppendAllText(file, DateTime.Now.ToString() + "\r\n" + text + "\r\n\r\n", Encoding.UTF8);
                }
            }
        }
    }
}
