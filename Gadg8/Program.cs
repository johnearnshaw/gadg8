﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using Shell32;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;

namespace Gadg8
{
    [ComVisible(true)]
    static class Program
    {
        private static uint InstanceId;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main(string[] args)
        {
            object activeInstance;
            Guid guid = typeof(Sidebar).GUID;
            
            if (Native.GetActiveObject(ref guid, IntPtr.Zero, out activeInstance) == 0)
            {
                if (args.Length > 0)
                {
                    try
                    {
                        InstanceNotifier notifier = new InstanceNotifier(args[0]);
                        Thread.Sleep(200);
                        notifier = null;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.InnerException.Message);
                    }
                }

                return;
            }
            else
            {
                // No instance running already
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                Settings.BinaryMode = true;

                LoadSidebar(args);
            }
        }

        private static void LoadSidebar(string[] args)
        {
            // If we have some args we presume our application has been launched by opening an associated file.
            string file = null;
            if (args.Length > 0)
            {
               file = args[0];
            }


            Sidebar frm = new Sidebar(file);
            
            Guid guid = typeof(Sidebar).GUID;
            Native.RegisterActiveObject(frm, ref guid, 0, out InstanceId);

            Application.ApplicationExit += (sender, e) => { Native.RevokeActiveObject(InstanceId, IntPtr.Zero); };
            Application.Run(frm);
            
        }
    }
}
