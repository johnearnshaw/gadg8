﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace Gadg8Gadget
{
    static class Program
    {
        static Gadget g;
        public static string GadgetPath;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            SetCompatibilityMode();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

#if DEBUG
            System.Diagnostics.Debugger.Launch();
#endif
            Application.ThreadException += new ThreadExceptionEventHandler(UnhandledThreadExceptionHandler);

            string path = "";
            string uniqueId = "";
            int top = 0;
            int left = 0;
            int hostIpcPort;
            GadgetManifest gadgetXML;

            if (args.Length == 5)
            {
                try
                {
                    path = args[0].Replace("*", " ");
                    left = int.Parse(args[1]);
                    top = int.Parse(args[2]);
                    uniqueId = args[3];
                    hostIpcPort = int.Parse(args[4]);
                }
                catch
                {
                    MessageBox.Show("args: \"Gadget Path\" \"Location Y\" \"Location Y\" \"Unique Id\" \"IPC PortNumber\"", "Gadg8Gadget.exe requires arguments to start.");
                    return;
                }
            }
            else
            {
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    left = 100;
                    top = 100;
                    uniqueId = Guid.NewGuid().ToString();
                    hostIpcPort = 0;
                    string strPort = "";
                    InputBox.Show("Open New Gadget", "IPC Port Number:", ref strPort);
                    InputBox.Show("Open New Gadget", "Gadget Path:", ref path);

                    if (strPort != "" && path != "")
                    {
                        hostIpcPort = int.Parse(strPort);
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("args: \"Gadget Path\" \"Location Y\" \"Location Y\" \"Unique Id\" \"IPC PortNumber\"", "Gadg8Gadget.exe requires arguments to start.");
                    return;
                }
            }

            // Normalize path and make sure it is a folder path with a trailing "\".
            path = path.Replace("/", "\\");
            if (!path.EndsWith("\\"))
                path = path + "\\";

            GadgetPath = path;

            // Do some checks to make sure the gadget exists and is valid.
            if (!Directory.Exists(path))
            {
                MessageBox.Show(path + " not found");
                return;
            }
            else
            {
                string folder;
                gadgetXML = Helpers.GetGadgetXML(path, out folder);
            }
            if (gadgetXML == null)
            {
                MessageBox.Show("Gadget manifest (gadget.xml) not found");
                return;
            }
            else
            {
                if (!Helpers.GadgetSourceExists(path, gadgetXML))
                {
                    MessageBox.Show(gadgetXML.hosts[0].Base.Src + " not found");
                    return;
                }
                else if (gadgetXML.hosts[0].Base.Type.ToLower() != "html")
                {
                    MessageBox.Show("Unsupported gadget. Only HTML base type is allowed");
                    return;
                }
            }
            
            // Finally we know by now that the gadget folder and host file exist so we can start the gadget process.
            g = new Gadget(path, left, top, uniqueId, hostIpcPort, gadgetXML, gadgetXML.Name);
            try
            {
                Application.Run(g);
            }
            catch (Exception e)
            {
                g.WriteLog(e.Message + "\r\n" + e.StackTrace);
            }
        }

        private static void UnhandledThreadExceptionHandler(object sender, ThreadExceptionEventArgs e)
        {
            g.WriteLog(e.Exception.Message + " " + e.Exception.InnerException.Message + "/r/n" + e.Exception.StackTrace);

            if (e.Exception.GetType() == typeof(System.Reflection.TargetInvocationException))
            {
                System.Reflection.TargetInvocationException ex =
                    (System.Reflection.TargetInvocationException)e.Exception;

                // Undocumented javascript error (Exception from HRESULT: 0x80020101)
                if (ex.InnerException.Message.ToLower().Contains("0x80020101"))
                    return;
            }

            MessageBox.Show(e.Exception.Message);
        }

        private static void SetCompatibilityMode()
        {
            Microsoft.Win32.RegistryKey reg;

            reg = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION", true);
                
            if (reg == null)
                reg = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION");
                
            if ((reg != null))
            {
                reg.SetValue(System.AppDomain.CurrentDomain.FriendlyName, 0x1B58, Microsoft.Win32.RegistryValueKind.DWord);
            }
        }
    }
}
