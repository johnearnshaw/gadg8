﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18449
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Gadg8Gadget.Localize {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Gadg8Gadget.Localize.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add gadgets....
        /// </summary>
        internal static string AddGadgets {
            get {
                return ResourceManager.GetString("AddGadgets", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Always on top.
        /// </summary>
        internal static string AlwaysOnTop {
            get {
                return ResourceManager.GetString("AlwaysOnTop", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Close.
        /// </summary>
        internal static string Close {
            get {
                return ResourceManager.GetString("Close", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Close gadget.
        /// </summary>
        internal static string CloseGadget {
            get {
                return ResourceManager.GetString("CloseGadget", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dock.
        /// </summary>
        internal static string Dock {
            get {
                return ResourceManager.GetString("Dock", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hello.
        /// </summary>
        internal static string Hello {
            get {
                return ResourceManager.GetString("Hello", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Move.
        /// </summary>
        internal static string Move {
            get {
                return ResourceManager.GetString("Move", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK.
        /// </summary>
        internal static string OK {
            get {
                return ResourceManager.GetString("OK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Opacity.
        /// </summary>
        internal static string Opacity {
            get {
                return ResourceManager.GetString("Opacity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Open gadget folder.
        /// </summary>
        internal static string OpenGadgetFolder {
            get {
                return ResourceManager.GetString("OpenGadgetFolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Options.
        /// </summary>
        internal static string Options {
            get {
                return ResourceManager.GetString("Options", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reload.
        /// </summary>
        internal static string Reload {
            get {
                return ResourceManager.GetString("Reload", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Allow ActiveX controls.
        /// </summary>
        internal static string SecurityAllowActiveX {
            get {
                return ResourceManager.GetString("SecurityAllowActiveX", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Allow CrossDomain scripting.
        /// </summary>
        internal static string SecurityAllowCrossDomain {
            get {
                return ResourceManager.GetString("SecurityAllowCrossDomain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Allow IFrames.
        /// </summary>
        internal static string SecurityAllowFrames {
            get {
                return ResourceManager.GetString("SecurityAllowFrames", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Security settings.
        /// </summary>
        internal static string SecuritySettings {
            get {
                return ResourceManager.GetString("SecuritySettings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Settings.
        /// </summary>
        internal static string Settings {
            get {
                return ResourceManager.GetString("Settings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Show developer console.
        /// </summary>
        internal static string ShowConsole {
            get {
                return ResourceManager.GetString("ShowConsole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Undock.
        /// </summary>
        internal static string Undock {
            get {
                return ResourceManager.GetString("Undock", resourceCulture);
            }
        }
    }
}
