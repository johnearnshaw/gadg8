﻿//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.Runtime.CompilerServices;

//namespace System.Runtime.CompilerServices
//{
//    [AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class
//         | AttributeTargets.Method)]
//    public sealed class ExtensionAttribute : Attribute { }
//}

//public static class Extensions
//{
//    [System.Runtime.CompilerServices.Extension()]
//    public static Bitmap Clone(Image b, int Width, int Height)
//    {
//        Bitmap functionReturnValue = default(Bitmap);
//        functionReturnValue = new Bitmap(Width, Height);
//        Graphics g = Graphics.FromImage(functionReturnValue);

//        g.InterpolationMode = InterpolationMode.High;
//        g.DrawImage(b, new Rectangle(0, 0, functionReturnValue.Width, functionReturnValue.Height));

//        g.Dispose();
//        return functionReturnValue;
//    }

//    [System.Runtime.CompilerServices.Extension()]
//    public static Bitmap Clone(Image b, Size Size)
//    {
//        return b.Clone(Size.Width, Size.Height);
//    }
//}
