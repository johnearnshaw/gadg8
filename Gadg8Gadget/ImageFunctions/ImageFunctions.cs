﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;

namespace Gadg8Gadget.ImageFunctions
{
    #region ImageFunctions
    public static class ImageFunctions
    {
        //public static DirectXBlur Blur = new DirectXBlur();

        private static byte BlendByte(byte From, byte To, byte Amount)
        {
            return Convert.ToByte((((int)(To) - From) * (Amount / 255)) + From);
        }

        public static void Clear(Bitmap b)
        {
            LockBitmap lockBitmap = new LockBitmap(b);
            lockBitmap.LockBits();
            Clear(lockBitmap);
            lockBitmap.UnlockBits();
        }

        public static void Clear(LockBitmap lockBitmap)
        {
            byte[] OutBytes = null;
            OutBytes = new byte[lockBitmap.Pixels.Length];
            lockBitmap.Pixels = OutBytes;
        }

        public static Bitmap StripAlphaToNewBitmap(Bitmap b)
        {
            Bitmap bmp = new Bitmap(b.Width, b.Height);
            LockBitmap lockBitmap = new LockBitmap(b);
            LockBitmap lockBitmap2 = new LockBitmap(bmp);
            lockBitmap.LockBits();
            lockBitmap2.LockBits();

            Color compareClr = Color.FromArgb(255, 255, 255, 255);
            for (int y = 0; y < lockBitmap.Height; y++)
            {
                for (int x = 0; x < lockBitmap.Width; x++)
                {
                    Color px = lockBitmap.GetPixel(x, y);
                    if (px.A < 255)
                    {
                        lockBitmap.SetPixel(x, y, Color.Magenta);
                        lockBitmap2.SetPixel(x, y, px);
                    }
                }
            }
            lockBitmap.UnlockBits();
            lockBitmap2.UnlockBits();

            return bmp;
        }

        public static Bitmap DrawText(String text, System.Drawing.Font font, Color color)
        {
            //create a brush for the text
            Brush textBrush = new SolidBrush(color);

            //create a dummy bitmap and graphics to measure string
            Bitmap b = new Bitmap(1, 1);
            Graphics g = Graphics.FromImage(b);
            SizeF textSize = g.MeasureString(text, font);
            //free up the dummy image and old graphics object
            b.Dispose();
            g.Dispose();

            //create a new image
            b = new Bitmap((int)textSize.Width, (int)textSize.Height);
            g = Graphics.FromImage(b);
            g.DrawString(text, font, textBrush, 0, 0);

            textBrush.Dispose();
            g.Dispose();

            return b;
        }

        public static Bitmap GetGlowOrShadowBitmap(Bitmap b, Color color, int blur)
        {
            Bitmap bmp = new Bitmap(b.Width + (blur * 2), b.Height + (blur * 2));

            Graphics g = Graphics.FromImage(bmp);

            g.DrawImage(b, 0, 0, b.Width, b.Height);
            g.Dispose();

            LockBitmap lockBitmap = new LockBitmap(bmp);
            lockBitmap.LockBits();
            AlphaMask(lockBitmap, Color.Transparent, color);
            GausianBlur(lockBitmap, blur);
            lockBitmap.UnlockBits();
            return bmp;
        }

        public static Bitmap AddImage(Bitmap destBmp, Bitmap srcBmp, int x, int y, int width, int height)
        {
            Graphics g = Graphics.FromImage(destBmp);
            g.CompositingMode = CompositingMode.SourceOver;
            g.DrawImage(srcBmp, x, y, width, height);
            g.Dispose();
            return destBmp;
        }

        public static Bitmap AddImage(Bitmap destBmp, Bitmap srcBmp, int x, int y, byte alpha = 255)
        {
            //IntPtr pTarget = e.Graphics.GetHdc();
            IntPtr pTarget = destBmp.GetHbitmap();
            IntPtr pSource = Native.CreateCompatibleDC(pTarget);
            IntPtr pOrig = Native.SelectObject(pSource, srcBmp.GetHbitmap(Color.Black));
            Native.AlphaBlend(pTarget, 0, 0, srcBmp.Width, srcBmp.Height, pSource, 0, 0, srcBmp.Width, srcBmp.Height, new Native.BLENDFUNCTION(Native.AC_SRC_OVER, 0, alpha, Native.AC_SRC_ALPHA));
            IntPtr pNew = Native.SelectObject(pSource, pOrig);
            Native.DeleteObject(pNew);
            Native.DeleteDC(pSource);
            //e.Graphics.ReleaseHdc(pTarget);
            return destBmp;
        }

        private static void MatrixBlend(Bitmap image1, Bitmap image2, byte alpha)
        {
            // for the matrix the range is 0.0 - 1.0
            float alphaNorm = (float)alpha / 255.0F;
            // just change the alpha
            ColorMatrix matrix = new ColorMatrix(new float[][]{
                new float[] {1F, 0, 0, 0, 0},
                new float[] {0, 1F, 0, 0, 0},
                new float[] {0, 0, 1F, 0, 0},
                new float[] {0, 0, 0, alphaNorm, 0},
                new float[] {0, 0, 0, 0, 1F}});

            ImageAttributes imageAttributes = new ImageAttributes();
            imageAttributes.SetColorMatrix(matrix);

            using (Graphics g = Graphics.FromImage(image1))
            {
                g.CompositingMode = CompositingMode.SourceOver;
                g.CompositingQuality = CompositingQuality.HighQuality;

                g.DrawImage(image2,
                    new System.Drawing.Rectangle(0, 0, image1.Width, image1.Height),
                    0,
                    0,
                    image2.Width,
                    image2.Height,
                    GraphicsUnit.Pixel,
                    imageAttributes);
            }
        }

        //public static Bitmap AddImage(LockBitmap lockDestBitmap, LockBitmap lockSrcBitmap, int x, int y)
        //{
        //    int startLocationX = 0;
        //    int startLocationY = 0;
        //    if (x < 0)
        //        startLocationX = Math.Abs(x);
        //    if (y < 0)
        //        startLocationY = Math.Abs(y);

        //    for (int iY = y; iY < lockDestBitmap.Height; iY++)
        //    {
        //        for (int iX = x; iX < lockDestBitmap.Width; iX++)
        //        {
        //            //Get Both Colours at the pixel point
        //            Color col1 = lockSrcBitmap.GetPixel(iX, iY);
        //            Color col2 = lockSrcBitmap.GetPixel(iX, iY);

        //            //Get the difference RGB
        //            int r = 0, g = 0, b = 0;
        //            r = Math.Abs(col1.R - col2.R);
        //            g = Math.Abs(col1.G - col2.G);
        //            b = Math.Abs(col1.B - col2.B);

        //            //Invert the difference average
        //            int dif = 255 - ((r + g + b) / 3);

        //            //Create new grayscale rgb colour
        //            Color newcol = Color.FromArgb(dif, dif, dif);

        //            diffBM.SetPixel(x, y, newcol);

        //        }
        //    }

        //    for (int i = 0; i < lockDestBitmap.PixelCount; i++)
        //    {
        //        if (lockDestBitmap.Pixels.Length >= i)
        //        {
        //            lockDestBitmap.SetPixel(i + x, i + y, lockDestBitmap.GetPixel(i, i));
        //        }
        //    }
        //    return destBmp;
        //}

        public static void Invert(Bitmap b)
        {
            LockBitmap lockBitmap = new LockBitmap(b);
            lockBitmap.LockBits();
            Invert(lockBitmap);
            lockBitmap.UnlockBits();
        }

        public static void Invert(LockBitmap lockBitmap)
        {
            for (int i = lockBitmap.Pixels.GetLowerBound(0); i <= lockBitmap.Pixels.GetUpperBound(0); i++)
            {
                switch (i % 4)
                {
                    case 0:
                    case 1:
                    case 2:
                        //blue, green, red
                        lockBitmap.Pixels[i] = Convert.ToByte(255 - lockBitmap.Pixels[i]);
                        break;
                    case 3:
                        //alpha
                        break;
                }
            }
        }

        public static void GausianBlur(Bitmap b, int Amount = 4)
        {
            LockBitmap lockBitmap = new LockBitmap(b);
            lockBitmap.LockBits();
            GausianBlur(lockBitmap, Amount);
            lockBitmap.UnlockBits();
        }

        public static void GausianBlur(LockBitmap lockBitmap, int Amount = 4)
        {
            byte[] OutBytes = null;
            OutBytes = new byte[lockBitmap.Pixels.Length];

            for (int i = lockBitmap.Pixels.GetLowerBound(0); i <= lockBitmap.Pixels.GetUpperBound(0); i += 4)
            {
                //find the average color of the area around the pixel...
                int cR = 0;
                int cG = 0;
                int cB = 0;
                int cA = 0;
                int PixelCounter = 0;

                int YLine = lockBitmap.YFromOffset(i);

                for (int iY = Math.Max(YLine - (int)(Amount / 2), 0); iY <= Math.Min(YLine + (int)(Amount / 2), lockBitmap.Source.Height - 1); iY++)
                {
                    int StartOfLine = lockBitmap.StartOfLineFromOffset(Convert.ToInt32(iY * (lockBitmap.Source.Width * 4)));
                    int EndOfLine = StartOfLine + ((lockBitmap.Source.Width * 4) - 1);
                    int ThisX = i + ((iY - YLine) * (lockBitmap.Source.Width * 4));

                    for (int iX = (int)(Math.Max(ThisX - ((int)(Amount / 2) * 4), StartOfLine)); iX <= (int)(Math.Min((ThisX + ((int)((Amount / 2) + 1) * 4)) - 1, EndOfLine)); iX++)
                    {
                        switch (iX % 4)
                        {
                            case 0:
                                //blue
                                cB += lockBitmap.Pixels[iX];
                                break;
                            case 1:
                                //green
                                cG += lockBitmap.Pixels[iX];
                                break;
                            case 2:
                                //red
                                cR += lockBitmap.Pixels[iX];
                                break;
                            case 3:
                                //alpha
                                cA += lockBitmap.Pixels[iX];
                                PixelCounter += 1;
                                break;
                        }
                    }
                }

                OutBytes[i] = Convert.ToByte(cB / PixelCounter);
                //blue
                OutBytes[i + 1] = Convert.ToByte(cG / PixelCounter);
                //green
                OutBytes[i + 2] = Convert.ToByte(cR / PixelCounter);
                //red
                OutBytes[i + 3] = Convert.ToByte(cA / PixelCounter);
                //alpha
            }

            lockBitmap.Pixels = OutBytes;
        }

        public static void Alpha(Bitmap b, byte Alpha = 127)
        {
            // for the matrix the range is 0.0 - 1.0
            float alphaNorm = (float)Alpha / 255.0F;
            // just change the alpha
            ColorMatrix cm = new ColorMatrix(new float[][]{
                new float[] {1F, 0, 0, 0, 0},
                new float[] {0, 1F, 0, 0, 0},
                new float[] {0, 0, 1F, 0, 0},
                new float[] {0, 0, 0, alphaNorm, 0},
                new float[] {0, 0, 0, 0, 1F}});

            //System.Drawing.Imaging.ColorMatrix cm = new System.Drawing.Imaging.ColorMatrix();
            //cm.Matrix33 = Convert.ToSingle(Alpha / 255);
            using (System.Drawing.Imaging.ImageAttributes ia = new System.Drawing.Imaging.ImageAttributes())
            {
                ia.SetColorMatrix(cm);
                System.Drawing.Rectangle rc = new System.Drawing.Rectangle(0, 0, b.Width, b.Height);
                using (Bitmap bClone = (Bitmap)b.Clone())
                {
                    using (Graphics g = Graphics.FromImage(b))
                    {
                        g.Clear(Color.Transparent);
                        g.DrawImage(bClone, rc, 0, 0, bClone.Width, bClone.Height, GraphicsUnit.Pixel, ia);
                    }
                }
            }
        }

        public static void GrayScale(Bitmap b)
        {
            System.Drawing.Imaging.ColorMatrix cm = new System.Drawing.Imaging.ColorMatrix(new float[][] {
                new float[] { 0.299f, 0.299f, 0.299f, 0, 0 },
                new float[] { 0.587f, 0.587f, 0.587f, 0, 0 },
                new float[] { 0.114f, 0.114f, 0.114f, 0, 0 },
                new float[] { 0, 0, 0, 1, 0 },
                new float[] { 0, 0, 0, 0, 1 }
            });
            using (System.Drawing.Imaging.ImageAttributes ia = new System.Drawing.Imaging.ImageAttributes())
            {
                ia.SetColorMatrix(cm);
                System.Drawing.Rectangle rc = new System.Drawing.Rectangle(0, 0, b.Width, b.Height);
                using (Bitmap bClone = (Bitmap)b.Clone())
                {
                    using (Graphics g = Graphics.FromImage(b))
                    {
                        g.Clear(Color.Transparent);
                        g.DrawImage(bClone, rc, 0, 0, bClone.Width, bClone.Height, GraphicsUnit.Pixel, ia);
                    }
                }
            }
        }


        public static void AlphaMask(Bitmap b, Color AlphaColor, Color SolidColor)
        {
            LockBitmap lockBitmap = new LockBitmap(b);
            lockBitmap.LockBits();
            AlphaMask(lockBitmap, AlphaColor, SolidColor);
            lockBitmap.UnlockBits();
        }

        public static void AlphaMask(LockBitmap lockBitmap, Color AlphaColor, Color SolidColor)
        {
            for (int i = lockBitmap.Pixels.GetLowerBound(0); i <= lockBitmap.Pixels.GetUpperBound(0); i += 4)
            {
                Byte AlphaByte = lockBitmap.Pixels[i + 3];
                //blue
                lockBitmap.Pixels[i] = BlendByte(AlphaColor.B, SolidColor.B, AlphaByte);
                //green
                lockBitmap.Pixels[i + 1] = BlendByte(AlphaColor.G, SolidColor.G, AlphaByte);
                //red
                lockBitmap.Pixels[i + 2] = BlendByte(AlphaColor.R, SolidColor.R, AlphaByte);
                //alpha
                lockBitmap.Pixels[i + 3] = BlendByte(AlphaColor.A, SolidColor.A, AlphaByte);
            }
        }

        public static void Brightness(Bitmap b, float amount = 0)
        {
            LockBitmap lockBitmap = new LockBitmap(b);
            lockBitmap.LockBits();
            Brightness(lockBitmap, amount);
            lockBitmap.UnlockBits();
        }

        public static void Brightness(LockBitmap lockBitmap, float amount = 0)
        {
            if (amount == 0)
                return;
            if (amount > 0)
            {
                for (int i = lockBitmap.Pixels.GetLowerBound(0); i <= lockBitmap.Pixels.GetUpperBound(0); i += 4)
                {
                    //blue
                    lockBitmap.Pixels[i] = Convert.ToByte(lockBitmap.Pixels[i] + (amount * (255 - lockBitmap.Pixels[i])));
                    //green
                    lockBitmap.Pixels[i + 1] = Convert.ToByte(lockBitmap.Pixels[i + 1] + (amount * (255 - lockBitmap.Pixels[i + 1])));
                    //red
                    lockBitmap.Pixels[i + 2] = Convert.ToByte(lockBitmap.Pixels[i + 2] + (amount * (255 - lockBitmap.Pixels[i + 2])));
                }
            }
            else
            {
                for (int i = lockBitmap.Pixels.GetLowerBound(0); i <= lockBitmap.Pixels.GetUpperBound(0); i += 4)
                {
                    //blue
                    lockBitmap.Pixels[i] = Convert.ToByte(lockBitmap.Pixels[i] - (Math.Abs(amount) * lockBitmap.Pixels[i]));
                    //green
                    lockBitmap.Pixels[i + 1] = Convert.ToByte(lockBitmap.Pixels[i + 1] - (Math.Abs(amount) * lockBitmap.Pixels[i + 1]));
                    //red
                    lockBitmap.Pixels[i + 2] = Convert.ToByte(lockBitmap.Pixels[i + 2] - (Math.Abs(amount) * lockBitmap.Pixels[i + 2]));
                }
            }
        }

        public static void Contrast(Bitmap b, float amount = 0)
        {
            LockBitmap lockBitmap = new LockBitmap(b);
            lockBitmap.LockBits();
            Contrast(lockBitmap, amount);
            lockBitmap.UnlockBits();
        }

        private static unsafe void Contrast(LockBitmap lockBitmap, float amount)
        {
            byte[] contrast_lookup = new byte[256];
            double newValue = 0;
            double c = (100.0 + amount) / 100.0;

            c *= c;

            for (int i = 0; i < 256; i++)
            {
                newValue = (double)i;
                newValue /= 255.0;
                newValue -= 0.5;
                newValue *= c;
                newValue += 0.5;
                newValue *= 255;

                if (newValue < 0)
                    newValue = 0;
                if (newValue > 255)
                    newValue = 255;
                contrast_lookup[i] = (byte)newValue;
            }

            int PixelSize = 4;

            for (int y = 0; y < lockBitmap.Height; y++)
            {
                byte* destPixels = (byte*)lockBitmap.Scan0 + (y * lockBitmap.Stride);
                for (int x = 0; x < lockBitmap.Width; x++)
                {
                    destPixels[x * PixelSize] = contrast_lookup[destPixels[x * PixelSize]]; // B
                    destPixels[x * PixelSize + 1] = contrast_lookup[destPixels[x * PixelSize + 1]]; // G
                    destPixels[x * PixelSize + 2] = contrast_lookup[destPixels[x * PixelSize + 2]]; // R
                    //destPixels[x * PixelSize + 3] = contrast_lookup[destPixels[x * PixelSize + 3]]; //A
                }
            }
        }

        public static void DropShadow(Bitmap b, Color ShadowColor, System.Drawing.Point Depth, int BlurAmount = 4)
        {
            LockBitmap lockBitmap = new LockBitmap(b);
            lockBitmap.LockBits();
            DropShadow(lockBitmap, ShadowColor, Depth, BlurAmount);
            lockBitmap.UnlockBits();
        }

        public static void DropShadow(LockBitmap lockBitmap, Color ShadowColor, System.Drawing.Point Depth, int BlurAmount = 4)
        {
            //clone the original image
            using (Bitmap bCloneShadow = (Bitmap)lockBitmap.Source.Clone())
            {
                using (Bitmap bImage = (Bitmap)lockBitmap.Source.Clone())
                {
                    //clear original image
                    Clear(lockBitmap);

                    using (Graphics g = Graphics.FromImage(lockBitmap.Source))
                    {
                        //draw shadow
                        LockBitmap lockClone = new LockBitmap(bCloneShadow);
                        lockClone.LockBits();
                        AlphaMask(lockClone, Color.Transparent, ShadowColor);
                        GausianBlur(lockClone, BlurAmount);
                        g.DrawImage(bCloneShadow, Depth);

                        //draw original image clone
                        g.DrawImage(bImage, System.Drawing.Point.Empty);
                    }
                }
            }
        }

        public static void Emboss(Bitmap b)
        {
            LockBitmap lockBitmap = new LockBitmap(b);
            lockBitmap.LockBits();
            Emboss(lockBitmap);
            lockBitmap.UnlockBits();
        }

        public static void Emboss(LockBitmap lockBitmap)
        {
            const byte Level1 = 127;
            const byte Level2 = 255;
            for (int i = lockBitmap.Pixels.GetLowerBound(0); i <= lockBitmap.Pixels.GetUpperBound(0); i += 4)
            {
                if ((i + 4) % (lockBitmap.Source.Width * 4) == 0)
                {
                    //right line - make gray
                    lockBitmap.Pixels[i] = Level1;
                    //b
                    lockBitmap.Pixels[i + 1] = Level1;
                    //g
                    lockBitmap.Pixels[i + 2] = Level1;
                    //r
                    continue;
                }
                int iY = lockBitmap.YFromOffset(i);
                if (iY == lockBitmap.Source.Height - 1)
                {
                    //bottom line - make gray
                    lockBitmap.Pixels[i] = Level1;
                    //b
                    lockBitmap.Pixels[i + 1] = Level1;
                    //g
                    lockBitmap.Pixels[i + 2] = Level1;
                    //r
                    continue;
                }

                int OffsetPixelPointer = i + ((lockBitmap.Source.Width + 1) * 4);

                lockBitmap.Pixels[i] = Convert.ToByte(Math.Min(Math.Abs((int)(lockBitmap.Pixels[i]) - (int)(lockBitmap.Pixels[OffsetPixelPointer])) + Level1, Level2));
                //b
                lockBitmap.Pixels[i + 1] = Convert.ToByte(Math.Min(Math.Abs((int)(lockBitmap.Pixels[i + 1]) - (int)(lockBitmap.Pixels[OffsetPixelPointer + 1])) + Level1, Level2));
                //g
                lockBitmap.Pixels[i + 2] = Convert.ToByte(Math.Min(Math.Abs((int)(lockBitmap.Pixels[i + 2]) - (int)(lockBitmap.Pixels[OffsetPixelPointer + 2])) + Level1, Level2));
                //r
                //lockBitmap.Pixels[i + 3] = 255 'a

            }
        }

        public static void TwoTone(Bitmap b, byte Amount = 128)
        {
            LockBitmap lockBitmap = new LockBitmap(b);
            lockBitmap.LockBits();
            TwoTone(lockBitmap, Amount);
            lockBitmap.UnlockBits();
        }

        public static void TwoTone(LockBitmap lockBitmap, byte Amount = 128)
        {
            for (int i = lockBitmap.Pixels.GetLowerBound(0); i <= lockBitmap.Pixels.GetUpperBound(0); i += 4)
            {
                byte Average = Convert.ToByte(((int)(lockBitmap.Pixels[i]) + (int)(lockBitmap.Pixels[i + 1]) + (int)(lockBitmap.Pixels[i + 2])) / 3);
                bool Black = false;
                if (Average < Amount)
                {
                    Black = true;
                }
                //blue
                lockBitmap.Pixels[i] = Convert.ToByte(Black ? 0 : 255);
                //green
                lockBitmap.Pixels[i + 1] = lockBitmap.Pixels[i];
                //red
                lockBitmap.Pixels[i + 2] = lockBitmap.Pixels[i];
            }
        }

        public static void EdgeDetection(Bitmap b)
        {
            LockBitmap lockBitmap = new LockBitmap(b);
            lockBitmap.LockBits();
            EdgeDetection(lockBitmap);
            lockBitmap.UnlockBits();
        }

        public static void EdgeDetection(LockBitmap lockBitmap)
        {
            Emboss(lockBitmap);
            TwoTone(lockBitmap, 130);
            Invert(lockBitmap);
        }

        public static Bitmap Clone(Bitmap srcBitmap)
        {
            Bitmap result = new Bitmap(srcBitmap.Width, srcBitmap.Height, PixelFormat.Format32bppArgb);

            System.Drawing.Rectangle bmpBounds = new System.Drawing.Rectangle(0, 0, srcBitmap.Width, srcBitmap.Height);
            BitmapData srcData = srcBitmap.LockBits(bmpBounds, ImageLockMode.ReadWrite, srcBitmap.PixelFormat);
            BitmapData resData = result.LockBits(bmpBounds, ImageLockMode.WriteOnly, result.PixelFormat);

            Int64 srcScan0 = srcData.Scan0.ToInt64();
            Int64 resScan0 = resData.Scan0.ToInt64();
            int srcStride = srcData.Stride;
            int resStride = resData.Stride;
            int rowLength = Math.Abs(srcData.Stride);
            try
            {
                byte[] buffer = new byte[rowLength];
                for (int y = 0; y < srcData.Height; y++)
                {
                    Marshal.Copy(new IntPtr(srcScan0 + y * srcStride), buffer, 0, rowLength);
                    Marshal.Copy(buffer, 0, new IntPtr(resScan0 + y * resStride), rowLength);
                }
            }
            finally
            {
                srcBitmap.UnlockBits(srcData);
                result.UnlockBits(resData);
            }

            return result;
        }

        //public byte[] imageToByteArray(System.Drawing.Image imageIn)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
        //    return ms.ToArray();
        //}

        //public Image byteArrayToImage(byte[] byteArrayIn)
        //{
        //    MemoryStream ms = new MemoryStream(byteArrayIn);
        //    Image returnImage = Image.FromStream(ms);
        //    return returnImage;
        //}
    }
    #endregion
}