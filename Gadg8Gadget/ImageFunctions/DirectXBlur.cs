﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Drawing;
//using System.Drawing.Imaging;
//using System.Windows.Forms;
//using System.Runtime.InteropServices;
//using Microsoft.DirectX;
//using Microsoft.DirectX.Direct3D;
//using System.Security.Permissions;

//namespace Gadg8.ImageFunctions
//{
//    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
//    public class DirectXBlur
//    {
//        int _Width;
//        int _Height;

//        Control _Control;
//        PresentParameters _PP;
//        Device _Device;
//        VertexBuffer _VB;

//        DataTexture _Texture;
//        DataSurface _Surface;
//        RenderTarget _Target;

//        Effect _Effect;

//        public DirectXBlur()
//        {
//            Load();
//        }

//        public void Load()
//        {
//            Size maxSize = GetMaxSize();
//            Size s = new Size(2048, 2048);
//            if (s.Width > maxSize.Width) s.Width = maxSize.Width;
//            if (s.Height > maxSize.Height) s.Height = maxSize.Height;

//            this._Width = s.Width;
//            this._Height = s.Height;

//            _Control = new Control();
//            _Control.Width = s.Width;
//            _Control.Height = s.Height;

//            _PP = new PresentParameters();
//            _PP.BackBufferCount = 1;
//            _PP.BackBufferFormat = Format.A8R8G8B8;
//            _PP.BackBufferWidth = s.Width;
//            _PP.BackBufferHeight = s.Height;
//            _PP.DeviceWindow = this._Control;
//            _PP.SwapEffect = SwapEffect.Discard;
//            _PP.Windowed = true;
//            _PP.PresentFlag = PresentFlag.None;

//            _Device = new Device(0, DeviceType.Hardware, _Control, CreateFlags.SoftwareVertexProcessing, _PP);
//            SetVB(s.Width, s.Height);

//            ResetDevice();
//        }

//        public delegate Bitmap Action();
//        public Bitmap Apply(Bitmap bmp, int value)
//        {
//            Action action = delegate()
//            {
//            start:
//                try
//                {
//                    string err = "";
//                    _Effect = Effect.FromString(_Device, (string)Properties.Resources.ResourceManager.GetObject("blur"), (Include)null, (ShaderFlags)ShaderFlags.None, (EffectPool)null, out err);
//                    if (err.Length > 0)
//                        throw new Exception(err);

//                    EffectHandle Amount = _Effect.GetParameter(null, "Amount");
//                    _Effect.SetValue(Amount, (float)value / 1000);


//                    SetFrameSize(bmp.Width, bmp.Height);

//                    BitmapData bd;
//                    Bitmap b = (Bitmap)bmp.Clone();
//                    Frame frame = Frame.OpenBitmap(b, out bd);

//                    _Texture.SetData(frame);

//                    _Device.SetRenderTarget(0, (Surface)_Target);
//                    _Device.Clear(ClearFlags.Target, Color.FromArgb(0, 0, 0, 0), 0, 0);
//                    _Device.BeginScene();

//                    _Device.VertexFormat = CustomVertex.TransformedTextured.Format;
//                    _Device.SetStreamSource(0, _VB, 0);

//                    //start rendering
//                    _Effect.Technique = _Effect.GetTechnique(0);
//                    _Effect.Begin(FX.DoNotSaveState);
//                    _Effect.BeginPass(0);

//                    _Device.SetTexture(0, (Texture)_Texture);
//                    _Device.DrawPrimitives(PrimitiveType.TriangleList, 0, 2);

//                    //end rendering
//                    _Effect.EndPass();
//                    _Effect.End();
//                    _Device.VertexShader = null;
//                    _Device.PixelShader = null;

//                    _Device.EndScene();
//                    _Device.GetRenderTargetData((Surface)_Target, (Surface)_Surface);

//                    Frame frm = _Surface.GetData(frame);
//                    Frame.CloseBitmap(b, bd);
//                    //return frm;

//                    _Effect.Dispose();
//                    _Surface.Dispose();

//                    return b;
//                }
//                catch (DeviceNotResetException ex)
//                {
//                    _Device.Reset(_PP);
//                    System.Threading.Thread.Sleep(50);
//                    goto start;
//                }
//                catch (DeviceLostException ex)
//                {
//                    System.Threading.Thread.Sleep(100);
//                    goto start;
//                }
//            };

//            return (Bitmap)_Control.Invoke(action);
//        }

//        internal void SetFrameSize(int width, int height)
//        {
//            if (_Target.disposed == true)
//            {
//                _Target = null;
//            }
//            else if (_Target.Width != width || _Target.Height != height)
//            {
//                _Target.Dispose();
//                _Target = null;
//            }
//            if (_Target == null)
//                _Target = new RenderTarget(_Device, width, height);

//            if (_Texture.disposed == true)
//            {
//                _Texture = null;
//            }
//            else if (_Texture.Width != width || _Texture.Height != height)
//            {
//                _Texture.Dispose();
//                _Texture = null;
//            }
//            if (_Texture == null)
//                _Texture = new DataTexture(_Device, width, height);

//            if (_Surface.disposed == true)
//            {
//                _Surface = null;
//            }
//            else if (_Surface.Width != width || _Surface.Height != height)
//            {
//                _Surface.Dispose();
//                _Surface = null;
//            }
//            if (_Surface == null)
//                _Surface = new DataSurface(_Device, width, height);

//            SetVB(width, height);
//        }

//        public void SetVB(int width, int height)
//        {
//            if (_VB == null) _VB = new VertexBuffer(typeof(CustomVertex.TransformedTextured), 6, _Device, Usage.SoftwareProcessing, CustomVertex.TransformedTextured.Format, Pool.Managed);

//            CustomVertex.TransformedTextured[] vertices = new CustomVertex.TransformedTextured[6];

//            vertices[0].Position = new Vector4(0, 0, 0, 1);
//            vertices[1].Position = new Vector4(width, 0, 0, 1);
//            vertices[2].Position = new Vector4(0, height, 0, 1);
//            vertices[3].Position = new Vector4(width, height, 0, 1);
//            vertices[4].Position = new Vector4(0, height, 0, 1);
//            vertices[5].Position = new Vector4(width, 0, 0, 1);


//            vertices[0].Tu = 0;
//            vertices[0].Tv = 0;

//            vertices[1].Tu = 1;
//            vertices[1].Tv = 0;

//            vertices[2].Tu = 0;
//            vertices[2].Tv = 1;

//            vertices[3].Tu = 1;
//            vertices[3].Tv = 1;

//            vertices[4].Tu = 0;
//            vertices[4].Tv = 1;

//            vertices[5].Tu = 1;
//            vertices[5].Tv = 0;

//            _VB.SetData(vertices, 0, LockFlags.None);
//        }

//        public void ResetDevice()
//        {
//            _Device.Reset(_PP);
//            //DefaultRt = Device.GetRenderTarget(0);

//            _Device.RenderState.CullMode = Cull.None;
//            _Device.RenderState.Lighting = false;
//            _Device.RenderState.SourceBlend = Microsoft.DirectX.Direct3D.Blend.SourceAlpha;
//            _Device.RenderState.DestinationBlend = Microsoft.DirectX.Direct3D.Blend.InvSourceAlpha;
//            _Device.RenderState.AlphaBlendEnable = true;

//            _Device.SetSamplerState(0, SamplerStageStates.MagFilter, (int)TextureFilter.Linear);
//            _Device.SetSamplerState(0, SamplerStageStates.MinFilter, (int)TextureFilter.Linear);
//            _Device.SetSamplerState(0, SamplerStageStates.MipFilter, (int)TextureFilter.Linear);

//            if (_Target != null) _Target.Dispose();
//            _Target = new RenderTarget(_Device, _PP.BackBufferWidth, _PP.BackBufferHeight);

//            if (_Surface != null) _Surface.Dispose();
//            _Surface = new DataSurface(_Device, _PP.BackBufferWidth, _PP.BackBufferHeight);

//            if (_Texture != null) _Texture.Dispose();
//            _Texture = new DataTexture(_Device, _PP.BackBufferWidth, _PP.BackBufferHeight);
//        }

//        private Bitmap BitmapFromSurface(Surface surface)
//        {
//            //GraphicsStream gs = SurfaceLoader.SaveToStream(ImageFileFormat.Bmp, surface);
//            //return new Bitmap(gs);

//            Bitmap ret = new Bitmap(surface.Description.Width, surface.Description.Height);
//            BitmapData data = ret.LockBits(new Rectangle(0, 0, ret.Width, ret.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
//            byte[] b = (byte[])surface.LockRectangle(typeof(byte), new Rectangle(0, 0, surface.Description.Width, surface.Description.Height), LockFlags.None, new int[] { surface.Description.Width * surface.Description.Height * 4 });

//            Marshal.Copy(b, 0, data.Scan0, b.Length);

//            surface.UnlockRectangle();
//            ret.UnlockBits(data);

//            return ret;
//        }

//        public static System.Drawing.Size GetMaxSize()
//        {
//            Caps dc = Microsoft.DirectX.Direct3D.Manager.GetDeviceCaps(0, DeviceType.Hardware);
//            return new Size(dc.MaxTextureWidth, dc.MaxTextureHeight);
//        }
//    }

//    #region RenderTargets
//    public unsafe struct Frame
//    {

//        public int Width;
//        public int Height;

//        public byte* Data;
//        public Frame(byte* data, int width, int height)
//        {
//            this.Data = data;
//            this.Width = width;
//            this.Height = height;

//        }

//        public static Frame OpenBitmap(Bitmap bitmap, out BitmapData bd)
//        {
//            bd = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppPArgb);
//            return new Frame((byte*)bd.Scan0.ToPointer(), bitmap.Width, bd.Height);
//        }
//        public static void CloseBitmap(Bitmap bitmap, BitmapData bitmapData)
//        {
//            bitmap.UnlockBits(bitmapData);
//        }


//    }

//    public class RenderTarget
//    {
//        public readonly Texture Texture;

//        public readonly Surface Surface;
//        public RenderTarget(Device device, int width, int height)
//        {
//            Texture = new Texture(device, width, height, 1, Usage.RenderTarget, Format.A8R8G8B8, Pool.Default);
//            Surface = Texture.GetSurfaceLevel(0);
//        }

//        public void Dispose()
//        {
//            if (Texture.Disposed == false)
//                Texture.Dispose();
//            if (Surface.Disposed == false)
//                Surface.Dispose();

//        }
//        public bool disposed
//        {
//            get { return Surface.Disposed; }
//        }
//        public static explicit operator Texture(RenderTarget rt)
//        {
//            return rt.Texture;
//        }
//        public static explicit operator Surface(RenderTarget rt)
//        {
//            return rt.Surface;
//        }

//        public int Width
//        {
//            get { return Surface.Description.Width; }
//        }
//        public int Height
//        {
//            get { return Surface.Description.Height; }
//        }

//    }

//    public class DataTexture
//    {
//        public readonly Texture Texture;

//        public readonly Surface Surface;
//        public DataTexture(Device device, int width, int height)
//        {
//            Texture = new Texture(device, width, height, 1, Usage.None, Format.A8R8G8B8, Pool.Managed);
//            Surface = Texture.GetSurfaceLevel(0);
//        }

//        public void Dispose()
//        {
//            if (Texture.Disposed == false)
//                Texture.Dispose();
//            if (Surface.Disposed == false)
//                Surface.Dispose();
//        }
//        public bool disposed
//        {
//            get { return Surface.Disposed; }
//        }

//        public unsafe void SetData(Frame data)
//        {
//            byte[] b = (byte[])Surface.LockRectangle(typeof(byte), new Rectangle(0, 0, this.Width, this.Height), LockFlags.DoNotWait, new int[] { this.Width * this.Height * 4 });

//            System.Runtime.InteropServices.Marshal.Copy((IntPtr)data.Data, b, 0, b.Length);

//            Surface.UnlockRectangle();
//        }
//        public void SetData(IntPtr scan)
//        {
//            byte[] b = (byte[])Surface.LockRectangle(typeof(byte), new Rectangle(0, 0, this.Width, this.Height), LockFlags.DoNotWait, new int[] { this.Width * this.Height * 4 });
//            System.Runtime.InteropServices.Marshal.Copy(scan, b, 0, b.Length);

//            Surface.UnlockRectangle();
//        }
//        public static explicit operator Texture(DataTexture rt)
//        {
//            return rt.Texture;
//        }
//        public static explicit operator Surface(DataTexture rt)
//        {
//            return rt.Surface;
//        }

//        public int Width
//        {
//            get { return Surface.Description.Width; }
//        }
//        public int Height
//        {
//            get { return Surface.Description.Height; }
//        }
//    }

//    public class DataSurface
//    {
//        public readonly Surface Surface;
//        public DataSurface(Device device, int width, int height)
//        {
//            Surface = device.CreateOffscreenPlainSurface(width, height, Format.A8R8G8B8, Pool.SystemMemory);
//        }

//        public void Dispose()
//        {
//            if (Surface.Disposed == false)
//                Surface.Dispose();
//        }
//        public bool disposed
//        {
//            get { return Surface.Disposed; }
//        }

//        public unsafe Frame GetData(Frame inFrame)
//        {
//            byte[] b = (byte[])Surface.LockRectangle(typeof(byte), new Rectangle(0, 0, this.Width, this.Height), LockFlags.None, new int[] { this.Width * this.Height * 4 });
//            Surface.UnlockRectangle();

//            System.Runtime.InteropServices.Marshal.Copy(b, 0, (IntPtr)inFrame.Data, b.Length);
//            return inFrame;
//        }

//        public static explicit operator Surface(DataSurface rt)
//        {
//            return rt.Surface;
//        }

//        public int Width
//        {
//            get { return Surface.Description.Width; }
//        }
//        public int Height
//        {
//            get { return Surface.Description.Height; }
//        }


//    }
//    public class RenderTexture
//    {

//        public readonly Surface Surface;
//        public readonly Texture Texture;

//        public RenderTexture(Device device, int width, int height)
//        {
//            Texture = new Texture(device, width, height, 1, Usage.RenderTarget, Format.A8R8G8B8, Pool.SystemMemory);
//            Surface = this.Texture.GetSurfaceLevel(0);

//        }

//        public unsafe Frame GetData(Frame inFrame)
//        {
//            byte[] b = (byte[])Surface.LockRectangle(typeof(byte), new Rectangle(0, 0, this.Width, this.Height), LockFlags.None, new int[] { this.Width * this.Height * 4 });
//            System.Runtime.InteropServices.Marshal.Copy(b, 0, (IntPtr)inFrame.Data, b.Length);
//            Surface.UnlockRectangle();
//            return inFrame;
//        }


//        public void Dispose()
//        {
//            if (Texture.Disposed == false)
//                Texture.Dispose();
//            if (Surface.Disposed == false)
//                Surface.Dispose();
//        }
//        public bool disposed
//        {
//            get { return Surface.Disposed; }
//        }


//        public static explicit operator Texture(RenderTexture rt)
//        {
//            return rt.Texture;
//        }
//        public static explicit operator Surface(RenderTexture rt)
//        {
//            return rt.Surface;
//        }

//        public int Width
//        {
//            get { return Surface.Description.Width; }
//        }
//        public int Height
//        {
//            get { return Surface.Description.Height; }
//        }


//    }
//    #endregion
//}
