﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace Gadg8Gadget.WebBrowserControl
{
    public class ActiveXObject
    {
        public static object GetActiveXObject(string progId)
        {
            try
            {
                Type wshType = Type.GetTypeFromProgID(progId);
                return Activator.CreateInstance(wshType);
            }
            catch { return null; }
        }
    }
}
