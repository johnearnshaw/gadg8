﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Permissions;
using mshtml;

namespace Gadg8Gadget.WebBrowserControl
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    public class GElementNamespaceFactory : IElementNamespaceFactory, IElementNamespaceFactoryCallback, IElementBehaviorFactory
    {
        xGadgetWindow ParentWindow;

        public GElementNamespaceFactory(xGadgetWindow parentWindow)
        {
            this.ParentWindow = parentWindow;
        }

        #region IElementNamespaceFactory
        public void create(IElementNamespace pNamespace)
        {
            pNamespace.AddTag("background", 0);
            pNamespace.AddTag("image", 0);
            pNamespace.AddTag("text", 0);
        }
        #endregion

        #region IElementNamespaceFactoryCallback
        public void Resolve(string Namespace, string TagName, string Attrs, IElementNamespace pNamespace)
        {
        }
        #endregion

        #region IElementBehaviorFactory
        public IElementBehavior FindBehavior(string bstrBehavior, string bstrBehaviorUrl, IElementBehaviorSite pSite)
        {
            string tagname = bstrBehavior.ToLower();

            if (tagname == "background")
            {
                Behaviors.GBackground gback = new Behaviors.GBackground(ParentWindow);

                // Init doesn't fire automatically like the body behavior we attach in the document complete event.
                gback.Init(pSite);
                return (IElementBehavior)gback;
            }
            else if (tagname == "image")
            {
                Behaviors.GImage gimage = new Behaviors.GImage(ParentWindow);

                gimage.Init(pSite);
                return (IElementBehavior)gimage;
            }
            else if (tagname == "text")
            {
                Behaviors.GText gtext = new Behaviors.GText(ParentWindow);

                gtext.Init(pSite);
                return (IElementBehavior)gtext;
            }

            return null;
        }
        #endregion
    }
}
