﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Globalization;
using mshtml;

namespace Gadg8Gadget.WebBrowserControl
{
    public class HtmlEvent : IDisposable, IReflect
    {
        // Delegate
        public delegate void DHTMLEventHandler(object sender, IHTMLEventObj e);
        //public delegate void DHTMLEventHandler(object sender, object e);

        // Fields
        private DHTMLEventHandler eventHandler;
//        private object eventHandler;

        private object sender;
        private IReflect typeIReflectImplementation;
        private IHTMLElement2 htmlElement = null;
        private IHTMLWindow3 htmlWindow = null;
        private IHTMLDocument3 htmlDocument = null;
        private string eventName = null;

        // private CTOR
        private HtmlEvent(string eventName, IHTMLElement2 htmlElement, DHTMLEventHandler eventHandler)
        {
            this.eventName = eventName;
            this.htmlElement = htmlElement;
            this.sender = this;
            this.eventHandler = eventHandler;
            Type type = typeof(HtmlEvent);
            this.typeIReflectImplementation = type;
        }

        private HtmlEvent(string eventName, IHTMLWindow3 htmlWindow, DHTMLEventHandler eventHandler)
        {
            this.eventName = eventName;
            this.htmlWindow = htmlWindow;
            this.sender = this;
            this.eventHandler = eventHandler;
            Type type = typeof(HtmlEvent);
            this.typeIReflectImplementation = type;
        }

        private HtmlEvent(string eventName, IHTMLDocument3 htmlDocument, DHTMLEventHandler eventHandler)
        {
            this.eventName = eventName;
            this.htmlDocument = htmlDocument;
            this.sender = this;
            this.eventHandler = eventHandler;
            Type type = typeof(HtmlEvent);
            this.typeIReflectImplementation = type;
        }

        public static HtmlEvent Create(string eventName, IHTMLElement2 htmlElement, DHTMLEventHandler eventHandler)
        {
            HtmlEvent newProxy = new HtmlEvent(eventName, htmlElement, eventHandler);
            htmlElement.attachEvent(eventName, newProxy);
            return newProxy;
        }

        public static HtmlEvent Create(string eventName, IHTMLWindow3 htmlWindow, DHTMLEventHandler eventHandler)
        {
            HtmlEvent newProxy = new HtmlEvent(eventName, htmlWindow, eventHandler);
            htmlWindow.attachEvent(eventName, newProxy);
            return newProxy;
        }

        public static HtmlEvent Create(string eventName, IHTMLDocument3 htmlDocument, DHTMLEventHandler eventHandler)
        {
            HtmlEvent newProxy = new HtmlEvent(eventName, htmlDocument, eventHandler);
            htmlDocument.attachEvent(eventName, newProxy);
            return newProxy;
        }

        /// <summary>
        /// detach only once (thread safe)
        /// </summary>
        public void Detach()
        {
            lock (this)
            {
                if (this.htmlElement != null)
                {
                    IHTMLElement2 elem = (IHTMLElement2)htmlElement;
                    elem.detachEvent(this.eventName, this);
                    this.htmlElement = null;
                }
                else if (this.htmlWindow != null)
                {
                    IHTMLWindow3 elem = (IHTMLWindow3)htmlWindow;
                    elem.detachEvent(this.eventName, this);
                    this.htmlElement = null;
                }
                else if (this.htmlDocument != null)
                {
                    IHTMLDocument3 elem = (IHTMLDocument3)htmlElement;
                    elem.detachEvent(this.eventName, this);
                    this.htmlElement = null;
                }
            }
        }

        /// <summary>
        /// HtmlElement property  
        /// </summary>
        public IHTMLElement2 HTMLElement
        {
            get
            {
                if (this.htmlWindow != null)
                {
                    return null;
                }
                else if (this.htmlDocument != null)
                {
                    return (IHTMLElement2)((IHTMLDocument3)htmlDocument).documentElement;
                }
                else
                {
                    return this.htmlElement;
                }
                
            }
        }

        /// <summary>
        /// HtmlWindow property  
        /// </summary>
        public IHTMLWindow2 HTMLWindow
        {
            get
            {
                if (this.htmlElement != null)
                {
                    return (IHTMLWindow2)((IHTMLDocument2)((IHTMLElement)this.htmlElement).document).parentWindow;
                }
                else if (this.htmlDocument != null)
                {
                    return (IHTMLWindow2)((IHTMLDocument2)htmlDocument).parentWindow;
                }
                else
                {
                    return (IHTMLWindow2)this.htmlWindow;
                }
            }
        }

        /// <summary>
        /// HtmlDocument property  
        /// </summary>
        public IHTMLDocument2 HTMLDocument
        {
            get
            {
                if (this.htmlWindow != null)
                {
                    return null;
                }
                else if (this.htmlElement != null)
                {
                    return (IHTMLDocument2)((IHTMLElement)htmlElement).document;
                }
                else
                {
                    return (IHTMLDocument2)this.htmlDocument;
                }
            }
        }

        #region IReflect

        FieldInfo IReflect.GetField(string name, BindingFlags bindingAttr)
        {
            return this.typeIReflectImplementation.GetField(name, bindingAttr);
        }

        FieldInfo[] IReflect.GetFields(BindingFlags bindingAttr)
        {
            return this.typeIReflectImplementation.GetFields(bindingAttr);
        }

        MemberInfo[] IReflect.GetMember(string name, BindingFlags bindingAttr)
        {
            return this.typeIReflectImplementation.GetMember(name, bindingAttr);
        }

        MemberInfo[] IReflect.GetMembers(BindingFlags bindingAttr)
        {
            return this.typeIReflectImplementation.GetMembers(bindingAttr);
        }

        MethodInfo IReflect.GetMethod(string name, BindingFlags bindingAttr)
        {
            return this.typeIReflectImplementation.GetMethod(name, bindingAttr);
        }

        MethodInfo IReflect.GetMethod(string name, BindingFlags bindingAttr, Binder binder, Type[] types, ParameterModifier[] modifiers)
        {
            return this.typeIReflectImplementation.GetMethod(name, bindingAttr, binder, types, modifiers);
        }

        MethodInfo[] IReflect.GetMethods(BindingFlags bindingAttr)
        {
            return this.typeIReflectImplementation.GetMethods(bindingAttr);
        }

        PropertyInfo[] IReflect.GetProperties(BindingFlags bindingAttr)
        {
            return this.typeIReflectImplementation.GetProperties(bindingAttr);
        }

        PropertyInfo IReflect.GetProperty(string name, BindingFlags bindingAttr)
        {
            return this.typeIReflectImplementation.GetProperty(name, bindingAttr);
        }

        PropertyInfo IReflect.GetProperty(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers)
        {
            return this.typeIReflectImplementation.GetProperty(name, bindingAttr, binder, returnType, types, modifiers);
        }

        object IReflect.InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters)
        {
            if (name == "[DISPID=0]")
            {
                if (this.eventHandler != null)
                {
                    IHTMLEventObj e = null;
                    if (args.Length != 0)
                    {
                        e = args[0] as IHTMLEventObj;
                        if (e != null)
                            this.eventHandler(this.sender, e);
                    }
                }
            }
            return null;
        }



        Type IReflect.UnderlyingSystemType
        {
            get
            {
                return this.typeIReflectImplementation.UnderlyingSystemType;
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            Detach();
        }

        #endregion
    }
}
