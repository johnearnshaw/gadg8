﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using mshtml;

namespace Gadg8Gadget.WebBrowserControl
{
    [ComVisible(true), ComImport(), Guid("00000117-0000-0000-C000-000000000046"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IOleInPlaceActiveObject
    {
        //IOleWindow
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetWindow([In(), Out()]
ref IntPtr phwnd);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int ContextSensitiveHelp([In(), MarshalAs(UnmanagedType.Bool)]
bool fEnterMode);
        //IOleInPlaceActiveObject
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int TranslateAccelerator([In(), MarshalAs(UnmanagedType.Struct)]
ref Native.tagMSG lpmsg);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int OnFrameWindowActivate([In(), MarshalAs(UnmanagedType.Bool)]
bool fActivate);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int OnDocWindowActivate([In(), MarshalAs(UnmanagedType.Bool)]
bool fActivate);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int ResizeBorder([In(), MarshalAs(UnmanagedType.Struct)]
ref tagRECT prcBorder, [In(), MarshalAs(UnmanagedType.Interface)]
ref IOleInPlaceUIWindow pUIWindow, [In(), MarshalAs(UnmanagedType.Bool)]
bool fFrameWindow);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int EnableModeless([In(), MarshalAs(UnmanagedType.Bool)]
bool fEnable);
    }
}
