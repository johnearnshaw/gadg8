﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.WebBrowserControl
{
    [ComImport(), Guid("00000112-0000-0000-C000-000000000046"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IOleObject
    {
        void SetClientSite(IOleClientSite pClientSite);
        void GetClientSite(ref IOleClientSite ppClientSite);
        void SetHostNames(object szContainerApp, object szContainerObj);
        void Close(int dwSaveOption);
        void SetMoniker(int dwWhichMoniker, object pmk);
        void GetMoniker(int dwAssign, int dwWhichMoniker, object ppmk);
        void InitFromData(System.Runtime.InteropServices.ComTypes.IDataObject pDataObject, bool fCreation, int dwReserved);
        void GetClipboardData(int dwReserved, ref System.Runtime.InteropServices.ComTypes.IDataObject ppDataObject);
        void DoVerb(int iVerb, int lpmsg, object pActiveSite, int lindex, int hwndParent, int lprcPosRect);
        void EnumVerbs(ref object ppEnumOleVerb);
        void Update();
        void IsUpToDate();
        void GetUserClassID(int pClsid);
        void GetUserType(int dwFormOfType, int pszUserType);
        void SetExtent(int dwDrawAspect, int psizel);
        void GetExtent(int dwDrawAspect, int psizel);
        void Advise(object pAdvSink, int pdwConnection);
        void Unadvise(int dwConnection);
        void EnumAdvise(ref object ppenumAdvise);
        void GetMiscStatus(int dwAspect, int pdwStatus);
        void SetColorScheme(object pLogpal);
    }
}
