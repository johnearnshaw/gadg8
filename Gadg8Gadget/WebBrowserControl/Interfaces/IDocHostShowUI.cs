﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using mshtml;

namespace Gadg8Gadget.WebBrowserControl
{
    [ComImport(), ComVisible(true)]
    [Guid("C4D244B0-D43E-11CF-893B-00AA00BDCE1A")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IDocHostShowUI
    {
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int ShowMessage(IntPtr hwnd, [MarshalAs(UnmanagedType.LPWStr)]
string lpstrText, [MarshalAs(UnmanagedType.LPWStr)]
string lpstrCaption, [MarshalAs(UnmanagedType.U4)]
uint dwType, [MarshalAs(UnmanagedType.LPWStr)]
string lpstrHelpFile, [MarshalAs(UnmanagedType.U4)]
uint dwHelpContext, [In(), Out()]
ref int lpResult);

        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int ShowHelp(IntPtr hwnd, [MarshalAs(UnmanagedType.LPWStr)]
string pszHelpFile, [MarshalAs(UnmanagedType.U4)]
uint uCommand, [MarshalAs(UnmanagedType.U4)]
uint dwData, [In(), MarshalAs(UnmanagedType.Struct)]
tagPOINT ptMouse, [Out(), MarshalAs(UnmanagedType.IDispatch)]
object pDispatchObjectHit);
    }
}
