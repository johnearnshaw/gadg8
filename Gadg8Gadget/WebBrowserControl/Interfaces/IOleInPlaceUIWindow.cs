﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using mshtml;

namespace Gadg8Gadget.WebBrowserControl
{
    [ComVisible(true), ComImport(), Guid("00000115-0000-0000-C000-000000000046"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IOleInPlaceUIWindow
    {
        //IOleWindow
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetWindow([In(), Out()]
ref IntPtr phwnd);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int ContextSensitiveHelp([In(), MarshalAs(UnmanagedType.Bool)]
bool fEnterMode);
        //IOleInPlaceUIWindow
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetBorder([In(), Out(), MarshalAs(UnmanagedType.Struct)]
ref tagRECT lprectBorder);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int RequestBorderSpace([In(), MarshalAs(UnmanagedType.Struct)]
ref tagRECT pborderwidths);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int SetBorderSpace([In(), MarshalAs(UnmanagedType.Struct)]
ref tagRECT pborderwidths);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int SetActiveObject([In(), MarshalAs(UnmanagedType.Interface)]
ref IOleInPlaceActiveObject pActiveObject, [In(), MarshalAs(UnmanagedType.LPWStr)]
string pszObjName);
    }
}
