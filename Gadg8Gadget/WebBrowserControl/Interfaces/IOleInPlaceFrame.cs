﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using mshtml;

namespace Gadg8Gadget.WebBrowserControl
{
    [ComVisible(true), ComImport(), Guid("00000116-0000-0000-C000-000000000046"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IOleInPlaceFrame
    {
        //IOleWindow
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetWindow([In(), Out()]
ref IntPtr phwnd);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int ContextSensitiveHelp([In(), MarshalAs(UnmanagedType.Bool)]
bool fEnterMode);
        //IOleInPlaceUIWindow
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetBorder([Out(), MarshalAs(UnmanagedType.LPStruct)]
tagRECT lprectBorder);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int RequestBorderSpace([In(), MarshalAs(UnmanagedType.Struct)]
ref tagRECT pborderwidths);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int SetBorderSpace([In(), MarshalAs(UnmanagedType.Struct)]
ref tagRECT pborderwidths);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int SetActiveObject([In(), MarshalAs(UnmanagedType.Interface)]
ref IOleInPlaceActiveObject pActiveObject, [In(), MarshalAs(UnmanagedType.LPWStr)]
string pszObjName);
        //IOleInPlaceFrame 
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int InsertMenus([In()]
IntPtr hmenuShared, [In(), Out(), MarshalAs(UnmanagedType.Struct)]
ref object lpMenuWidths);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int SetMenu([In()]
IntPtr hmenuShared, [In()]
IntPtr holemenu, [In()]
IntPtr hwndActiveObject);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int RemoveMenus([In()]
IntPtr hmenuShared);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int SetStatusText([In(), MarshalAs(UnmanagedType.LPWStr)]
string pszStatusText);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int EnableModeless([In(), MarshalAs(UnmanagedType.Bool)]
bool fEnable);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int TranslateAccelerator([In(), MarshalAs(UnmanagedType.Struct)]
ref Native.tagMSG lpmsg, [In(), MarshalAs(UnmanagedType.U2)]
short wID);
    }
}
