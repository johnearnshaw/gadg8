﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.WebBrowserControl
{
    [ComImport(), Guid("00000118-0000-0000-C000-000000000046"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IOleClientSite
    {

        int SaveObject();
        int GetMoniker(int dwAssign, int dwWhichMoniker, ref object ppmk);
        int GetContainer(ref object ppContainer);
        int ShowObject();
        int OnShowWindow(bool fShow);
        int RequestNewObjectLayout();
    }
}
