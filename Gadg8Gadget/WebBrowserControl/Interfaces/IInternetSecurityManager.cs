﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace Gadg8Gadget.WebBrowserControl
{
    [ComVisible(true), ComImport(), GuidAttribute("79EAC9EE-BAF9-11CE-8C82-00AA004BA90B"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IInternetSecurityManager
    {
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int SetSecuritySite([In()]
        IntPtr pSite);

        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetSecuritySite(ref IntPtr pSite);

        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int MapUrlToZone([In(), MarshalAs(UnmanagedType.LPWStr)]
        string pwszUrl, ref UInt32 pdwZone, [In()]
        UInt32 dwFlags);

        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetSecurityId([In(), MarshalAs(UnmanagedType.LPWStr)]
        string pwszUrl, [Out()]
        IntPtr pbSecurityId, [In(), Out()]
        ref UInt32 pcbSecurityId, [In()]
        ref UInt32 dwReserved);

        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int ProcessUrlAction([In(), MarshalAs(UnmanagedType.LPWStr)]
        string pwszUrl, UInt32 dwAction, IntPtr pPolicy, UInt32 cbPolicy, IntPtr pContext, UInt32 cbContext, UInt32 dwFlags, UInt32 dwReserved);

        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int QueryCustomPolicy([In(), MarshalAs(UnmanagedType.LPWStr)]
        string pwszUrl, ref Guid guidKey, ref IntPtr ppPolicy, ref UInt32 pcbPolicy, IntPtr pContext, UInt32 cbContext, UInt32 dwReserved);

        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int SetZoneMapping(UInt32 dwZone, [In(), MarshalAs(UnmanagedType.LPWStr)]
        string lpszPattern, UInt32 dwFlags);

        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetZoneMappings([In()]
        UInt32 dwZone, ref IEnumString ppenumString, [In()]
        UInt32 dwFlags);
    }
}