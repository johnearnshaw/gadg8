﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.WebBrowserControl
{
    [ComImport()]
    [Guid("cb728b20-f786-11ce-92ad-00aa00a74cd0")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IProfferService
    {
        void ProfferService(ref Guid guidService, IServiceProvider psp, ref uint cookie);
        void RevokeService(uint cookie);
    }
}
