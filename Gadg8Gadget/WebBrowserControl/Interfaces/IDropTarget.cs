﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Gadg8Gadget.WebBrowserControl
{
    public interface IDropTarget
    {
        void OnDragDrop(DragEventArgs e);
        void OnDragEnter(DragEventArgs e);
        void OnDragLeave(EventArgs e);
        void OnDragOver(DragEventArgs e);
    }
}
