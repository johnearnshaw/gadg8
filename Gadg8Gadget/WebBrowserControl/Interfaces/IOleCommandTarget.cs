﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.WebBrowserControl
{
    [ComImport(), ComVisible(true), Guid("B722BCCB-4E68-101B-A2BC-00AA00404770"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IOleCommandTarget
    {

        //This parameter must be IntPtr, as it can be null
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int QueryStatus([In()]
IntPtr pguidCmdGroup, [In(), MarshalAs(UnmanagedType.U4)]
uint cCmds, [In(), Out(), MarshalAs(UnmanagedType.Struct)]
ref Native.tagOLECMD prgCmds, [In(), Out()]
IntPtr pCmdText);

        //[In] ref Guid pguidCmdGroup,
        //have to be IntPtr, since null values are unacceptable
        //and null is used as default group!
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int Exec([In()]
IntPtr pguidCmdGroup, [In(), MarshalAs(UnmanagedType.U4)]
uint nCmdID, [In(), MarshalAs(UnmanagedType.U4)]
uint nCmdexecopt, [In()]
IntPtr pvaIn, [In(), Out()]
IntPtr pvaOut);
    }
}
