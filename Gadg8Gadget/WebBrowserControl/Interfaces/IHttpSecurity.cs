﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.WebBrowserControl
{
    [ComImport(), ComVisible(true), Guid("79eac9d7-bafa-11ce-8c82-00aa004ba90b"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IHttpSecurity
    {
        //IWindowForBindingUI
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetWindow([In()]
ref Guid rguidReason, [In(), Out()]
ref IntPtr phwnd);

        //IHttpSecurity
        //http://msdn.microsoft.com/library/default.asp?url=/workshop/networking/moniker/reference/ifaces/ihttpsecurity/onsecurityproblem.asp?frame=true
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int OnSecurityProblem([In(), MarshalAs(UnmanagedType.U4)]
uint dwProblem);
        //dwProblem one of wininet error Messages
        //http://msdn.microsoft.com/library/default.asp?url=/library/en-us/wininet/wininet/wininet_errors.asp?frame=true
    }
}
