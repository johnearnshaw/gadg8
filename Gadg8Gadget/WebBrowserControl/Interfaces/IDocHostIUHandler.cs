﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using mshtml;

namespace Gadg8Gadget.WebBrowserControl
{
    [ComVisible(true), ComImport()]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [GuidAttribute("bd3f23c0-d43e-11cf-893b-00aa00bdce1a")]
    public interface IDocHostUIHandler
    {
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int ShowContextMenu([In(), MarshalAs(UnmanagedType.U4)]
uint dwID, [In(), MarshalAs(UnmanagedType.Struct)]
ref tagPOINT pt, [In(), MarshalAs(UnmanagedType.IUnknown)]
object pcmdtReserved, [In(), MarshalAs(UnmanagedType.IDispatch)]
object pdispReserved);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetHostInfo([In(), Out(), MarshalAs(UnmanagedType.Struct)]
ref Native.DOCHOSTUIINFO info);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int ShowUI([In(), MarshalAs(UnmanagedType.I4)]
int dwID, [In(), MarshalAs(UnmanagedType.Interface)]
IOleInPlaceActiveObject activeObject, [In(), MarshalAs(UnmanagedType.Interface)]
IOleCommandTarget commandTarget, [In(), MarshalAs(UnmanagedType.Interface)]
IOleInPlaceFrame frame, [In(), MarshalAs(UnmanagedType.Interface)]
IOleInPlaceUIWindow doc);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int HideUI();
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int UpdateUI();
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int EnableModeless([In(), MarshalAs(UnmanagedType.Bool)]
bool fEnable);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int OnDocWindowActivate([In(), MarshalAs(UnmanagedType.Bool)]
bool fActivate);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int OnFrameWindowActivate([In(), MarshalAs(UnmanagedType.Bool)]
bool fActivate);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int ResizeBorder([In(), MarshalAs(UnmanagedType.Struct)]
ref tagRECT rect, [In(), MarshalAs(UnmanagedType.Interface)]
IOleInPlaceUIWindow doc, [In(), MarshalAs(UnmanagedType.Bool)]
bool fFrameWindow);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int TranslateAccelerator([In(), MarshalAs(UnmanagedType.Struct)]
ref Native.tagMSG msg, [In()]
ref Guid @group, [In(), MarshalAs(UnmanagedType.U4)]
uint nCmdID);
        //out IntPtr pbstrKey,
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetOptionKeyPath([In(), Out(), MarshalAs(UnmanagedType.LPWStr)]
ref String pbstrKey, [In(), MarshalAs(UnmanagedType.U4)]
uint dw);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetDropTarget([In(), MarshalAs(UnmanagedType.Interface)]
IDropTarget pDropTarget, [In(), Out(), MarshalAs(UnmanagedType.Interface)]
ref IDropTarget ppDropTarget);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int GetExternal([In(), Out(), MarshalAs(UnmanagedType.IDispatch)]
ref object ppDispatch);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int TranslateUrl([In(), MarshalAs(UnmanagedType.U4)]
uint dwTranslate, [In(), MarshalAs(UnmanagedType.LPWStr)]
string strURLIn, [In(), Out(), MarshalAs(UnmanagedType.LPWStr)]
ref string pstrURLOut);
        [PreserveSig()]
        [return: MarshalAs(UnmanagedType.I4)]
        int FilterDataObject([In(), MarshalAs(UnmanagedType.Interface)]
System.Runtime.InteropServices.ComTypes.IDataObject pDO, [In(), Out(), MarshalAs(UnmanagedType.Interface)]
ref System.Runtime.InteropServices.ComTypes.IDataObject ppDORet);
    }
}
