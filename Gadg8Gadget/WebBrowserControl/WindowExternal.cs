﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using mshtml;
using System.Diagnostics;
using System.Security.Permissions;

namespace Gadg8Gadget.WebBrowserControl
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    [ComVisible(true)]
    public class WindowExternal :MarshalByRefObject
    {
        public override object InitializeLifetimeService()
        {
            return null;
        }

        private WebBrowser _WebBrowser;
        private Gadg8Gadget.GadgetSystemNamespace.SystemNamespace _SystemNamespace;
        private Gadg8Gadget.GadgetGadg8Namespace.Gadg8Namespace _Gadg8Namespace;
        
        public WindowExternal(WebBrowser browser, GadgetSystemNamespace.SystemNamespace systemNamespace,
            GadgetGadg8Namespace.Gadg8Namespace gadg8Namespace)
        {
            _WebBrowser = browser;
            _SystemNamespace = systemNamespace;
            _Gadg8Namespace = gadg8Namespace;
        }

        public object showModalDialog(string dialog, object varArgIn = null, object varOptions = null)
        {
            // this preserves the default behavior.
            IHTMLDocument2 doc2 = (IHTMLDocument2)_WebBrowser.Document.DomDocument;
            object options = varOptions;
            return doc2.parentWindow.showModalDialog(dialog, varArgIn, options);
        }

        public object showModelessDialog(string dialog, object varArgIn = null, object varOptions = null)
        {
            // this preserves the default behavior.
            IHTMLDocument2 doc2 = (IHTMLDocument2)_WebBrowser.Document.DomDocument;
            object options = varOptions;
            return ((IHTMLWindow3)doc2.parentWindow).showModelessDialog(dialog, varArgIn, options);
        }

        public object SystemNamespace
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            get
            {
                return _SystemNamespace;
            }
        }

        public object Gadg8Namespace
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            get
            {
                return _Gadg8Namespace;
            }
        }

        public object ActiveX(string progid)
        {
            object o = ActiveXObject.GetActiveXObject(progid);
            return o;
        }

        public void SaveSettingsToDisk()
        {
            _SystemNamespace.GadgetWindow.SaveSettings();
        }
    }
}
