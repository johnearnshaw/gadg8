﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using mshtml;
using System.Drawing;
using System.Drawing.Imaging;
using System.Security.Permissions;

namespace Gadg8Gadget.Behaviors
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class GBackground : GObject, mshtml.IElementBehavior, mshtml.IElementBehaviorFactory, IHTMLPainter2 // Had to declare own interface for IHTMLPainter for element.addBehavior compatibility.
    {

        xGadgetWindow ParentWindow;

        public bool Enabled;

        public List<GObject> AddGObjectList;

        
        private IHTMLElement Element;
        private IHTMLElement2 Element2;

        public GBackground(xGadgetWindow parentWindow)
        {
            this.ParentWindow = parentWindow;
            this.ParentWindow.GBackground = this;
        }

        #region IHTMLPainter Members

        public void Draw(tagRECT bounds, tagRECT update, int lDrawFlags, IntPtr hdc, IntPtr pvDrawObject)
        {
            DrawBackground(hdc);
        }

        public void onresize(tagSIZE tagSize)
        {
            //this.ParentWindow.Width = tagSize.cx;
            //this.ParentWindow.Height = tagSize.cy;
        }

        public void GetPainterInfo(out _HTML_PAINTER_INFO pInfo)
        {
            pInfo.lFlags = 0x1; // Opaque
            pInfo.lZOrder = 0x3; // Replace Background
            pInfo.iidDrawObject = Guid.Empty;
            pInfo.rcExpand = new tagRECT() { bottom = 0, left = 0, right = 0, top = 0 };
        }

        public void HitTestPoint(tagPOINT tagPoint, out int ptx, out int pty)
        {
            ptx = 0;
            pty = 0;
        }
        #endregion

        private void DrawBackground(IntPtr hdc)
        {
            Bitmap bmp;

            if (this.Enabled)
            {
                bmp = new Bitmap(ActualWidth, ActualHeight);
                ColorMatrix cm;
                ImageAttributes ia;

                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.Clear(Color.Transparent);

                    if (this.image != null)
                    {
                        g.DrawImage(this.image, new Rectangle(this.x, this.y, this.ActualWidth, this.ActualHeight), 0, 0, this.image.Width, this.image.Height, GraphicsUnit.Pixel); //, ia);
                    }

                    if (this.AddGObjectList != null)
                    {
                        foreach (GObject obj in this.AddGObjectList)
                        {
                            int offsetX;
                            int offsetY;

                            
                            float value = (float)obj.brightness / 100.0f;

                            cm = new ColorMatrix(new float[][]{
                                    new float[] {1, 0, 0, 0, 0},
                                    new float[] {0, 1, 0, 0, 0},
                                    new float[] {0, 0, 1, 0, 0},
                                    new float[] {0, 0, 0, 1, 0},
                                    new float[] {value, value, value, 0, 0}
                                    });
                            
                            cm.Matrix33 = (float)obj.opacity / 100.0f;
                            
                            ia = new ImageAttributes();
                            ia.SetColorMatrix(cm, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

                            if (!(obj is GText))
                            {
                                offsetX = (obj.image.Width - obj.ActualWidth) / 2;
                                offsetY = (obj.image.Height - obj.ActualHeight) / 2;

                                if (obj.rotation == 0)
                                {
                                    g.DrawImage(obj.image, new Rectangle(obj.x + offsetX, obj.y + offsetY, obj.ActualWidth, obj.ActualHeight),
                                        0, 0, obj.image.Width, obj.image.Height, GraphicsUnit.Pixel, ia);
                                }
                                else
                                {
                                    Bitmap rotatedImage = obj.RotateImage(obj.image, obj.rotation);
                                    g.DrawImage(rotatedImage, new Rectangle(obj.x - ((rotatedImage.Width - obj.ActualWidth) / 2) + offsetX, obj.y - ((rotatedImage.Height - ActualHeight) / 2) + offsetY, /*width*/ rotatedImage.Width, /*height*/ rotatedImage.Height),
                                    0, 0, rotatedImage.Width, rotatedImage.Height, GraphicsUnit.Pixel, ia);
                                }

                            }
                            else
                            {
                                offsetX = (obj.image.Width - obj.ActualWidth) / 2;
                                offsetY = (obj.image.Height - obj.ActualHeight) / 2;

                                g.DrawImage(obj.image, new Rectangle(0 - offsetX, 0 - offsetY, obj.image.Width, obj.image.Height), //obj.ActualWidth, obj.ActualHeight),
                                    0, 0, obj.image.Width, obj.image.Height, GraphicsUnit.Pixel, ia);
                            }
                        }
                    }
                }

                if (bmp == null)
                    return;

                if (this.opacity < 100 || this.brightness != 0)
                {
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        Bitmap temp = (Bitmap)bmp.Clone();

                        g.Clear(Color.Transparent);

                        float value = (float)this.brightness / 100.0f;

                        cm = new ColorMatrix(new float[][]{
                                new float[] {1, 0, 0, 0, 0},
                                new float[] {0, 1, 0, 0, 0},
                                new float[] {0, 0, 1, 0, 0},
                                new float[] {0, 0, 0, 1, 0},
                                new float[] {value, value, value, 0, 0}
                                });
                        
                        cm.Matrix33 = (float)this.opacity / 100.0f;
                        
                        ia = new ImageAttributes();
                        ia.SetColorMatrix(cm, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

                        g.DrawImage(temp, new Rectangle(0, 0, temp.Width, temp.Height), 0, 0, this.image.Width, this.image.Height, GraphicsUnit.Pixel, ia);
                        temp.Dispose();
                    }
                }

                // Strip alpha pixels from body bitmap to a new bitmap which we set as AlphaFrame background image.
                Bitmap bmp2 = ImageFunctions.ImageFunctions.StripAlphaToNewBitmap(bmp);

                using (Graphics g = Graphics.FromHdc(hdc))
                {
                    g.PageUnit = GraphicsUnit.Pixel;
                    g.Clear(Color.Magenta);
                    g.DrawImage(bmp, 0, 0);
                }

                ParentWindow.AlphaFrameController.SetBitmap(bmp2);
            }
        }

        #region IElementBehaviorFactory Members

        public mshtml.IElementBehavior FindBehavior(string bstrBehavior, string bstrBehaviorUrl, mshtml.IElementBehaviorSite pSite)
        {
            return this;
        }

        #endregion

        #region IElementBehavior Members

        public void Notify(int lEvent, ref object pVar)
        {
            return;
            // TODO: Add Behavior.Notify implementation
        }

        public void Init(mshtml.IElementBehaviorSite pBehaviorSite)
        {
            IntPtr ppvObject = IntPtr.Zero;
            Guid riid = typeof(IHTMLPaintSite).GUID;
            IntPtr unk = Marshal.GetIUnknownForObject(pBehaviorSite);
            Marshal.QueryInterface(unk, ref riid, out ppvObject);

            if (ppvObject != IntPtr.Zero)
                PaintSite = (IHTMLPaintSite2)Marshal.GetObjectForIUnknown(ppvObject);

            if (this.AddGObjectList == null)
                this.AddGObjectList = new List<GObject>();

            this.Enabled = true;
            
            // Keep reference to the html element for future use.
            Element = pBehaviorSite.GetElement();
            Element2 = (IHTMLElement2)Element;

            WebBrowserControl.HtmlEvent.Create("onpropertychange", Element2, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(PropertyChangedEventHandler));

            this.width = Helpers.GetHTMLAttribute<string>(Element, "style.width", "");
            this.height = Helpers.GetHTMLAttribute<string>(Element, "style.height", "");
            this.src = Helpers.GetHTMLAttribute<string>(Element, "src", "");

            this.top = Helpers.GetHTMLAttribute<string>(Element, "style.top", "0").Replace("%", "").Replace("px", "");
            this.left = Helpers.GetHTMLAttribute<string>(Element, "style.left", "0").Replace("%", "").Replace("px", "");
            this.opacity = int.Parse(Helpers.GetHTMLAttribute<string>(Element, "opacity", "100").Replace("%", ""));
            this.brightness = int.Parse(Helpers.GetHTMLAttribute<string>(Element, "brightness", "0").Replace("%", ""));

            Update();
        }
        
        public void Detach()
        {
            this.Enabled = false;
            ParentWindow.GBackground = null;
            return;
        }

        #endregion

        #region GBackground Members

        // Gets or sets the image file used by the g:background element.
        private string _src;
        public string src
        {
            get
            {
                return _src;
            }
            set
            {
                _src = value;

                // Stupid bug in MS's Calculator gadget and other gadgets where they set style width and height to 0 before
                // setting g:background src property means we need to clear the values of width and height when src is set,
                // duh! took me ages to find this one :P
                IHTMLElement parent = (IHTMLElement)this.Element.parentElement;
                if (string.IsNullOrEmpty(this.width) || this.width == "0" || this.width.StartsWith("0"))
                {
                    this.Element2.runtimeStyle.width = parent.offsetWidth;
                    //this.width = parent.clientWidth.ToString();
                }
                if (string.IsNullOrEmpty(this.height) || this.height == "0" || this.height.StartsWith("0"))
                {
                    this.Element2.runtimeStyle.height = parent.offsetHeight;
                    //this.height = parent.clientHeight.ToString();
                }

                Update();
            }
        }

        // Gets or sets the amount of edge softening that is applied to the g:background element.
        private int _softEdge;
        public int softEdge
        {
            get
            {
                return _softEdge;
            }
            set
            {
                _softEdge = value;
            }
        }

        public void removeObjects()
        {
            if (this.AddGObjectList != null)
                this.AddGObjectList.Clear();
        }

        public GImage addImageObject(string src, int x, int y)
        {
            if (this.AddGObjectList == null)
                this.AddGObjectList = new List<GObject>();
            
            GImage img = new GImage(this.ParentWindow);
            img.x = x;
            img.y = y;
            img.src = src;
            
            this.AddGObjectList.Add(img);

            Redraw();
            return img;
        }

        public GText addTextObject(string text, string font, int fontSize,
            string color, int xOffset, int yOffset)
        {
            if (this.AddGObjectList == null)
                this.AddGObjectList = new List<GObject>();

            GText txt = new GText(this.ParentWindow);
            txt.font = font;
            txt.fontsize = fontSize;
            txt.color = color;
            txt.x = xOffset;
            txt.y = yOffset;
            txt.value = text;

            this.AddGObjectList.Add(txt);

            Redraw();
            return txt;
        }

        [ComVisible(false)]
        public override void Redraw()
        {
            if (this.image != null)
            {
                //if (PaintSite != null)
                //{
                //    var rect = new tagRECT();
                //    rect.top = 0;
                //    rect.bottom = top + height;
                //    rect.left = 0;
                //    rect.right = left + width;
                //    PaintSite.InvalidateRect(rect);
                //    return;
                //}

                //int offsetX = (this.image.Width - this.width) / 2;
                //int offsetY = (this.image.Height - this.height) / 2;
                this.ParentWindow.InvalidateWebBrowser(new Rectangle(this.x, this.y, this.ActualWidth, this.ActualHeight));
            }
        }

        [ComVisible(false)]
        public override void Update()
        {
            if (image != null)
                image.Dispose();

            if (!string.IsNullOrEmpty(src))
            {
                string s = Helpers.GetAbsolutePath(ParentWindow.Path, src, true);

                if (System.IO.File.Exists(s))
                {
                    image = (Bitmap)Bitmap.FromFile(s);
                }
                else
                {
                    this.image = new Bitmap(100, 100, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                    using (Graphics g = Graphics.FromImage(this.image))
                    {
                        g.Clear(Color.Black);
                    }
                }
            }
            else
            {
                this.image = new Bitmap(100, 100, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                using (Graphics g = Graphics.FromImage(this.image))
                {
                    g.Clear(Color.Black);
                }
            }

            //if (brightness != 0)
            //    ImageFunctions.ImageFunctions.Contrast(image, brightness);

            if (glow != null)
                glow.Update();

            if (shadow != null)
                shadow.Update();

            CalculateDimensions();
        }

        public override void CalculateDimensions()
        {
            int w, h;

            IHTMLElement parent = (IHTMLElement)this.Element.parentElement;

            this.ActualWidth = parent.offsetWidth;
            this.ActualHeight = parent.offsetHeight;

            if (!string.IsNullOrEmpty(this.width))
            {
                if (this.width.Contains("%"))
                {
                    if (int.TryParse(this.width.Replace("%", ""), out w))
                    {
                        this.ActualWidth = (parent.offsetWidth / 100) * w;
                    }
                }
                else
                {
                    if (int.TryParse(this.width.Replace("px", ""), out w))
                    {
                        this.ActualWidth = w;
                    }
                }
            }

            if (!string.IsNullOrEmpty(this.height))
            {
                if (this.height.Contains("%"))
                {
                    if (int.TryParse(this.height.Replace("%", ""), out h))
                    {
                        this.ActualHeight = (parent.offsetHeight / 100) * h;
                    }
                }
                else
                {
                    if (int.TryParse(this.height.Replace("px", ""), out h))
                    {
                        this.ActualHeight = h;
                    }
                }
            }

            Redraw();
        }

        internal void SetBackgroundImage(string src)
        {
            this.src = src;
            Update();
        }
        #endregion

        private void PropertyChangedEventHandler(object sender, IHTMLEventObj e)
        {
            IHTMLEventObj2 ev2 = (IHTMLEventObj2)e;
            IHTMLElement2 el = (IHTMLElement2)e.srcElement;

            string p = ev2.propertyName.ToLower().Replace("style.", "");

            switch (p)
            {
                case "src":
                    this.src = Helpers.GetHTMLAttribute<string>(Element, "src");
                    break;

                case "opacity":
                    this.opacity = int.Parse(Helpers.GetHTMLAttribute<string>(Element, "opacity", "100").Replace("%", ""));
                    break;

                case "top":
                    this.top = Helpers.GetHTMLAttribute<string>(Element, "style.top", "0").Replace("%", "").Replace("px", "");
                    break;

                case "left":
                    this.left = Helpers.GetHTMLAttribute<string>(Element, "style.left", "0").Replace("%", "").Replace("px", "");
                    break;

                case "width":
                    this.width = Helpers.GetHTMLAttribute<string>(Element, "style.width", "");
                    break;

                case "height":
                    this.height = Helpers.GetHTMLAttribute<string>(Element, "style.height", "");
                    break;

                case "brightness":
                    this.brightness = int.Parse(Helpers.GetHTMLAttribute<string>(Element, "brightness", "0").Replace("%", ""));
                    break;
            }
            
            Update();
        }
    }
}
