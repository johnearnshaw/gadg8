﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using mshtml;
using System.Security.Permissions;
using System.Windows.Forms;

namespace Gadg8Gadget.Behaviors
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class BodyBehavior : Behaviors.IElementBehaviorFactory, Behaviors.IElementBehavior, IHTMLPainter2, mshtml.IHTMLNamespace
    {
        xGadgetWindow ParentWindow;
        Control ParentControl;
        Boolean IsSettings = false;

        private IHTMLPaintSite2 PaintSite { get; set; }
        private mshtml.IHTMLElement Element;
        private IHTMLElement2 Element2;
        private Bitmap BackgroundImage;

        public BodyBehavior(xGadgetWindow parentWindow)
        {
            this.ParentWindow = parentWindow;
            ParentControl = (Control)parentWindow.WebBrowser;

            if (parentWindow is GadgetSettings)
                IsSettings = true;
        }

        #region "IElementBehaviorFactory Members"

        public IElementBehavior FindBehavior(string bstrBehavior, string bstrBehaviorUrl, IElementBehaviorSite pSite)
        {
            return this;
        }

        #endregion

        #region "IElementBehavior Members"

        public void Notify(int lEvent, IntPtr pVar)
        {
            return;
            // TODO: Add Behavior.Notify implementation
        }

        public void Init(IElementBehaviorSite pBehaviorSite)
        {
            IntPtr ppvObject = IntPtr.Zero;
            Guid riid = typeof(IHTMLPaintSite).GUID;
            IntPtr unk = Marshal.GetIUnknownForObject(pBehaviorSite);
            Marshal.QueryInterface(unk, ref riid, out ppvObject);

            if (ppvObject != IntPtr.Zero)
                PaintSite = (IHTMLPaintSite2)Marshal.GetObjectForIUnknown(ppvObject);

            // Keep reference to the html element for future use.
            Element = pBehaviorSite.GetElement();
            Element2 = (IHTMLElement2)Element;

            WebBrowserControl.HtmlEvent.Create("onpropertychange", Element2, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(PropertyChangedEventHandler));

            string s = Element2.currentStyle.backgroundImage;

            this.SetBackgroundImage(s);
            this.SetBackgroundColor();

            string strw = Element2.currentStyle.width.ToString();
            string strh = Element2.currentStyle.height.ToString();

            int w = 0;
            int h = 0;

            int.TryParse(strw.Substring(0, strw.Length - 2), out w);
            int.TryParse(strh.Substring(0, strh.Length - 2), out h);

            if (w == 0)
                w = 130;
            if (h == 0)
                h = 60;

            Resize(w, h);
        }

        public void Detach()
        {
            return;
            // TODO: Add Behavior.Detach implementation
        }

        #endregion

        #region IHTMLPainter Members

        public void Draw(tagRECT bounds, tagRECT update, int lDrawFlags, IntPtr hdc, IntPtr pvDrawObject)
        {
            DrawBackground(hdc);
        }

        public void onresize(tagSIZE tagSize)
        {
            //if (this.height > 0 && this.width > 0)
            //    Resize(tagSize.cx, tagSize.cy);

            string strw = Element2.currentStyle.width.ToString();
            string strh = Element2.currentStyle.height.ToString();

            int w = 0;
            int h = 0;

            int.TryParse(strw.Substring(0, strw.Length - 2), out w);
            int.TryParse(strh.Substring(0, strh.Length - 2), out h);

            Resize(w, h);
        }

        public void GetPainterInfo(out _HTML_PAINTER_INFO pInfo)
        {
            pInfo.lFlags = 0x1; // Opaque
            pInfo.lZOrder = 0x3; // Replace Background
            pInfo.iidDrawObject = Guid.Empty;
            pInfo.rcExpand = new tagRECT() { bottom = 0, left = 0, right = 0, top = 0 };
        }

        public void HitTestPoint(tagPOINT tagPoint, out int ptx, out int pty)
        {
            ptx = 0;
            pty = 0;
        }
        #endregion

        private void DrawBackground(IntPtr hdc)
        {
            if (ParentWindow != null)
            {
                if (this.BackgroundImage == null)
                    return;

                Bitmap bmp = new Bitmap(width, height);

                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.Clear(Color.Transparent);
                    g.DrawImage(this.BackgroundImage, new Rectangle(0, 0, this.width, this.height), 0, 0, this.BackgroundImage.Width, this.BackgroundImage.Height, GraphicsUnit.Pixel);
                }

                // Strip alpha pixels from body bitmap to a new bitmap which we set as AlphaFrame background image.
                Bitmap bmp2 = ImageFunctions.ImageFunctions.StripAlphaToNewBitmap(bmp);

                using (Graphics g = Graphics.FromHdc(hdc))
                {
                    g.PageUnit = GraphicsUnit.Pixel;
                    g.Clear(Color.Magenta);
                    g.DrawImage(bmp, 0, 0);
                }

                if (ParentWindow.GBackground == null || !ParentWindow.GBackground.Enabled)
                    ParentWindow.AlphaFrameController.SetBitmap(bmp2);
            }
        }

        #region "IHTMLNamespace Members"

        public bool attachEvent(string @event, object pdisp)
        {
            return false;
        }


        public void detachEvent(string @event, object pdisp)
        {
        }


        public void doImport(string bstrImplementationUrl)
        {
        }

        public string name
        {
            get { return "bodybehavior"; }
        }

        public object onreadystatechange { get; set; }

        public object readyState { get; set; }

        public object tagNames { get; set; }

        public string urn { get; set; }
        #endregion

        #region custom elements

        int width;
        int height;

        private void PropertyChangedEventHandler(object sender, IHTMLEventObj e)
        {
            IHTMLEventObj2 ev2 = (IHTMLEventObj2)e;
            IHTMLElement2 el = (IHTMLElement2)e.srcElement;

            string p = ev2.propertyName.ToLower().Replace("style.", "");

            if (p == "width" || p == "height" || p == "classname")
            {
                string strw = el.currentStyle.width.ToString();
                string strh = el.currentStyle.height.ToString();

                int w;
                int h;

                int.TryParse(strw.Substring(0, strw.Length - 2), out w);
                int.TryParse(strh.Substring(0, strh.Length - 2), out h);
 
                Resize(w, h);
            }
            else if (p == "backgroundimage" || p == "background" || p == "backgroundcolor")
            {
                string s = el.currentStyle.backgroundImage;

                this.SetBackgroundImage(s);
                this.SetBackgroundColor();
            }
            return;
        }

        private void SetBackgroundColor()
        {
            string col = "#ff00ff"; // Magenta

            // If the body  behavior is attached to gadget settings WebBrowser control, we want to set the background
            // color to WinForms control to match the settings dialog background;
            if (IsSettings)
            {
                col = ColorTranslator.ToHtml(SystemColors.Control);
            }

            if (!string.IsNullOrEmpty((string)Element2.currentStyle.backgroundImage) ||
                (string)Element2.currentStyle.backgroundColor == "transparent")
            {
                Element2.runtimeStyle.backgroundColor = col;
            }
            
        }

        public void SetBackgroundImage(string src)
        {
            src = src.ToLower();

            if (this.BackgroundImage != null)
                this.BackgroundImage.Dispose();

            if (!string.IsNullOrEmpty(src) && src != "none")
            {
                string s = Helpers.GetAbsolutePath(ParentWindow.Path, src, true);

                if (System.IO.File.Exists(s))
                {
                    this.BackgroundImage = (Bitmap)Bitmap.FromFile(s);
                }
                else
                {
                    this.BackgroundImage = new Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                    using (Graphics g = Graphics.FromImage(this.BackgroundImage))
                    {
                        g.Clear(Color.Transparent);
                    }
                }
            }

            Redraw(new tagRECT() { top = 0, left = 0, bottom = ParentWindow.Height, right = ParentWindow.Width });
        }

        public void Resize(int width, int height)
        {
            if (width < 20)
                width = 20;
            if (height < 20)
                height = 20;

            ParentControl.Width = this.width = width;
            ParentControl.Height = this.height = height;

            if (this.ParentWindow.GBackground != null)
                this.ParentWindow.GBackground.CalculateDimensions();

            if (this.ParentWindow.GObjectList != null)
                foreach (Behaviors.GObject g in this.ParentWindow.GObjectList)
                {
                    g.CalculateDimensions();
                }
            return;
        }
        #endregion

        public void Redraw(tagRECT rect)
        {
            //if (PaintSite != null)
            //{
            //    PaintSite.InvalidateRect(rect);
            //}
            //else
            //{
            this.ParentWindow.InvalidateWebBrowser(rect);
                //this.ParentWindow.WebBrowser.Invalidate(new Rectangle(rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top), true);
            //}
        }
    }
}
