﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Security.Permissions;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.Behaviors
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public abstract class GObject
    {
        public IHTMLPaintSite2 PaintSite { get; set; }

        #region Glow class
        [ComVisible(false)]
        public class Glow
        {
            public int Alpha;
            public string Color;
            public int Radius;

            public Bitmap image;
            public Color color;

            public void Update()
            {
                if (image != null)
                    image.Dispose();
            }
        }
        #endregion

        #region Shadow class
        [ComVisible(false)]
        public class Shadow
        {
            public int Alpha;
            public string Color;
            public int DeltaX;
            public int DeltaY;
            public int Radius;

            public Bitmap image;
            public Color color;

            public void Update()
            {
                if (image != null)
                    image.Dispose();
            }
        }
        #endregion

        public GObject()
        {
        }

        abstract public void Update();
        abstract public void Redraw();
        abstract public void CalculateDimensions();

        [ComVisible(false)]
        public Glow glow;
        [ComVisible(false)]
        public Shadow shadow;

        //[ComVisible(false)]
        public Bitmap image;

        private int _blur;
        public int blur // Gets or sets the level of blur applied to the g element.
        {
            get
            {
                return _blur;
            }
            set
            {
                if (_blur != value)
                {
                    _blur = value;

                    Update();
                }
            }
        }

        private int _brightness = 0;
        public int brightness // Gets or sets the brightness of the g element.
        {
            get
            {
                return _brightness;
            }
            set
            {
                if (_brightness != value)
                {
                    _brightness = value;

                    Redraw();
                }
            }
        }

        private int _ActualHeight;
        [ComVisible(false)]
        public int ActualHeight
        {
            get
            {
                return _ActualHeight;
            }
            set
            {
                _ActualHeight = value;
            }
        }

        private string _height;
        public string height  // Gets or sets the height of the g element.
        {
            get
            {
                if (string.IsNullOrEmpty(_height))
                    return ActualHeight.ToString();
                return _height;
            }
            set
            {
                _height = value;
                CalculateDimensions();
            }
        }

        public int Height
        {
            get { return ActualHeight; }
            set { height = value.ToString(); }
        }

        public string left
        {
            get { return y.ToString(); }
            set
            {
                int i = 0;
                int.TryParse(value, out i);
                y = i;
            }
        } // Gets or sets the number of pixels from the left edge of the gadget to position the g element.
        
        public string Left
        {
            get { return y.ToString(); }
            set
            {
                int i = 0;
                int.TryParse(value, out i);
                y = i;
            }
        }

        private int _opacity = 100;
        public int opacity
        {
            get
            {
                return _opacity;
            }
            set
            {
                _opacity = value;
                Redraw();
            }
        } // Gets or sets the opacity of the g element.

        private float _rotation;
        public float rotation
        {
            get
            {
                return _rotation;
            }
            set
            {
                _rotation = value;
                Redraw();
            }
        } // Gets or sets the degree of rotation applied to the g element.

        public string top
        {
            get { return x.ToString(); }
            set {
                int i = 0;
                int.TryParse(value, out i);
                x = i;
            }
        } // Gets or sets the number of pixels from the top edge of the gadget to position the g element.

        public string Top
        {
            get { return x.ToString(); }
            set
            {
                int i = 0;
                int.TryParse(value, out i);
                x = i;
            }
        }

        
        private int _ActualWidth;

        [ComVisible(false)]
        public int ActualWidth
        {
            get
            {
                return _ActualWidth;
            }
            set
            {
                _ActualWidth = value;
            }
        }

        private string _width = "";
        public string width  // Gets or sets the width of the g element.
        {
            get
            {
                if (string.IsNullOrEmpty(_width))
                    return ActualWidth.ToString();

                return _width;
            }
            set
            {
                _width = value;
                CalculateDimensions();
            }
        }

        public int Width
        {
            get { return ActualWidth; }
            set { width = value.ToString(); }
        }

        private int _x;
        public int x
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value;
                Redraw();
            }
        }

        public int X { get { return x; } set { x = value; } }

        private int _y;
        public int y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value;
                Redraw();
            }
        }

        public int Y { get { return y; } set { y = value; } }

        public int addGlow(string color, int radius, int alpha)
        {
            if (radius == 0 || alpha == 0)
            {
                if (glow != null)
                {
                    if (glow.image != null)
                        glow.image.Dispose();
                    glow = null;
                }
            }
            else
            {
                if (glow == null)
                    glow = new Glow();

                glow.Alpha = alpha;
                glow.Color = color;
                glow.Radius = radius;

                glow.color = Helpers.ParseColor(color);
                glow.Update();
            }
            return WebBrowserControl.Native.S_OK;
        }

        public int addShadow(string color, int radius, int alpha, int deltaX, int deltaY)
        {
            if (radius == 0 || alpha == 0)
            {
                if (shadow != null)
                {
                    if (shadow.image != null)
                        shadow.image.Dispose();
                    //shadow = null;
                }
            }
            else
            {
                if (shadow == null)
                    shadow = new Shadow();

                shadow.Alpha = alpha;
                shadow.Color = color;
                shadow.Radius = radius;
                shadow.DeltaX = deltaX;
                shadow.DeltaY = deltaY;

                shadow.color = Helpers.ParseColor(color);
                shadow.Update();
            }

            return WebBrowserControl.Native.S_OK;
        }

        [ComVisible(false)]
        public Bitmap RotateImage(Image img, float rotationAngle)
        {
            Bitmap bmp;
            if (img.Width > img.Height)
                bmp = new Bitmap(img.Width, img.Width);
            else
                bmp = new Bitmap(img.Height, img.Height);

            Graphics g = Graphics.FromImage(bmp);

            g.TranslateTransform((float)bmp.Width / 2, (float)bmp.Height / 2);
            g.RotateTransform(rotationAngle);
            g.TranslateTransform(-(float)bmp.Width / 2, -(float)bmp.Height / 2);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            g.DrawImage(img, (float)bmp.Width / 2 - (float)img.Width / 2, (float)bmp.Height / 2 - (float)img.Height / 2, (float)img.Width, (float)img.Height);
            g.Dispose();

            return bmp;
        }
    }
}