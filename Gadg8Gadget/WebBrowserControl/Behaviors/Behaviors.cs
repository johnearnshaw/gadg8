﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Runtime.InteropServices;
using mshtml;
using System.Drawing;

namespace Gadg8Gadget.Behaviors
{
    [ComVisible(true), Guid("3050F6A6-98B5-11CF-BB82-00AA00BDCE0B"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IHTMLPainter
    {
        void Draw([In(), MarshalAs(UnmanagedType.I4)]
    int leftBounds, [In(), MarshalAs(UnmanagedType.I4)]
    int topBounds, [In(), MarshalAs(UnmanagedType.I4)]
    int rightBounds, [In(), MarshalAs(UnmanagedType.I4)]
    int bottomBounds, [In(), MarshalAs(UnmanagedType.I4)]
    int leftUpdate, [In(), MarshalAs(UnmanagedType.I4)]
    int topUpdate, [In(), MarshalAs(UnmanagedType.I4)]
    int rightUpdate, [In(), MarshalAs(UnmanagedType.I4)]
    int bottomUpdate, [In(), MarshalAs(UnmanagedType.U4)]
    int lDrawFlags, [In()]
    IntPtr hdc,
        [In()]

    IntPtr pvDrawObject);
        void OnResize([In(), MarshalAs(UnmanagedType.I4)]
    int cx, [In(), MarshalAs(UnmanagedType.I4)]

    int cy);
        void GetPainterInfo([Out()]

    HTML_PAINTER_INFO htmlPainterInfo);
        [return: MarshalAs(UnmanagedType.Bool)]
        bool HitTestPoint([In(), MarshalAs(UnmanagedType.I4)]
    int ptx, [In(), MarshalAs(UnmanagedType.I4)]
    int pty, [Out(), MarshalAs(UnmanagedType.LPArray)]
    int[] pbHit, [Out(), MarshalAs(UnmanagedType.LPArray)]
    int[] plPartID);

    }

    //[ComVisible(true), Guid("3050f6a7-98b5-11cf-bb82-00aa00bdce0b"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    //public interface IHTMLPaintSite
    //{

    //    void InvalidatePainterInfo();

    //    void InvalidateRect([In()]
    //IntPtr pRect);
    //}

    [ComVisible(true), StructLayout(LayoutKind.Sequential)]
    public class HTML_PAINTER_INFO
    {

        [MarshalAs(UnmanagedType.I4)]

        public int lFlags;
        [MarshalAs(UnmanagedType.I4)]

        public int lZOrder;
        [MarshalAs(UnmanagedType.Struct)]

        public Guid iidDrawObject;
        [MarshalAs(UnmanagedType.Struct)]
        public RECT rcBounds;
    }

    [ComVisible(true), StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        public int left;
        public int top;
        public int right;

        public int bottom;
        public RECT(int left, int top, int right, int bottom)
        {
            this.left = left;
            this.top = top;
            this.right = right;
            this.bottom = bottom;
        }

        public static RECT FromXYWH(int x, int y, int width, int height)
        {
            return new RECT(x, y, x + width, y + height);
        }
    }

    [ComVisible(true), Guid("3050F425-98B5-11CF-BB82-00AA00BDCE0B"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IElementBehavior
    {

        void Init([In(), MarshalAs(UnmanagedType.Interface)]

    IElementBehaviorSite pBehaviorSite);
        void Notify([In(), MarshalAs(UnmanagedType.U4)]
    int dwEvent, [In()]

    IntPtr pVar);
        void Detach();
    }

    [ComVisible(true), Guid("3050F429-98B5-11CF-BB82-00AA00BDCE0B"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IElementBehaviorFactory
    {

        [return: MarshalAs(UnmanagedType.Interface)]
        IElementBehavior FindBehavior([In(), MarshalAs(UnmanagedType.BStr)]
    string bstrBehavior, [In(), MarshalAs(UnmanagedType.BStr)]
    string bstrBehaviorUrl, [In(), MarshalAs(UnmanagedType.Interface)]
    IElementBehaviorSite pSite);
    }

    [ComVisible(true), Guid("3050F427-98B5-11CF-BB82-00AA00BDCE0B"), InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IElementBehaviorSite
    {
        [return: MarshalAs(UnmanagedType.Interface)]
        IHTMLElement GetElement();

        void RegisterNotification([In(), MarshalAs(UnmanagedType.I4)]
    int lEvent);
    }

    //public class TestBehavior : IElementBehaviorFactory, IElementBehavior, IHTMLPainter
    //{


    //    private mshtml.IHTMLElement m_Element;
    //    #region "IElementBehaviorFactory Members"

    //    public IElementBehavior FindBehavior(string bstrBehavior, string bstrBehaviorUrl, IElementBehaviorSite pSite)
    //    {
    //        return this;
    //    }

    //    #endregion

    //    #region "IElementBehavior Members"

    //    public void Notify(int lEvent, IntPtr pVar)
    //    {
    //        return;
    //        // TODO: Add Behavior.Notify implementation
    //    }

    //    public void Init(IElementBehaviorSite pBehaviorSite)
    //    {
    //        // then you can extract some information from the element
    //        m_Element = pBehaviorSite.GetElement();
    //    }

    //    public void Detach()
    //    {
    //        return;
    //        // TODO: Add Behavior.Detach implementation
    //    }

    //    #endregion

    //    #region "IHTMLPainter Members"

    //    public void Draw(int leftBounds, int topBounds, int rightBounds, int bottomBounds, int leftUpdate, int topUpdate, int rightUpdate, int bottomUpdate, int lDrawFlags, IntPtr hdc,
    //    IntPtr pvDrawObject)
    //    {
    //        //Graphics g = default(Graphics);
    //        //g = Graphics.FromHdc(hdc);
    //        //g.PageUnit = GraphicsUnit.Pixel;
    //        //g.Clear(Color.Black);
    //        //g.DrawIcon(Icon.ExtractAssociatedIcon(Application.ExecutablePath), leftBounds - 16, topBounds);
    //    }

    //    public void OnResize(int cx, int cy)
    //    {
    //        return;
    //        // TODO: Add Behavior.OnResize implementation
    //    }

    //    public void GetPainterInfo(_HTML_PAINTER_INFO pInfo)
    //    {
    //        pInfo.lFlags = 0x2;
    //        pInfo.lZOrder = 4;
    //        pInfo.iidDrawObject = Guid.Empty;
    //        pInfo.rcExpand = new tagRECT() { bottom = 0, left = 0, right = 0, top = 0 };  //.rcBounds = new RECT(0, 0, 0, 0);
    //    }

    //    public bool HitTestPoint(int ptx, int pty, int[] pbHit, int[] plPartID)
    //    {
    //        return false;
    //    }

    //    #endregion
    //}
    //#endregion

}
