﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using mshtml;

namespace Gadg8Gadget.Behaviors
{
    [ComImport, Guid("3050F6A6-98B5-11CF-BB82-00AA00BDCE0B"),
           InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IHTMLPainter2
    {
        // CORRECTED METHOD: Use IntPtr, 
        // not ref _RemotableHandle hdc.
        void Draw(mshtml.tagRECT rcBounds,
                  mshtml.tagRECT rcUpdate,
                  System.Int32 lDrawFlags,
                  System.IntPtr hdc,
                  System.IntPtr pvDrawObject);
        void onresize(mshtml.tagSIZE size);
        void GetPainterInfo(
            out _HTML_PAINTER_INFO pInfo);
        void HitTestPoint(
            mshtml.tagPOINT pt,
            out System.Int32 pbHit,
            out System.Int32 plPartID);
    }

}
