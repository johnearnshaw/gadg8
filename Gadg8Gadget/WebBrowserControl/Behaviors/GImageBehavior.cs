﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Security.Permissions;
using mshtml;
using System.Drawing.Imaging;

namespace Gadg8Gadget.Behaviors
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class GImage : GObject, mshtml.IElementBehavior, mshtml.IElementBehaviorFactory, IHTMLPainter2 // Had to declare own interface for IHTMLPainter for element.addBehavior compatibility.
    {
        xGadgetWindow ParentWindow;

        private IHTMLElement Element;
        private IHTMLElement2 Element2;

        public GImage(xGadgetWindow parentWindow)
        {
            this.ParentWindow = parentWindow;
        }

        #region IHTMLPainter Members

        public void Draw(tagRECT bounds, tagRECT update, int lDrawFlags, IntPtr hdc, IntPtr pvDrawObject) //(int leftBounds, int topBounds, int rightBounds, int bottomBounds, int leftUpdate, int topUpdate, int rightUpdate, int bottomUpdate, int lDrawFlags, IntPtr hdc, IntPtr pvDrawObject)
        {
            using (Graphics g = Graphics.FromHdc(hdc))
            {
                int offsetX = (image.Width - ActualWidth) / 2;
                int offsetY = (image.Height - ActualHeight) / 2;

                ColorMatrix cm;
                ImageAttributes ia;
                
                float value = (float)this.brightness / 100.0f;

                cm = new ColorMatrix(new float[][]{
                        new float[] {1, 0, 0, 0, 0},
                        new float[] {0, 1, 0, 0, 0},
                        new float[] {0, 0, 1, 0, 0},
                        new float[] {0, 0, 0, 1, 0},
                        new float[] {value, value, value, 0, 0}
                        });

                cm.Matrix33 = (float)this.opacity / 100.0f;

                ia = new ImageAttributes();
                ia.SetColorMatrix(cm, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

                g.PageUnit = GraphicsUnit.Pixel;
                
                //g.Clear(Color.Magenta);
                Bitmap rotatedImage;
                
                if (rotation == 0)
                    rotatedImage = image;
                else
                    rotatedImage = RotateImage(image, this.rotation);

                g.DrawImage(rotatedImage, new Rectangle(bounds.left - ((rotatedImage.Width - ActualWidth)/2) + offsetX, bounds.top - ((rotatedImage.Height - ActualHeight)/2) + offsetY, /*width*/ rotatedImage.Width, /*height*/ rotatedImage.Height),
                    0, 0, rotatedImage.Width, rotatedImage.Height, GraphicsUnit.Pixel, ia);
            }
        }

        public void onresize(tagSIZE tagSize)
        {
        }

        public void GetPainterInfo(out _HTML_PAINTER_INFO pInfo)
        {
            pInfo.lFlags = 0x2; // Transparent
            pInfo.lZOrder = 0x3; // Replace Background
            pInfo.iidDrawObject = Guid.Empty;
            pInfo.rcExpand = new tagRECT() { bottom = 5, left = 0, right = 5, top = 0 };
        }

        public void HitTestPoint(tagPOINT tagPoint, out int ptx, out int pty)
        {
            ptx = 0;
            pty = 0;
        }
        #endregion

        #region IElementBehaviorFactory Members

        public mshtml.IElementBehavior FindBehavior(string bstrBehavior, string bstrBehaviorUrl, mshtml.IElementBehaviorSite pSite)
        {
            return this;
        }

        #endregion

        #region IElementBehavior Members

        public void Notify(int lEvent, ref object pVar)
        {
            return;
            // TODO: Add Behavior.Notify implementation
        }

        public void Init(mshtml.IElementBehaviorSite pBehaviorSite)
        {
            IntPtr ppvObject = IntPtr.Zero;
            Guid riid = typeof(IHTMLPaintSite2).GUID;
            IntPtr unk = Marshal.GetIUnknownForObject(pBehaviorSite);
            Marshal.QueryInterface(unk, ref riid, out ppvObject);

            if (ppvObject != IntPtr.Zero)
                PaintSite = (IHTMLPaintSite2)Marshal.GetObjectForIUnknown(ppvObject);

            if (this.ParentWindow != null)
            {
                if (this.ParentWindow.GObjectList == null)
                    this.ParentWindow.GObjectList = new List<GObject>();

                this.ParentWindow.GObjectList.Add(this);
                
                // Keep reference to the html element for future use.
                Element = pBehaviorSite.GetElement();
                Element2 = (IHTMLElement2)Element;

                WebBrowserControl.HtmlEvent.Create("onpropertychange", Element2, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(PropertyChangedEventHandler));

                this.src = Helpers.GetHTMLAttribute<string>(Element, "src", "");
                this.width = Helpers.GetHTMLAttribute<string>(Element, "width", "");
                this.height = Helpers.GetHTMLAttribute<string>(Element, "height", "");

                this.top = Helpers.GetHTMLAttribute<string>(Element, "top", "0").Replace("%", "").Replace("px", "");
                this.left = Helpers.GetHTMLAttribute<string>(Element, "left", "0").Replace("%", "").Replace("px", "");
                this.opacity = int.Parse(Helpers.GetHTMLAttribute<string>(Element, "opacity", "100").Replace("%", ""));
                this.brightness = int.Parse(Helpers.GetHTMLAttribute<string>(Element, "brightness", "0").Replace("%", ""));

                Update();
            }
        }

        public void Detach()
        {
            if (this.ParentWindow.GObjectList == null)
                if (this.ParentWindow.GObjectList.Contains(this))
                    this.ParentWindow.GObjectList.Remove(this);
            return;
        }
        #endregion

        #region GImage Members
        public int softEdge { get; set; } //Gets or sets the amount of edge softening that is applied to the g:image element.

        private string _src;
        public string src //Gets or sets the image file used by the g:image element.
        {
            get
            {
                return _src;
            }
            set
            {
                if (_src != value)
                {
                    _src = value;

                    Update();
                }
            }
        }

        [ComVisible(false)]
        public override void Redraw()
        {
            if (this.image != null)
            {
                //if (PaintSite != null)
                //{
                //var rect = new tagRECT();
                //rect.top = top;
                //rect.bottom = top + height;
                //rect.left = top;
                //rect.right = left + width;
                //    PaintSite.InvalidateRegion(0);//.InvalidateRect(rect);
                //    return;
                //}
                //this.ParentWindow.BodyBehavior.Redraw(rect);

                int offsetX = (this.image.Width - this.ActualWidth) / 2;
                int offsetY = (this.image.Height - this.ActualHeight) / 2;
                this.ParentWindow.InvalidateWebBrowser(new Rectangle(this.x + offsetX, this.y + offsetY, ActualWidth, ActualHeight));
            }
        }

        [ComVisible(false)]
        public override void Update()
        {
            if (image != null)
                image.Dispose();

            if (!string.IsNullOrEmpty(src))
            {
                string s = Helpers.GetAbsolutePath(ParentWindow.Path, src, true);

                if (System.IO.File.Exists(s))
                {
                    image = (Bitmap)Bitmap.FromFile(Helpers.GetAbsolutePath(this.ParentWindow.Path, src, true));
                }
                else
                {
                    image = new Bitmap(1, 1);
                    using (Graphics g = Graphics.FromImage(this.image))
                    {
                        g.Clear(Color.Transparent);
                    }
                }
            }
            else
            {
                image = new Bitmap(1, 1);
                using (Graphics g = Graphics.FromImage(this.image))
                {
                    g.Clear(Color.Transparent);
                }
            }

            if (glow != null)
                glow.Update();

            if (shadow != null)
                shadow.Update();

            CalculateDimensions();
        }

        public override void CalculateDimensions()
        {
            int w, h;

            this.ActualWidth = this.image.Width;
            this.ActualHeight = this.image.Height;

            if (!string.IsNullOrEmpty(this.width))
            {
                if (this.width.Contains("%"))
                {
                    if (int.TryParse(this.width.Replace("%", ""), out w))
                    {
                        this.ActualWidth = (this.ParentWindow.Width / 100) * w;
                    }
                }
                else
                {
                    if (int.TryParse(this.width.Replace("px", ""), out w))
                    {
                        this.ActualWidth = w;
                    }
                }
            }

            if (!string.IsNullOrEmpty(this.height))
            {
                if (this.height.Contains("%"))
                {
                    if (int.TryParse(this.height.Replace("%", ""), out h))
                    {
                        this.ActualHeight = (this.ParentWindow.Height / 100) * h;
                    }
                }
                else
                {
                    if (int.TryParse(this.height.Replace("px", ""), out h))
                    {
                        this.ActualHeight = h;
                    }
                }
            }

            Redraw();
        }
        #endregion

        private void PropertyChangedEventHandler(object sender, IHTMLEventObj e)
        {
            IHTMLEventObj2 ev2 = (IHTMLEventObj2)e;
            IHTMLElement2 el = (IHTMLElement2)e.srcElement;

            int i = 0;
            string p = ev2.propertyName.ToLower().Replace("style.", "");

            switch (p)
            {
                case "src":
                    this.src = Helpers.GetHTMLAttribute<string>(Element, "src");
                    break;

                case "opacity":
                    this.opacity = int.Parse(Helpers.GetHTMLAttribute<string>(Element, "opacity", "100").Replace("%", ""));
                    break;

                case "top":
                    this.top = Helpers.GetHTMLAttribute<string>(Element, "top", "0").Replace("%", "").Replace("px", "");
                    break;

                case "left":
                    this.left = Helpers.GetHTMLAttribute<string>(Element, "left", "0").Replace("%", "").Replace("px", "");
                    break;

                case "width":
                    this.width = Helpers.GetHTMLAttribute<string>(Element, "style.width", "");
                    break;

                case "height":
                    this.height = Helpers.GetHTMLAttribute<string>(Element, "style.height", "");
                    break;

                case "brightness":
                    this.brightness = int.Parse(Helpers.GetHTMLAttribute<string>(Element, "brightness", "0").Replace("%", ""));
                    break;
            }

            Update();
        }
    }
}
