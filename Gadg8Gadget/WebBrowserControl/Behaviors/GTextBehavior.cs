﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Imaging;
using System.Security.Permissions;
using mshtml;

namespace Gadg8Gadget.Behaviors
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class GText : GObject, mshtml.IElementBehavior, mshtml.IElementBehaviorFactory, IHTMLPainter2 // Had to declare own interface for IHTMLPainter for element.addBehavior compatibility.
    {
        public xGadgetWindow ParentWindow;

        private IHTMLElement Element;
        private IHTMLElement2 Element2;

        private Color Color = Color.Black;

        public GText(xGadgetWindow parentWindow)
        {
            this.ParentWindow = parentWindow;
        }

        #region IHTMLPainter Members

        public void Draw(tagRECT bounds, tagRECT update, int lDrawFlags, IntPtr hdc, IntPtr pvDrawObject) //(int leftBounds, int topBounds, int rightBounds, int bottomBounds, int leftUpdate, int topUpdate, int rightUpdate, int bottomUpdate, int lDrawFlags, IntPtr hdc, IntPtr pvDrawObject)
        {
            using (Graphics g = Graphics.FromHdc(hdc))
            {
                int offsetX = 0;// (image.Width - ActualWidth) / 2;
                int offsetY = 0;// (image.Height - ActualHeight) / 2;

                ColorMatrix cm;
                ImageAttributes ia;

                float value = (float)this.brightness / 100.0f;

                cm = new ColorMatrix(new float[][]{
                        new float[] {1, 0, 0, 0, 0},
                        new float[] {0, 1, 0, 0, 0},
                        new float[] {0, 0, 1, 0, 0},
                        new float[] {0, 0, 0, 1, 0},
                        new float[] {value, value, value, 0, 0}
                        });

                cm.Matrix33 = (float)this.opacity / 100.0f;

                ia = new ImageAttributes();
                ia.SetColorMatrix(cm, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

                g.PageUnit = GraphicsUnit.Pixel;

                //g.Clear(Color.Magenta);
                Bitmap rotatedImage;

                if (rotation == 0)
                    rotatedImage = image;
                else
                    rotatedImage = RotateImage(image, this.rotation);

                g.DrawImage(rotatedImage, new Rectangle(bounds.left - ((rotatedImage.Width - ActualWidth) / 2) + offsetX, bounds.top - ((rotatedImage.Height - ActualHeight) / 2) + offsetY, /*width*/ rotatedImage.Width, /*height*/ rotatedImage.Height),
                    0, 0, rotatedImage.Width, rotatedImage.Height, GraphicsUnit.Pixel, ia);
            }
        }

        public void onresize(tagSIZE tagSize)
        {
        }

        public void GetPainterInfo(out _HTML_PAINTER_INFO pInfo)
        {
            pInfo.lFlags = 0x2; // Transparent
            pInfo.lZOrder = 0x3; // Replace Background
            pInfo.iidDrawObject = Guid.Empty;
            pInfo.rcExpand = new tagRECT() { bottom = 0, left = 5, right = 0, top = 5 };
        }

        public void HitTestPoint(tagPOINT tagPoint, out int ptx, out int pty)
        {
            ptx = 0;
            pty = 0;
        }
        #endregion

        #region IElementBehaviorFactory Members

        public mshtml.IElementBehavior FindBehavior(string bstrBehavior, string bstrBehaviorUrl, mshtml.IElementBehaviorSite pSite)
        {
            return this;
        }

        #endregion

        #region IElementBehavior Members

        public void Notify(int lEvent, ref object pVar)
        {
            return;
            // TODO: Add Behavior.Notify implementation
        }

        public void Init(mshtml.IElementBehaviorSite pBehaviorSite)
        {
            IntPtr ppvObject = IntPtr.Zero;
            Guid riid = typeof(IHTMLPaintSite2).GUID;
            IntPtr unk = Marshal.GetIUnknownForObject(pBehaviorSite);
            Marshal.QueryInterface(unk, ref riid, out ppvObject);

            if (ppvObject != IntPtr.Zero)
                PaintSite = (IHTMLPaintSite2)Marshal.GetObjectForIUnknown(ppvObject);

            if (this.ParentWindow != null)
            {

                if (this.ParentWindow.GObjectList == null)
                    this.ParentWindow.GObjectList = new List<GObject>();

                this.ParentWindow.GObjectList.Add(this);

                // Keep reference to the html element for future use.
                Element = pBehaviorSite.GetElement();
                Element2 = (IHTMLElement2)Element;

                WebBrowserControl.HtmlEvent.Create("onpropertychange", Element2, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(PropertyChangedEventHandler));

                this.width = Helpers.GetHTMLAttribute<string>(Element, "width", "");
                this.height = Helpers.GetHTMLAttribute<string>(Element, "height", "");

                this.top = Helpers.GetHTMLAttribute<string>(Element, "top", "0").Replace("%", "").Replace("px", "");
                this.left = Helpers.GetHTMLAttribute<string>(Element, "left", "0").Replace("%", "").Replace("px", "");
                this.opacity = int.Parse(Helpers.GetHTMLAttribute<string>(Element, "opacity", "100").Replace("%", ""));
                this.brightness = int.Parse(Helpers.GetHTMLAttribute<string>(Element, "brightness", "0").Replace("%", ""));

                CalculateDimensions();
                Update();
            }
        }

        public void Detach()
        {
            if (this.ParentWindow.GObjectList == null)
                if (this.ParentWindow.GObjectList.Contains(this))
                    this.ParentWindow.GObjectList.Remove(this);
            return;
        }

        #endregion

        #region GText Members
        private int pAlign = 0;
        // Gets or sets the text alignment.
        // 0 Default. Based on Sidebar alignment. Text is aligned right when the Sidebar is anchored to the
        // right and aligned left when the Sidebar is anchored to the left.
        // 1 Center. 2 Left. 4 Right.
        public int align
        {
            get
            {
                return pAlign;
            }
            set
            {
                pAlign = value;
                Update();
            }
        }

        private string pColor;
        // Gets or sets the text color.
        public string color
        {
            get
            {
                return pColor;
            }
            set
            {
                pColor = value;
                this.Color = Helpers.ParseColor(value);
                Update();
            }
        }

        private string pFont = "Arial";
        // Gets or sets the font name.
        public string font
        {
            get
            {
                return pFont;
            }
            set
            {
                pFont = value;
                Update();
            }
        }

        private int pFontsize = 12;
        // Gets or sets the font size.
        public int fontsize
        {
            get
            {
                return pFontsize;
            }
            set
            {
                pFontsize = value;
                Update();
            }
        }

        private string pValue;
        // Gets or sets the value of the g:text element.
        public string value
        {
            get
            {
                return pValue;
            }
            set
            {
                pValue = value;
                Update();
            }
        }

        [ComVisible(false)]
        public override void Redraw()
        {
            if (this.image != null)
            {
                //if (PaintSite != null)
                //{
                //    var rect = new tagRECT();
                //    rect.top = 0;
                //    rect.bottom = top + height;
                //    rect.left = 0;
                //    rect.right = left + width;
                //    PaintSite.InvalidateRegion(0);
                //    return;
                //}

                int offsetX = (this.image.Width - this.ActualWidth) / 2;
                int offsetY = (this.image.Height - this.ActualHeight) / 2;
                this.ParentWindow.InvalidateWebBrowser(new Rectangle(this.x + offsetX, this.y + offsetY, ActualWidth, ActualHeight));
            }
        }

        public override void Update()
        {
            if (image != null)
                image.Dispose();

            image = new Bitmap(this.ParentWindow.Width, this.ParentWindow.Height);

            using (Graphics g = Graphics.FromImage(image))
            {
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;

                SizeF size = g.MeasureString(this.value, new Font(this.font, (float)fontsize, GraphicsUnit.Pixel));
                this.ActualWidth = (int)size.Width;
                this.ActualHeight = (int)size.Height;

                StringFormat stringFormat = new StringFormat();

                if (this.align == 1)
                    stringFormat.LineAlignment = StringAlignment.Center;
                else if (this.align == 2)
                    stringFormat.LineAlignment = StringAlignment.Near;
                else if (this.align == 3)
                    stringFormat.LineAlignment = StringAlignment.Far;
                else
                {
                    if (ParentWindow.SystemNamespace.Gadget.Sidebar.dockSide == "right")
                        stringFormat.LineAlignment = StringAlignment.Far;
                    else
                        stringFormat.LineAlignment = StringAlignment.Near;
                }

                g.DrawString(this.value, new Font(this.font, (float)fontsize), new SolidBrush(this.Color),
                    this.x, this.y, stringFormat);
            }

            if (glow != null)
                glow.Update();

            if (shadow != null)
                shadow.Update();

            //int offsetX = (this.image.Width - this.ActualWidth) / 2;
            //int offsetY = (this.image.Height - this.ActualHeight) / 2;
            this.ParentWindow.WebBrowser.Invalidate(new Rectangle(this.x /*+ offsetX*/, this.y /*+ offsetY*/,
                this.ParentWindow.Width, this.ParentWindow.Height), true);

            CalculateDimensions();
        }

        public override void CalculateDimensions()
        {
            int w, h;

            this.ActualWidth = this.image.Width;
            this.ActualHeight = this.image.Width;

            if (!string.IsNullOrEmpty(this.width))
            {
                if (this.width.Contains("%"))
                {
                    if (int.TryParse(this.width.Replace("%", ""), out w))
                    {
                        this.ActualWidth = (this.ParentWindow.Width / w) * 100;
                    }
                }
                else
                {
                    if (int.TryParse(this.width.Replace("px", ""), out w))
                    {
                        this.ActualWidth = w;
                    }
                }
            }

            if (!string.IsNullOrEmpty(this.height))
            {
                if (this.height.Contains("%"))
                {
                    if (int.TryParse(this.height.Replace("%", ""), out h))
                    {
                        this.ActualHeight = (this.ParentWindow.Height / h) * 100;
                    }
                }
                else
                {
                    if (int.TryParse(this.height.Replace("px", ""), out h))
                    {
                        this.ActualHeight = h;
                    }
                }
            }

            Redraw();
        }
        #endregion

        private void PropertyChangedEventHandler(object sender, IHTMLEventObj e)
        {
            IHTMLEventObj2 ev2 = (IHTMLEventObj2)e;
            IHTMLElement2 el = (IHTMLElement2)e.srcElement;

            string p = ev2.propertyName.ToLower();

            switch (p)
            {
                case "opacity":
                    this.opacity = int.Parse(Helpers.GetHTMLAttribute<string>(Element, "opacity", "100").Replace("%", ""));
                    break;

                case "top":
                    this.top = Helpers.GetHTMLAttribute<string>(Element, "top", "0").Replace("%", "").Replace("px", "");
                    break;

                case "left":
                    this.left = Helpers.GetHTMLAttribute<string>(Element, "left", "0").Replace("%", "").Replace("px", "");
                    break;

                case "width":
                    this.width = Helpers.GetHTMLAttribute<string>(Element, "width", "");
                    break;

                case "height":
                    this.height = Helpers.GetHTMLAttribute<string>(Element, "height", "");
                    break;

                case "brightness":
                    this.brightness = int.Parse(Helpers.GetHTMLAttribute<string>(Element, "brightness", "0").Replace("%", ""));
                    break;
            }

            return;
        }
    }
}
