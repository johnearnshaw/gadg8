﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Security;
using System.Drawing;

namespace Gadg8Gadget.WebBrowserControl
{
    public sealed class Native
    {
        #region Declarations
        [DllImport("user32", EntryPoint = "SendMessageA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern int SendMessage(IntPtr hwnd, uint wMsg, IntPtr wParam, IntPtr lParam);

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        [DllImport("user32")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr window, Helpers.dGetWindowHandle callback, ArrayList lParam);

        //[DllImport("user32", EntryPoint = "GetClassNameA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        //[DllImport("user32", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //[return: MarshalAs(UnmanagedType.Bool)]
        //public static extern int EnumChildWindows(IntPtr window, Helpers.dGetWindowHandle callback, ArrayList lParam);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);

        [DllImport("user32.dll", CharSet = CharSet.Auto, EntryPoint = "GetWindow", SetLastError = true)]
        public static extern IntPtr GetNextWindow(IntPtr hwnd, [MarshalAs(UnmanagedType.U4)] int wFlag);

        [DllImport("user32.dll")]
        public static extern bool ClientToScreen(IntPtr hWnd, ref Point lpPoint);

        [DllImport("urlmon.DLL")]
        public static extern int CoInternetSetFeatureEnabled(INTERNETFEATURELIST featureEntry, int dwFlags, bool fEnable);

        [DllImport("urlmon.DLL")]
        public static extern int CoInternetIsFeatureEnabled(INTERNETFEATURELIST featureEntry, int dwFlags);

        [SecurityCritical(), SuppressUnmanagedCodeSecurity(), DllImport("urlmon.dll", ExactSpelling = true)]
        public static extern int CoInternetSetFeatureEnabled(int featureEntry, int dwFlags, bool fEnable);

        [DllImport("ole32.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern int CreateBindCtx([MarshalAs(UnmanagedType.U4)] uint dwReserved, [Out, MarshalAs(UnmanagedType.Interface)] out IBindCtx ppbc);

        [DllImport("urlmon.dll", ExactSpelling = true, CharSet = CharSet.Auto)]
        public static extern int CreateURLMoniker([MarshalAs(UnmanagedType.Interface)] IMoniker pmkContext, [MarshalAs(UnmanagedType.LPWStr)] string szURL,
            [Out, MarshalAs(UnmanagedType.Interface)] out IMoniker ppmk);

        [DllImport("ole32.dll", CharSet = CharSet.Auto)]
        public static extern int CreateStreamOnHGlobal(IntPtr hGlobal, bool fDeleteOnRelease, [MarshalAs(UnmanagedType.Interface)] out IStream ppstm);
        #endregion

        #region Constants
        public const int S_OK = 0;
        public const int S_FALSE = 1;

        public const int E_NOTIMPL = unchecked((int)0x80004001);
        public const int E_NOINTERFACE = unchecked((int)0x80004002);
        public const int E_FAIL = unchecked((int)0x80004005);

        public const int SET_FEATURE_ON_PROCESS = 0x2;

        public const int URLPOLICY_ALLOW = 0x00;
        public const int URLPOLICY_DISALLOW = 0x03;
        //public const uint STGM_READ = 0x00000000;
        #endregion

        #region Structures
        [ComVisible(true), StructLayout(LayoutKind.Sequential)]
        public struct DOCHOSTUIINFO
        {
            [MarshalAs(UnmanagedType.U4)]
            public uint cbSize;
            [MarshalAs(UnmanagedType.U4)]
            public uint dwFlags;
            [MarshalAs(UnmanagedType.U4)]
            public uint dwDoubleClick;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pchHostCss;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pchHostNS;
        }

        [ComVisible(true), StructLayout(LayoutKind.Sequential)]
        public struct tagMSG
        {
            public IntPtr hwnd;
            [MarshalAs(UnmanagedType.I4)]
            public int message;
            public IntPtr wParam;
            public IntPtr lParam;
            [MarshalAs(UnmanagedType.I4)]
            public int time;
            // pt was a by-value POINT structure
            [MarshalAs(UnmanagedType.I4)]
            public int pt_x;
            [MarshalAs(UnmanagedType.I4)]
            public int pt_y;
            //public tagPOINT pt;
        }

        [ComVisible(true), StructLayout(LayoutKind.Sequential)]
        public struct tagOLECMD
        {
            [MarshalAs(UnmanagedType.U4)]
            public uint cmdID;
            [MarshalAs(UnmanagedType.U4)]
            public uint cmdf;
        }
        #endregion

        #region Enums
        public enum INTERNETFEATURELIST
        {
            FEATURE_OBJECT_CACHING = 0,
            FEATURE_ZONE_ELEVATION = 1,
            FEATURE_MIME_HANDLING = 2,
            FEATURE_MIME_SNIFFING = 3,
            FEATURE_WINDOW_RESTRICTIONS = 4,
            FEATURE_WEBOC_POPUPMANAGEMENT = 5,
            FEATURE_BEHAVIORS = 6,
            FEATURE_DISABLE_MK_PROTOCOL = 7,
            FEATURE_LOCALMACHINE_LOCKDOWN = 8,
            FEATURE_SECURITYBAND = 9,
            FEATURE_RESTRICT_ACTIVEXINSTALL = 10,
            FEATURE_VALIDATE_NAVIGATE_URL = 11,
            FEATURE_RESTRICT_FILEDOWNLOAD = 12,
            FEATURE_ADDON_MANAGEMENT = 13,
            FEATURE_PROTOCOL_LOCKDOWN = 14,
            FEATURE_HTTP_USERNAME_PASSWORD_DISABLE = 15,
            FEATURE_SAFE_BINDTOOBJECT = 16,
            FEATURE_UNC_SAVEDFILECHECK = 17,
            FEATURE_GET_URL_DOM_FILEPATH_UNENCODED = 18,
            FEATURE_ENTRY_COUNT = 19,
            FEATURE_DISABLE_NAVIGATION_SOUNDS = 21
        }

        public enum DOCHOSTUIFLAG
        {
            DIALOG = 0x1,
            DISABLE_HELP_MENU = 0x2,
            NO3DBORDER = 0x4,
            SCROLL_NO = 0x8,
            DISABLE_SCRIPT_INACTIVE = 0x10,
            OPENNEWWIN = 0x20,
            DISABLE_OFFSCREEN = 0x40,
            FLAT_SCROLLBAR = 0x80,
            DIV_BLOCKDEFAULT = 0x100,
            ACTIVATE_CLIENTHIT_ONLY = 0x200,
            OVERRIDEBEHAVIORFACTORY = 0x400,
            CODEPAGELINKEDFONTS = 0x800,
            URL_ENCODING_DISABLE_UTF8 = 0x1000,
            URL_ENCODING_ENABLE_UTF8 = 0x2000,
            ENABLE_FORMS_AUTOCOMPLETE = 0x4000,
            ENABLE_INPLACE_NAVIGATION = 0x10000,
            IME_ENABLE_RECONVERSION = 0x20000,
            THEME = 0x40000,
            NOTHEME = 0x80000,
            NOPICS = 0x100000,
            NO3DOUTERBORDER = 0x200000,
            DISABLE_EDIT_NS_FIXUP = 0x400000,
            LOCAL_MACHINE_ACCESS_CHECK = 0x800000,
            DISABLE_UNTRUSTEDPROTOCOL = 0x1000000,
            //To match the behavior of Internet Explorer in your application
            //Users cannot directly interact with Microsoft ActiveX controls 
            //loaded by the APPLET, EMBED, or OBJECT elements.
            //Users can interact with such controls after activating their user interfaces. 
            ENABLE_ACTIVEX_INACTIVATE_MODE = 0x10
        }

        public enum OLECMDID
        {
            OLECMDID_OPEN = 1,
            OLECMDID_NEW = 2,
            OLECMDID_SAVE = 3,
            OLECMDID_SAVEAS = 4,
            OLECMDID_SAVECOPYAS = 5,
            OLECMDID_PRINT = 6,
            OLECMDID_PRINTPREVIEW = 7,
            OLECMDID_PAGESETUP = 8,
            OLECMDID_SPELL = 9,
            OLECMDID_PROPERTIES = 10,
            OLECMDID_CUT = 11,
            OLECMDID_COPY = 12,
            OLECMDID_PASTE = 13,
            OLECMDID_PASTESPECIAL = 14,
            OLECMDID_UNDO = 15,
            OLECMDID_REDO = 16,
            OLECMDID_SELECTALL = 17,
            OLECMDID_CLEARSELECTION = 18,
            OLECMDID_ZOOM = 19,
            OLECMDID_GETZOOMRANGE = 20,
            OLECMDID_UPDATECOMMANDS = 21,
            OLECMDID_REFRESH = 22,
            OLECMDID_STOP = 23,
            OLECMDID_HIDETOOLBARS = 24,
            OLECMDID_SETPROGRESSMAX = 25,
            OLECMDID_SETPROGRESSPOS = 26,
            OLECMDID_SETPROGRESSTEXT = 27,
            OLECMDID_SETTITLE = 28,
            OLECMDID_SETDOWNLOADSTATE = 29,
            OLECMDID_STOPDOWNLOAD = 30,
            OLECMDID_ONTOOLBARACTIVATED = 31,
            OLECMDID_FIND = 32,
            OLECMDID_DELETE = 33,
            OLECMDID_HTTPEQUIV = 34,
            OLECMDID_HTTPEQUIV_DONE = 35,
            OLECMDID_ENABLE_INTERACTION = 36,
            OLECMDID_ONUNLOAD = 37,
            OLECMDID_PROPERTYBAG2 = 38,
            OLECMDID_PREREFRESH = 39,
            OLECMDID_SHOWSCRIPTERROR = 40,
            OLECMDID_SHOWMESSAGE = 41,
            OLECMDID_SHOWFIND = 42,
            OLECMDID_SHOWPAGESETUP = 43,
            OLECMDID_SHOWPRINT = 44,
            OLECMDID_CLOSE = 45,
            OLECMDID_ALLOWUILESSSAVEAS = 46,
            OLECMDID_DONTDOWNLOADCSS = 47,
            OLECMDID_UPDATEPAGESTATUS = 48,
            OLECMDID_PRINT2 = 49,
            OLECMDID_PRINTPREVIEW2 = 50,
            OLECMDID_SETPRINTTEMPLATE = 51,
            OLECMDID_GETPRINTTEMPLATE = 52,
            OLECMDID_PAGEACTIONBLOCKED = 55,
            OLECMDID_PAGEACTIONUIQUERY = 56,
            OLECMDID_FOCUSVIEWCONTROLS = 57,
            OLECMDID_FOCUSVIEWCONTROLSQUERY = 58,
            OLECMDID_SHOWPAGEACTIONMENU = 59,
            OLECMDID_ADDTRAVELENTRY = 60,
            OLECMDID_UPDATETRAVELENTRY = 61,
            OLECMDID_UPDATEBACKFORWARDSTATE = 62,
            OLECMDID_OPTICAL_ZOOM = 63,
            OLECMDID_OPTICAL_GETZOOMRANGE = 64,
            OLECMDID_WINDOWSTATECHANGED = 65
            //OLECMDID_IE7_SHOWSCRIPTERROR = 69
        }

        public enum OLECMDEXECOPT
        {
            OLECMDEXECOPT_DODEFAULT = 0,
            OLECMDEXECOPT_PROMPTUSER = 1,
            OLECMDEXECOPT_DONTPROMPTUSER = 2,
            OLECMDEXECOPT_SHOWHELP = 3
        }

        public enum ELEMENTNAMESPACE_FLAGS
        {
            ELEMENTNAMESPACEFLAGS_ALLOWANYTAG = 0x1,
            ELEMENTNAMESPACEFLAGS_QUERYFORUNKNOWNTAGS = 0x2,
            ELEMENTNAMESPACE_FLAGS_Max = 2147483647
        }

        public enum WinInetErrors : int
        {
            HTTP_STATUS_CONTINUE = 100,
            //The request can be continued.
            HTTP_STATUS_SWITCH_PROTOCOLS = 101,
            //The server has switched protocols in an upgrade header.
            HTTP_STATUS_OK = 200,
            //The request completed successfully.
            HTTP_STATUS_CREATED = 201,
            //The request has been fulfilled and resulted in the creation of a new resource.
            HTTP_STATUS_ACCEPTED = 202,
            //The request has been accepted for processing, but the processing has not been completed.
            HTTP_STATUS_PARTIAL = 203,
            //The returned meta information in the entity-header is not the definitive set available from the origin server.
            HTTP_STATUS_NO_CONTENT = 204,
            //The server has fulfilled the request, but there is no new information to send back.
            HTTP_STATUS_RESET_CONTENT = 205,
            //The request has been completed, and the client program should reset the document view that caused the request to be sent to allow the user to easily initiate another input action.
            HTTP_STATUS_PARTIAL_CONTENT = 206,
            //The server has fulfilled the partial GET request for the resource.
            HTTP_STATUS_AMBIGUOUS = 300,
            //The server couldn't decide what to return.
            HTTP_STATUS_MOVED = 301,
            //The requested resource has been assigned to a new permanent URI (Uniform Resource Identifier), and any future references to this resource should be done using one of the returned URIs.
            HTTP_STATUS_REDIRECT = 302,
            //The requested resource resides temporarily under a different URI (Uniform Resource Identifier).
            HTTP_STATUS_REDIRECT_METHOD = 303,
            //The response to the request can be found under a different URI (Uniform Resource Identifier) and should be retrieved using a GET HTTP verb on that resource.
            HTTP_STATUS_NOT_MODIFIED = 304,
            //The requested resource has not been modified.
            HTTP_STATUS_USE_PROXY = 305,
            //The requested resource must be accessed through the proxy given by the location field.
            HTTP_STATUS_REDIRECT_KEEP_VERB = 307,
            //The redirected request keeps the same HTTP verb. HTTP/1.1 behavior.
            HTTP_STATUS_BAD_REQUEST = 400,
            HTTP_STATUS_DENIED = 401,
            HTTP_STATUS_PAYMENT_REQ = 402,
            HTTP_STATUS_FORBIDDEN = 403,
            HTTP_STATUS_NOT_FOUND = 404,
            HTTP_STATUS_BAD_METHOD = 405,
            HTTP_STATUS_NONE_ACCEPTABLE = 406,
            HTTP_STATUS_PROXY_AUTH_REQ = 407,
            HTTP_STATUS_REQUEST_TIMEOUT = 408,
            HTTP_STATUS_CONFLICT = 409,
            HTTP_STATUS_GONE = 410,
            HTTP_STATUS_LENGTH_REQUIRED = 411,
            HTTP_STATUS_PRECOND_FAILED = 412,
            HTTP_STATUS_REQUEST_TOO_LARGE = 413,
            HTTP_STATUS_URI_TOO_LONG = 414,
            HTTP_STATUS_UNSUPPORTED_MEDIA = 415,
            HTTP_STATUS_RETRY_WITH = 449,
            HTTP_STATUS_SERVER_ERROR = 500,
            HTTP_STATUS_NOT_SUPPORTED = 501,
            HTTP_STATUS_BAD_GATEWAY = 502,
            HTTP_STATUS_SERVICE_UNAVAIL = 503,
            HTTP_STATUS_GATEWAY_TIMEOUT = 504,
            HTTP_STATUS_VERSION_NOT_SUP = 505,

            ERROR_INTERNET_ASYNC_THREAD_FAILED = 12047,
            //The application could not start an asynchronous thread.
            ERROR_INTERNET_BAD_AUTO_PROXY_SCRIPT = 12166,
            //There was an error in the automatic proxy configuration script.
            ERROR_INTERNET_BAD_OPTION_LENGTH = 12010,
            //The length of an option supplied to InternetQueryOption or InternetSetOption is incorrect for the type of option specified.
            ERROR_INTERNET_BAD_REGISTRY_PARAMETER = 12022,
            //A required registry value was located but is an incorrect type or has an invalid value.
            ERROR_INTERNET_CANNOT_CONNECT = 12029,
            //The attempt to connect to the server failed.
            ERROR_INTERNET_CHG_POST_IS_NON_SECURE = 12042,
            //The application is posting and attempting to change multiple lines of text on a server that is not secure.
            ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED = 12044,
            //The server is requesting client authentication.
            ERROR_INTERNET_CLIENT_AUTH_NOT_SETUP = 12046,
            //Client authorization is not set up on this computer.
            ERROR_INTERNET_CONNECTION_ABORTED = 12030,
            //The connection with the server has been terminated.
            ERROR_INTERNET_CONNECTION_RESET = 12031,
            //The connection with the server has been reset.
            ERROR_INTERNET_DIALOG_PENDING = 12049,
            //Another thread has a password dialog box in progress.
            ERROR_INTERNET_DISCONNECTED = 12163,
            //The Internet connection has been lost.
            ERROR_INTERNET_EXTENDED_ERROR = 12003,
            //An extended error was returned from the server. This is typically a string or buffer containing a verbose error message. Call InternetGetLastResponseInfo to retrieve the error text.
            ERROR_INTERNET_FAILED_DUETOSECURITYCHECK = 12171,
            //The function failed due to a security check.
            ERROR_INTERNET_FORCE_RETRY = 12032,
            //The function needs to redo the request.
            ERROR_INTERNET_FORTEZZA_LOGIN_NEEDED = 12054,
            //The requested resource requires Fortezza authentication.
            ERROR_INTERNET_HANDLE_EXISTS = 12036,
            //The request failed because the handle already exists.
            ERROR_INTERNET_HTTP_TO_HTTPS_ON_REDIR = 12039,
            //The application is moving from a non-SSL to an SSL connection because of a redirect.
            ERROR_INTERNET_HTTPS_HTTP_SUBMIT_REDIR = 12052,
            //The data being submitted to an SSL connection is being redirected to a non-SSL connection.
            ERROR_INTERNET_HTTPS_TO_HTTP_ON_REDIR = 12040,
            //The application is moving from an SSL to an non-SSL connection because of a redirect.
            ERROR_INTERNET_INCORRECT_FORMAT = 12027,
            //The format of the request is invalid.
            ERROR_INTERNET_INCORRECT_HANDLE_STATE = 12019,
            //The requested operation cannot be carried out because the handle supplied is not in the correct state.
            ERROR_INTERNET_INCORRECT_HANDLE_TYPE = 12018,
            //The type of handle supplied is incorrect for this operation.
            ERROR_INTERNET_INCORRECT_PASSWORD = 12014,
            //The request to connect and log on to an FTP server could not be completed because the supplied password is incorrect.
            ERROR_INTERNET_INCORRECT_USER_NAME = 12013,
            //The request to connect and log on to an FTP server could not be completed because the supplied user name is incorrect.
            ERROR_INTERNET_INSERT_CDROM = 12053,
            //The request requires a CD-ROM to be inserted in the CD-ROM drive to locate the resource requested.
            ERROR_INTERNET_INTERNAL_ERROR = 12004,
            //An internal error has occurred.
            ERROR_INTERNET_INVALID_CA = 12045,
            //The function is unfamiliar with the Certificate Authority that generated the server's certificate.
            ERROR_INTERNET_INVALID_OPERATION = 12016,
            //The requested operation is invalid.
            ERROR_INTERNET_INVALID_OPTION = 12009,
            //A request to InternetQueryOption or InternetSetOption specified an invalid option value.
            ERROR_INTERNET_INVALID_PROXY_REQUEST = 12033,
            //The request to the proxy was invalid.
            ERROR_INTERNET_INVALID_URL = 12005,
            //The URL is invalid.
            ERROR_INTERNET_ITEM_NOT_FOUND = 12028,
            //The requested item could not be located.
            ERROR_INTERNET_LOGIN_FAILURE = 12015,
            //The request to connect and log on to an FTP server failed.
            ERROR_INTERNET_LOGIN_FAILURE_DISPLAY_ENTITY_BODY = 12174,
            //The MS-Logoff digest header has been returned from the Web site. This header specifically instructs the digest package to purge credentials for the associated realm. This error will only be returned if INTERNET_ERROR_MASK_LOGIN_FAILURE_DISPLAY_ENTITY_BODY has been set.
            ERROR_INTERNET_MIXED_SECURITY = 12041,
            //The content is not entirely secure. Some of the content being viewed may have come from unsecured servers.
            ERROR_INTERNET_NAME_NOT_RESOLVED = 12007,
            //The server name could not be resolved.
            ERROR_INTERNET_NEED_MSN_SSPI_PKG = 12173,
            //Not currently implemented.
            ERROR_INTERNET_NEED_UI = 12034,
            //A user interface or other blocking operation has been requested.
            ERROR_INTERNET_NO_CALLBACK = 12025,
            //An asynchronous request could not be made because a callback function has not been set.
            ERROR_INTERNET_NO_CONTEXT = 12024,
            //An asynchronous request could not be made because a zero context value was supplied.
            ERROR_INTERNET_NO_DIRECT_ACCESS = 12023,
            //Direct network access cannot be made at this time.
            ERROR_INTERNET_NOT_INITIALIZED = 12172,
            //Initialization of the WinINet API has not occurred. Indicates that a higher-level function, such as InternetOpen, has not been called yet.
            ERROR_INTERNET_NOT_PROXY_REQUEST = 12020,
            //The request cannot be made via a proxy.
            ERROR_INTERNET_OPERATION_CANCELLED = 12017,
            //The operation was canceled, usually because the handle on which the request was operating was closed before the operation completed.
            ERROR_INTERNET_OPTION_NOT_SETTABLE = 12011,
            //The requested option cannot be set, only queried.
            ERROR_INTERNET_OUT_OF_HANDLES = 12001,
            //No more handles could be generated at this time.
            ERROR_INTERNET_POST_IS_NON_SECURE = 12043,
            //The application is posting data to a server that is not secure.
            ERROR_INTERNET_PROTOCOL_NOT_FOUND = 12008,
            //The requested protocol could not be located.
            ERROR_INTERNET_PROXY_SERVER_UNREACHABLE = 12165,
            //The designated proxy server cannot be reached.
            ERROR_INTERNET_REDIRECT_SCHEME_CHANGE = 12048,
            //The function could not handle the redirection, because the scheme changed (for example, HTTP to FTP).
            ERROR_INTERNET_REGISTRY_VALUE_NOT_FOUND = 12021,
            //A required registry value could not be located.
            ERROR_INTERNET_REQUEST_PENDING = 12026,
            //The required operation could not be completed because one or more requests are pending.
            ERROR_INTERNET_RETRY_DIALOG = 12050,
            //The dialog box should be retried.
            ERROR_INTERNET_SEC_CERT_CN_INVALID = 12038,
            //SSL certificate common name (host name field) is incorrect—for example, if you entered www.server.com and the common name on the certificate says www.different.com.
            ERROR_INTERNET_SEC_CERT_DATE_INVALID = 12037,
            //SSL certificate date that was received from the server is bad. The certificate is expired.
            ERROR_INTERNET_SEC_CERT_ERRORS = 12055,
            //The SSL certificate contains errors.
            ERROR_INTERNET_SEC_CERT_NO_REV = 12056,
            ERROR_INTERNET_SEC_CERT_REV_FAILED = 12057,
            ERROR_INTERNET_SEC_CERT_REVOKED = 12170,
            //SSL certificate was revoked.
            ERROR_INTERNET_SEC_INVALID_CERT = 12169,
            //SSL certificate is invalid.
            ERROR_INTERNET_SECURITY_CHANNEL_ERROR = 12157,
            //The application experienced an internal error loading the SSL libraries.
            ERROR_INTERNET_SERVER_UNREACHABLE = 12164,
            //The Web site or server indicated is unreachable.
            ERROR_INTERNET_SHUTDOWN = 12012,
            //WinINet support is being shut down or unloaded.
            ERROR_INTERNET_TCPIP_NOT_INSTALLED = 12159,
            //The required protocol stack is not loaded and the application cannot start WinSock.
            ERROR_INTERNET_TIMEOUT = 12002,
            //The request has timed out.
            ERROR_INTERNET_UNABLE_TO_CACHE_FILE = 12158,
            //The function was unable to cache the file.
            ERROR_INTERNET_UNABLE_TO_DOWNLOAD_SCRIPT = 12167,
            //The automatic proxy configuration script could not be downloaded. The INTERNET_FLAG_MUST_CACHE_REQUEST flag was set.
            INET_E_INVALID_URL = unchecked((int)0x800c0002),
            INET_E_NO_SESSION = unchecked((int)0x800c0003),
            INET_E_CANNOT_CONNECT = unchecked((int)0x800c0004),
            INET_E_RESOURCE_NOT_FOUND = unchecked((int)0x800c0005),
            INET_E_OBJECT_NOT_FOUND = unchecked((int)0x800c0006),
            INET_E_DATA_NOT_AVAILABLE = unchecked((int)0x800c0007),
            INET_E_DOWNLOAD_FAILURE = unchecked((int)0x800c0008),
            INET_E_AUTHENTICATION_REQUIRED = unchecked((int)0x800c0009),
            INET_E_NO_VALID_MEDIA = unchecked((int)0x800c000a),
            INET_E_CONNECTION_TIMEOUT = unchecked((int)0x800c000b),
            INET_E_DEFAULT_ACTION = unchecked((int)0x800c0011),
            INET_E_INVALID_REQUEST = unchecked((int)0x800c000c),
            INET_E_UNKNOWN_PROTOCOL = unchecked((int)0x800c000d),
            INET_E_QUERYOPTION_UNKNOWN = unchecked((int)0x800c0013),
            INET_E_SECURITY_PROBLEM = unchecked((int)0x800c000e),
            INET_E_CANNOT_LOAD_DATA = unchecked((int)0x800c000f),
            INET_E_CANNOT_INSTANTIATE_OBJECT = unchecked((int)0x800c0010),
            INET_E_REDIRECT_FAILED = unchecked((int)0x800c0014),
            INET_E_REDIRECT_TO_DIR = unchecked((int)0x800c0015),
            INET_E_CANNOT_LOCK_REQUEST = unchecked((int)0x800c0016),
            INET_E_USE_EXTEND_BINDING = unchecked((int)0x800c0017),
            INET_E_TERMINATED_BIND = unchecked((int)0x800c0018),
            INET_E_ERROR_FIRST = unchecked((int)0x800c0002),
            INET_E_CODE_DOWNLOAD_DECLINED = unchecked((int)0x800c0100),
            INET_E_RESULT_DISPATCHED = unchecked((int)0x800c0200),
            INET_E_CANNOT_REPLACE_SFP_FILE = unchecked((int)0x800c0300),

            HTTP_COOKIE_DECLINED = 12162,
            //The HTTP cookie was declined by the server.
            HTTP_COOKIE_NEEDS_CONFIRMATION = 12161,
            //The HTTP cookie requires confirmation.
            HTTP_DOWNLEVEL_SERVER = 12151,
            //The server did not return any headers.
            HTTP_HEADER_ALREADY_EXISTS = 12155,
            //The header could not be added because it already exists.
            HTTP_HEADER_NOT_FOUND = 12150,
            //The requested header could not be located.
            HTTP_INVALID_HEADER = 12153,
            //The supplied header is invalid.
            HTTP_INVALID_QUERY_REQUEST = 12154,
            //The request made to HttpQueryInfo is invalid.
            HTTP_INVALID_SERVER_RESPONSE = 12152,
            //The server response could not be parsed.
            HTTP_NOT_REDIRECTED = 12160,
            //The HTTP request was not redirected.
            HTTP_REDIRECT_FAILED = 12156,
            //The redirection failed because either the scheme changed (for example, HTTP to FTP) or all attempts made to redirect failed (default is five attempts).
            HTTP_REDIRECT_NEEDS_CONFIRMATION = 12168
            //The redirection requires user confirmation.
        }

        public enum URLACTION
        {
            URLACTION_MIN = 0x00001000,

            URLACTION_DOWNLOAD_MIN = 0x00001000,
            URLACTION_DOWNLOAD_SIGNED_ACTIVEX = 0x00001001,
            URLACTION_DOWNLOAD_UNSIGNED_ACTIVEX = 0x00001004,
            URLACTION_DOWNLOAD_CURR_MAX = 0x00001004,
            URLACTION_DOWNLOAD_MAX = 0x000011FF,

            URLACTION_ACTIVEX_MIN = 0x00001200,
            URLACTION_ACTIVEX_RUN = 0x00001200,
            URLACTION_ACTIVEX_OVERRIDE_OBJECT_SAFETY = 0x00001201,
            URLACTION_ACTIVEX_OVERRIDE_DATA_SAFETY = 0x00001202,
            URLACTION_ACTIVEX_OVERRIDE_SCRIPT_SAFETY = 0x00001203,
            URLACTION_SCRIPT_OVERRIDE_SAFETY = 0x00001401,
            URLACTION_ACTIVEX_CONFIRM_NOOBJECTSAFETY = 0x00001204,
            URLACTION_ACTIVEX_TREATASUNTRUSTED = 0x00001205,
            URLACTION_ACTIVEX_NO_WEBOC_SCRIPT = 0x00001206,
            URLACTION_ACTIVEX_OVERRIDE_REPURPOSEDETECTION = 0x00001207,
            URLACTION_ACTIVEX_OVERRIDE_OPTIN = 0x00001208,
            URLACTION_ACTIVEX_SCRIPTLET_RUN = 0x00001209,
            URLACTION_ACTIVEX_DYNSRC_VIDEO_AND_ANIMATION = 0x0000120A,
            URLACTION_ACTIVEX_OVERRIDE_DOMAINLIST = 0x0000120B,
            URLACTION_ACTIVEX_CURR_MAX = 0x0000120B,
            URLACTION_ACTIVEX_MAX = 0x000013ff,

            URLACTION_SCRIPT_MIN = 0x00001400,
            URLACTION_SCRIPT_RUN = 0x00001400,
            URLACTION_SCRIPT_JAVA_USE = 0x00001402,
            URLACTION_SCRIPT_SAFE_ACTIVEX = 0x00001405,
            URLACTION_CROSS_DOMAIN_DATA = 0x00001406,
            URLACTION_SCRIPT_PASTE = 0x00001407,
            URLACTION_ALLOW_XDOMAIN_SUBFRAME_RESIZE = 0x00001408,
            URLACTION_SCRIPT_XSSFILTER = 0x00001409,
            URLACTION_SCRIPT_CURR_MAX = 0x00001409,
            URLACTION_SCRIPT_MAX = 0x000015ff,

            URLACTION_HTML_MIN = 0x00001600,
            URLACTION_HTML_SUBMIT_FORMS = 0x00001601,
            URLACTION_HTML_SUBMIT_FORMS_FROM = 0x00001602,
            URLACTION_HTML_SUBMIT_FORMS_TO = 0x00001603,
            URLACTION_HTML_FONT_DOWNLOAD = 0x00001604,
            URLACTION_HTML_JAVA_RUN = 0x00001605,
            URLACTION_HTML_USERDATA_SAVE = 0x00001606,
            URLACTION_HTML_SUBFRAME_NAVIGATE = 0x00001607,
            URLACTION_HTML_META_REFRESH = 0x00001608,
            URLACTION_HTML_MIXED_CONTENT = 0x00001609,
            URLACTION_HTML_INCLUDE_FILE_PATH = 0x0000160A,
            URLACTION_HTML_MAX = 0x000017ff,

            URLACTION_SHELL_MIN = 0x00001800,
            URLACTION_SHELL_INSTALL_DTITEMS = 0x00001800,
            URLACTION_SHELL_MOVE_OR_COPY = 0x00001802,
            URLACTION_SHELL_FILE_DOWNLOAD = 0x00001803,
            URLACTION_SHELL_VERB = 0x00001804,
            URLACTION_SHELL_WEBVIEW_VERB = 0x00001805,
            URLACTION_SHELL_SHELLEXECUTE = 0x00001806,

            URLACTION_SHELL_EXECUTE_HIGHRISK = 0x00001806,
            URLACTION_SHELL_EXECUTE_MODRISK = 0x00001807,
            URLACTION_SHELL_EXECUTE_LOWRISK = 0x00001808,
            URLACTION_SHELL_POPUPMGR = 0x00001809,
            URLACTION_SHELL_RTF_OBJECTS_LOAD = 0x0000180A,
            URLACTION_SHELL_ENHANCED_DRAGDROP_SECURITY = 0x0000180B,
            URLACTION_SHELL_EXTENSIONSECURITY = 0x0000180C,
            URLACTION_SHELL_SECURE_DRAGSOURCE = 0x0000180D,

            URLACTION_SHELL_REMOTEQUERY = 0x0000180E,
            URLACTION_SHELL_PREVIEW = 0x0000180F,

            URLACTION_SHELL_CURR_MAX = 0x0000180F,
            URLACTION_SHELL_MAX = 0x000019ff,

            URLACTION_NETWORK_MIN = 0x00001A00,
            URLACTION_CREDENTIALS_USE = 0x00001A00,
            URLACTION_AUTHENTICATE_CLIENT = 0x00001A01,
            URLACTION_COOKIES = 0x00001A02,
            URLACTION_COOKIES_SESSION = 0x00001A03,
            URLACTION_CLIENT_CERT_PROMPT = 0x00001A04,
            URLACTION_COOKIES_THIRD_PARTY = 0x00001A05,
            URLACTION_COOKIES_SESSION_THIRD_PARTY = 0x00001A06,
            URLACTION_COOKIES_ENABLED = 0x00001A10,
            URLACTION_NETWORK_CURR_MAX = 0x00001A10,
            URLACTION_NETWORK_MAX = 0x00001Bff,

            URLACTION_JAVA_MIN = 0x00001C00,
            URLACTION_JAVA_PERMISSIONS = 0x00001C00,
            URLACTION_JAVA_CURR_MAX = 0x00001C00,
            URLACTION_JAVA_MAX = 0x00001Cff,

            URLACTION_INFODELIVERY_MIN = 0x00001D00,
            URLACTION_INFODELIVERY_NO_ADDING_CHANNELS = 0x00001D00,
            URLACTION_INFODELIVERY_NO_EDITING_CHANNELS = 0x00001D01,
            URLACTION_INFODELIVERY_NO_REMOVING_CHANNELS = 0x00001D02,
            URLACTION_INFODELIVERY_NO_ADDING_SUBSCRIPTIONS = 0x00001D03,
            URLACTION_INFODELIVERY_NO_EDITING_SUBSCRIPTIONS = 0x00001D04,
            URLACTION_INFODELIVERY_NO_REMOVING_SUBSCRIPTIONS = 0x00001D05,
            URLACTION_INFODELIVERY_NO_CHANNEL_LOGGING = 0x00001D06,
            URLACTION_INFODELIVERY_CURR_MAX = 0x00001D06,
            URLACTION_INFODELIVERY_MAX = 0x00001Dff,
            URLACTION_CHANNEL_SOFTDIST_MIN = 0x00001E00,
            URLACTION_CHANNEL_SOFTDIST_PERMISSIONS = 0x00001E05,
            URLACTION_CHANNEL_SOFTDIST_MAX = 0x00001Eff,

            URLACTION_DOTNET_USERCONTROLS = 0x00002005,

            URLACTION_BEHAVIOR_MIN = 0x00002000,
            URLACTION_BEHAVIOR_RUN = 0x00002000,

            URLACTION_FEATURE_MIN = 0x00002100,
            URLACTION_FEATURE_MIME_SNIFFING = 0x00002100,
            URLACTION_FEATURE_ZONE_ELEVATION = 0x00002101,
            URLACTION_FEATURE_WINDOW_RESTRICTIONS = 0x00002102,
            URLACTION_FEATURE_SCRIPT_STATUS_BAR = 0x00002103,
            URLACTION_FEATURE_FORCE_ADDR_AND_STATUS = 0x00002104,
            URLACTION_FEATURE_BLOCK_INPUT_PROMPTS = 0x00002105,
            URLACTION_FEATURE_DATA_BINDING = 0x00002106,
            URLACTION_FEATURE_CROSSDOMAIN_FOCUS_CHANGE = 0x00002107,

            URLACTION_AUTOMATIC_DOWNLOAD_UI_MIN = 0x00002200,
            URLACTION_AUTOMATIC_DOWNLOAD_UI = 0x00002200,
            URLACTION_AUTOMATIC_ACTIVEX_UI = 0x00002201,
            URLACTION_ALLOW_RESTRICTEDPROTOCOLS = 0x00002300,

            URLACTION_ALLOW_APEVALUATION = 0x00002301,

            URLACTION_WINDOWS_BROWSER_APPLICATIONS = 0x00002400,
            URLACTION_XPS_DOCUMENTS = 0x00002401,
            URLACTION_LOOSE_XAML = 0x00002402,
            URLACTION_LOWRIGHTS = 0x00002500,
            URLACTION_WINFX_SETUP = 0x00002600,
            URLACTION_INPRIVATE_BLOCKING = 0x00002700,

            URLACTION_ALLOW_AUDIO_VIDEO = 0x00002701,
            URLACTION_ALLOW_ACTIVEX_FILTERING = 0x00002702,
            URLACTION_ALLOW_STRUCTURED_STORAGE_SNIFFING = 0x00002703,
        }

        public enum tagURLZONE
        {
            URLZONE_PREDEFINED_MIN = 0,
            URLZONE_LOCAL_MACHINE = 0,
            URLZONE_INTRANET = URLZONE_LOCAL_MACHINE + 1,
            URLZONE_TRUSTED = URLZONE_INTRANET + 1,
            URLZONE_INTERNET = URLZONE_TRUSTED + 1,
            URLZONE_UNTRUSTED = URLZONE_INTERNET + 1,
            URLZONE_PREDEFINED_MAX = 999,
            URLZONE_USER_MIN = 1000,
            URLZONE_USER_MAX = 10000,
            URLZONE_ESC_FLAG = 0x100
        }
        #endregion
    }
}
