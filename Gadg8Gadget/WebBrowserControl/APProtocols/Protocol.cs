﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace AsyncPluggableProtocol
{
    public interface IProtocol
    {
        string Name { get; }
        string GetMimeType(string url);
        Stream GetStream(string url);
        string GetPath(string url);
    }

    public class ProtocolFactory : IClassFactory
    {
        object obj;
        private static IInternetSession GetSession()
        {
            IInternetSession session;
            int res = NativeMethods.CoInternetGetSession(0, out session, 0);

            if (res != NativeConstants.S_OK || session == null)
                throw new InvalidOperationException("CoInternetGetSession failed.");

            return session;
        }

        private IProtocol _factory;

        private ProtocolFactory(IProtocol factory)
        {
            _factory = factory;
        }

        public static void Register(string name, IProtocol factory)
        {
            string emptyStr = null;

            IInternetSession session = GetSession();
            try
            {
                Guid handlerGuid = typeof(Protocol).GUID;
                session.RegisterNameSpace(
                    new ProtocolFactory(factory),
                    ref handlerGuid,
                    name,
                    0,
                    ref emptyStr,
                    0);
            }
            finally
            {
                Marshal.ReleaseComObject(session);
                session = null;
            }
        }

        public void LockServer(bool Lock)
        {
        }

        [STAThread]
        public int CreateInstance(IntPtr pUnkOuter, ref Guid riid, out IntPtr ppvObject)
        {
            ppvObject = IntPtr.Zero;

            if (pUnkOuter != IntPtr.Zero)
                return NativeConstants.CLASS_E_NOAGGREGATION;

            if (typeof(IInternetProtocol).GUID.Equals(riid)
                || typeof(IInternetProtocolRoot).GUID.Equals(riid)
                || typeof(IInternetProtocolInfo).GUID.Equals(riid))
            {
                if (obj == null)
                    obj = new Protocol(_factory);

                IntPtr objPtr = Marshal.GetIUnknownForObject(obj);
                IntPtr resultPtr;
                Guid refIid = riid;
                Marshal.QueryInterface(objPtr, ref refIid, out resultPtr);
                ppvObject = resultPtr;
                return NativeConstants.S_OK;
            }

            return NativeConstants.E_NOINTERFACE;
        }
    }

    public class Protocol : IInternetProtocol, IInternetProtocolInfo
    {
        private IProtocol _Protocol;

        public Protocol(IProtocol protocol)
        {
            _Protocol = protocol;
        }

        private byte[] data;
        private int readPos;

        public void Start(string szUrl, IInternetProtocolSink pOIProtSink, IInternetBindInfo pOIBindInfo, PI_FLAGS grfPI, int dwReserved)
        {
            var bytes = new byte[4096];

            try
            {
                using (var ms = new MemoryStream())
                using (var stream = _Protocol.GetStream(szUrl))
                {
                    while (true)
                    {
                        var n = stream.Read(bytes, 0, bytes.Length);

                        if (n == 0)
                        {
                            string mime = _Protocol.GetMimeType(szUrl);
                            if (!string.IsNullOrEmpty(mime))
                                pOIProtSink.ReportProgress((uint)BINDSTATUS.BINDSTATUS_VERIFIEDMIMETYPEAVAILABLE, mime);

                            data = ms.ToArray();
                            readPos = 0;
                            pOIProtSink.ReportData(BSCF.BSCF_DATAFULLYAVAILABLE, (uint)ms.Length, 0);
                            pOIProtSink.ReportResult(NativeConstants.S_OK, 0, "");
                            break;
                        }

                        ms.Write(bytes, 0, n);
                    }
                }
            }
            catch (Exception ex)
            {
                pOIProtSink.ReportResult(NativeConstants.E_FAIL, 0, ex.Message);
            }
        }

        public void Continue(ref PROTOCOLDATA pProtocolData)
        {
        }

        public void Abort(int hrReason, int dwOptions)
        {
        }

        public void Terminate(int dwOptions)
        {
        }

        public void Suspend()
        {
        }

        public void Resume()
        {
        }

        public int Read(IntPtr pv, int cb, out int pcbRead)
        {
            if (readPos >= data.Length)
            {
                pcbRead = 0;
                return NativeConstants.S_FALSE;
            }

            var n = Math.Min(cb, data.Length - readPos);
            Marshal.Copy(data, readPos, pv, n);
            readPos += n;
            pcbRead = n;

            return NativeConstants.S_OK;
        }

        public void Seek(long dlibMove, int dwOrigin, out long plibNewPosition)
        {
            int origin = 0;

            switch (dwOrigin)
            {
                case 0:
                    origin = 0;
                    break;
                case 1:
                    origin = readPos;
                    break;
                case 2:
                    origin = data.Length;
                    break;
            }

            readPos = origin + (int)dlibMove;
            plibNewPosition = readPos;
        }

        public void LockRequest(int dwOptions)
        {
        }

        public void UnlockRequest()
        {
        }

        public int CombineUrl(string pwzBaseUrl, string pwzRelativeUrl, uint dwCombineFlags, IntPtr pwzResult, uint cchResult, out uint pcchResult, uint dwReserved)
        {
            pcchResult = (uint)pwzRelativeUrl.Length;

            if (pwzRelativeUrl.Length > cchResult)
                return NativeConstants.S_FALSE; // buffer too small

            //WriteLPWStr(pwzRelativeUrl, ref pwzResult);
            return NativeConstants.INET_E_DEFAULT_ACTION;
        }

        public int CompareUrl(string pwzUrl1, string pwzUrl2, uint dwCompareFlags)
        {
            return pwzUrl1 == pwzUrl2 ? NativeConstants.S_OK : NativeConstants.S_FALSE;
        }

        public int ParseUrl(string pwzUrl, PARSEACTION ParseAction, uint dwParseFlags, IntPtr pwzResult, uint cchResult, out uint pcchResult, uint dwReserved)
        {
            string result = DoParseUrl(pwzUrl, ParseAction);
            if (result != null)
            {
                pcchResult = (uint)result.Length;

                if (result.Length > cchResult)
                    return NativeConstants.S_FALSE; // Buffer too small

                WriteLPWStr(result, ref pwzResult);
                return NativeConstants.S_OK;
            }

            pcchResult = 0;
            return NativeConstants.INET_E_DEFAULT_ACTION;
        }

        private string DoParseUrl(string pwzUrl, PARSEACTION ParseAction)
        {
            switch (ParseAction)
            {
                case PARSEACTION.PARSE_CANONICALIZE:
                    return pwzUrl;

                case PARSEACTION.PARSE_SECURITY_URL:
                    //return pwzUrl;
                    //return "file:///" + _Protocol.GetPath(pwzUrl);
                    return "x-gadget://localhost/";

                case PARSEACTION.PARSE_SECURITY_DOMAIN:
                    //return pwzUrl;
                    //return "file:///" + _Protocol.GetPath(pwzUrl);
                    return "x-gadget:0000";

                case PARSEACTION.PARSE_SCHEMA:
                    return "x-gadget";

                default:
                    return null;
            }
        }

        public int QueryInfo(string pwzUrl, QUERYOPTION QueryOption, uint dwQueryFlags, IntPtr pBuffer, uint cbBuffer, ref uint pcbBuf, uint dwReserved)
        {
            return NativeConstants.INET_E_DEFAULT_ACTION;
        }

        public static void WriteLPWStr(string value, ref IntPtr targetPtr)
        {
            Marshal.Copy(value.ToCharArray(), 0, targetPtr, value.Length);
            Marshal.WriteInt16(targetPtr, value.Length * 2, 0);
        }
    }
}
