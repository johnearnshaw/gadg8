﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace AsyncPluggableProtocol
{
    public class xGadgetProtocol : IProtocol
    {
        string m_Path;

        public xGadgetProtocol(string path)
        {
            if (!path.EndsWith("\\"))
                path = path + "\\";
            this.m_Path = path;
        }

        public string Name
        {
            get
            {
                return "x-gadget";
            }
        }

        public string Path
        {
            get
            {
                return this.m_Path;
            }
        }

        public string GetPath(string url)
        {
            string file = url.Substring(Name.Length + 4).Replace("/", "\\").TrimStart("\\".ToCharArray());
            return Gadg8Gadget.Helpers.GetLocalizedFilePath(Path, file);
        }

        public Stream GetStream(string url)
        {
            string file = GetPath(url);
            Stream s = new MemoryStream(File.ReadAllBytes(file));
            // Some gadgets load scripts with the defer attribute which causes the scripts to be loaded in the wrong order
            // by our WebBrowser control. Defer is not really necessary so until we find a better fix, we shall strip it out
            // of the html text.
            //if (file.EndsWith(".htm") || file.EndsWith(".html"))
            //{
            //    try
            //    {
            //        s.Position = 0;
            //        StreamReader reader = new StreamReader(s, true);
            //        string text = reader.ReadToEnd();

            //        text = text.Replace("defer=\"defer\"", "");

            //        byte[] byteArray = reader.CurrentEncoding.GetBytes(text);
            //        s = new MemoryStream(byteArray);

            //        reader.Dispose();
            //    }
            //    finally { }
            //}

            s.Position = 0;
            return s;
        }

        public string GetMimeType(string url)
        {
            try
            {
                string mime;
                IntPtr mimeout;
                string path = GetPath(url);

                if (!System.IO.File.Exists(path))
                    throw new FileNotFoundException(url + " not found");

                int MaxContent = (int)new FileInfo(path).Length;
                if (MaxContent > 4096) MaxContent = 4096;
                //if (MaxContent > 256) MaxContent = 256;
                FileStream fs = File.OpenRead(path);

                byte[] buf = new byte[MaxContent];
                fs.Read(buf, 0, MaxContent);
                fs.Close();
                int result = Native.FindMimeFromData(IntPtr.Zero, url, buf, MaxContent, null, 0, out mimeout, 0);

                if (result != 0)
                    throw Marshal.GetExceptionForHR(result);
                mime = Marshal.PtrToStringUni(mimeout);
                Marshal.FreeCoTaskMem(mimeout);

                if (string.IsNullOrEmpty(mime) ||
                    mime == "text/plain" || mime == "application/octet-stream")
                {
                    return GetMimeFromRegistry(url);
                }

                return mime;
            }
            catch
            {
                return GetMimeFromRegistry(url);
            }
        }

        private string GetMimeFromRegistry(string Filename)
        {
            string mime = "application/octetstream";
            string ext = System.IO.Path.GetExtension(Filename).ToLower();
            Microsoft.Win32.RegistryKey rk = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (rk != null && rk.GetValue("Content Type") != null)
                mime = rk.GetValue("Content Type").ToString();
            return mime;
        }
    }
}