﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace AsyncPluggableProtocol
{
    public class gImageProtocol : IProtocol
    {
        //Gadg8Gadget.ThumbnailExtractorVista vistaThumb;
        Gadg8Gadget.ShellThumbnail xpThumb;
        bool isVista = false;

        public gImageProtocol()
        {
            isVista = Gadg8Gadget.Helpers.IsVistaOrHigher();

            //if (isVista)
            //{
            //    vistaThumb = new Gadg8Gadget.ThumbnailExtractorVista();
            //    vistaThumb.Init();
            //}
            //else
            //{
                xpThumb = new Gadg8Gadget.ShellThumbnail();
            //}
        }

        public string Name
        {
            get
            {
                return "gimage";
            }
        }

        public string GetPath(string url)
        {
            string file = url.Substring(Name.Length + 4).Replace("/", "\\").TrimStart("\\".ToCharArray());
            file = Uri.UnescapeDataString(file);
            if (file.Contains("?"))
                file = file.Substring(0, file.IndexOf("?"));

            return file;
        }

        private Size GetImageSize(string url)
        {
            Size size = new Size(-1, -1);
            string[] paramArray;
            string urlParams = url.Substring(Name.Length + 4).Replace("/", "\\").TrimStart("\\".ToCharArray());
            int width, height;

            urlParams = Uri.UnescapeDataString(urlParams);
            
            if (urlParams.Contains("?"))
            {
                urlParams = urlParams.Substring(urlParams.IndexOf("?"), urlParams.Length - urlParams.IndexOf("?")).Replace("?", "");

                paramArray = urlParams.Split('&');

                foreach (string str in paramArray)
                {
                    string s = str.ToLower();

                    if (s.StartsWith("width="))
                    {
                        if (int.TryParse(s.Replace("width=", ""), out width))
                            size.Width = width;
                    }
                    else if (s.StartsWith("height="))
                    {
                        if (int.TryParse(s.Replace("height=", ""), out height))
                            size.Height = height;
                    }
                }
            }

            return size;
        }

        public Stream GetStream(string url)
        {
            string file = GetPath(url);
            Size s = GetImageSize(url);
            Bitmap b;

            if (s.Height < 0 && s.Width < 0)
            {
                s.Height = 50;
                s.Width = 50;
            }

            //if (isVista)
            //{
            //    b = vistaThumb.ExtractThumbnail(file, s, Gadg8Gadget.ThumbnailExtractorVista.SIIGBF.SIIGBF_RESIZETOFIT);
            //}
            //else
            //{
                b = xpThumb.GetThumbnail(file, s.Width, s.Height);
            //}

            MemoryStream m = new MemoryStream();

            if (b == null)
                return m;

            List<ImageCodecInfo> encoders = new List<ImageCodecInfo>(ImageCodecInfo.GetImageEncoders());
            var imageCodecInfo = encoders.Find(encoder => encoder.MimeType == "image/png");

            if (imageCodecInfo == null)
            {
                return m;
            }

            EncoderParameters param = new EncoderParameters(1);
            param.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

            b.Save(m, imageCodecInfo, param);
            b.Dispose();

            m.Position = 0;
            return m;
        }

        private Stream ScaleImage(string imgPath, int maxWidth, int maxHeight)
        {
            try
            {
                Stream imgStream = new MemoryStream(File.ReadAllBytes(imgPath));

                Image destBmp;
                Image sourceBmp = Image.FromStream(imgStream);

                imgStream.Close();

                //if (sourceBmp.Width <= maxWidth & sourceBmp.Height <= maxHeight)
                //{
                //    destBmp = new Bitmap(sourceBmp.Width, sourceBmp.Height);
                //}
                //else
                //{
                    float percentWidth = maxWidth / sourceBmp.Width;
                    float percentHeight = maxHeight / sourceBmp.Height;
                    if (percentHeight < percentWidth)
                    {
                        maxWidth = (int)Math.Ceiling(sourceBmp.Width * percentHeight);
                    }
                    else
                    {
                        maxHeight = (int)Math.Ceiling(sourceBmp.Height * percentWidth);
                    }
                    destBmp = new Bitmap(maxWidth, maxHeight);
                //}

                Graphics g = Graphics.FromImage(destBmp);
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.DrawImage(sourceBmp, 0, 0, destBmp.Width, destBmp.Height);

                imgStream = new MemoryStream();

                destBmp.Save(imgStream, System.Drawing.Imaging.ImageFormat.Png);

                sourceBmp.Dispose();
                g.Dispose();
                destBmp.Dispose();

                MessageBox.Show(imgStream.Length.ToString());

                return imgStream;
            }
            catch
            {
                return new System.IO.MemoryStream();
            }
        }

        public string GetMimeType(string url)
        {
            return "image/png";
        }
    }
}