﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gadg8Gadget.WebBrowserControl
{

    // CLSIDs & IIDs
    public sealed class IidClsid
    {
        public static Guid IID_IProfferService = new Guid("cb728b20-f786-11ce-92ad-00aa00a74cd0");

        public static Guid IID_IHttpSecurity = new Guid("79eac9d7-bafa-11ce-8c82-00aa004ba90b");
        public static Guid IID_IInternetSecurityManager = new Guid("79EAC9EE-BAF9-11CE-8C82-00AA004BA90B");

        public static Guid IID_IElementBehaviorFactory = new Guid("3050f429-98b5-11cf-bb82-00aa00bdce0b");
        public static Guid IID_IHostBehaviorInit = new Guid("3050f842-98b5-11cf-bb82-00aa00bdce0b");
        public static Guid IID_IElementNamespaceTable = new Guid("3050F670-98B5-11CF-BB82-00AA00BDCE0B");
    }
}
