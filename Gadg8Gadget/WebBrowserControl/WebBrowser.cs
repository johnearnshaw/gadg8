﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using mshtml;
using System.Security;
//using System.Security.Permissions;
using System.Windows.Forms;
using System.Globalization;

namespace Gadg8Gadget.WebBrowserControl
{
    //[PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    public class WebBrowser : System.Windows.Forms.WebBrowser, IServiceProvider, IOleClientSite, IDocHostShowUI, IDocHostUIHandler, IHostBehaviorInit, IInternetSecurityManager, DWebBrowserEvents2
    {
        public GElementNamespaceFactory NamespaceFactory;
        private string CSS;
        WindowExternal External;

        AxHost.ConnectionPointCookie cookie;

        #region Events
        public delegate void EscapeKeyPressEventHandler(object sender, EventArgs e);

        // Events
        public event EscapeKeyPressEventHandler EscapeKeyPress;

        #region Invoker Methods
        protected virtual void InvokeEscapeKeyPress(EventArgs e)
        {

            EscapeKeyPressEventHandler h = EscapeKeyPress;
            if (h != null)
            {
                h(this, e);
            }
        }
        #endregion
        #endregion

        public WebBrowser(GadgetSystemNamespace.SystemNamespace systemNamespace, GadgetGadg8Namespace.Gadg8Namespace gadg8Namespace, xGadgetWindow parentWindow, string css = "")
        {
            base.CreateSink();
            
            // Tell GarbageCollector to keep the WebBrowser alive. Because we use native methods to add the controls to our BasicWindow
            // the managed reference count is not increased so after some length of time .NET will kill it.
            GC.KeepAlive(this);

            // SystemNamespace is exposed to the html document in using our WindowExternal class in IDocHostUIHandler.GetExternal.
            this.External = new WindowExternal(this, systemNamespace, gadg8Namespace);

            // Navigate to a page to initialize the underlying activex object and create an internet session within the process so we
            // can use CoInternetGetSession and call get GetServiceProvider etc... with no issues.
            this.Navigate("about:blank");

            // Hook web browser events for the underlying activex object.
            cookie = new AxHost.ConnectionPointCookie(this.ActiveXInstance, this, typeof(DWebBrowserEvents2));

            // Add an ElementNamespaceFactory to expose Windows Sidebar's custom elements g:background, g:image and g:text.
            NamespaceFactory = new GElementNamespaceFactory(parentWindow);

            // Just incase we want to add more css to the default css used by our WebBrowser control.
            this.CSS = css;

            SetProfferService();
            SetClientSite();
            DisableNavSounds();

            // Register the x-gadget protocol used by Windows Sidebar. and set the resolve path to the gadget's path.
            AsyncPluggableProtocol.ProtocolFactory.Register("x-gadget", new AsyncPluggableProtocol.xGadgetProtocol(systemNamespace.Gadget.path));
            
            // Register the gimage protocol used by gadgets such as MS Slide Show.
            AsyncPluggableProtocol.ProtocolFactory.Register("gimage", new AsyncPluggableProtocol.gImageProtocol());

            // Surpress ie shortcuts.
            this.PreviewKeyDown += new PreviewKeyDownEventHandler(PreviewKeyDownHandler);
        }

        protected override void OnDocumentCompleted(WebBrowserDocumentCompletedEventArgs e)
        {
            // Due to an issue with IElementBehaviorFactory not being queried in our IServiceProvider.QueryService method,
            // I added this nasty hack which adds a html select to about:blank, focuses it then tells the Internet Explorer_Server
            // (child of WebBrowser control) that spacebar has been pressed using SendMessage.
            // See here for reference to the question which Andy E asked on stackoverflow:
            // http://stackoverflow.com/questions/22095747/iserviceprovider-ielementbehaviorfactory-not-queried-until-an-unhandled-javascri
            // Until someone finds the real fix for this issue, this dirty hack will have to do :(
            if (this.Url.ToString() == "about:blank")
            {
                this.Document.Body.InnerHtml =
                    "<select id=\"select\"><option>1</option><option>2</option></select>";
                HtmlElement select = this.Document.Body.Children[0];
                select.Focus();

                IHTMLElement2 select2 = (IHTMLElement2)((IHTMLDocument3)this.Document.DomDocument).getElementById("select");
                IHTMLRect selrect = select2.getBoundingClientRect();

                object[] handles = Helpers.GetIEServerWindowHandles(this.Handle).ToArray();

                if (handles.Length == 0)
                {
                    IntPtr handle = Helpers.GetIEServerWindowHandle2(this.Handle);
                    handles = new object[] { handle };
                }

                foreach (IntPtr h in handles)
                {
                    // Send space once to open the select, then enter key once to close. - only works in newer IE (9 onwards I think)
                    //Helpers.SendKeyPress(h, Keys.Space);
                    //Helpers.SendKeyPress(h, Keys.Enter);

                    // Alt+Down opens select on earlier versions of IE but can't get it to take the Alt key with SendMessage
                    //Helpers.PressAltAndDown(h);

                    // Send mouse click with SendMessage to open and close the select menu
                    Helpers.SendMouseClick(h, new System.Drawing.Point(selrect.left, selrect.top));
                    Helpers.SendMouseClick(h, new System.Drawing.Point(selrect.left, selrect.top));
                }
            }
            
            base.OnDocumentCompleted(e);
        }
        
        // Implement DWebBrowserEvents2.BeforeNavigate2 to only allow about:blank etc... x-gadget and file urls to be shown
        // in the WebBrowser control. Microsoft's Sidebar would open all other urls in a new IE window but we open them in
        // the default browser.
        #region DWebBrowserEvents2
        public void BeforeNavigate2(object pDisp, ref string URL, ref int Flags, ref object TargetFrameName, ref object PostData, ref object Headers, ref bool Cancel)
        {
            StringComparison c = StringComparison.InvariantCultureIgnoreCase;

            if (Flags == 64) // 64 = User Action
            {
                if (!URL.StartsWith("about:", c) && !URL.StartsWith("x-gadget:///", c) && !URL.StartsWith("file://", c)
                     && !URL.StartsWith("javascript:", c) && URL != "#")
                {
                    Gadg8Gadget.Helpers.OpenLink(URL);
                    Cancel = true;
                }
            }
        }
        #endregion

        protected override void OnNavigating(WebBrowserNavigatingEventArgs e)
        {
            AddScripts();
            base.OnNavigating(e);
        }

        protected override void OnNavigated(WebBrowserNavigatedEventArgs e)
        {
            AddScripts();
            base.OnNavigated(e);
        }

        private void AddScripts()
        {
            if (this.Document != null)
            {
                mshtml.IHTMLDocument2 doc = (IHTMLDocument2)this.Document.DomDocument;

                // Add System and Gadg8 namespaces as javascript objects so gadgets can communicate with our app like they do in
                // Windows Sidebar.
                doc.parentWindow.execScript("System = window.external.SystemNamespace;"
                                          + "Gadg8 = window.external.Gadg8Namespace;");

                //doc.parentWindow.execScript("window.showModalDialog = window.external.showModalDialog"
                //                          + "window.showModelessDialog = window.external.showModelessDialog");

                // Override native XMLHTTPRequest and new ActiveXObject to allow cross domain requests which seems to be a bug in the
                // WebBrowser control while using a custom protocol. Any help getting this to work natively would be appreciated.

                doc.parentWindow.execScript("window.XMLHttpRequest = function () {"
                                          + "    if (this instanceof XMLHttpRequest)"
                                          + "        return window.external.ActiveX(\"Microsoft.XMLHTTP\");"
                                          + "    else"
                                          + "        throw new TypeError('XMLHttpRequest constructor called without new operator');"
                                          + "};"
                                          + "window.ActiveXObject = function (prg) {"
                                          + "    if (this instanceof ActiveXObject)"
                                          + "        return window.external.ActiveX(prg);"
                                          + "    else"
                                          + "        throw new TypeError('ActiveXObject constructor called without new operator');"
                                          + "}");
            }
        }
        public IServiceProvider GetDocumentServiceProvider()
        {
            if (this.Document != null)
            {
                return (IServiceProvider)this.Document.DomDocument;
            }
            return null;
        }

        public IServiceProvider GetAXServiceProvider()
        {
            if (this.ActiveXInstance != null)
            {
                return (IServiceProvider)this.ActiveXInstance;
            }
            return null;
        }

        private void SetProfferService()
        {
            uint cookie = 0;
            IntPtr profferServicePtr = new IntPtr();
            IServiceProvider prov = this.GetAXServiceProvider();

            if (prov != null)
            {
                prov.QueryService(IidClsid.IID_IProfferService, IidClsid.IID_IProfferService, ref profferServicePtr);
                if (profferServicePtr != IntPtr.Zero)
                {
                    IProfferService prof = (IProfferService)Marshal.GetObjectForIUnknown(profferServicePtr);

                    prof.ProfferService(typeof(IServiceProvider).GUID, (IServiceProvider)this, ref cookie);
                    prof.ProfferService(IidClsid.IID_IElementBehaviorFactory, (IServiceProvider)this, ref cookie);
                    prof.ProfferService(IidClsid.IID_IHostBehaviorInit, (IServiceProvider)this, ref cookie);
                    prof.ProfferService(IidClsid.IID_IElementNamespaceTable, (IServiceProvider)this, ref cookie);
                    prof.ProfferService(IidClsid.IID_IHttpSecurity, (IServiceProvider)this, ref cookie);
                    prof.ProfferService(IidClsid.IID_IInternetSecurityManager, (IServiceProvider)this, ref cookie);
                }

                ////TEST REMOVE:
                //IntPtr dummy = IntPtr.Zero;
                //prov.QueryService(IidClsid.IID_IHostBehaviorInit, IidClsid.IID_IHostBehaviorInit, dummy);
            }
        }

        private void SetClientSite()
        {
            IOleObject o = (IOleObject)this.ActiveXInstance;
            o.SetClientSite(this);
        }

        private bool DisableNavSounds()
        {
            //Disable Navigation Sounds with CoInternetSetFeatureEnabled
            Native.INTERNETFEATURELIST featureToEnable = Native.INTERNETFEATURELIST.FEATURE_DISABLE_NAVIGATION_SOUNDS;

            if (Native.CoInternetSetFeatureEnabled(featureToEnable, Native.SET_FEATURE_ON_PROCESS, true) == 0)
            {
                // Check to make sure that the API worked as expected
                if (Native.CoInternetIsFeatureEnabled(featureToEnable, Native.SET_FEATURE_ON_PROCESS) != 0)
                {
                    return true;
                }
                return false;
            }
            else
            {
                return false;
            }
        }

        public void SetFontZoom(int zoomLevel)
        {
            if (zoomLevel > 4)
            {
                zoomLevel = 4;
            }
            else if (zoomLevel < 0)
            {
                zoomLevel = 0;
            }

            IntPtr zoom = (IntPtr)4;
            var wb = this.ActiveXInstance.GetType();
            object o = 0;

            wb.InvokeMember("ExecWB", System.Reflection.BindingFlags.InvokeMethod, null, this.ActiveXInstance, new[] { Native.OLECMDID.OLECMDID_ZOOM, Native.OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER, o, o });
        }

        // Slave the WebBrowserShortcutsEnabled setting based on keys type by user.
        // It allows fine grained control on which keys are allowed to be used without giving
        // too much shortcuts possibility (which could mess up the control usage )
        private void PreviewKeyDownHandler(object sender, PreviewKeyDownEventArgs e)
        {
            // Fire our Escape key event so gadgets can close the flyout if it's open.
            if (e.KeyData == Keys.Escape)
                InvokeEscapeKeyPress(new EventArgs());

            switch (e.KeyData)
            {
                // Browser special keys
                case Keys.BrowserBack:
                case Keys.BrowserFavorites:
                case Keys.BrowserForward:
                case Keys.BrowserHome:
                case Keys.BrowserRefresh:
                case Keys.BrowserSearch:
                case Keys.BrowserStop:
                // Fullscreen
                case Keys.F11:
                // Homepage
                case Keys.Alt | Keys.Home:
                // Next
                case Keys.Alt | Keys.Right:
                // Back
                case Keys.Alt | Keys.Left:
                // Shortcut menu for link
                case Keys.Shift | Keys.F10:
                // Next frame
                case Keys.Control | Keys.Tab:
                case Keys.F6:
                // Previous frame
                case Keys.Control | Keys.Shift | Keys.Tab:
                // Find
                case Keys.Control | Keys.F:
                // Refresh
                case Keys.F5:
                case Keys.Control | Keys.F5:
                case Keys.Control | Keys.R:
                // Stop page loading
                case Keys.Escape:
                // New location
                case Keys.Control | Keys.O:
                case Keys.Control | Keys.L:
                // New window
                case Keys.Control | Keys.N:
                // Close current window
                case Keys.Control | Keys.W:
                // Save page
                case Keys.Control | Keys.S:
                // Print page
                case Keys.Control | Keys.P:
                // Open search box
                case Keys.Control | Keys.E:
                // Open favorites box
                case Keys.Control | Keys.I:
                // Open history box
                case Keys.Control | Keys.H:
                // Add to favorites
                case Keys.Control | Keys.D:
                // Open organize favorites
                case Keys.Control | Keys.B:
                    ((WebBrowser)sender).WebBrowserShortcutsEnabled = false;
                    break;

                default:
                    ((WebBrowser)sender).WebBrowserShortcutsEnabled = true;
                    break;
            }
        }

        #region IOleClientSite Members
        public int SaveObject()
        {
            //return Native.E_NOTIMPL;
            return Native.S_OK;
        }

        public int GetMoniker(int dwAssign, int dwWhichMoniker, ref object ppmk)
        {
            //ppmk = null;
            return Native.E_NOTIMPL;
        }

        public int GetContainer(ref object ppContainer)
        {
            //ppContainer = null;
            return Native.E_NOTIMPL;
        }

        public int ShowObject()
        {
            return Native.S_OK;
        }

        public int OnShowWindow(bool fShow)
        {
            //return Native.E_NOTIMPL;
            return Native.S_OK;
        }

        public int RequestNewObjectLayout()
        {
            return Native.E_NOTIMPL;
            //return Native.S_FALSE;
        }
        #endregion

        #region IShowDocHostUI Members
        public int ShowMessage(IntPtr hwnd, string lpstrText, string lpstrCaption, uint dwType, string lpstrHelpFile, uint dwHelpContext, ref int lpResult)
        {
            // Return S_OK so are not displayed by the WebBrowser control. This does not surpress vbscript MsgBox and InputBox
            // or javascript prompt. This surpresses alert and confirm like Windows Sidebar does.
            return Native.S_OK;
        }

        public int ShowHelp(IntPtr hwnd, string pszHelpFile, uint uCommand, uint dwData, tagPOINT ptMouse, object pDispatchObjectHit)
        {
            //TODO:
            // Show our own help instead of Internet Explorers default help. For now we just surpress this.
            return Native.S_OK;
        }
        #endregion

        #region IDocHostUIHandler Members
        public int ShowContextMenu(uint dwID, ref tagPOINT pt, object pcmdtReserved, object pdispReserved)
        {
            // Returning S_OK is like returning true, this stops the default menu from showing. We handle contextmenu from within
            // the document events.
            return Native.S_OK;
        }

        public int GetHostInfo(ref Native.DOCHOSTUIINFO info)
        {
            // Set some flags to change the default behavior of the WebBrowser control.
            info.dwFlags = (uint)Native.DOCHOSTUIFLAG.DISABLE_SCRIPT_INACTIVE | (uint)Native.DOCHOSTUIFLAG.NO3DBORDER |
                (uint)Native.DOCHOSTUIFLAG.NO3DOUTERBORDER | (uint)Native.DOCHOSTUIFLAG.SCROLL_NO | (uint)Native.DOCHOSTUIFLAG.THEME;
            info.cbSize = Convert.ToUInt32(Marshal.SizeOf(info));

            // We add some default css to change the curser from I-Beam to pointer on the document body etc...
            info.pchHostCss = "body { cursor:default; min-width:20px; min-height:20px; }" + this.CSS;
            // Return S_OK to say we have handled GetHostInfo.
            return Native.S_OK;
        }

        public int ShowUI(int dwID, IOleInPlaceActiveObject activeObject, IOleCommandTarget commandTarget, IOleInPlaceFrame frame, IOleInPlaceUIWindow doc)
        {
            return Native.S_FALSE;
        }

        public int HideUI()
        {
            return Native.S_FALSE;
        }

        public int UpdateUI()
        {
            return Native.S_FALSE;
        }

        public int EnableModeless(bool fEnable)
        {
            return Native.S_FALSE;
        }

        public int OnDocWindowActivate(bool fActivate)
        {
            return Native.S_FALSE;
        }

        public int OnFrameWindowActivate(bool fActivate)
        {
            return Native.S_FALSE;
        }

        public int ResizeBorder(ref tagRECT rect, IOleInPlaceUIWindow doc, bool fFrameWindow)
        {
            return Native.S_FALSE;
        }

        // Overrides the default WebBrowserShortcutsEnabled behavior.
        public int TranslateAccelerator(ref Native.tagMSG msg, ref Guid group, uint nCmdID)
        {
            if (this.WebBrowserShortcutsEnabled)
                return Native.S_FALSE;

            System.Diagnostics.Debug.WriteLine("translate accelerator: msg=" + msg.ToString() + " nCMDID=" + nCmdID);
            return Native.S_OK;
        }

        public int GetOptionKeyPath(ref string pbstrKey, uint dw)
        {
            return Native.S_FALSE;
        }

        public int GetDropTarget(IDropTarget pDropTarget, ref IDropTarget ppDropTarget)
        {
            return Native.S_FALSE;
        }

        public int GetExternal(ref object ppDispatch)
        {
            // Return our WindowExternal with SystemNamespace so we can set the "System" namespace that gadgets use to communicate with
            // our application.
            ppDispatch = External;
            return Native.S_OK;
        }

        public int TranslateUrl(uint dwTranslate, string strURLIn, ref string pstrURLOut)
        {
            return Native.S_FALSE;
        }

        public int FilterDataObject(System.Runtime.InteropServices.ComTypes.IDataObject pDO, ref System.Runtime.InteropServices.ComTypes.IDataObject ppDORet)
        {
            return Native.S_FALSE;
        }
        #endregion

        //#region IElementBehaviorFactory Members
        //public IElementBehavior FindBehavior(string bstrBehavior, string bstrBehaviorUrl, IElementBehaviorSite pSite)
        //{
        //    throw new NotImplementedException();
        //}
        //#endregion

        #region IHostBehaviorInit Members
        public void PopulateNamespaceTable()
        {
            try
            {
                IServiceProvider serviceProvider = GetDocumentServiceProvider();
                if (serviceProvider != null)
                {

                    IntPtr pTable = IntPtr.Zero;

                    serviceProvider.QueryService(ref IidClsid.IID_IElementNamespaceTable, ref IidClsid.IID_IElementNamespaceTable, ref pTable);
                    IElementNamespaceTable table = (IElementNamespaceTable)Marshal.GetObjectForIUnknown(pTable);

                    object factory = NamespaceFactory;
                    table.AddNamespace("g", "", (int)Native.ELEMENTNAMESPACE_FLAGS.ELEMENTNAMESPACEFLAGS_QUERYFORUNKNOWNTAGS, ref factory);

                    Marshal.Release(pTable);
                }
            }
            catch (Exception e)
            {
                //System.Windows.Forms.MessageBox.Show("Unable to initialize g namespace. " + e.Message);
                throw new Exception("Unable to initialize g namespace.", e);
            }
        }
        #endregion

        #region IInternetSecurityManager Members
        int IInternetSecurityManager.SetSecuritySite(IntPtr pSite)
        {
            return (int)Native.WinInetErrors.INET_E_DEFAULT_ACTION;
        }

        int IInternetSecurityManager.GetSecuritySite(ref IntPtr pSite)
        {
            return (int)Native.WinInetErrors.INET_E_DEFAULT_ACTION;
        }

        int IInternetSecurityManager.MapUrlToZone(string pwszUrl, ref uint pdwZone, uint dwFlags)
        {
            //if (pwszUrl.StartsWith("x-gadget:///", StringComparison.InvariantCultureIgnoreCase))
            //{
            //    //All x-gadget URLs are on the local machine - most trusted and return S_OK;
            //    pdwZone = (uint)Native.tagURLZONE.URLZONE_TRUSTED;
            //    return Native.S_OK;
            //}
            return (int)Native.WinInetErrors.INET_E_DEFAULT_ACTION;
        }

        private const string strSecurityId = @"https:www.mysite.com\2\0\0"; //"file:0000"; //"none:localhost0000";
        int IInternetSecurityManager.GetSecurityId(string pwszUrl, IntPtr pbSecurityId, ref uint pcbSecurityId, ref uint dwReserved)
        {
            //if (pwszUrl.StartsWith("x-gadget:///", StringComparison.InvariantCultureIgnoreCase))
            //{
                if (pcbSecurityId >= 512)
                {
                    byte[] bytes = new byte[strSecurityId.Length * sizeof(char)];
                    System.Buffer.BlockCopy(strSecurityId.ToCharArray(), 0, bytes, 0, bytes.Length);

                    Marshal.Copy(bytes, 0, pbSecurityId, bytes.Length);
                    pcbSecurityId = (uint)bytes.Length; //+4
                    return Native.S_OK;
                }
            //}
            return (int)Native.WinInetErrors.INET_E_DEFAULT_ACTION;
        }

        //
        // MSDN:
        // The current list of URLACTION that will not be passed to the custom security manager
        // in most circumstances by Internet Explorer 5 are:
        // URLACTION_SHELL_FILE_DOWNLOAD 
        // URLACTION_COOKIES 
        // URLACTION_JAVA_PERMISSIONS 
        // URLACTION_SCRIPT_PASTE 
        // There is no workaround for this problem. The behavior for the URLACTION can only be
        // changed for all browser clients on the system by altering the security zone settings
        // from Internet Options.
        // 

        int IInternetSecurityManager.ProcessUrlAction(string pwszUrl, uint dwAction, IntPtr pPolicy, uint cbPolicy, IntPtr pContext, uint cbContext, uint dwFlags, uint dwReserved)
        {
            if ((Native.URLACTION)dwAction == Native.URLACTION.URLACTION_CROSS_DOMAIN_DATA)
                MessageBox.Show("url: " + pwszUrl);

            if (pwszUrl.StartsWith("x-gadget:///", StringComparison.InvariantCultureIgnoreCase))
            {
                switch ((Native.URLACTION)dwAction)
                {
                    case Native.URLACTION.URLACTION_ACTIVEX_RUN:
                    case Native.URLACTION.URLACTION_DOTNET_USERCONTROLS:
                    case Native.URLACTION.URLACTION_ACTIVEX_CONFIRM_NOOBJECTSAFETY:
                        pPolicy = (IntPtr)Native.URLPOLICY_ALLOW;
                        return Native.S_OK;

                    case Native.URLACTION.URLACTION_BEHAVIOR_RUN:
                        pPolicy = (IntPtr)Native.URLPOLICY_ALLOW;
                        return Native.S_OK;

                    case Native.URLACTION.URLACTION_DOWNLOAD_SIGNED_ACTIVEX:
                    case Native.URLACTION.URLACTION_DOWNLOAD_UNSIGNED_ACTIVEX:
                        pPolicy = (IntPtr)Native.URLPOLICY_ALLOW;
                        return Native.S_OK;

                    case Native.URLACTION.URLACTION_CROSS_DOMAIN_DATA:
                        pPolicy = (IntPtr)Native.URLPOLICY_ALLOW;
                        return Native.S_OK;

                    case Native.URLACTION.URLACTION_HTML_MIXED_CONTENT:
                        pPolicy = (IntPtr)Native.URLPOLICY_ALLOW;
                        return Native.S_OK;

                    case Native.URLACTION.URLACTION_AUTOMATIC_ACTIVEX_UI:
                        pPolicy = (IntPtr)Native.URLPOLICY_ALLOW;
                        return Native.S_OK;

                    default:
                        //return (int)Native.WinInetErrors.INET_E_DEFAULT_ACTION;
                        pPolicy = (IntPtr)Native.URLPOLICY_ALLOW;
                        return Native.S_OK;
                }
            }

            return (int)Native.WinInetErrors.INET_E_DEFAULT_ACTION;
        }


        int IInternetSecurityManager.QueryCustomPolicy(string pwszUrl, ref Guid guidKey, ref IntPtr ppPolicy, ref uint pcbPolicy, IntPtr pContext, uint cbContext, uint dwReserved)
        {
            return (int)Native.WinInetErrors.INET_E_DEFAULT_ACTION;
        }

        int IInternetSecurityManager.SetZoneMapping(uint dwZone, string lpszPattern, uint dwFlags)
        {
            return (int)Native.WinInetErrors.INET_E_DEFAULT_ACTION;
        }

        int IInternetSecurityManager.GetZoneMappings(uint dwZone, ref IEnumString ppenumString, uint dwFlags)
        {
            return (int)Native.WinInetErrors.INET_E_DEFAULT_ACTION;
        }
        #endregion

        #region IServiceProvider Members
        int IServiceProvider.QueryService(ref Guid guidService, ref Guid riid, ref IntPtr ppvObject)
        {
            int hr = Native.E_NOINTERFACE;
            ppvObject = IntPtr.Zero;

            if (guidService == IidClsid.IID_IHostBehaviorInit ||
                guidService == IidClsid.IID_IInternetSecurityManager)
            {
                IntPtr unk = Marshal.GetIUnknownForObject(this);
                try
                {
                    hr = Marshal.QueryInterface(unk, ref riid, out ppvObject);
                }
                finally
                {
                    Marshal.Release(unk);
                }
            }
            else if (guidService == IidClsid.IID_IElementBehaviorFactory ||
                riid == IidClsid.IID_IElementBehaviorFactory)
            {
                hr = Native.S_OK;
            }

            return hr;
        }
        #endregion
    }
}