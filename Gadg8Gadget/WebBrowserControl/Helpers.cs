﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Gadg8Gadget.WebBrowserControl
{
    public class Helpers
    {
        public delegate void dGetWindowHandle(IntPtr handleWindow, ArrayList handles);
        public static ArrayList GetIEServerWindowHandles(IntPtr handle)
        {
            var windowHandles = new ArrayList();
            Helpers.dGetWindowHandle callBackPtr = GetIEServerWindowHandle;
            Native.EnumChildWindows(handle, callBackPtr, windowHandles);

            return windowHandles;
        }

        private static void GetIEServerWindowHandle(IntPtr windowHandle, ArrayList windowHandles)
        {
            int ret;
            // Pre-allocate 25 characters, 256 characters is the maximum class name length but we only need to check against
            // "Internet Explorer_Server".
            StringBuilder className = new StringBuilder(25);

            // Get the window class name and check to see if it is an internet explorer server.
            ret = Native.GetClassName(windowHandle, className, className.Capacity);
            if (ret != 0)
            {
                //MessageBox.Show(className.ToString());

                if ((string.Compare(className.ToString(), "Internet Explorer_Server", true,
                    System.Globalization.CultureInfo.InvariantCulture) == 0))
                    windowHandles.Add(windowHandle);
            }
        }

        public static IntPtr GetIEServerWindowHandle2(IntPtr parent)
        {
            return FindWindowRecursive(parent, "Internet Explorer_Server", null);
        }

        private static IntPtr FindWindowRecursive(IntPtr parent, string windowClass, string windowCaption)
        {
            var found = Native.FindWindowEx(parent, IntPtr.Zero, windowClass, windowCaption);
            if (found != IntPtr.Zero)
            {
                return found;
            }

            var child = Native.FindWindowEx(parent, IntPtr.Zero, null, null);
            while (child != IntPtr.Zero)
            {
                found = FindWindowRecursive(child, windowClass, windowCaption);
                if (found != IntPtr.Zero)
                {
                    return found;
                }
                child = Native.GetNextWindow(child, 2);
            }
            return IntPtr.Zero;
        }

        public static void SendKeyPress(IntPtr handle, Keys key)
        {
            Native.SendMessage(handle, (uint)Gadg8Gadget.Native.WindowsMessageFlags.WM_KEYDOWN, (IntPtr)key, (IntPtr)0);
            Native.SendMessage(handle, (uint)Gadg8Gadget.Native.WindowsMessageFlags.WM_KEYUP, (IntPtr)key, (IntPtr)0);
        }

        public static void PressAltAndDown(IntPtr handle)
        {
            Native.SendMessage(handle, (uint)Gadg8Gadget.Native.WindowsMessageFlags.WM_KEYDOWN, (IntPtr)Gadg8Gadget.Native.VK_MENU, IntPtr.Zero);
            Native.SendMessage(handle, (uint)Gadg8Gadget.Native.WindowsMessageFlags.WM_KEYDOWN, (IntPtr)Gadg8Gadget.Native.VK_DOWN, IntPtr.Zero);
            Native.SendMessage(handle, (uint)Gadg8Gadget.Native.WindowsMessageFlags.WM_KEYUP, (IntPtr)Gadg8Gadget.Native.VK_DOWN, IntPtr.Zero);
            Native.SendMessage(handle, (uint)Gadg8Gadget.Native.WindowsMessageFlags.WM_KEYUP, (IntPtr)Gadg8Gadget.Native.VK_MENU, IntPtr.Zero);
        }

        public static void SendMouseClick(IntPtr handle, Point pos)
        {
            Native.SendMessage(handle, (int)Gadg8Gadget.Native.WindowsMessageFlags.WM_LBUTTONDOWN, (IntPtr)0x1,
                (IntPtr)((pos.Y << 16) | (pos.X & 0xFFFF)));

            Native.SendMessage(handle, (int)Gadg8Gadget.Native.WindowsMessageFlags.WM_LBUTTONUP, (IntPtr)0x1,
                        (IntPtr)((pos.Y << 16) | (pos.X & 0xFFFF)));
        }
    }
}
