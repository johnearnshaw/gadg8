﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Ipc;
using System.Security.Permissions;

namespace Gadg8Gadget
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    public class GadgetClient : MarshalByRefObject
    {
        public override object InitializeLifetimeService()
        {
            return null;
        }

        public bool CreateChannelSink(int portNumber)
        {
            string addr = "ipc://localhost:" + portNumber + "/GadgetHost.rem";

            // Create the channel.
            IpcChannel channel = new IpcChannel();

            // Register the channel.
            System.Runtime.Remoting.Channels.ChannelServices.RegisterChannel(channel, true);

            // Register as client for remote object.
            System.Runtime.Remoting.WellKnownClientTypeEntry remoteType =
                new System.Runtime.Remoting.WellKnownClientTypeEntry(typeof(IGadgetClient), addr);
            System.Runtime.Remoting.RemotingConfiguration.RegisterWellKnownClientType(remoteType);

            // Create a message sink.
            string objectUri;
            System.Runtime.Remoting.Messaging.IMessageSink messageSink =
                channel.CreateMessageSink(addr, null, out objectUri);
            
            if (messageSink != null)
            {
                return true;
            }
            return false;
        }
    }
}
