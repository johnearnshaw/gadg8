﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;

namespace Gadg8Gadget
{
    public class RunningObjectTable
    {
        private const int ROTFLAGS_REGISTRATIONKEEPSALIVE = 1;
        private const int ROTFLAGS_ALLOWANYCLIENT = 2;

        [DllImport("ole32.dll", ExactSpelling=true, PreserveSig=false)]
        private static extern IRunningObjectTable GetRunningObjectTable(int reserved);

        [DllImport("ole32.dll", CharSet=CharSet.Unicode, ExactSpelling=true, PreserveSig=false)]
        private static extern IMoniker CreateItemMoniker([In] string lpszDelim, [In] string lpszItem);

        [DllImport("ole32.dll", ExactSpelling=true, PreserveSig=false)]
        public static extern IBindCtx CreateBindCtx(int reserved);

        private int dwRegister = 0;

        public static t GetActiveObject<t>(string progId)
        {
            t obj = default(t);
            IRunningObjectTable runningObjectTable;
            IEnumMoniker monikerEnumerator;
            IMoniker[] monikers = new IMoniker[1];

            runningObjectTable = GetRunningObjectTable(0);
            runningObjectTable.EnumRunning(out monikerEnumerator);
            monikerEnumerator.Reset();

            IntPtr numFetched = IntPtr.Zero;
            while (monikerEnumerator.Next(1, monikers, numFetched) == 0)
            {
                IBindCtx ctx;
                ctx = CreateBindCtx(0);

                string runningObjectName;
                monikers[0].GetDisplayName(ctx, null, out runningObjectName);
                if (runningObjectName.Contains(progId))
                {

                    object runningObjectVal;
                    runningObjectTable.GetObject(monikers[0], out runningObjectVal);
                    obj = (t)runningObjectVal;
                }
            }

            return obj;
        }

        public void AddToROT<t>(t item, string progId)
        {
            IRunningObjectTable rot = null;
            IMoniker moniker = null;
            try 
            {
                // Get the ROT
                rot = GetRunningObjectTable(0);

                // Create a moniker for the graph
                moniker = CreateItemMoniker("", progId);
        
                // Registers the graph in the running object table
                // ROTFLAGS_ALLOWANYCLIENT|
                dwRegister = rot.Register(ROTFLAGS_REGISTRATIONKEEPSALIVE, (t)item, moniker);
            }
            finally
            {
                // Releases the COM objects
                if (moniker != null) 
                    while(Marshal.ReleaseComObject(moniker)>0); 
                if (rot != null) while(Marshal.ReleaseComObject(rot)>0); 
            }
        }

        public void RemoveFromROT()
        {
            if (dwRegister != 0 )
            {
                IRunningObjectTable rot = null;
                try 
                {
                    // Get the running object table and revoke the cookie
                    rot = GetRunningObjectTable(0);
                    rot.Revoke(dwRegister);
                    dwRegister = 0;
                }
                finally
                {
                    if (rot != null) while(Marshal.ReleaseComObject(rot)>0); 
                }
            }
        }
    }
}
