﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using mshtml;

namespace Gadg8Gadget
{
    public class Helpers
    {
        public static bool IsWin64
        {
            get
            {
                return (IntPtr.Size == 8);
            }
        }

        public static bool IsXPOrHigher()
        {
            OperatingSystem OS = Environment.OSVersion;
            return (OS.Platform == PlatformID.Win32NT) && ((OS.Version.Major > 5) || ((OS.Version.Major == 5) && (OS.Version.Minor >= 1)));
        }

        public static bool IsVistaOrHigher()
        {
            OperatingSystem OS = Environment.OSVersion;
            return (OS.Platform == PlatformID.Win32NT) && (OS.Version.Major >= 6);
        }

        public static void RemoveFromAeroPeek(IntPtr Handle)
        {
            int value = (int)Native.DWMNCRENDERINGPOLICY.DWMNCRP_ENABLED;
            Native.DwmSetWindowAttribute(Handle, Native.DWMWINDOWATTRIBUTE.DWMWA_EXCLUDED_FROM_PEEK, ref value, Marshal.SizeOf(value));
        }

        public static void SetHandleToDesktop(IntPtr handle)
        {
            IntPtr dh = IntPtr.Zero;
            dh = Native.FindWindow("ProgMan", null);

            dh = Native.FindWindowEx(dh, IntPtr.Zero, "SHELLDLL_DefVIew", null);
            dh = Native.FindWindowEx(dh, IntPtr.Zero, "SysListView32", null);

            Native.SetParent(handle, dh);
        }

        public static bool IsDesktopWindowActive()
        {
            IntPtr activeWindow = Native.GetActiveWindow();

            IntPtr dh = IntPtr.Zero;
            dh = Native.FindWindow("ProgMan", null);

            if (dh != IntPtr.Zero)
            {
                if (activeWindow == dh)
                    return true;

                dh = Native.FindWindowEx(dh, IntPtr.Zero, "SHELLDLL_DefVIew", null);
                if (activeWindow == dh)
                    return true;

                dh = Native.FindWindowEx(dh, IntPtr.Zero, "SysListView32", null);
                if (activeWindow == dh)
                    return true;
            }

            if (Native.GetDesktopWindow() == activeWindow)
                return true;

            return false;
        }

        public static void OpenLink(string url)
        {
            System.Diagnostics.Process.Start(url);
            //Native.ShellExecute(0, "open", url, "0", "0", 1);
        }

        public static void MoveWindow(IntPtr handle)
        {
            Native.ReleaseCapture();
            Native.SendMessage(handle, 161, 2, 0);
        }

        public static void ChangeIcon(IntPtr handle, System.Drawing.Icon icon)
        {
            ChangeIcon(handle, icon.Handle);
        }

        public static void ChangeIcon(IntPtr handle, System.Drawing.Bitmap bmp)
        {
            ChangeIcon(handle, bmp.GetHicon());
        }

        private static void ChangeIcon(IntPtr handle, IntPtr hIcon)
        {
            Native.SendMessage(handle, (int)Native.WindowsMessageFlags.WM_SETICON, Native.ICON_SMALL, hIcon);
            Native.SendMessage(handle, (int)Native.WindowsMessageFlags.WM_SETICON, Native.ICON_BIG, hIcon);
        }

        public static GadgetManifest GetGadgetXML(string folder, out string path)
        {
            GadgetManifest xml = null;

            try
            {
                string XmlPath = GetLocalizedFilePath(folder, "gadget.xml");
                if (File.Exists(XmlPath))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(XmlPath);

                    // When we try to deserialize the xml comments will cause it to fail so we strip them our of the
                    // xml doc first.
                    XmlNodeList comments = xmlDoc.SelectNodes("//comment()");
                    if (comments.Count != 0)
                    {
                        foreach (XmlNode node in comments) node.ParentNode.RemoveChild(node);
                    }

                    MemoryStream ms = new MemoryStream();
                    xmlDoc.Save(ms);
                    ms.Seek(0, SeekOrigin.Begin);

                    using (StreamReader reader = new StreamReader(ms))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(GadgetManifest));
                        xml = (GadgetManifest)serializer.Deserialize(reader);
                        reader.Close();
                    }
                    ms.Close();
                    path = Path.GetDirectoryName(XmlPath);
                }
                else
                {
                    path = null;
                }
            }
            catch 
            {
                path = null;
            }

            return xml;
        }

        public static bool GadgetSourceExists(string path, GadgetManifest xml)
        {
            if (!string.IsNullOrEmpty(xml.hosts[0].Base.Src) && File.Exists(GetLocalizedFilePath(path, xml.hosts[0].Base.Src)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetAbsolutePath(string gadgetPath, string path, bool translateXGadget = false, bool addXGadget = false)
        {
            string p = path.ToLower();

            if (p.StartsWith("url("))
            {
                p = p.Substring(4, p.Length - 5);
                p = p.Trim(new char[] {'\''}).Trim(new char[] {'\\'}).Trim(new char[] {'"'});
            }

            // We really should try to get the absolute path from the relative path but we don't currently have access to the base
            // file location which "path" should be relative to. We ignore this for now and let our GetLocalizedFilePath method take
            // care of returning a suitable file location.
            //if (p.StartsWith(".."))
            //{
            //    p = Path.GetFullPath(new Uri(Path.Combine(gadgetPath, p)).LocalPath);
            //    return p;
            //}

            if (translateXGadget && p.StartsWith("x-gadget://"))
            {
                return GetLocalizedFilePath(gadgetPath, p.Substring(12));
            }
            else if (p.StartsWith("x-gadget://") || p.StartsWith("http://") || p.StartsWith("https://") ||
                    p.Substring(1).StartsWith(":\\") || p.Substring(1).StartsWith(":/"))
            {
                return p;
            }
            else
            {
                if (addXGadget)
                    return "x-gadget:///" + p;
                
                return GetLocalizedFilePath(gadgetPath, p);
            }
        }

        public static t GetHTMLAttribute<t>(IHTMLElement el, string attrName, t defaultValue = default(t))
        {
            try
            {
                object attr = el.getAttribute(attrName);
                if (attr != null && !(attr is System.DBNull))
                {
                    t ret = (t)attr;

                    if (ret != null)
                        return ret;
                }
            }
            finally
            {
            }

            return defaultValue;
        }

        public static string GetLocalizedFilePath(string basePath, string filename)
        {
            // Ok so this function looks all over the gadget folder for the requested file. I'm sure it could be optimized loads
            // but for now it is what it is.
            filename = filename.Replace("/", "\\");

            if (!basePath.EndsWith("\\"))
                basePath += "\\";

            // Get the current country/language
            string locale = CultureInfo.CurrentCulture.Name.ToLower();
            string language = locale.Substring(0, 2);

            // Get a list of all directories in the gadget folder.
            string[] directories = Directory.GetDirectories(basePath);
            Dictionary<string, string> folders = new Dictionary<string, string>();

            //Check for folders which could match locale folder names (eg. en-US) and add them to a new list for filtering.
            foreach (string d in directories)
            {
                if (d.EndsWith(locale, StringComparison.InvariantCultureIgnoreCase))
                {
                    folders.Add(locale, d);
                }
                else if (d.EndsWith(language + "-" + language, StringComparison.InvariantCultureIgnoreCase))
                {
                    folders.Add(language + "-" + language, d);
                }
                else if (d.EndsWith(language, StringComparison.InvariantCultureIgnoreCase))
                {
                    folders.Add(language + folders.Count, d);
                }
                else if (d.Substring(0, d.Length - 3).EndsWith(language, StringComparison.InvariantCultureIgnoreCase))
                {
                    folders.Add(language + folders.Count, d);
                }
                else if (language != "en" && d.StartsWith("en", StringComparison.InvariantCultureIgnoreCase))
                {
                    folders.Add(d, d);
                }
            }

            // Look in each folder in the new list for the requested file. We use a slightly different search method if the
            // current language is not English.
            if (folders.ContainsKey(locale) && File.Exists(folders[locale] + "\\" + filename))
            {
                return folders[locale] + "\\" + filename;
            }
            else if (language != "en")
            {
                if (folders.ContainsKey(language + "-" + language) && File.Exists(folders[language + "-" + language] + "\\" + filename))
                {
                    return folders[language + "-" + language] + "\\" + filename;
                }
                else
                {
                    foreach (string f in folders.Values)
                    {
                        if (File.Exists(f + "\\" + filename))
                            return f + "\\" + filename;
                    }

                    // If we get to here it's not looking good with our localized file search so if the file is not located in
                    // the base folder, we do one last look in every sub folder.
                    if (!File.Exists(basePath + filename))
                    {
                        foreach (string d in directories)
                        {
                            if (File.Exists(d + "\\" + filename))
                                return d + "\\" + filename;
                        }
                    }
                }
            }
            // And if language is English (almost same as above).
            else
            {
                if (!File.Exists(basePath + filename))
                {
                    foreach (string f in folders.Values)
                    {
                        if (File.Exists(f + "\\" + filename))
                            return f + "\\" + filename;
                    }

                    foreach (string d in directories)
                    {
                        if (File.Exists(d + "\\" + filename))
                            return d + "\\" + filename;
                    }
                }
            }

            // Return
            return basePath + filename;
        }

        #region Parse Web Color Function
        public static Color ParseColor(string color)
        {
            // Accept strings like "Color(255, 255, 0, 0)"
            Regex regex = new Regex(@"^\w+\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})\)");
            Match match = regex.Match(color);
            if (match.Success)
            {
                Color col = Color.FromArgb(int.Parse(match.Groups[2].Value), int.Parse(match.Groups[3].Value), int.Parse(match.Groups[4].Value));
                return col;
            }
            // Accept strings like "#FF0000"
            else if (Regex.IsMatch(color, "^(#[0-9A-Fa-f]{3})$|^(#[0-9A-Fa-f]{6})$"))
            {
                Color col = ColorTranslator.FromHtml(color);
                return col;
            }
            // Check against color name string like "Red"
            else
            {
                ColorConverter c = new ColorConverter();
                ColorConverter.StandardValuesCollection svc = (ColorConverter.StandardValuesCollection)c.GetStandardValues();
                foreach (Color o in svc)
                {
                    if (o.Name.Equals(color, StringComparison.OrdinalIgnoreCase))
                    {
                        Color col = (Color)c.ConvertFromString(color);
                        return col;
                    }
                }
            }
            // If all else fails return black ;)
            return Color.Black;
        }
        #endregion
    }
}
