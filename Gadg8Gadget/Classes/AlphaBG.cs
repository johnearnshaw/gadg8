﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Gadg8Gadget
{
    public class AlphaBG
    {
        private BasicWindow frm;
        private Bitmap bmp;

        private byte pOpacity = 255;
        public double Opacity
        {
            get { return (double)pOpacity / 255; }
            set
            {
                if (value > 1)
                {
                    value = 1;
                }
                else if (value < 0)
                {
                    value = 0;
                }

                pOpacity = (byte)Math.Ceiling(value * 255);
                Paint();
            }
        }


        public AlphaBG(BasicWindow frm)
        {
            bmp = new Bitmap(frm.Location.Width, frm.Location.Height, PixelFormat.Format32bppArgb);
            Clear();
            this.frm = frm;
        }

        public void Clear()
        {
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.Clear(Color.Transparent);
            }
        }

        public void Paint()
        {
            SetBitmap(bmp);
            Native.RedrawWindow(frm.Handle, IntPtr.Zero, IntPtr.Zero, Native.RedrawWindowFlags.RDW_VALIDATE);
        }

        public struct ARGB
        {
            public byte Blue;
            public byte Green;
            public byte Red;
            public byte Alpha;
        }

        public struct BLENDFUNCTION
        {
            public byte BlendOp;
            public byte BlendFlags;
            public byte SourceConstantAlpha;
            public byte AlphaFormat;
        }

        public const Int32 ULW_COLORKEY = 0x1;
        public const Int32 ULW_ALPHA = 0x2;
        public const Int32 ULW_OPAQUE = 0x4;
        public const byte AC_SRC_OVER = 0x0;
        public const byte AC_SRC_ALPHA = 0x1;

        //[DllImport("user32", EntryPoint = "UpdateLayeredWindow", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //private static extern bool UpdateLayeredWindow(IntPtr hwnd, IntPtr hdcDst, ref Point pptDst, ref Size psize, IntPtr hdcSrc, ref Point pprSrc, Int32 crKey, ref BLENDFUNCTION pblend, Int32 dwFlags);
        [DllImport("user32.dll", ExactSpelling = true, SetLastError = true)]
        static extern bool UpdateLayeredWindow(IntPtr hwnd, IntPtr hdcDst, ref Point pptDst, ref Size psize, IntPtr hdcSrc,
            ref Point pptSrc, uint crKey, [In] ref BLENDFUNCTION pblend, uint dwFlags);

        //[DllImport("user32", EntryPoint = "GetDC", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //private static extern IntPtr GetDC(IntPtr hWnd);
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr GetDC(IntPtr hWnd);

        //[DllImport("gdi32.dll", EntryPoint = "CreateCompatibleDC", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //private static extern IntPtr CreateCompatibleDC(IntPtr hDC);
        [DllImport("gdi32.dll", EntryPoint = "CreateCompatibleDC", SetLastError = true)]
        static extern IntPtr CreateCompatibleDC([In] IntPtr hdc);

        //[DllImport("user32", EntryPoint = "ReleaseDC", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //private static extern int ReleaseDC(IntPtr hWnd, IntPtr hDC);
        [DllImport("user32.dll")]
        static extern bool ReleaseDC(IntPtr hWnd, IntPtr hDC);

        //[DllImport("gdi32.dll", EntryPoint = "DeleteDC", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //private static extern bool DeleteDC(IntPtr hDC);
        [DllImport("gdi32.dll", EntryPoint = "DeleteDC")]
        static extern bool DeleteDC([In] IntPtr hdc);

        //[DllImport("gdi32.dll", EntryPoint = "SelectObject", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //private static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);
        [DllImport("gdi32.dll", EntryPoint = "SelectObject", SetLastError = true)]
        static extern IntPtr SelectObject([In] IntPtr hdc, [In] IntPtr hgdiobj);

        //[DllImport("gdi32.dll", EntryPoint = "DeleteObject", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        //private static extern bool DeleteObject(IntPtr hObject);
        [DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool DeleteObject([In] IntPtr hObject);

        public void SetBitmap(Bitmap bitmap) //, byte opacity, Form frm)
        {
            bmp = bitmap;

            if (bitmap.PixelFormat != PixelFormat.Format32bppArgb)
                throw new ApplicationException("The bitmap must be 32ppp with alpha-channel.");

            // The idia of this is very simple,
            // 1. Create a compatible DC with screen;
            // 2. Select the bitmap with 32bpp with alpha-channel in the compatible DC;
            // 3. Call the UpdateLayeredWindow.

            IntPtr screenDc = GetDC(IntPtr.Zero);
            IntPtr memDc = CreateCompatibleDC(screenDc);
            IntPtr hBitmap = IntPtr.Zero;
            IntPtr oldBitmap = IntPtr.Zero;

            try
            {
                hBitmap = bitmap.GetHbitmap(Color.FromArgb(0));
                // grab a GDI handle from this GDI+ bitmap
                oldBitmap = SelectObject(memDc, hBitmap);
                Size size = new Size(bitmap.Width, bitmap.Height);
                Point pointSource = new Point(0, 0);
                Point topPos = new Point(frm.Location.Left, frm.Location.Top);
                BLENDFUNCTION blend = new BLENDFUNCTION();
                blend.BlendOp = AC_SRC_OVER;
                blend.BlendFlags = 0;
                blend.SourceConstantAlpha = pOpacity;
                blend.AlphaFormat = AC_SRC_ALPHA;
                UpdateLayeredWindow(frm.Handle, screenDc, ref topPos, ref size, memDc, ref pointSource, 0, ref blend, ULW_ALPHA);
            }
            finally
            {
                ReleaseDC(IntPtr.Zero, screenDc);
                if (hBitmap != IntPtr.Zero)
                {
                    SelectObject(memDc, oldBitmap);
                    DeleteObject(hBitmap);
                }
                DeleteDC(memDc);
            }
        }
    }
}