﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels.Ipc;
using System.Security.Permissions;

namespace Gadg8Gadget
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    public class GadgetServer : MarshalByRefObject
    {
        //public override object InitializeLifetimeService()
        //{
        //    return null;
        //}

        public int CreateChannel()
        {
            IpcChannel serverChannel;
            Random rnd = new Random();

            int portNumber = 0;
            int tryCount = 0;
            
        createChannel: //try 5 times to create a unique channel, by then we should know there's something else wrong?
            portNumber = rnd.Next(1, 35564);

            try
            {
                // Create the server channel.
                serverChannel = new IpcChannel("localhost:" + portNumber);
                
            }
            catch (RemotingException e)
            {
                if (tryCount < 5)
                {
                    tryCount += 1;
                    goto createChannel;
                }
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                rnd = null;
            }

            // Register the server channel.
            System.Runtime.Remoting.Channels.ChannelServices.RegisterChannel(serverChannel, true);

            // Expose an object for remote calls.
            System.Runtime.Remoting.RemotingConfiguration.
                RegisterWellKnownServiceType(typeof(IGadgetHost), "GadgetHost.rem", System.Runtime.Remoting.WellKnownObjectMode.Singleton);

            return portNumber;
        }
    }
}
