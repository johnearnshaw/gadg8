﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Gadg8Gadget
{
    [Serializable()]
    [XmlRoot("gadget")]
    public class GadgetManifest
    {
        [XmlElement("name")]
        public string Name { get; set; }

        [XmlElement("namespace")]
        public string Namespace { get; set; }

        [XmlElement("version")]
        public string Version { get; set; }

        [XmlElement("author")]
        public author Author { get; set; }

        [XmlElement("copyright")]
        public string Copyright { get; set; }

        [XmlElement("description")]
        public string description { get; set; }

        [XmlArray("icons")]
        [XmlArrayItem("icon", typeof(icon))]
        public icon[] Icons { get; set; }

        [XmlArray("hosts")]
        [XmlArrayItem("host", typeof(host))]
        public host[] hosts { get; set; }

        [Serializable()]
        public class host
        {
            [XmlAttribute("name")]
            public string name { get; set; }

            [XmlElement("base")]
            public gadgetBase Base { get; set; }

            [XmlElement("permissions")]
            public string permissions { get; set; }

            [XmlElement("platform")]
            public platform Platform { get; set; }

            [XmlElement("defaultImage")]
            public image DefaultImage { get; set; }
        }

        [Serializable()]
        public class author
        {
            [XmlAttribute("name")]
            public string Name { get; set; }

            [XmlElement("info")]
            public info Info { get; set; }

            [XmlElement("logo")]
            public image Logo { get; set; }
        }

        [Serializable()]
        public class info
        {
            [XmlAttribute("url")]
            public string Url { get; set; }

            [XmlAttribute("text")]
            public string Text { get; set; }
        }

        [Serializable()]
        public class gadgetBase
        {
            [XmlAttribute("type")]
            public string Type { get; set; }

            [XmlAttribute("apiVersion")]
            public string ApiVersion { get; set; }

            [XmlAttribute("src")]
            public string Src { get; set; }
        }

        [Serializable()]
        public class icon
        {
            [XmlAttribute("height")]
            public int Height { get; set; }

            [XmlAttribute("width")]
            public int Width { get; set; }

            [XmlAttribute("src")]
            public string Src { get; set; }
        }

        [Serializable()]
        public class image
        {
            [XmlAttribute("src")]
            public string Src { get; set; }
        }

        [Serializable()]
        public class platform
        {
            [XmlAttribute("minPlatformVersion")]
            public string minPlatformVersion { get; set; }
        }
    }
}