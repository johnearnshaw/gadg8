﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Security.Permissions;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Gadg8Gadget.GadgetGadg8Namespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
    public class Gadg8Namespace : MarshalByRefObject
    {
        [ComVisible(false)]
        public Gadg8Gadget.Gadget GadgetWindow;

        public Gadg8Namespace(Gadg8Gadget.Gadget gadgetWindow)
        {
            this.GadgetWindow = gadgetWindow;
        }

        public void writeLog(object text, string file)
        {
            if (text == null)
                text = "";

            if (string.IsNullOrEmpty(file))
                throw new Exception("WriteLog(string text, string file): file can not be empty."
                    + " If the file does not exist it will be created befor appending text.");
            Log.Write(text.ToString(), file);
        }
    }
}
