﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using mshtml;

namespace Gadg8Gadget
{
    [ComVisible(true)]
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    public class Gadget : xGadgetWindow, IGadgetClient
    {
        #region Declarations
        // GadgetHost is a reference to the host application used for weading and writing settings.
        public IGadgetHost GadgetHost;

        // Client sets channel sink for ipc with the host application.
        public GadgetClient Client;

        // GadgetFrame shows the chrome (glass) around the gadget and flyout when the flyout is open.
        // It is also set as the owner of both the gadget and flyout windows to keep them at the same z-order.
        public Frame GadgetFrame;

        // ControlBox adds a fading controls tab to each gadget for closing, managing settings, etc...
        public new GadgetControls ControlBox;

        // UniqueId is received by the gadget host application and is used to identify the gadget
        // for reading and writing its settings.
        public string UniqueId;

        // SetingsWindow is used by gadgets which have set a settingsUI path in "System.Gadget" namespace.
        public GadgetSettings SettingsWindow;

        // FlyoutWindow allows a gadget to display more information in a window adjacent to its self.
        public Flyout FlyoutWindow;
        #endregion

        #region Properties
        // IsFlyoutOpen is used by Frame to tell if it needs to paint the chrome around the gadget & flyout and
        // along with IsSettingsOpen, tells the gadget control box whether to display or not.
        public bool IsFlyoutOpen { get; set; }
        public bool IsSettingsOpen { get; set; }

        // Docked is read from settings when the gadget is first run and is changed by the dock/undock button
        // in GadgetControls.
        private bool pDocked = true;
        public bool Docked
        {
            get { return pDocked; }
            set
            {
                if (value != pDocked)
                {
                    this.WriteSetting("PrivateSetting_Docked", value, true);
                    this.SaveSettings();
                }
                pDocked = value;
                if (SystemNamespace != null)
                    SystemNamespace.Gadget.InvokeDockUndockHandler();
            }
        }

        // HasSettings is set to true if the gadget has a settingsUI path in "System.Gadget" namespace and is used 
        // to add the settings button to ControlBox.
        private bool pHasSettings;
        public bool HasSettings
        {
            get { return pHasSettings; }
            set
            {
                pHasSettings = value;
                this.ControlBox.UpdateControls(value, this.HasUndock);
            }
        }

        // HasUndock is set to true if the gadget has both "onDock" and "onUndock" objects set in "System.Gadget"
        // namespace and is used to add the dock/undock button to ControlBox.
        private bool pHasUndock;
        public bool HasUndock
        {
            get { return pHasUndock; }
            set
            {
                pHasUndock = value;
                this.ControlBox.UpdateControls(this.HasSettings, value);
            }
        }

        private double pOpacity;
        public new double Opacity
        {
            get
            {
                return pOpacity;
            }
            set
            {
                pOpacity = value;
                this.WriteSetting("PrivateSetting_Opacity", value, true);
                this.CalculateOpacity();
            }
        }

        // Replace base TopMost property with our own which gets/sets GadgetFrame TopMost.
        public new bool TopMost
        {
            get
            {
                return GadgetFrame.TopMost;
            }
            set
            {
                this.GadgetFrame.TopMost = value;
                this.WriteSetting("PrivateSetting_TopMost", value, true);
            }
        }

        // Custom gadget security property
        private bool pSecurityAllowFrames;
        public bool SecurityAllowFrames
        {
            get
            {
                return this.pSecurityAllowFrames;
            }
            set
            {
                this.pSecurityAllowFrames = value;
                this.WriteSetting("PrivateSetting_SecurityAllowFrames", value, true);
            }
        }

        // Custom gadget security property
        private bool pSecurityAllowActiveX;
        public bool SecurityAllowActiveX
        {
            get
            {
                return this.pSecurityAllowActiveX;
            }
            set
            {
                this.pSecurityAllowActiveX = value;
                this.WriteSetting("PrivateSetting_SecurityAllowActiveX", value, true);
            }
        }

        // Custom gadget security property
        private bool pSecurityAllowCrossDomain;
        public bool SecurityAllowCrossDomain
        {
            get
            {
                return this.pSecurityAllowCrossDomain;
            }
            set
            {
                this.pSecurityAllowCrossDomain = value;
                this.WriteSetting("PrivateSetting_SecurityAllowCrossDomain", value, true);
            }
        }
        #endregion;

        #region Initialize
        public Gadget(string path, int left, int top, string uniqueId, int hostIpcPort, GadgetManifest gadgetXML, string caption = "")
            : base(caption)
        {
            this.UniqueId = uniqueId;
            this.Path = path;
            this.GadgetXML = gadgetXML;
            
            this.CreateControl();
            this.Text = caption;
            this.Visible = false;
            
            this.AlphaFrame = new BasicWindow();
            this.GadgetFrame = new Frame(this, caption);

            this.GadgetFrame.Activated += GadgetActivationChanged;
            
            this.GadgetFrame.MouseOver += (sender, e) => 
            {
                this.ControlBox.IsMouseOver = true;
                CalculateOpacity();
            };

            this.GadgetFrame.MouseLeave += (sender, e) =>
            {
                this.ControlBox.IsMouseOver = false;
                CalculateOpacity();
            };

            this.ControlBox = new GadgetControls(this);
            this.ControlBox.SetOwner(GadgetFrame);

            this.StartPosition = FormStartPosition.Manual;
            this.SetLocation(left, top, 1, 1);

            Client = new GadgetClient();
            Client.CreateChannelSink(hostIpcPort);

            try
            {
                GadgetHost = RunningObjectTable.GetActiveObject<IGadgetHost>("Gadg8.GadgetHost." + hostIpcPort);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                CloseGadget(true);
            }

            GadgetHost.Handshake(this.Handle, this.UniqueId, this.GadgetXML);

            this.SystemNamespace = new GadgetSystemNamespace.SystemNamespace(this);
            this.Gadg8Namespace = new GadgetGadg8Namespace.Gadg8Namespace(this);

            this.Move += OnGadgetMove;
            this.Resize += OnGadgetResize;
            this.Load += GadgetLoad;
            this.VisibleChanged += GadgetVisibleChanged;

            this.SetOwner(GadgetFrame);
            this.GadgetFrame.CalculateFrameRectangle();
            
            // Had to take these two lines out. Previously we were adding the AlphaFrame window as a child control of the gadget window
            // but due to a wierd positioning bug it doesn't show on previous versions of Windows.
            //this.AddChild(AlphaFrame);
            //this.AlphaFrame.SetLocation(0 - this.Location.Left, 0 - this.Location.Top, this.Location.Width, this.Location.Height);
            //AlphaFrame.Hide();
            AlphaFrame.SetOwner(this.Handle);
            this.AlphaFrame.SetLocation(this.Location.Left, this.Location.Top, this.Location.Width, this.Location.Height);

            this.AlphaFrameController = new AlphaBG(AlphaFrame);

            this.WebBrowser = new WebBrowserControl.WebBrowser(this.SystemNamespace, this.Gadg8Namespace, this, "")
            { Width = 100, Height = 100 };
            
            // Close the flyout if Escape key is pressed.
            this.WebBrowser.EscapeKeyPress += (sender, e) => { this.CloseFlyout(); };
            
            // Fill this form with our extended webbrowser control.
            this.WebBrowser.Dock = DockStyle.Fill;
            this.WebBrowser.Visible = false;
            this.Controls.Add(this.WebBrowser);
            
            // Try and send the form that handles the alpha-pixels of our g:background implementation behind the webbrowser control.
            // This doesn't seem to work, maybe I'm doing something wrong with SetWindowPos inside the method.
            //AlphaFrame.SendToBack();
            WebBrowser.BringToFront();

            // There seems to be a nasty issue with auction sidebar tool gadget which causes a stack overflow exception the
            // second time it runs. I presume that this is due to Gadg8Gadget initialization conflicting with the gadget's
            // javascript initialization/reading settings etc... as both the gadget and Gadg8Gadget share the same memory stack
            // which defaults to 1MB in .NET applications.
            this.NavigateWebBrowser();
            
            // Attach document specific events to the webbrowser control so we can get mouseenter/leave, contextmenu etc...
            AttachWebBrowserEvents();

            // BodyBehavior is assigned after WebBrowser
            this.BodyBehavior = new Behaviors.BodyBehavior(this);

            // Set gadget location, opacity, ontop, security settings etc...
            this.ReadPrivateSettings();

            // Catch javascript errors so they don't pop up in a window.
            AddErrorHandler();

            // Set up the gadget context menu.
            BuildContextMenu();

        }

        private void GadgetVisibleChanged(object sender, EventArgs e)
        {
            this.SystemNamespace.Gadget.InvokeVisibilityChangedHandler();
        }

        private void NavigateWebBrowser()
        {
            // Navigate WebBrowser control to the x-gadget source url.
            try
            {
                this.WebBrowser.Navigate("x-gadget:///" + this.GadgetXML.hosts[0].Base.Src);
                //ShowGadget();
                //this.SetLocation(this.Location.Left, this.Location.Top, 100, 100);
            }
            catch (Exception ex)
            {
                Log.Write(this.GadgetXML.Name + "\r\n" + ex.Message + "\r\n" + ex.StackTrace);
                this.CloseGadget(true);
            }
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            // Make sure webbrowser controls window.onunloading/onunload are fired
            this.WebBrowser.Navigate("about:blank");
            base.OnClosing(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            System.Diagnostics.Process p = System.Diagnostics.Process.GetCurrentProcess();
            if (!p.HasExited)
                p.Close();
        }

        private void GadgetLoad(object sender, EventArgs e)
        {
            // Set up a windows event hook to watch for foreground window switching and check to see if it is the desktop.
            // Currently unused.
            //WinEvent= new WinEventDelegate(WinEventProc);
            //IntPtr m_hhook = Native.SetWinEventHook(Native.EVENT_SYSTEM_FOREGROUND, Native.EVENT_SYSTEM_FOREGROUND,
            //    IntPtr.Zero, WinEvent, 0, 0, Native.WINEVENT_OUTOFCONTEXT);
            //GC.KeepAlive(WinEvent);
        }

        // Delegate to pass to SetWinEventHook
        public delegate void WinEventDelegate(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject,
            int idChild, uint dwEventThread, uint dwmsEventTime);
        
        // Keep a reference to WinEvent so it doesn't get GC'd or gadgets will crash when WinEventProc is trying to fire.
        public WinEventDelegate WinEvent;
        
        private void WinEventProc(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild,
            uint dwEventThread, uint dwmsEventTime)
        {
            // This was originally used to keep gadgets visible when "show desktop" (Win+D) was pressed but we use a
            // different method for that now. Thought I'd leave the code in for the event hook incase we decide to
            // use it for something else later
        }

        // Add a error handler to catch & handle javascript errors so the error dialogs don't show.
        // Currently, if we handle script errors, our extended webbrowser control isn't
        // queried for IElementBehaviorFactory.
        private void AddErrorHandler()
        {
            if (WebBrowser.Document != null)
            {
                WebBrowser.Document.Window.Error += OnWebBrowserDocumentWindowError;
            }
        }
        #endregion

        private void GadgetActivationChanged(object sender, EventArgs e)
        {
            if(!this.GadgetFrame.Active)
                if (this.FlyoutWindow != null && this.IsFlyoutOpen)
                    this.CloseFlyout();

            CalculateOpacity();
        }

        public void AttachWebBrowserEvents()
        {
            if (this.WebBrowser != null)
            {
                this.WebBrowser.DocumentCompleted += OnWebBrowserDocumentCompleted;
                this.WebBrowser.Navigating += OnWebBrowserNavigating;
                this.WebBrowser.Navigated += OnWebBrowserNavigated;
                this.WebBrowser.SizeChanged += OnSizeChanged;
            }
        }

        private void OnSizeChanged(object sender, EventArgs e)
        {
            this.Width = WebBrowser.Width;
            this.Height = WebBrowser.Height;
        }

        private void ShowGadget()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(this.ShowGadget));
                return;
            }

            // If debug build, show gadget on taskbar. Otherwise hide it.
#if DEBUG
            this.GadgetFrame.Show(false, true);
#else
            this.GadgetFrame.Show(false, false);
#endif
            this.ControlBox.Show(false);
            this.AlphaFrame.Show(false);
            this.Visible = true;
            this.WebBrowser.Visible = true;

            // Because we call ShowGadget from WebBrowser.DocumentCompleted event, some gadget's the dimensions aren't set
            // before calling SetLocations so we do this in a small timeout. Also calling System.Gadget.visibilityChanged
            // here allow javascript time to attach its events.
            System.Threading.Timer t = new System.Threading.Timer((x) =>
            {
                SetLocations();
                this.SystemNamespace.Gadget.InvokeDockUndockHandler();
                this.SystemNamespace.Gadget.InvokeVisibilityChangedHandler();

            }, null, 1000, System.Threading.Timeout.Infinite);
        }

        #region WebBrowser Events
        private List<string> LeftClickTagNames = new List<string>() { "INPUT", "TEXTAREA", "SELECT", "LABEL", "BUTTON", "A" };
        private List<string> RightClickTagNames = new List<string>() { "INPUT", "TEXTAREA" };

        public void OnWebBrowserDocumentWindowError(object sender, HtmlElementErrorEventArgs e)
        {
            try
            {
                // Ignore the error and suppress the error dialog box. 
                string url = e.Url.ToString().ToLower();

                if (url != "about:blank")
                {
                    string path = Helpers.GetAbsolutePath(this.Path, url, true);
                    this.OutputScriptError(e.Description, path, e.LineNumber);
                }   
            }
            catch { }

            e.Handled = true;
        }

        private void OnWebBrowserNavigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            AddErrorHandler();
        }

        private void OnWebBrowserNavigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            AddErrorHandler();
        }

        private void WebBrowserFocus(object sender, EventArgs e)
        {
            //if (FlyoutOpen)
            //FlyoutWindow.BringToFront();
        }

        private void OnWebBrowserDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.ToString() != "about:blank")
            {
                WebBrowser.SetFontZoom(0);
                WebBrowser.Document.Window.AttachEventHandler("onload", OnLoadEventHandler);

                AddErrorHandler();

                IHTMLDocument2 doc2 = (IHTMLDocument2)WebBrowser.Document.DomDocument;
                IHTMLDocument3 doc3 = (IHTMLDocument3)WebBrowser.Document.DomDocument;

                if (doc2.body != null)
                {

                    if (BodyBehavior == null)
                        BodyBehavior = new Behaviors.BodyBehavior(this);

                    IHTMLElement2 el2 = (IHTMLElement2)doc2.body;

                    if ((el2 != null))
                    {
                        WebBrowserControl.HtmlEvent.Create("onmouseenter", el2, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(MouseOverEventHandler));
                        WebBrowserControl.HtmlEvent.Create("onmouseleave", el2, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(MouseOverEventHandler));
                        WebBrowserControl.HtmlEvent.Create("onmousedown", el2, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(MouseDownEventHandler));
                        WebBrowserControl.HtmlEvent.Create("oncontextmenu", el2, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(ContextMenuEventHandler));

                        WebBrowserControl.HtmlEvent.Create("onselectstart", el2, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(CancelEventHandler));
                        WebBrowserControl.HtmlEvent.Create("ondragstart", el2, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(CancelEventHandler));
                        //WebBrowserControl.HtmlEvent.Create("ondrop", el2, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(DropEventHandler));
                        el2.addBehavior(null, BodyBehavior);
                    }

                    System.Threading.Timer t = null;
                    t = new System.Threading.Timer((state) =>
                    {
                        ShowGadget();
                        t.Dispose();
                    }, null, 200, System.Threading.Timeout.Infinite);
               }
            }
        }

        private void OnLoadEventHandler(object sender, EventArgs e)
        {
            // We show the gadget in document.window.onload event handler to ensure all scripts are loaded and the gadget
            // should look complete. We delay invoking ShowGadget and the gadget's events incase of deffered javascript loading.
            // Hopefully fixes (some) SuperSearch layout issues.
            System.Threading.Timer t = null;
            
            t = new System.Threading.Timer((state) =>
            {
                ShowGadget();
                t.Dispose();
            }, null, 200, System.Threading.Timeout.Infinite);
        }

        private bool CheckForMouseEvents(mshtml.IHTMLElement el)
        {
            if (el.onmousedown != DBNull.Value || el.onclick != DBNull.Value)
            {
                return true;
            }
            else
            {
                while (el.parentElement != null)
                {
                    el = el.parentElement;

                    if (el.onmousedown != DBNull.Value || el.onclick != DBNull.Value)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        private bool CheckForMenuEvents(mshtml.IHTMLElement el)
        {
            if (((IHTMLElement2)el).oncontextmenu != DBNull.Value)
            {
                return true;
            }
            else
            {
                while (el.parentElement != null)
                {
                    el = el.parentElement;

                    if (((IHTMLElement2)el).oncontextmenu != DBNull.Value)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        private void CancelEventHandler(object sender, IHTMLEventObj e)
        {
            IHTMLElement3 el = (IHTMLElement3)e.srcElement;
            if (e.srcElement.tagName != "BODY" && (e.srcElement.isTextEdit || el.isContentEditable))
            {
                e.returnValue = true;
                return;
            }

            e.returnValue = false;
        }

        private void MouseDownEventHandler(object sender, IHTMLEventObj e)
        {
            IHTMLElement3 el = (IHTMLElement3)e.srcElement;
            if (!LeftClickTagNames.Contains(e.srcElement.tagName) || !e.srcElement.isTextEdit || !el.isContentEditable)
            {
                this.Activate();
                if (!CheckForMouseEvents(e.srcElement))
                    MoveGadget();
            }
            
            return;
        }

        private void ContextMenuEventHandler(object sender, IHTMLEventObj e)
        {
            IHTMLElement3 el = (IHTMLElement3)e.srcElement;
            if (!RightClickTagNames.Contains(e.srcElement.tagName) || !e.srcElement.isTextEdit || !el.isContentEditable)
            {
                if (!CheckForMenuEvents(e.srcElement))
                {
                    //System.Diagnostics.Debug.WriteLine("need to show context menu!");
                    ShowContextMenu(Cursor.Position);
                    e.returnValue = false;
                    return;
                }
            }
        }

        private void MouseOverEventHandler(object sender, IHTMLEventObj e)
        {
            if (e.type == "mouseenter")
            {
                this.ControlBox.IsMouseOver = true;
                this.CalculateOpacity();
            }
            else
            {
                this.ControlBox.IsMouseOver = false;
                this.CalculateOpacity();
            }

            return;
        }
        #endregion

        #region Gadget Window Methods
        public void MoveGadget()
        {
            if (!IsFlyoutOpen)
            {
                ControlBox.Mouse_Down(this, EventArgs.Empty);
                Helpers.MoveWindow(this.Handle);
                ControlBox.Mouse_Up(this, EventArgs.Empty);

                if (FlyoutWindow != null)
                    FlyoutWindow.SetFlyoutLocation();

            }
        }

        public void OnGadgetMove(object sender, EventArgs e)
        {
            SetLocations();
        }

        public void OnGadgetResize(object sender, EventArgs e)
        {
            Rectangle desk = Screen.GetWorkingArea(this);

            // Dont let the gadget resize out of desktop bounds.
            if (this.Location.Right > desk.Right)
            {
                this.SetLocation((desk.Right - this.Location.Width) - this.ControlBox.Location.Width, this.Top);
            }
            else if (this.Location.Left < desk.Left)
            {
                this.SetLocation(desk.Left, this.Top);
            }

            if (this.Location.Top < desk.Top)
            {
                this.SetLocation(this.Left, desk.Top);
            }
            else if (this.Location.Bottom > desk.Bottom)
            {
                this.SetLocation(this.Left, desk.Bottom - this.Location.Height);
            }

            SetLocations();
        }

        private void SetLocations()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(this.SetLocations));
                return;
            }

            // Set the gbackground frame to the same width and height of the gadget window.
            if (this.AlphaFrame != null)
            {
                //this.AlphaFrame.SetLocation(0 - this.Location.Left, 0 - this.Location.Top, this.Location.Width, this.Location.Height);
                this.AlphaFrame.SetLocation(this.Location.Left, this.Location.Top, this.Location.Width, this.Location.Height);
            }

            // Move the gadget control buttons to the top-right of the gadget window.
            // this fails when first loading of some gadgets for some reason???
            if (ControlBox != null)
                ControlBox.SetLocation(this.Location.Right, this.Location.Top);

            if (this.FlyoutWindow != null)
                this.FlyoutWindow.SetFlyoutLocation();

            this.GadgetFrame.CalculateFrameRectangle();

            GadgetHost.GadgetMoved(this.Location, this.UniqueId);

            this.WriteSetting("PrivateSetting_X", this.Location.Left, true);
            this.WriteSetting("PrivateSetting_Y", this.Location.Top, true);
            this.SaveSettings();
        }

        public bool CloseGadget(bool closeSilently = false)
        {
            bool res = false;

            if (closeSilently)
            {
                res = true;
            }
            else if (GadgetHost != null)
            {
                try
                {
                    res = GadgetHost.GadgetClosing(this.Handle, this.UniqueId);
                }
                catch
                {
                    res = true;
                }
            }
            else
            {
                res = true;
            }

            if (res)
            {
                if (this.ControlBox != null)
                    this.ControlBox.Close();
                if (this.GadgetFrame != null)
                    this.GadgetFrame.Close();
                if (this.AlphaFrame != null)
                    this.AlphaFrame.Close();
                if (this.FlyoutWindow != null)
                    this.FlyoutWindow.CloseFlyout(true);

                this.Close();
            }
            return res;
        }
        #endregion

        #region Flyout Window Methods
        public void ShowFlyout(string path)
        {
            if (this.GadgetFrame.Active || this.ContainsFocus || this.WebBrowser.ContainsFocus)
            {
                this.IsFlyoutOpen = true;
                this.ControlBox.Fade();

                if (FlyoutWindow == null)
                {
                    FlyoutWindow = new Flyout(this);
                    FlyoutWindow.SetOwner(GadgetFrame);
                    FlyoutWindow.Resize += (sender, e) => { this.GadgetFrame.CalculateFrameRectangle(); };
                }

                FlyoutWindow.ShowFlyout(path);
                this.GadgetFrame.CalculateFrameRectangle();
            }
        }

        public void CloseFlyout()
        {
            System.Threading.Timer t = null;

            this.IsFlyoutOpen = false;

            if (FlyoutWindow != null)
            {
                FlyoutWindow.CloseFlyout();
                SystemNamespace.Gadget.Flyout.InvokeShowHideEventHandler();
            }

            // Set a single poll timer to check if the flyout is still open in 5 minutes (30000 ms) and if not we dispose it
            // then force garbage collection to free up some memory.
            t = new System.Threading.Timer((e) =>
            {
                if (!this.IsFlyoutOpen && this.FlyoutWindow != null)
                {
                    FlyoutWindow.CloseFlyout(true);
                    FlyoutWindow = null;
                }

                t.Dispose();
                t = null;

            }, null, 300000, System.Threading.Timeout.Infinite);

            this.GadgetFrame.CalculateFrameRectangle();
        }

        public object GetFlyoutDocument()
        {
            if (this.IsFlyoutOpen && (this.FlyoutWindow != null && this.FlyoutWindow.WebBrowser.Document != null))
                return this.FlyoutWindow.WebBrowser.Document.DomDocument;
            return null;
        }
        #endregion

        #region Settings Window Methods
        internal void OnSettingsClosing(GadgetSystemNamespace.Settings.ClosingEvent closingEvent)
        {
            this.SystemNamespace.Gadget.InvokeOnSettingsClosingHandler(closingEvent);
        }

        internal void OnSettingsClosed(GadgetSystemNamespace.Settings.ClosingEvent closingEvent)
        {
            IsSettingsOpen = false;
            ControlBox.Fade();
            this.SystemNamespace.Gadget.InvokeOnSettingsClosedHandler(closingEvent);
        }

        public void ShowSettings()
        {
            if (SettingsWindow == null && !string.IsNullOrEmpty(SystemNamespace.Gadget.settingsUI))
            {
                SettingsWindow = new GadgetSettings(GadgetXML, SystemNamespace.Gadget.settingsUI, this);
                SettingsWindow.FormClosed += (sender, e) =>
                {
                    SettingsWindow.Dispose();
                    SettingsWindow = null;
                };
                IsSettingsOpen = true;
                ControlBox.Fade();
                SettingsWindow.ShowDialog(this);
                SystemNamespace.Gadget.InvokeOnShowSettingsHandler();
            }
        }
        #endregion

        #region Gadget Settings Methods
        public void ReadPrivateSettings()
        {
            int x = this.ReadSetting<int>("PrivateSetting_X", this.Left);
            int y = this.ReadSetting<int>("PrivateSetting_Y", this.Top);
            bool docked = this.ReadSetting<bool>("PrivateSetting_Docked", true);
            bool topMost = this.ReadSetting<bool>("PrivateSetting_TopMost", false);
            double opacity = this.ReadSetting<double>("PrivateSetting_Opacity", 1.0);

            this.SetLocation(x, y);
            this.Docked = docked;
            this.TopMost = topMost;
            this.Opacity = opacity;
            this.CalculateOpacity();

            ReadSecuritySettings();
        }

        private void ReadSecuritySettings()
        {
            SecurityAllowFrames = this.ReadSetting("PrivateSetting_SecurityAllowFrames", false);
            SecurityAllowActiveX = this.ReadSetting("PrivateSetting_SecurityAllowActiveX", false);
            SecurityAllowCrossDomain = this.ReadSetting("PrivateSetting_SecurityAllowCrossDomain", false);
        }

        public string ReadSetting(string strName)
        {
            return ReadSetting<string>(strName, "");
        }

        public t ReadSetting<t>(string strName, t defaultValue)
        {
            if (this.GadgetHost != null)
                return GadgetHost.ReadSetting<t>(strName, defaultValue, this.UniqueId);
            return default(t);
        }

        public void WriteSetting(string strName, object value, bool privateSetting)
        {
            if (this.GadgetHost != null)
            {
                if (!this.GadgetHost.SettingsChanged & this.WebBrowser != null && this.WebBrowser.Document != null)
                    ((mshtml.IHTMLDocument2)this.WebBrowser.Document.DomDocument).parentWindow.execScript("setTimeout(function() {window.external.SaveSettingsToDisk()})");

                this.GadgetHost.WriteSetting(strName, value, this.UniqueId, privateSetting);
            }
        }

        public void SaveSettings()
        {
            if (this.GadgetHost != null && this.GadgetHost.SettingsChanged)
                this.GadgetHost.SaveSettings();
        }

        #endregion

        private void CalculateOpacity()
        {
            if (this.GadgetFrame.Active || this.IsFlyoutOpen || this.ControlBox.IsMouseOver)
            {
                SetOpacity(1);
            }
            else
            {
                // Set a timeout to delay the opacity change to stop flickering.
                System.Threading.Timer t = null;

                t = new System.Threading.Timer((e) =>
                {
                    if (!this.GadgetFrame.Active && !this.IsFlyoutOpen && !this.ControlBox.IsMouseOver)
                    {
                        SetOpacity(this.Opacity);
                    }
                    t.Dispose();
                    t = null;

                }, null, 1000, System.Threading.Timeout.Infinite);
            }
        }

        delegate void dSetOpacity(double opacity);
        private void SetOpacity(double opacity)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new dSetOpacity(SetOpacity), new object[] { opacity });
            }
            else
            {
                if (this.AlphaFrameController != null)
                {
                    this.AlphaFrameController.Opacity = opacity;
                    base.Opacity = opacity;
                }
            }
        }

        internal void SetBackground()
        {
            //if (this.GBackground == null)
            //    this.GBackground = new Behaviors.GNamespace(this);

            //this.GBackground.SetBackgroundImage(this.SystemNamespace.Gadget.background);
            this.BodyBehavior.SetBackgroundImage(this.SystemNamespace.Gadget.background);
        }

        private void BuildContextMenu()
        {
            bool devMode = false;
            if (GadgetHost != null)
                devMode = GadgetHost.ReadSetting("showdevoptions", false, "");

            if (this.ContextMenu == null)
            {
                this.ContextMenu = new ContextMenu();
                this.ContextMenu.Popup += (sender, e) => { this.BuildContextMenu(); };
            }
            this.ContextMenu.MenuItems.Clear();

            MenuItem[] menuItems;
            MenuItem[] opacityMenu = new MenuItem[5];
            MenuItem[] securityMenu = new MenuItem[3];

            // Set up opacity submenu.
            int i = 0;

            opacityMenu[i] = new MenuItem("20%", (sender, e) => { this.Opacity = 0.2; });
            opacityMenu[i].Checked = (this.Opacity == 0.2);
            opacityMenu[++i] = new MenuItem("40%", (sender, e) => { this.Opacity = 0.4; });
            opacityMenu[i].Checked = (this.Opacity == 0.4);
            opacityMenu[++i] = new MenuItem("60%", (sender, e) => { this.Opacity = 0.6; });
            opacityMenu[i].Checked = (this.Opacity == 0.6);
            opacityMenu[++i] = new MenuItem("80%", (sender, e) => { this.Opacity = 0.8; });
            opacityMenu[i].Checked = (this.Opacity == 0.8);
            opacityMenu[++i] = new MenuItem("100%", (sender, e) => { this.Opacity = 1; });
            opacityMenu[i].Checked = (this.Opacity == 1);

            // Set up security submenu.
            i = 0;

            securityMenu[i] = new MenuItem(Localize.Strings.SecurityAllowActiveX, (sender, e) =>
            {
                MenuItem itm = (MenuItem)sender;
                itm.Checked = !itm.Checked;
                this.SecurityAllowActiveX = itm.Checked;
            });
            securityMenu[i].Checked = this.SecurityAllowActiveX;

            securityMenu[++i] = new MenuItem(Localize.Strings.SecurityAllowCrossDomain, (sender, e) =>
            {
                MenuItem itm = (MenuItem)sender;
                itm.Checked = !itm.Checked;
                this.SecurityAllowCrossDomain = itm.Checked;
            });
            securityMenu[i].Checked = this.SecurityAllowCrossDomain;

            securityMenu[++i] = new MenuItem(Localize.Strings.SecurityAllowFrames, (sender, e) =>
            {
                MenuItem itm = (MenuItem)sender;
                itm.Checked = !itm.Checked;
                this.SecurityAllowFrames = itm.Checked;
            });
            securityMenu[i].Checked = this.SecurityAllowFrames;

            // Set up main context menu.
            i = 8;

            if (devMode)
                i += 4;

            if (this.HasSettings)
                i++;
            
            menuItems = new MenuItem[i];

            i = 0;

            menuItems[i] = new MenuItem(Localize.Strings.AddGadgets, (sender, e) => { this.AddGadgets(); });

            // Original sidebar has Move option in context menu but until we hook it up properly we are leaving it out.
            //menuItems[++i] = new MenuItem("-");
            //menuItems[++i] = new MenuItem(Localize.Strings.Move);

            menuItems[++i] = new MenuItem("-");
            menuItems[++i] = new MenuItem(Localize.Strings.AlwaysOnTop, (sender, e) => { this.TopMost = !this.TopMost; });
            menuItems[i].Checked = this.TopMost;
            menuItems[++i] = new MenuItem(Localize.Strings.Opacity, opacityMenu);
            menuItems[++i] = new MenuItem("-");

            if (this.HasSettings)
            {
                menuItems[++i] = new MenuItem(Localize.Strings.Options, (sender, e) => { this.ShowSettings(); });
            }

            menuItems[++i] = new MenuItem(Localize.Strings.SecuritySettings, securityMenu);

            if (devMode)
            {
                menuItems[++i] = new MenuItem("-");
                menuItems[++i] = new MenuItem(Localize.Strings.Reload, (sender, e) => 
                {
                    this.NavigateWebBrowser();
                });

                menuItems[++i] = new MenuItem(Localize.Strings.ShowConsole, (sender, e) =>
                {
                    this.ShowConsole();
                });

                menuItems[++i] = new MenuItem(Localize.Strings.OpenGadgetFolder, (sender, e) =>
                {
                    Helpers.OpenLink(System.IO.Path.GetDirectoryName(this.Path));
                });
            }

            menuItems[++i] = new MenuItem("-");
            menuItems[++i] = new MenuItem(Localize.Strings.Close, (sender, e) => { this.CloseGadget(false); });

            this.ContextMenu.MenuItems.AddRange(menuItems);
        }

        private void ShowContextMenu(Point location)
        {
            this.ContextMenu.Show(this, new Point((0 - this.Location.Left) + location.X, (0 - this.Location.Top) + location.Y));
        }

        private void AddGadgets()
        {
            if (GadgetHost != null)
                GadgetHost.AddGadgets();
        }

        private void ShowConsole()
        {
            MessageBox.Show("console not implemented yet");
        }

        public void OutputString(string strOutputString)
        {
            if (GadgetHost != null)
                GadgetHost.OutputString(strOutputString, false);

            Native.OutputDebugString(strOutputString);
        }

        public void OutputScriptError(string message, string filePath, int lineNumber)
        {
            WriteLog(message + " Line:" + lineNumber + " File:" + filePath);
            if (GadgetHost != null)
                GadgetHost.OutputString(message, true, filePath, lineNumber);
        }

        public void WriteLog(string text)
        {
            string name = "";

            if (this.GadgetXML != null)
                name = this.GadgetXML.Name + "\r\n";

            if (this.GadgetHost != null)
                this.GadgetHost.WriteLog(name + text);
        }
        
        public IntPtr GadgetWindowHandle
        {
            get { return this.Handle; }
        }

        public bool StillResponding()
        {
            return true;
        }

        string IGadgetClient.UniqueId
        {
            get { return this.UniqueId; }
        }
    }
}
