﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Gadg8Gadget
{
    public class Frame : BasicWindow
    {
        Gadget GadgetWindow;
        NativeWindow DesktopPin;

        private Rectangle GadgetRect;
        private Rectangle FlyoutRect;

        public int Padding { get; set; }
        public int CornerRadius { get; set; }

        public new bool TopMost
        {
            get { return base.TopMost; }
            set
            {
                if (value)
                {
                    this.SetOffDesktop();
                }
                else
                {
                    this.SetOnDesktop();
                }
                base.TopMost = value;
            }
        }

        public Frame(Gadget gadgetWindow, string caption) : base(caption)
        {
            this.GadgetWindow = gadgetWindow;

            // Create a new handle and set it's parent to desktop. This allows us to dynamically add the gadget to the desktop
            // children so the gadget is not hidden when a user hits Win+D or the "show desktop" button.
            DesktopPin = new NativeWindow();
            DesktopPin.CreateHandle(new CreateParams());
            Helpers.SetHandleToDesktop(DesktopPin.Handle);

            // Remove gadget from aero peek.
            if (Helpers.IsVistaOrHigher())
            {
                if (Native.DwmIsCompositionEnabled())
                {
                    //Helpers.RemoveFromAeroPeek(this.Handle);
                    Helpers.RemoveFromAeroPeek(this.GadgetWindow.Handle);
                    Helpers.RemoveFromAeroPeek(this.GadgetWindow.AlphaFrame.Handle);
                }
            }

            // Set default properties.
            this.Padding = 6;
            this.CornerRadius = 8;
            this.Opacity = 0.6;

            // If Frame is closing, tell Gadget form to close. Frame is set as the owner of Gadget and can
            // be shown on taskbar in debug builds for easy closing.
            this.Closing += (sender, e) => { e.Cancel = !GadgetWindow.CloseGadget(true); };
        }

        // Overrides our OnPaint method to draw the gadget and flyout chrome if necessary.
        protected override void OnPaint(Graphics g)
        {
            base.OnPaint(g);
            DrawBackground(g, CornerRadius, Padding);
        }

        public void SetOnDesktop()
        {
            this.SetOwner(this.DesktopPin);
        }

        public void SetOffDesktop()
        {
            this.SetOwner();
        }

        public new void Close()
        {
            DesktopPin.DestroyHandle();
            base.Close();
        }

        // Keep Frame always at the same location as Gadget so they look like just one form. This method also
        // sizes frame to the gadget and flyout windows.
        public void CalculateFrameRectangle()
        {
            int left, top, width, height;

            Gadget g = GadgetWindow;
            Flyout f = GadgetWindow.FlyoutWindow;

            if (f != null && GadgetWindow.IsFlyoutOpen)
            {
                if (g.Location.Left > f.Location.Left)
                {
                    left = f.Location.Left - (Padding + 1);
                }
                else
                {
                    left = g.Location.Left - (Padding + 1);
                }

                if (g.Location.Top > f.Location.Top)
                {
                    top = f.Location.Top - (Padding + 1);
                }
                else
                {
                    top = g.Location.Top - (Padding + 1);
                }

                if (g.Location.Height > f.Location.Height)
                {
                    height = g.Location.Height + ((Padding + 1) * 2);
                }
                else
                {
                    height = f.Location.Height + ((Padding + 1) * 2);
                }

                width = g.Location.Width + f.Location.Width + ((Padding + 1) * 2);
                GadgetRect = new Rectangle(g.Location.Left - left, g.Location.Top - top, g.Location.Width, g.Location.Height);
                FlyoutRect = new Rectangle(f.Location.Left - left, f.Location.Top - top, f.Location.Width, f.Location.Height);
            }
            else
            {
                left = g.Location.Left - (Padding);
                top = g.Location.Top - (Padding);
                height = g.Location.Height + ((Padding) * 2);
                width = g.Location.Width + ((Padding) * 2);
                GadgetRect = new Rectangle(g.Location.Left - left, g.Location.Top - top, g.Location.Width, g.Location.Height);
                FlyoutRect = new Rectangle(left + width, top, 20, height);
            }

            this.SetLocation(left, top, width, height);
            this.Refresh();
        }

        // Draw our chrome border if the flyout window is open.
        private void DrawBackground(Graphics g, int radius, int padding)
        {
            if (GadgetWindow.IsFlyoutOpen)
            {
                g.SmoothingMode = SmoothingMode.None;

                GraphicsPath path = CreateFramePath(GadgetRect, FlyoutRect, radius, padding);
                GraphicsPath path2 = CreateFramePath(GadgetRect, FlyoutRect, radius, padding - 1);

                g.FillPath(new SolidBrush(Color.Silver), path);
                g.DrawPath(new Pen(Color.Black, 1), path);
                g.DrawPath(new Pen(Color.White, 1), path2);
            }
        }

        // Calculate the path to draw or fill around the gadget and flyout windows.
        private GraphicsPath CreateFramePath(Rectangle gadgetRect, Rectangle flyoutRect, int radius, int padding)
        {
            GraphicsPath path = new GraphicsPath();

            int d = radius * 2;

            Rectangle first;
            Rectangle second;

            int topDiff = 0;
            int botDiff = 0;

            // Check to see if the flyout window is opened to the right of the gadget window. This happens if the
            // gadget window is too near the left side of the screen.
            if (gadgetRect.Left < flyoutRect.Left)
            {
                first = gadgetRect;
                second = flyoutRect;
            }
            else
            {
                first = flyoutRect;
                second = gadgetRect;
            }

            // Check to see if the flyout window is highter than the gadget window. and whether we need to draw the
            // curve. This happens if the gadget is too near to the bottom of the screen.
            if (first.Top > second.Top)
            {
                topDiff = first.Top - second.Top;
                if (topDiff < d + padding)
                {
                    second.Y -= topDiff;
                    second.Height += topDiff;
                }
            }
            else if (second.Top > first.Top)
            {
                topDiff = second.Top - first.Top;
                if (topDiff < d + padding)
                {
                    first.Y -= topDiff;
                    first.Height += topDiff;
                }
            }

            // Check to see if the flyout is tall or short enough to draw the curve. If the flyout window is almost
            // the same size as the gadget, we just padd it out and draw straight across like Windows Sidebar does.
            if (first.Bottom > second.Bottom)
            {
                botDiff = first.Bottom - second.Bottom;
                if (botDiff < d + padding)
                {
                    second.Height += botDiff;
                }
            }
            else if (second.Bottom > first.Bottom)
            {
                botDiff = second.Bottom - first.Bottom;
                if (botDiff < d + padding)
                {
                    first.Height += botDiff;
                }
            }

            // Draw top line of first rectangle
            path.AddLine((first.Left + d) - padding, first.Top - padding, (first.Right - d) - padding, first.Top - padding);

            if (first.Top > second.Top)
            {
                path.AddArc(Rectangle.FromLTRB((first.Right - radius) - padding, (first.Top - radius) - padding, first.Right - padding, first.Top - padding), 90, -90);
                path.AddArc(Rectangle.FromLTRB(second.Left - padding, second.Top - padding, (second.Left + d) - padding, (second.Top + d) - padding), 180, 90);
            }
            else if (second.Top > first.Top)
            {
                path.AddArc(Rectangle.FromLTRB((first.Right - d) + padding, first.Top - padding, first.Right + padding, (first.Top + d) - padding), -90, 90);
                path.AddArc(Rectangle.FromLTRB(first.Right + padding, (second.Top - radius) - padding, (first.Right + radius) + padding, second.Top - padding), -180, -90);
            }

            // Draw top line of second rectangle
            path.AddLine((second.Left + d) + padding, second.Top - padding, (second.Right - d) + padding, second.Top - padding);

            // Draw top right corner
            path.AddArc(Rectangle.FromLTRB((second.Right - d) + padding, second.Top - padding, second.Right + padding, (second.Top + d) - padding), -90, 90);

            // Draw right line
            path.AddLine(second.Right + padding, (second.Top + d) - padding, second.Right + padding, (second.Bottom - d) + padding);

            // Draw bottom right corner
            path.AddArc(Rectangle.FromLTRB((second.Right - d) + padding, (second.Bottom - d) + padding, second.Right + padding, second.Bottom + padding), 0, 90);

            // Draw bottom line of second rectangle
            path.AddLine((second.Right - d) + padding, second.Bottom + padding, (second.Left + d) + padding, second.Bottom + padding);

            if (second.Bottom > first.Bottom)
            {
                path.AddArc(Rectangle.FromLTRB(second.Left - padding, (second.Bottom - d) + padding, (second.Left + d) - padding, second.Bottom + padding), 90, 90);
                path.AddArc(Rectangle.FromLTRB((second.Left - radius) - padding, first.Bottom + padding, first.Right - padding, (first.Bottom + radius) + padding), 0, -90);
            }
            else if (first.Bottom > second.Bottom)
            {
                path.AddArc(Rectangle.FromLTRB(second.Left + padding, second.Bottom + padding, (second.Left + radius) + padding, (second.Bottom + radius) + padding), -90, -90);
                path.AddArc(Rectangle.FromLTRB((first.Right - d) + padding, (first.Bottom - d) + padding, first.Right + padding, first.Bottom + padding), 0, 90);
            }

            // Draw bottom line of first rectangle
            path.AddLine((first.Right - d) - padding, first.Bottom + padding, (first.Left + d) - padding, first.Bottom + padding);

            // Draw bottom left corner
            path.AddArc(Rectangle.FromLTRB(first.Left - padding, (first.Bottom - d) + padding, (first.Left + d) - padding, first.Bottom + padding), 90, 90);

            // Draw right line
            path.AddLine(first.Left - padding, (first.Bottom - d) + padding, first.Left - padding, (first.Top + d) - padding);

            // Draw top left corner
            path.AddArc(Rectangle.FromLTRB(first.Left - padding, first.Top - padding, (first.Left + d) - padding, (first.Top + d) - padding), 180, 90);

            path.CloseFigure();

            return path;
        }
    }
}
