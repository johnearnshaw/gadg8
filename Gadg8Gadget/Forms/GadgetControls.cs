﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace Gadg8Gadget
{
    public class GadgetControls : BasicWindow
    {
        private ToolTip Tip;
        private Timer FadeTimer;
        public Gadget GadgetWindow;
        public bool IsMouseDown;

        public PictureBox GadgetClose;
        public PictureBox GadgetDock;
        public PictureBox GadgetSettings;
        public PictureBox GadgetMove;

        private bool pIsMouseOver;
        public bool IsMouseOver
        {
            get
            {
                return pIsMouseOver;
            }
            set
            {
                pIsMouseOver = value;
                Fade();
            }
        }

        public GadgetControls(Gadget gadgetWindow)
        {
            this.GadgetWindow = gadgetWindow;

            Tip = new ToolTip();
            Tip.ShowAlways = true;

            FadeTimer = new Timer();
            FadeTimer.Interval = 10;
            FadeTimer.Tick += FadeTimer_Tick;

            this.Opacity = 0;
            UpdateControls(false, false);

            this.Closing += GadgetControls_Closing;
        }

        private void GadgetControls_Closing(object sender, FormClosingEventArgs e)
        {
            if (GadgetClose != null)
                GadgetClose.Dispose();
            if (GadgetDock != null)
                GadgetDock.Dispose();
            if (GadgetSettings != null)
                GadgetSettings.Dispose();
            if (GadgetMove != null)
                GadgetMove.Dispose();
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);
        }

        public void UpdateControls(bool showSettings, bool showDock)
        {
            int width = 17;
            int height = 0;

            Tip.RemoveAll();

            if (GadgetClose == null)
            {
                GadgetClose = new PictureBox() { Width = 17, Height = 17 };
                GC.KeepAlive(GadgetClose);
                GadgetClose.MouseMove += Mouse_Move;
                GadgetClose.MouseEnter += Mouse_Enter;
                GadgetClose.MouseLeave += Mouse_Leave;
                GadgetClose.MouseDown += Mouse_Down;
                GadgetClose.MouseUp += Mouse_Up;
                GadgetClose.MouseClick += CloseClick;
                GadgetClose.BackColor = Color.Magenta;
                GadgetClose.Image = Properties.Resources.gadget_close;
                GadgetClose.Left = 0;
            }

            GadgetClose.Top = height;
            this.AddChild(GadgetClose);
            height += 17;
            
            if (showSettings)
            {
                if (GadgetSettings == null)
                {
                    GadgetSettings = new PictureBox() { Width = 17, Height = 16 };
                    GC.KeepAlive(GadgetSettings);
                    GadgetSettings.MouseMove += Mouse_Move;
                    GadgetSettings.MouseEnter += Mouse_Enter;
                    GadgetSettings.MouseLeave += Mouse_Leave;
                    GadgetSettings.MouseDown += Mouse_Down;
                    GadgetSettings.MouseUp += Mouse_Up;
                    GadgetSettings.MouseClick += SettingsClick;
                    GadgetSettings.BackColor = Color.Magenta;
                    GadgetSettings.Image = Properties.Resources.gadget_settings;
                    GadgetSettings.Left = 0;
                }

                GadgetSettings.Top = height;
                this.AddChild(GadgetSettings);
                height += 16;
            }
            else if (GadgetSettings != null)
            {
                GadgetSettings.Dispose();
                GadgetSettings = null;
            }

            if (showDock)
            {
                if (GadgetDock == null)
                {
                    GadgetDock = new PictureBox() { Width = 17, Height = 16 };
                    GC.KeepAlive(GadgetDock);
                    GadgetDock.MouseMove += Mouse_Move;
                    GadgetDock.MouseEnter += Mouse_Enter;
                    GadgetDock.MouseLeave += Mouse_Leave;
                    GadgetDock.MouseDown += Mouse_Down;
                    GadgetDock.MouseUp += Mouse_Up;
                    GadgetDock.MouseClick += DockClick;
                    GadgetDock.BackColor = Color.Magenta;
                    GadgetDock.Image = Properties.Resources.gadget_docked;
                    GadgetDock.Left = 0;
                }

                GadgetDock.Top = height;
                this.AddChild(GadgetDock);
                height += 16;
            }
            else if (GadgetDock != null)
            {
                GadgetDock.Dispose();
                GadgetDock = null;
            }

            if (GadgetMove == null)
            {
                GadgetMove = new PictureBox() { Width = 17, Height = 16 };
                GC.KeepAlive(GadgetMove);
                GadgetMove.MouseMove += Mouse_Move;
                GadgetMove.MouseEnter += Mouse_Enter;
                GadgetMove.MouseLeave += Mouse_Leave;
                GadgetMove.MouseDown += Mouse_Down;
                GadgetMove.MouseUp += Mouse_Up;
                GadgetMove.MouseMove += MoveMove;
                GadgetMove.BackColor = Color.Magenta;
                GadgetMove.Image = Properties.Resources.gadget_move;
                GadgetMove.Left = 0;
            }

            GadgetMove.Top = height;
            this.AddChild(GadgetMove);
            height += 17;

            this.SetLocation(GadgetWindow.Location.Left, GadgetWindow.Location.Top, width, height);
        }

        public void Mouse_Move(object sender, EventArgs e)
        {
            IsMouseOver = true;
            Fade();
        }

        public void Mouse_Enter(object sender, EventArgs e)
        {
            if (sender == GadgetClose)
            {
                GadgetClose.Image = Properties.Resources.gadget_close_hover;
                Tip.Show(Localize.Strings.Close, GadgetClose, 20, 20);
            }
            else if (sender == GadgetDock)
            {
                GadgetDock.Image = Properties.Resources.gadget_docked_hover;
                string tip = Localize.Strings.Dock;
                
                if (GadgetWindow.Docked)
                    tip = Localize.Strings.Undock;

                Tip.Show(tip, GadgetDock, 20, 20);
            }
            else if (sender == GadgetSettings)
            {
                GadgetSettings.Image = Properties.Resources.gadget_settings_hover;
                Tip.Show(Localize.Strings.Settings, GadgetSettings, 20, 20);
            }

            IsMouseOver = true;
            Fade();
        }

        public void Mouse_Leave(object sender, EventArgs e)
        {
            if (sender == GadgetClose)
            {
                GadgetClose.Image = Properties.Resources.gadget_close;
                Tip.Hide(GadgetClose);
            }
            else if (sender == GadgetDock)
            {
                GadgetDock.Image = Properties.Resources.gadget_docked;
                Tip.Hide(GadgetDock);
            }
            else if (sender == GadgetSettings)
            {
                GadgetSettings.Image = Properties.Resources.gadget_settings;
                Tip.Hide(GadgetSettings);
            }

            IsMouseOver = false;
            Fade();
        }

        public void Mouse_Down(object sender, EventArgs e)
        {
            IsMouseDown = true;
        }

        public void Mouse_Up(object sender, EventArgs e)
        {
            IsMouseDown = false;
            Fade();
        }

        private void CloseClick(object sender, EventArgs e)
        {
            GadgetWindow.CloseGadget();
        }

        private void SettingsClick(object sender, EventArgs e)
        {
            GadgetWindow.ShowSettings();
        }

        private void DockClick(object sender, EventArgs e)
        {
            GadgetWindow.Docked = !GadgetWindow.Docked;
            string tip = Localize.Strings.Dock;
            if (GadgetWindow.Docked)
                tip = Localize.Strings.Undock;
            Tip.Show(tip, GadgetDock, 20, 20);
        }

        private void MoveMove(object sender, EventArgs e)
        {
            if (this.IsMouseDown)
                GadgetWindow.MoveGadget();
        }

        public void Fade()
        {
            //this.SetLocation(GadgetWindow.Location.Right + 2, GadgetWindow.Location.Top, this.Location.Width, this.Location.Height);

            if (!FadeTimer.Enabled)
                FadeTimer.Start();
        }

        int FadeDelay;
        bool FadingIn;

        private void FadeTimer_Tick(object sender, EventArgs e)
        {
            if (GadgetWindow.IsFlyoutOpen || GadgetWindow.IsSettingsOpen)
            {
                Opacity = 0;
                FadeTimer.Stop();
            }
            else if (IsMouseOver || FadingIn)
            {
                FadeDelay = 50;
                FadingIn = true;

                if (Opacity < 1)
                {
                    Opacity += 0.05;
                }
                else
                {
                    FadeTimer.Stop();
                    FadingIn = false;
                    if (!IsMouseOver)
                        Fade();
                }
            }
            else if (!IsMouseDown)
            {
                if (FadeDelay == 0)
                {
                    if (Opacity > 0)
                    {
                        Opacity -= 0.05;
                    }
                    else
                    {
                        FadeTimer.Stop();
                    }
                }
                else
                {
                    FadeDelay -= 1;
                }
            }
            else
            {
                FadeTimer.Stop();
            }
        }
    }
}
