﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using mshtml;
using System.Drawing;
using System.Diagnostics;

namespace Gadg8Gadget
{
    public class GadgetSettings : xGadgetWindow
    {
        Panel WebBrowserPanel;
        Panel ButtonsPanel;
        Button ButtonOK;
        Button ButtonCancel;

        //public WebBrowserControl.WebBrowser WebBrowser;
        Gadget GadgetWindow;

        public GadgetSettings(GadgetManifest gadgetXml, string path, Gadget gadgetWindow)
        {
            this.StartPosition = FormStartPosition.CenterParent;
            
            // Set form icon to application icon.
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            
            this.GadgetWindow = gadgetWindow;
            this.GadgetXML = gadgetXml;
            this.Path = path;
            this.SystemNamespace = gadgetWindow.SystemNamespace;
            this.Gadg8Namespace = gadgetWindow.Gadg8Namespace;

            this.ButtonsPanel = new System.Windows.Forms.Panel();
            this.ButtonOK = new System.Windows.Forms.Button();
            this.WebBrowserPanel = new System.Windows.Forms.Panel();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.ButtonsPanel.SuspendLayout();
            this.SuspendLayout();
            
            // ButtonsPanel
            this.ButtonsPanel.Controls.Add(this.ButtonCancel);
            this.ButtonsPanel.Controls.Add(this.ButtonOK);
            this.ButtonsPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ButtonsPanel.Location = new System.Drawing.Point(0, 218);
            this.ButtonsPanel.Size = new System.Drawing.Size(284, 35);
            this.ButtonsPanel.TabIndex = 0;
            
            // ButtonOK
            this.ButtonOK.Location = new System.Drawing.Point(119, 6);
            this.ButtonOK.Size = new System.Drawing.Size(75, 23);
            this.ButtonOK.TabIndex = 0;
            this.ButtonOK.Text = Localize.Strings.OK;
            this.ButtonOK.UseVisualStyleBackColor = true;
            this.ButtonOK.Click += OnOKClick;
            // ButtonCancel
            this.ButtonCancel.Location = new System.Drawing.Point(200, 6);
            this.ButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.ButtonCancel.TabIndex = 1;
            this.ButtonCancel.Text = Localize.Strings.Cancel;
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += OnCancelClick;
            
            // WebBrowserPanel
            this.WebBrowserPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WebBrowserPanel.Location = new System.Drawing.Point(0, 0);
            this.WebBrowserPanel.Size = new System.Drawing.Size(284, 218);
            this.WebBrowserPanel.TabIndex = 1;
            
            // frmGadgetSettings
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.WebBrowserPanel);
            this.Controls.Add(this.ButtonsPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Text = "Gadget Settings";
            this.BackColor = SystemColors.Control;
            this.ButtonsPanel.ResumeLayout(false);
            this.ResumeLayout(false);

            WebBrowser = new WebBrowserControl.WebBrowser(SystemNamespace, Gadg8Namespace, this); //, "body, table, td {font-size:7pt;}");
            
            WebBrowser.Top = 6;
            WebBrowser.Left = 6;
            WebBrowser.Height = WebBrowserPanel.Height - 6;
            WebBrowser.Width = WebBrowserPanel.Width - 6;
            
            WebBrowserPanel.Controls.Add(WebBrowser);

            WebBrowser.DocumentCompleted += WebBrowser_DocumentCompleted;
            WebBrowser.Navigating += WebBrowser_Navigating;
            WebBrowser.Navigated += WebBrowser_Navigated;
            WebBrowser.SizeChanged += OnSizeChanged;

            WebBrowser.Navigate(Helpers.GetAbsolutePath(GadgetWindow.Path, path, false, true));

            AddScriptErrorHandler();
        }

        private void OnCancelClick(object sender, EventArgs e)
        {
            GadgetSystemNamespace.Settings.ClosingEvent closingEvent =
                new GadgetSystemNamespace.Settings.ClosingEvent(false, false);
            
            GadgetWindow.OnSettingsClosing(closingEvent);
            GadgetWindow.OnSettingsClosed(closingEvent);
            this.Close();
        }

        private void OnOKClick(object sender, EventArgs e)
        {
            GadgetSystemNamespace.Settings.ClosingEvent closingEvent =
                new GadgetSystemNamespace.Settings.ClosingEvent(true, true);
            
            GadgetWindow.OnSettingsClosing(closingEvent);
            if (!closingEvent.cancel)
            {
                GadgetWindow.OnSettingsClosed(closingEvent);
                this.Close();
            }
        }

        private void AddScriptErrorHandler()
        {
            if (WebBrowser.Document != null)
            {
                WebBrowser.Document.Window.Error += OnWebBrowserDocumentWindowError;
            }
        }

        public void OnWebBrowserDocumentWindowError(object sender, HtmlElementErrorEventArgs e)
        {
            try
            {
                // Ignore the error and suppress the error dialog box. 
                string url = e.Url.ToString().ToLower();

                if (this.WebBrowser.Url.ToString() != "about:blank")
                {
                    string path = Helpers.GetAbsolutePath(this.Path, url, true);
                    this.GadgetWindow.OutputScriptError(e.Description, path, e.LineNumber);
                }
            }
            catch { }

            e.Handled = true;
        }
        
        private void WebBrowser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            AddScriptErrorHandler();
        }

        private void WebBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            AddScriptErrorHandler();
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            WebBrowser.SetFontZoom(0);

            AddScriptErrorHandler();

            IHTMLDocument2 doc2 = (IHTMLDocument2)WebBrowser.Document.DomDocument;
            IHTMLDocument3 doc3 = (IHTMLDocument3)WebBrowser.Document.DomDocument;

            if (doc2.body != null)
            {
                if (BodyBehavior == null)
                    BodyBehavior = new Behaviors.BodyBehavior(this);

                mshtml.IHTMLElement2 field = (IHTMLElement2)doc2.body;

                if ((field != null))
                {
                    WebBrowserControl.HtmlEvent.Create("oncontextmenu", field, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(ContextMenuEventHandler));

                    WebBrowserControl.HtmlEvent.Create("onselectstart", field, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(CancelEventHandler));
                    WebBrowserControl.HtmlEvent.Create("ondragstart", field, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(CancelEventHandler));

                    field.addBehavior(null, BodyBehavior);
                }
            }
                
            doc2.body.style.fontSize = "9pt";
            doc2.body.style.fontFamily = "SegoeUI, Tahoma";
        }

        private bool CheckForMouseEvents(mshtml.IHTMLElement el)
        {
            if (el.onmousedown != DBNull.Value || el.onclick != DBNull.Value)
            {
                return true;
            }
            else
            {
                while (el.parentElement != null)
                {
                    el = el.parentElement;

                    if (el.onmousedown != DBNull.Value || el.onclick != DBNull.Value)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        private bool CheckForMenuEvents(mshtml.IHTMLElement el)
        {
            if (((IHTMLElement2)el).oncontextmenu != DBNull.Value)
            {
                return true;
            }
            else
            {
                while (el.parentElement != null)
                {
                    el = el.parentElement;

                    if (((IHTMLElement2)el).oncontextmenu != DBNull.Value)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        private void CancelEventHandler(object sender, IHTMLEventObj e)
        {
            IHTMLElement3 el = (IHTMLElement3)e.srcElement;
            if (e.srcElement.tagName != "BODY" && (e.srcElement.isTextEdit || el.isContentEditable))
            {
                e.returnValue = true;
                return;
            }
            e.returnValue = false;
        }

        private List<string> RightTagNames = new List<string>() { "INPUT", "TEXTAREA" };
        private void ContextMenuEventHandler(object sender, IHTMLEventObj e)
        {
            IHTMLElement3 el = (IHTMLElement3)e.srcElement;
            if (!RightTagNames.Contains(e.srcElement.tagName) || !e.srcElement.isTextEdit || !el.isContentEditable)
            {
                if (!CheckForMenuEvents(e.srcElement))
                {
                    Debug.WriteLine("need to show context menu!");
                    e.returnValue = false;
                    return;
                }
            }
        }

        private void OnSizeChanged(object sender, EventArgs e)
        {
            this.ClientSize = new Size(WebBrowser.Width + 12, WebBrowser.Height + 12 + ButtonsPanel.Height);
            ButtonCancel.Left = ButtonsPanel.Width - ButtonCancel.Width - 6;
            ButtonOK.Left = ButtonsPanel.Width - ButtonOK.Width - ButtonCancel.Width - 12;

            //this.Width = WebBrowser.Width + (GetLeftOffest());
            //this.Height = WebBrowser.Height + ButtonsPanel.Height + GetTopOffest();
        }

        private int GetTopOffest()
        {
            return SystemInformation.CaptionHeight + (SystemInformation.FrameBorderSize.Height);
        }

        private int GetLeftOffest()
        {
            return SystemInformation.FrameBorderSize.Width;
        }
    }
}
