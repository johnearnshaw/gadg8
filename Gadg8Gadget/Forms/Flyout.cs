﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;
using mshtml;

namespace Gadg8Gadget
{
    [ComVisible(true)]
    public class Flyout : xGadgetWindow
    {
        public Gadget GadgetWindow;

        public Flyout(Gadget gadgetWindow, string caption = "") : base(caption)
        {
            this.GadgetWindow = gadgetWindow;
            this.Path = this.GadgetWindow.Path;

            this.CreateControl();
            this.StartPosition = FormStartPosition.Manual;

            this.SetLocation(GadgetWindow.Location.Right, GadgetWindow.Location.Top, 20, 20);
            this.Resize += OnResize;

            this.GadgetXML = gadgetWindow.GadgetXML;
            this.SystemNamespace = gadgetWindow.SystemNamespace;
            this.Gadg8Namespace = gadgetWindow.Gadg8Namespace;
            
            AlphaFrame = new BasicWindow();
            AlphaFrame.SetLocation(0 - this.Location.Left, 0 - this.Location.Top, this.Location.Width, this.Location.Height);
            this.AddChild(AlphaFrame);
            
            AlphaFrameController = new AlphaBG(AlphaFrame);
            
            WebBrowser = new WebBrowserControl.WebBrowser(this.SystemNamespace, this.Gadg8Namespace, this)
            {Width = 20, Height = 20};

            // Tell GadgetWindow to close the flyout if Escape key is pressed.
            this.WebBrowser.EscapeKeyPress += (sender, e) => { this.GadgetWindow.CloseFlyout(); };

            this.Controls.Add(this.WebBrowser);
            
            this.WebBrowser.Dock = DockStyle.Fill;
            AlphaFrame.SendToBack();

            AttachWebBrowserEvents();

            // BodyBehavior is assigned after WebBrowser
            BodyBehavior = new Behaviors.BodyBehavior(this);

            AddErrorHandler();
        }

        public void AttachWebBrowserEvents()
        {
            if (this.WebBrowser != null)
            {
                WebBrowser.DocumentCompleted += OnWebBrowserDocumentCompleted;
                WebBrowser.Navigating += OnWebBrowserNavigating;
                WebBrowser.Navigated += OnWebBrowserNavigated;
                WebBrowser.SizeChanged += OnSizeChanged;
            }
        }

        #region WebBrowser Events
        private List<string> LeftClickTagNames = new List<string>() { "INPUT", "TEXTAREA", "SELECT", "LABEL", "BUTTON", "A" };
        private List<string> RightClickTagNames = new List<string>() { "INPUT", "TEXTAREA" };

        private void AddErrorHandler()
        {
            if (WebBrowser.Document != null)
            {
                WebBrowser.Document.Window.Error += OnWebBrowserDocumentWindowError;
            }
        }

        public void OnWebBrowserDocumentWindowError(object sender, HtmlElementErrorEventArgs e)
        {
            try
            {
                // Ignore the error and suppress the error dialog box. 
                string url = e.Url.ToString().ToLower();

                if (url != "about:blank")
                {
                    string path = Helpers.GetAbsolutePath(this.Path, url, true);
                    this.GadgetWindow.OutputScriptError(e.Description, path, e.LineNumber);
                }
            }
            catch { }
            e.Handled = true;
        }

        private void OnWebBrowserNavigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            AddErrorHandler();
        }

        private void OnWebBrowserNavigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            AddErrorHandler();
        }

        private void OnSizeChanged(object sender, EventArgs e)
        {
            this.Width = WebBrowser.Width;
            this.Height = WebBrowser.Height;
        }

        private void WebBrowserFocus(object sender, EventArgs e)
        {
            //if (FlyoutOpen)
            //FlyoutWindow.BringToFront();
        }

        private void OnWebBrowserDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.ToString() != "about:blank")
            {
                AddErrorHandler();

                IHTMLDocument2 doc2 = (IHTMLDocument2)WebBrowser.Document.DomDocument;

                if (doc2.body != null)
                {
                    if (BodyBehavior == null)
                        BodyBehavior = new Behaviors.BodyBehavior(this);

                    mshtml.IHTMLElement2 field = (IHTMLElement2)doc2.body;

                    if ((field != null))
                    {
                        WebBrowserControl.HtmlEvent.Create("onselectstart", field, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(CancelEventHandler));
                        WebBrowserControl.HtmlEvent.Create("ondragstart", field, new WebBrowserControl.HtmlEvent.DHTMLEventHandler(CancelEventHandler));

                        field.addBehavior(null, BodyBehavior);
                    }
                }

                SystemNamespace.Gadget.Flyout.InvokeShowHideEventHandler();
                this.Show();
                this.WebBrowser.Focus();
            }
        }

        private bool CheckForMouseEvents(mshtml.IHTMLElement el)
        {
            if (el.onmousedown != DBNull.Value || el.onclick != DBNull.Value)
            {
                return true;
            }
            else
            {
                while (el.parentElement != null)
                {
                    el = el.parentElement;

                    if (el.onmousedown != DBNull.Value || el.onclick != DBNull.Value)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        private bool CheckForMenuEvents(mshtml.IHTMLElement el)
        {
            if (((IHTMLElement2)el).oncontextmenu != DBNull.Value)
            {
                return true;
            }
            else
            {
                while (el.parentElement != null)
                {
                    el = el.parentElement;

                    if (((IHTMLElement2)el).oncontextmenu != DBNull.Value)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        private void CancelEventHandler(object sender, IHTMLEventObj e)
        {
            IHTMLElement3 el = (IHTMLElement3)e.srcElement;
            if (/*e.srcElement.tagName != "BODY" && */(e.srcElement.isTextEdit || el.isContentEditable))
            {
                e.returnValue = true;
                return;
            }
            e.returnValue = false;
        }
        #endregion

        public void OnResize(object sender, EventArgs e)
        {
            this.AlphaFrame.SetLocation(0 - this.Location.Left, 0 - this.Location.Top, this.Location.Width, this.Location.Height);
            this.SetFlyoutLocation();
        }

        public void ShowFlyout(string path)
        {
            if (this.GBackground != null)
            {
                this.GBackground.SetBackgroundImage("");
                this.GBackground.Enabled = false;
            }

            this.WebBrowser.Navigate(Helpers.GetAbsolutePath(this.GadgetWindow.Path, path, false, true));
            //this.Show();
        }

        delegate void dCloseFlyout(bool destroy);
        public void CloseFlyout(bool destroy = false)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new dCloseFlyout(CloseFlyout), new object[] { destroy });
                return;
            }

            this.Hide();
            this.WebBrowser.Navigate("about:blank");
            this.SetLocation(this.Location.Left, this.Location.Top, 10, 10);

            if (this.GBackground != null)
            {
                this.GBackground.SetBackgroundImage("");
                this.GBackground.Enabled = false;
            }

            if (destroy)
                this.Close();
        }

        public void SetFlyoutLocation()
        {
            int top, left, sw = 0, sh = 0;

            foreach (Screen scr in Screen.AllScreens)
            {
                sw += scr.WorkingArea.Width;
                if (sh < scr.WorkingArea.Height) sh = scr.WorkingArea.Height;
            }

            if (this.GadgetWindow.Location.Right + this.Location.Width < sw)
            {
                left = this.GadgetWindow.Location.Right;
            }
            else
            {
                left = this.GadgetWindow.Location.Left - this.Location.Width;
            }

            if (this.Location.Bottom < sh)
            {
                top = this.GadgetWindow.Location.Top;
            }
            else
            {
                top = sh - this.Location.Height;
            }

            this.SetLocation(left, top);
        }
    }
}
