﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Security.Permissions;
using System.Runtime.InteropServices;

namespace Gadg8Gadget
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    public class xGadgetWindow : Form
    {
        #region Declarations
        // GNamespace exposes methods used by gadgets for adding rgba (.png) images directly to the html body.
        public Behaviors.GBackground GBackground;
        public List<Behaviors.GObject> GObjectList;
        // BodyBehavior is used to strip semi-transparent pixels from the background image and replace them with
        // magenta before painting the image to the html body. magenta is used as the transparent key for the html
        // window.
        public Behaviors.BodyBehavior BodyBehavior;

        // SystemNamespace is MS Sidebars app-gadget communication bridge and exposes properties & methods for the
        // gadget to communicate with the html host window. This is set to "Window.External.System" and then to "System"
        // using javascript execution in the webbrowser control.
        public GadgetSystemNamespace.SystemNamespace SystemNamespace;

        // Gadg8Namespace is our own app-gadget communication bridge to allow developers to add extra functionality to
        // gadgets which is specific to the Gadg8 platform.
        public GadgetGadg8Namespace.Gadg8Namespace Gadg8Namespace;

        // AlphaFrame & AlphaFrameController render the alpha-pixels of the background image. These pixels need to be
        // painted to a seperate form to avoid MS's "pink fringing".
        public BasicWindow AlphaFrame;
        public AlphaBG AlphaFrameController;

        // WebBrowser diplays the gadget html. It is an extended WebBrowserControl implementing the "x-gadget" protocol,
        // "g:background", "g:image" and "g:text" namespaces, etc...
        public WebBrowserControl.WebBrowser WebBrowser;

        #endregion

        #region Properties
        // Path is the location of the html file which will be displayed by WebBrowser.
        public string Path { get; set; }

        // GadgetXML contains all the data from the gadgets manifest file "gadget.xml" which is located in the gadget package.
        public GadgetManifest GadgetXML { get; set; }
        #endregion

        public new Rectangle Location
        {
            get
            {
                return new Rectangle(this.Left, this.Top, this.Width, this.Height);
            }
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                // This form has to have the WS_EX_LAYERED extended style
                cp.ExStyle = (cp.ExStyle | (int)Native.ExStyleFlags.WS_EX_LAYERED);
                return cp;
            }
        }

        public xGadgetWindow(string caption = "") : base()
        {
            this.Text = caption;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.ShowInTaskbar = false;
            this.TransparencyKey = System.Drawing.Color.Magenta;
            this.BackColor = System.Drawing.Color.Magenta;
        }

        protected override void WndProc(ref Message m)
        {
            switch ((Native.WindowsMessageFlags)m.Msg)
            {
                case Native.WindowsMessageFlags.WM_GETMINMAXINFO:
                    Native.MINMAXINFO minMaxInfo = (Native.MINMAXINFO)m.GetLParam(typeof(Native.MINMAXINFO));
                    minMaxInfo.ptMinTrackSize.x = 20;
                    minMaxInfo.ptMinTrackSize.y = 20;
                    Marshal.StructureToPtr(minMaxInfo, m.LParam, true);
                    break;
            }
            base.WndProc(ref m);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            e.Graphics.Clear(System.Drawing.Color.Magenta);
            base.OnPaint(e);
        }

        public void AddChild(IWin32Window child)
        {
            Native.SetWindowLong(child.Handle, (int)Native.GetWindowLongFlags.GWL_HWNDPARENT, this.Handle);
            Native.SetParent(child.Handle, this.Handle);
        }

        public void SetOwner(IWin32Window owner)
        {
            if (!Helpers.IsWin64)
            {
                Native.SetWindowLong(this.Handle, (int)Native.GetWindowLongFlags.GWL_HWNDPARENT, owner.Handle);
            }
            else
            {
                Native.SetWindowLongPtr(this.Handle, (int)Native.GetWindowLongFlags.GWL_HWNDPARENT, owner.Handle);
            }
        }

        public void SetLocation(Rectangle rect)
        {
            SetLocation(rect.Left, rect.Top, rect.Width, rect.Height);
        }

        public void SetLocation(int left, int top)
        {
            SetLocation(left, top, this.Location.Width, this.Location.Height);
        }

        public void SetLocation(int left, int top, int width, int height)
        {
            this.Top = top;
            this.Left = left;
            this.Width = width;
            this.Height = height;
        }

        public void SetControlToFront(IWin32Window control)
        {
            Native.SetWindowPos(control.Handle, (IntPtr)Native.HWND_TOP, 0, 0, 0, 0, (int)Native.SetWindowPosFlags.SWP_NOMOVE | (int)Native.SetWindowPosFlags.SWP_NOSIZE);
        }

        public void InvalidateWebBrowser(mshtml.tagRECT rect)
        {
            //if (PaintSite != null)
            //{
            //    PaintSite.InvalidateRect(rect);
            //}
            //else
            //{
            this.WebBrowser.Invalidate(true);
            //this.ParentWindow.WebBrowser.Invalidate(new Rectangle(rect.left, rect.top, rect.right-rect.left, rect.bottom-rect.top), true);
            //}
        }
        public void InvalidateWebBrowser(Rectangle rect)
        {
            this.WebBrowser.Invalidate(true);
            //this.ParentWindow.WebBrowser.Invalidate(rect, true);
        }
    }
}
