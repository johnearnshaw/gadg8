﻿using System; 
using System.Collections.Generic; 
using System.IO; 
using System.Reflection; 
using System.Text; 
using System.Windows.Forms;
using System.Xml; 
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

namespace Gadg8Gadget
{
    public static class Log
    {
        public static void Write(string text, string file = "")
        {
            if (string.IsNullOrEmpty(file))
            {
                File.AppendAllText(Program.GadgetPath + "gadg8_log.txt", DateTime.Now.ToString() + "\r\n" + text + "\r\n\r\n", Encoding.UTF8);
            }
            else
            {
                File.AppendAllText(file, text + "\r\n\r\n", Encoding.UTF8);
            }
        }
    }
}
