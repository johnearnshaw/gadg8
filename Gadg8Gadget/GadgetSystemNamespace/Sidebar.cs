﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Permissions;
using System.Runtime;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
    public class Sidebar : MarshalByRefObject, Gadg8Gadget.GadgetSystemNamespace.ISidebar
    {
        public object onDockSideChanged { get; set; }

        public string dockSide
        {
            get
            {
                return "Right";
            }
        }
    }
}
