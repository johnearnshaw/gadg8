﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Security.Permissions;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Management;
using Microsoft.Win32;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class Machine : MarshalByRefObject
    {
        private Gadg8Gadget.Gadget GadgetWindow;
        private PerformanceCounter ramCounter;

        public PowerStatusClass PowerStatus = new PowerStatusClass();

        public Machine(Gadg8Gadget.Gadget gadgetWindow)
        {
            this.GadgetWindow = gadgetWindow;
            SystemEvents.PowerModeChanged += new PowerModeChangedEventHandler(PowerModeChanged);
        }

        private CpuCollection _CPUs;
        public CpuCollection CPUs
        {
            get
            {
                if (_CPUs == null)
                    _CPUs = new CpuCollection();
                return _CPUs;
            }
        }

        public string processorArchitecture
        {
            get
            {
                Cpu cpu = CPUs.item(0);

                switch (cpu.Architecture)
                {
                    case 0:
                        return "x86";
                    case 1:
                        return "MIPS";
                    case 2:
                        return "Alpha";
                    case 3:
                        return "PowerPC";
                    case 6:
                        return "Intel Itanium Processor Family (IPF)";
                    case 9:
                        return "x64";
                    default:
                        return "Unknown";
                }
            }
        }

        public long availableMemory
        {
            get { return GetAvailableRAM(); }
        }

        public long totalMemory
        {
            get { return GetTotalPhysicalMemory(); }
        }

        private void PowerModeChanged(object sender, PowerModeChangedEventArgs e)
        {
            PowerStatus.InvokePowerLineStausHandler();
        }

        private long GetAvailableRAM()
        {
            if (ramCounter == null)
                ramCounter = new PerformanceCounter("Memory", "Available MBytes");
            return long.Parse(ramCounter.NextValue().ToString());
        }

        private long GetTotalPhysicalMemory()
        {
            string strMemory = null;

            //initialize the select query with command text
            SelectQuery query = new SelectQuery("Select * from Win32_ComputerSystem");

            //initialize the searcher with the query it is supposed to execute
            using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(query))
            {
                //execute the query
                foreach (ManagementObject obj in searcher.Get())
                {
                    strMemory = obj["TotalPhysicalMemory"].ToString();
                }
            }

            return (long.Parse(strMemory) / 1024) / 1024;
        }

        #region CpuCollection
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        [ComVisible(true)]
        public class CpuCollection
        {
            private static bool initialized;
            private static List<Cpu> CpuList;
            private static System.Threading.Timer timer;

            private List<Cpu> CPUs
            {
                get
                {
                    if (initialized == false)
                    {
                        timer = new System.Threading.Timer((state) =>
                        {
                            CpuList = GetWmiProcessorInfo();
                        }, null, 3500, 1500);

                        initialized = true;

                        return GetWmiProcessorInfo();
                    }
                    else
                    {
                        return CpuList;
                    }
                }
            }

            public int count
            {
                get
                {
                    return CPUs.Count;
                }
            }

            public Cpu item(int index)
            {
                return CPUs[index];
            }

            [ComVisible(false)]
            public static List<Cpu> GetWmiProcessorInfo()
            {
                //'**Full Win32_Processor Members List at bottom**''
                List<Cpu> cpuList = new List<Cpu>();

                //initialize the select query with command text
                SelectQuery query = new SelectQuery("Select * from Win32_Processor");
                
                //initialize the searcher with the query it is supposed to execute
                using (ManagementObjectSearcher searcher = new ManagementObjectSearcher(query))
                {
                    searcher.Options.ReturnImmediately = true;

                    //execute the query
                    foreach (ManagementObject obj in searcher.Get())
                    {
                        Cpu cpu = new Cpu();
                        cpu.Architecture = Int16.Parse(obj["Architecture"].ToString());
                        cpu.name = obj["Name"].ToString();
                        cpu.usagePercentage = int.Parse(obj["LoadPercentage"].ToString());
                        cpuList.Add(cpu);
                    }
                }

                CpuList = cpuList;
                return cpuList;
            }
        }
        #endregion

        #region Cpu
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        [ComVisible(true)]
        public class Cpu
        {
            public Int16 Architecture;
            public string name;
            public int usagePercentage;
        }
        #endregion

        #region PowerStatusClass
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        [ComVisible(true)]
        public class PowerStatusClass : MarshalByRefObject
        {
            public void InvokePowerLineStausHandler()
            {
                if ((this.powerLineStatusChanged != null))
                    this.powerLineStatusChanged.GetType().InvokeMember("call", System.Reflection.BindingFlags.InvokeMethod, null, this.powerLineStatusChanged, new object[] { });
            }

            private object pPowerLineStatusChanged;
            public object powerLineStatusChanged
            {
                [return: MarshalAs(UnmanagedType.IUnknown)]
                get
                {
                    return pPowerLineStatusChanged;
                }
                set
                {
                    pPowerLineStatusChanged = value;
                }
            }

            public object batteryCapacityRemaining
            {
                get { return SystemInformation.PowerStatus.BatteryLifeRemaining; }
            }

            public object batteryCapacityTotal
            {
                get { return SystemInformation.PowerStatus.BatteryFullLifetime; }
            }

            public object batteryPercentRemaining
            {
                get { return (int)(SystemInformation.PowerStatus.BatteryLifePercent * 100); }
            }

            public object batteryStatus
            {
                get { return SystemInformation.PowerStatus.BatteryChargeStatus; }
            }

            public object isBatteryCharging
            {
                get { return SystemInformation.PowerStatus.PowerLineStatus; }
            }

            public object isPowerLineConnected
            {
                get { return SystemInformation.PowerStatus.PowerLineStatus; }
            }
        }
#endregion

    }
}
