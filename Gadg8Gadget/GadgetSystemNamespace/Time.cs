﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Collections.ObjectModel;
using System.Globalization;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
	[ComVisible(true)]
	public class Time : MarshalByRefObject
	{
        Gadg8Gadget.Gadget GadgetWindow;

        public Time(Gadg8Gadget.Gadget gadgetWindow)
        {
            this.GadgetWindow = gadgetWindow;
        }

			//TODO
        private object _timeZones;
        public object timeZones
        {
            get
            {
                if (_timeZones == null)
                    _timeZones = new TimeZonesCollection();

                return _timeZones;
            }
        }

		public object currentTimeZone
        {
			get { return TimeZoneInfo.CurrentTimeZone; }
		}

		public string getLocalTime(object oTimeZone)
		{
            TimeZoneInfo timeZoneInfo = oTimeZone as TimeZoneInfo;
            if (timeZoneInfo != null)
            {
                return timeZoneInfo.CurrentTime.ToString("R", CultureInfo.InvariantCulture);
            }
            return DateTime.Now.ToString("R", CultureInfo.InvariantCulture);
		}

        public class TimeZonesCollection
        {
            private TimeZoneInfo[] _timeZones = TimeZoneInfo.GetTimeZones();
            public TimeZoneInfo[] timeZones
            {
                get
                {
                    return _timeZones;
                }
            }

            public int count
            {
                get
                {
                    return _timeZones.Length;
                }
            }

            public TimeZoneInfo item(int index)
            {
                return _timeZones[index];
            }
        }
	}
}
