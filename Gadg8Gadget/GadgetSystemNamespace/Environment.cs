﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class Environment : MarshalByRefObject
    {
        private Gadg8Gadget.Gadget GadgetForm;
        public Environment(Gadg8Gadget.Gadget gadgetForm)
        {
            this.GadgetForm = gadgetForm;
        }

        public string machineName
        {
            get { return System.Environment.MachineName; }
        }

        public string getEnvironmentVariable(string strEnvVar)
        {
            return System.Environment.GetEnvironmentVariable(strEnvVar);
            //ALLUSERSPROFILE=C:\ProgramData 'APPDATA=C:\Users\user\AppData\Roaming 'HOMEPATH=\Users\user 'LOCALAPPDATA=C:\Users\user\AppData\Local 
            //PROGRAMDATA=C:\ProgramData 'PUBLIC=C:\Users\Public 'TEMP=C:\Users\user\AppData\Local\Temp 'TMP=C:\Users\user\AppData\Local\Temp 
            //USERPROFILE=C:\Users\user
        }
    }
}
