﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    static class Helpers
    {
        // Ref: http://www.pinvoke.net/default.aspx/shell32.shgetknownfolderpath
        // Ref: http://www.pinvoke.net/default.aspx/shell32.shgetspecialfolderlocation
        public static string GetKnownFolderPath(string strKnownFolderID)
        {
            string file;
            string ret = "";
            strKnownFolderID = strKnownFolderID.ToUpper().Replace("{", "").Replace("}", "");

            switch (strKnownFolderID)
            {
                case "DESKTOP":
                case "B4BFCC3A-DB2C-424C-B029-7FE99A87C641":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_DESKTOPDIRECTORY);
                    break;

                case "STARTUP":
                case "B97D20BB-F46A-4C97-BA10-5E3608430854":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_STARTUP);
                    break;

                case "STARTMENU":
                case "625B53C3-AB48-4EC1-BA1F-A1EF4146FC19":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_STARTMENU);
                    break;

                case "DOCUMENTS":
                case "FDD39AD0-238F-46AF-ADB4-6C85480369C7":
                    // Use System.Environment.GetFolderPath instead.
                    /* SHGetFolderPath function hToken [in] Windows XP and later: This parameter is usually set
                     * to NULL, but you might need to assign a non-NULL value to hToken for those folders that
                     * can have multiple users but are treated as belonging to a single user. The most commonly
                     * used folder of this type is Documents.
                     http://msdn.microsoft.com/en-gb/library/windows/desktop/bb762181(v=vs.85).aspx */
                    //ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_MYDOCUMENTS);
                    ret = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
                    break;

                case "PROGRAMS":
                case "A77F5D77-2E2B-44C3-A6A2-ABA601054A51":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_PROGRAMS);
                    break;

                case "COMMONSTARTUP":
                case "82A5EA35-D9CD-47C5-9629-E15D2F714E6E":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_COMMON_STARTUP);
                    break;

                case "COMMONPROGRAMS":
                case "0139D44E-6AFE-49F2-8690-3DAFCAE6FFB8":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_COMMON_PROGRAMS);
                    break;

                case "PUBLICDESKTOP":
                case "C4AA340D-F20F-4863-AFEF-F87EF2E6BA25":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_COMMON_DESKTOPDIRECTORY);
                    break;

                case "PUBLICFAVORITES": // Unknown GUID.
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_COMMON_FAVORITES);
                    break;

                case "PUBLICDOCUMENTS":
                case "ED4824AF-DCE4-45A8-81E2-FC7965083634":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_COMMON_DOCUMENTS);
                    break;

                case "SYSTEM":
                case "1AC14E77-02E7-4E5D-B744-2EB1AE5198B7":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_SYSTEM);
                    break;

                case "SYSTEMX86":
                case "D65231B0-B2F1-4857-A4CE-A8E7C6EA7D27":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_SYSTEMX86);
                    break;

                case "PROFILE":
                case "5E6C858F-0E22-4760-9AFE-EA3317B67173":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_PROFILE);
                    break;

                case "WINDOWS":
                case "F38BF404-1D43-42F2-9305-67DE0B28FC23":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_WINDOWS);
                    break;

                case "PICTURES":
                case "33E28130-4E1E-4676-835A-98395C3BC3BB":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_MYPICTURES);
                    break;

                case "MUSIC":
                case "4BD8D571-6D19-48D3-BE97-422220080E43":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_MYMUSIC);
                    break;

                case "VIDEOS":
                case "18989B1D-99B5-455B-841C-AB7C74E4DDFC":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_MYVIDEO);
                    break;

                case "PROGRAMFILES":
                case "905e63b6-c1bf-494e-b29c-65b732d3d21a":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_PROGRAM_FILES);
                    break;

                case "PROGRAMFILESX86":
                case "7C5A40EF-A0FB-4BFC-874A-C0F2E0B9FA8E":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_PROGRAM_FILESX86);
                    break;

                case "PROGRAMFILESCOMMON":
                case "F7F1ED05-9F6D-47A2-AAAE-29D317C6F066":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_PROGRAM_FILES_COMMON);
                    break;

                case "PROGRAMFILESCOMMONX86":
                case "DE974D24-D9C6-4D3E-BF91-F4455120B917":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_PROGRAM_FILES_COMMONX86);
                    break;

                case "ADMINTOOLS":
                case "724EF170-A42D-4FEF-9F26-B60E846FBA4F":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_ADMINTOOLS);
                    break;

                case "COMMONADMINTOOLS":
                case "D0384E7D-BAC3-4797-8F14-CBA229B392B5":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_COMMON_ADMINTOOLS);
                    break;

                case "PUBLICMUSIC":
                case "3214FAB5-9757-4298-BB61-92A9DEAA44FF":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_COMMON_MUSIC);
                    break;

                case "PUBLICPICTURES":
                case "B6EBFB86-6907-413C-9AF7-4FC2ABF07CC5":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_COMMON_PICTURES);
                    break;

                case "PUBLICVIDEOS":
                case "2400183A-6185-49FB-A2D8-4A392A602BA3":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_COMMON_VIDEO);
                    break;
                
                case "USERPROFILES":
                case "0762D272-C50A-4BB0-A382-697DCD729B80":
                    ret = System.IO.Path.GetDirectoryName(GetSpecialFolderLocation(Native.CSIDL.CSIDL_PROFILE));
                    break;

                case "DOWNLOADS":
                case "374DE290-123F-4565-9164-39C4925E467B":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_PROFILE) + "\\Downloads";
                    break;

                case "PUBLICDOWNLOADS":
                case "3D644C9B-1FB8-4f30-9B45-F670235F79C0":
                    ret = System.IO.Path.GetDirectoryName(GetSpecialFolderLocation(Native.CSIDL.CSIDL_COMMON_DOCUMENTS)) + "\\Downloads";;
                    break;

                case "GADGETSUSER": // Unknown GUID.
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_APPDATA) + "\\Gadg8\\Desktop Gadgets";
                    break;

                case "RECYCLEBINFOLDER":
                case "B7534046-3ECB-4C18-BE4E-64CD4CB7D6AC":
                    ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_BITBUCKET);
                    break;

                case "PICTURESLIBRARY":
                case "A990AE9F-A03B-4E80-94BC-9912D7504104":
                    file = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
                        + "\\Microsoft\\Windows\\Libraries\\Pictures";
                    if (File.Exists(file))
                        ret = file;
                    else
                        ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_MYPICTURES);
                    break;

                case "VIDEOSLIBRARY":
                case "491E922F-5643-4AF4-A7EB-4E7A138D8174":
                    file = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
                         + "\\Microsoft\\Windows\\Libraries\\Videos";
                    if (File.Exists(file))
                        ret = file;
                    else
                        ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_MYVIDEO);
                    break;

                case "DOCUMENTSLIBRARY":
                case "7B0DB17D-9CD2-4A93-9733-46CC89022E7C":
                    file = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
                         + "\\Microsoft\\Windows\\Libraries\\Documents";
                    if (File.Exists(file))
                        ret = file;
                    else
                        // See case DOCUMENTS.
                        //ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_MYDOCUMENTS);
                        ret = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
                    break;

                case "MUSICLIBRARY":
                case "2112AB0A-C86A-4FFE-A368-0DE96E47012E":
                    file = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
                         + "\\Microsoft\\Windows\\Libraries\\Music";
                    if (File.Exists(file))
                        ret = file;
                    else
                        ret = GetSpecialFolderLocation(Native.CSIDL.CSIDL_MYMUSIC);
                    break;

                default:
                    break;
            }

            return ret;
        }

        public static string GetSpecialFolderLocation(Native.CSIDL csidl)
        {
            string ret = "";
            IntPtr ptrDir = IntPtr.Zero;

            Native.SHGetSpecialFolderLocation(IntPtr.Zero, csidl, ref ptrDir);
            
            StringBuilder sbDir = new StringBuilder(Native.MAX_PATH);

            if (Native.SHGetPathFromIDListW(ptrDir, sbDir) == true)
            {
                ret = sbDir.ToString();
            }

            sbDir = null;
            Marshal.FreeCoTaskMem(ptrDir);
            
            return ret;
        }

        public static string GetShortcutTarget(string file)
        {
            try
            {
                if (System.IO.Path.GetExtension(file).ToLower() != ".lnk")
                {
                    throw new Exception("Supplied file must be a .LNK file");
                }

                FileStream fileStream = File.Open(file, FileMode.Open, FileAccess.Read);
                using (System.IO.BinaryReader fileReader = new BinaryReader(fileStream))
                {
                    fileStream.Seek(0x14, SeekOrigin.Begin);     // Seek to flags
                    uint flags = fileReader.ReadUInt32();        // Read flags
                    if ((flags & 1) == 1)
                    {                      // Bit 1 set means we have to
                        // skip the shell item ID list
                        fileStream.Seek(0x4c, SeekOrigin.Begin); // Seek to the end of the header
                        uint offset = fileReader.ReadUInt16();   // Read the length of the Shell item ID list
                        fileStream.Seek(offset, SeekOrigin.Current); // Seek past it (to the file locator info)
                    }

                    long fileInfoStartsAt = fileStream.Position; // Store the offset where the file info
                    // structure begins
                    uint totalStructLength = fileReader.ReadUInt32(); // read the length of the whole struct
                    fileStream.Seek(0xc, SeekOrigin.Current); // seek to offset to base pathname
                    uint fileOffset = fileReader.ReadUInt32(); // read offset to base pathname
                    // the offset is from the beginning of the file info struct (fileInfoStartsAt)
                    fileStream.Seek((fileInfoStartsAt + fileOffset), SeekOrigin.Begin); // Seek to beginning of
                    // base pathname (target)
                    long pathLength = (totalStructLength + fileInfoStartsAt) - fileStream.Position - 2; // read
                    // the base pathname. I don't need the 2 terminating nulls.
                    char[] linkTarget = fileReader.ReadChars((int)pathLength); // should be unicode safe
                    var link = new string(linkTarget);

                    int begin = link.IndexOf("\0\0");
                    if (begin > -1)
                    {
                        int end = link.IndexOf("\\\\", begin + 2) + 2;
                        end = link.IndexOf('\0', end) + 1;

                        string firstPart = link.Substring(0, begin);
                        string secondPart = link.Substring(end);

                        return firstPart + secondPart;
                    }
                    else
                    {
                        return link;
                    }
                }
            }
            catch
            {
                return "";
            }
        }
    }
}
