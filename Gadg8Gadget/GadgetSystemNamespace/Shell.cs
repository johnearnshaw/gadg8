﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Diagnostics;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class Shell : MarshalByRefObject
    {
        Gadg8Gadget.Gadget GadgetWindow;

        public Shell(Gadg8Gadget.Gadget gadgetWindow)
        {
            this.GadgetWindow = gadgetWindow;
        }

        #region Methods
        
        [return: MarshalAs(UnmanagedType.IUnknown)]
        public object chooseFile(bool bForOpen, string strFilter, string strInitialDirectory, string strFileInit)
        {
            if (bForOpen)
            {
                FileDialog dialog = default(FileDialog);
                dialog = new OpenFileDialog();
                dialog.Filter = strFilter.Replace(":", "|").Replace("\\0", "|");
                dialog.FileName = strFileInit;
                dialog.InitialDirectory = strInitialDirectory;

                DialogResult result = dialog.ShowDialog(GadgetWindow);
                if (result == DialogResult.OK)
                    return new Item(dialog.FileName);
            }
            else
            {
                return saveFileDialog(strInitialDirectory, strFilter);
            }
            
            return null;
        }

        public object chooseFolder(string strTitle, int intOptions = 0)
        {
            // intOptions is an integer that specifies the folder picker dialog box options. Note The only supported value is zero (0); effectively no options.
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = strTitle;

            DialogResult result = dialog.ShowDialog(this.GadgetWindow);
            if (result == DialogResult.OK)
                return new Folder(dialog.SelectedPath);
            
            return null;
        }
    
        public void execute(string strFile, string strArgs = "", string strDir = "", string strOperation = "")
        {
            Gadg8Gadget.Native.ShellExecute(0, strOperation, strFile, strArgs, strDir, 1);
        }

        public object itemFromFileDrop()
        {
            //TODO
            return null;
        }

        [return: MarshalAs(UnmanagedType.IUnknown)]
        public object itemFromPath(string strPath)
        {
            return new Item(strPath);
        }

        public object knownFolder(string strKnownFolderPath)
        {
            try
            {
                return new Folder(Helpers.GetKnownFolderPath(strKnownFolderPath));
            }
            catch
            {
                throw new Exception("Automation Server can't create object");
            }
        }

        public string knownFolderPath(string strKnownFolderID)
        {
            return Helpers.GetKnownFolderPath(strKnownFolderID); // Retrieves a Known Folder path as string
        }

        public void refreshDesktop()
        {
            Native.SHChangeNotify(0x8000000, 0x1000, IntPtr.Zero, IntPtr.Zero); // Refreshes the desktop after adding or removing files.  
        }

        public object saveFileDialog(string strPath, string strFilter)
        {
            //TODO
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = strFilter.Replace(":", "|").Replace("\\0", "|");
            dialog.InitialDirectory = strPath;

            DialogResult result = dialog.ShowDialog(this.GadgetWindow);

            if (result == DialogResult.OK)
                return dialog.FileName;

            return null;
        }

        #endregion

    }

    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class Item : MarshalByRefObject
    {
        private string FilePath;

        public Item(string path)
        {
            FileAttributes attr = File.GetAttributes(path);

            if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
            {
                if (!Directory.Exists(path))
                    throw new Exception(path + " - directory not found");
            }
            else
            {
                 if (!File.Exists(path))
                     throw new Exception(path + " - file not found");
            }

            FilePath = path;
        }

        #region Methods
        //invokeVerb Invokes a verb associated with the item.
        public void invokeVerb(string strVerb)
        {
            //TODO
        }

        public string metadata(string strMeta)
        {
            //metadata Retrieves the metadata value for a canonical property name key of the System.Shell.Item
            //TODO
            return "";
        }
        #endregion

        #region Properties
         
        public bool isFileSystem
        {
            //isFileSystem Gets whether the object is a file.  
            get { return (File.GetAttributes(FilePath) & FileAttributes.Directory) != FileAttributes.Directory; }
        }

        public bool isFolder
        {
            //isFolder Gets whether the object is a folder.
            get { return (File.GetAttributes(FilePath) & FileAttributes.Directory) == FileAttributes.Directory; }
        }

        public bool isLink
        {
            get 
            {
                //isLink Gets whether the object is a link (or shortcut).
                string path = System.IO.Path.GetDirectoryName(FilePath);
                string filename = System.IO.Path.GetFileName(FilePath);
                bool ret = false;

                Type ShellAppType = Type.GetTypeFromProgID("Shell.Application");
                Shell32.Shell sh = (Shell32.Shell)Activator.CreateInstance(ShellAppType);

                Shell32.Folder folder = sh.NameSpace(path);
                Shell32.FolderItem item = folder.ParseName(filename);
                if (item != null)
                {
                    ret = item.IsLink;
                }
                Marshal.ReleaseComObject(sh);
                return ret;
            }
        }

        public object link
        {
            get 
            {
                string ret = "";
                
                if (isLink)
                    ret = Helpers.GetShortcutTarget(FilePath);
                
                return ret;
            }
        }

        public string modifyDate
        {
            //modifyDate Gets the last modified date of the System.Shell.Item.
            get { return File.GetLastWriteTime(FilePath).ToString(); }
        }

        public string name
        {
            //name Gets the name of the System.Shell.Item.
            get
            {
                Microsoft.Win32.RegistryKey reg = default(Microsoft.Win32.RegistryKey);
                int val = 0;

                try
                {
                    reg = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced");
                    val = int.Parse(reg.GetValue("HideFileExt").ToString());
                }
                finally
                {
                    if ((reg != null))
                        reg.Close();
                }
                
                if (val == 1)
                {
                    return Path.GetFileNameWithoutExtension(FilePath);
                }
                else
                {
                    return Path.GetFileName(FilePath);
                }
            }
        }

        //path Gets the Universal Naming Convention (UNC) path (with filename) of the System.Shell.Item.  
        public string path
        {
            get { return FilePath; }
        }

        //SHFolder Gets a System.Shell.Folder object from the System.Shell.Item.  
        private object SHFol;
        public object SHFolder
        {
            get
            {
                if ((File.GetAttributes(FilePath) & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    if (SHFol == null)
                        SHFol = new Folder(FilePath + "");
                }
                else
                {
                    if (SHFol == null)
                        SHFol = new Folder(Path.GetDirectoryName(FilePath) + "");
                }
                return SHFol;
            }
        }

        //size Gets the size (in bytes) of the System.Shell.Item.  
        public long size
        {
            get
            {
                FileInfo info = new FileInfo(FilePath);
                return info.Length; 
            }
        }

        //type Gets the verbose file type (not the extension) of the Microsoft Windows Shell item. 
        public string type
        {
            get
            {
                //TODO
                return null;
            }
        }
        #endregion
    }

    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class Folder : MarshalByRefObject
    {
        private string FolderPath;
        public Folder(string path)
        {
            FolderPath = path;
            ITMS = new Items(FolderPath);
        }

        #region Collections
        private object ITMS;
        public object Items
        {
            get { return ITMS; }
        }
        #endregion

        #region Methods
        
        // Copies a System.Shell.Item to a folder.  
        public void copyHere(object oItem, int option = 0)
        {
            Type ShellAppType = Type.GetTypeFromProgID("Shell.Application");
            Shell32.Shell sh = (Shell32.Shell)Activator.CreateInstance(ShellAppType);
            
            Shell32.Folder fldr = sh.NameSpace(this.FolderPath);

            Item itm = (Item)oItem;
            fldr.CopyHere(itm.path, option);

            Marshal.ReleaseComObject(sh);
        }

        // Moves a System.Shell.Item to a folder.  
        public void moveHere(string strPath)
        {
            Type ShellAppType = Type.GetTypeFromProgID("Shell.Application");
            Shell32.Shell sh = (Shell32.Shell)Activator.CreateInstance(ShellAppType);

            Shell32.Folder fldr = sh.NameSpace(this.FolderPath);

            fldr.MoveHere(strPath);

            Marshal.ReleaseComObject(sh);
        }

        // Creates a new folder.  
        public void newFolder(string strPath)
        {
            Type ShellAppType = Type.GetTypeFromProgID("Shell.Application");
            Shell32.Shell sh = (Shell32.Shell)Activator.CreateInstance(ShellAppType);

            Shell32.Folder fldr = sh.NameSpace(this.FolderPath);

            fldr.NewFolder(strPath);

            Marshal.ReleaseComObject(sh);
        }

        // Retrieves a System.Shell.Folder object that represents the folder containing the specified file.
        public object parse(string strFile)
        {
            Type ShellAppType = Type.GetTypeFromProgID("Shell.Application");
            Shell32.Shell sh = (Shell32.Shell)Activator.CreateInstance(ShellAppType);

            return sh.NameSpace(Path.GetDirectoryName(strFile));
        }
        #endregion

        #region Properties
        // Parent Gets a System.Shell.Item object that represents the parent folder.  
        public object Parent
        {
            get
            {
                try
                {
                    return new Item(Path.GetDirectoryName(this.FolderPath));
                }
                catch
                {
                    return null;
                }
            }
        }

        // Self Gets a duplicate System.Shell.Folder object. 
        public object Self
        {
            get
            {
                return new Folder(this.FolderPath);
            }
        }

        // name and path are missing from MS documentation:
        // http://msdn.microsoft.com/en-us/library/windows/desktop/ms723216(v=vs.85).aspx#properties
        public string name
        {
            get
            {
                return Path.GetFileName(this.FolderPath);
            }
        }

        public string path
        {
            get { return this.FolderPath; }
        }
        #endregion
    }

    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class Drive : MarshalByRefObject
    {

        #region Collections
        //
        #endregion

        #region Methods
        //
        #endregion

        #region Properties
        //
        #endregion
    }

    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class Items : MarshalByRefObject
    {

        #region Collections
        //
        public Items(string strPath)
        {
            ITMS = new ItemCollection(strPath);
        }
        #endregion

        #region Methods
        //
        #endregion

        #region Properties
        //
        public ItemCollection ITMS;
        public object item(int index)
        {
            return ITMS.Item(index);
        }

        public int count
        {
            get { return ITMS.Count; }
        }
        #endregion
    }

    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class ItemCollection : MarshalByRefObject
    {

        #region Collections
        public List<Item> ItemColl = new List<Item>();
        public ItemCollection(string strPath)
        {
            if ((File.GetAttributes(strPath) & FileAttributes.Directory) == FileAttributes.Directory)
            {
                foreach (string f in Directory.GetDirectories(strPath + "\\"))
                {
                    ItemColl.Add(new Item(f));
                }
                foreach (string i in Directory.GetFiles(strPath + "\\"))
                {
                    ItemColl.Add(new Item(i));
                }
            }
            else
            {
                ItemColl.Add(new Item(strPath));
            }
        }
        #endregion

        #region Methods
        //
        public object Item(int index)
        {
            return ItemColl[index];
        }
        #endregion

        #region Properties
        //
        public int Count
        {
            get { return ItemColl.Count; }
        }
        #endregion
    }

}
