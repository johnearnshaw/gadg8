﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Security.Permissions;
using System.Runtime;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComVisible(true)]
    public class Flyout : MarshalByRefObject
    {
        private Gadg8Gadget.Gadget GadgetWindow;
        public Flyout(Gadg8Gadget.Gadget gadgetWindow)
        {
            this.GadgetWindow = gadgetWindow;
            
        }

        public void InvokeShowHideEventHandler()
        {
            if (GadgetWindow.IsFlyoutOpen)
            {
                if (onShow != null)
                    this.onShow.GetType().InvokeMember("call", System.Reflection.BindingFlags.InvokeMethod, null, this.onShow, new object[] { null });
            }
            else
            {
                if (onHide != null)
                    this.onHide.GetType().InvokeMember("call", System.Reflection.BindingFlags.InvokeMethod, null, this.onHide, new object[] { null });
            }
        }

        // onHide Event fired when the gadget Flyout is hidden.          
        private object pOnHide;
        public object onHide
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            get
            {
                return pOnHide;
            }
            set
            {
                pOnHide = value;
            }
        }

        // onShow Event fired when the gadget Flyout is shown.  
        private object pOnShow;
        public object onShow
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            get
            {
                return pOnShow;
            }
            set
            {
                pOnShow = value;
            }
        }

        // document Gets an object that represents the Document Object Model (DOM) of the gadget Flyout 
        public object document
        {
            get 
            {
                return GadgetWindow.GetFlyoutDocument();
            }
        }

        // file Gets or sets the HTML filename for the gadget Flyout.
        private string pFile = "";
        public string file
        {
            get { return pFile; }
            set
            {
                pFile = value;
                if (!string.IsNullOrEmpty(value))
                {
                    GadgetWindow.ShowFlyout(value);
                }
                else
                {
                    GadgetWindow.CloseFlyout();
                }
            }
        }

        // show Gets or sets whether a gadget Flyout is visible.
        public bool show
        {
            get { return GadgetWindow.IsFlyoutOpen; }
            set 
            {
                if (value)
                {
                    if(!string.IsNullOrEmpty(this.file))
                        GadgetWindow.ShowFlyout(this.file);
                }
                else
                {
                    GadgetWindow.CloseFlyout();
                }
            }
        }
    }
}
