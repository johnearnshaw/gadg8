﻿using System;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [ComVisible(true)]
    [Guid("F3275E2E-0139-4126-BD5F-BCC669306F36")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface ISystemNamespace
    {
        [DispId(1)]
        System.Collections.Generic.List<SystemNamespace.Contact> Contacts { get; }

        [DispId(2)]
        Debug Debug { get; set; }

        [DispId(3)]
        Environment Environment { get; set; }

        [DispId(4)]
        Gadget Gadget { get; set; }

        [DispId(5)]
        Machine Machine { get; set; }

        [DispId(6)]
        Shell Shell { get; set; }

        [DispId(7)]
        Sound Sound { get; set; }

        [DispId(8)]
        Time Time { get; set; }
    }
}
