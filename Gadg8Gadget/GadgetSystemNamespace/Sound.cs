﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Media;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class Sound : MarshalByRefObject
    {
        Gadg8Gadget.Gadget GadgetForm;

        public Sound(Gadg8Gadget.Gadget gadgetForm)
        {
            this.GadgetForm = gadgetForm;
        }

        public void beep()
        {
            SystemSounds.Beep.Play();            
        }

        public void playSound(string strSound)
        {
            if (string.IsNullOrEmpty(strSound))
            {
                Gadg8Gadget.Native.PlaySound(null, UIntPtr.Zero, 0);
            }
            else
            {
                // play asynchronously with SND_ASYNC
                Gadg8Gadget.Native.PlaySound(strSound, UIntPtr.Zero, Gadg8Gadget.Native.SND_ASYNC);
            }
        }
    }
}
