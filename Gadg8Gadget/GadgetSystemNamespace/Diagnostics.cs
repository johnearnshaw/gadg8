﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class Diagnostics : MarshalByRefObject
    {
        private Gadg8Gadget.Gadget GadgetForm;
        public Diagnostics(Gadg8Gadget.Gadget gadgetForm)
        {
            this.GadgetForm = gadgetForm;
        }

        public EventLogClass EventLog = new EventLogClass();
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        [ComVisible(true)]
        public class EventLogClass : MarshalByRefObject
        {

            public void writeEntry(string strEventLogEntry, int intEventType = 0)
            {
                //add logging stuff
            }
        }
    }
}
