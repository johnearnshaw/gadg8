﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Security.Permissions;
using System.Xml;
using System.Runtime;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class Settings : MarshalByRefObject
    {
        private Gadg8Gadget.Gadget GadgetForm;
        
        public Settings(Gadg8Gadget.Gadget gadgetForm)
        {
            this.GadgetForm = gadgetForm;
        }
        
        #region Methods
        public object read(string strName)
        {
            string s = GadgetForm.ReadSetting(strName);
            double n;
            bool b;

            if (double.TryParse(s, out n))
            {
                return n;
            }
            else if (bool.TryParse(s, out b))
            {
                return b;
            }
            else
            {
                return s;
            }
        }

        public string readString(string strName)
        {
            return GadgetForm.ReadSetting(strName);
        }

        public void writeString(string strName, string strValue)
        {
            write(strName, strValue);
        }

        public void write(string strName, object value)
        {
            GadgetForm.WriteSetting(strName, value.ToString(), false);
        }
        #endregion

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        [ComVisible(true)]
        public class Action
        {
            public bool cancel { get { return false; } }
            public bool commit { get { return true; } }
        }

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        [ComVisible(true)]
        public class ClosingEvent
        {
            private Action pAction = new Action();
            public Action Action { get { return pAction; } }

            public bool cancel { get; set; } // Read/write - Gets or sets whether to cancel the onSettingsClosing event.
            public bool cancellable { get; private set; } // Read-only - Gets whether the onSettingsClosing event can be canceled.
            public bool closeAction { get; private set; } //Read-only - Gets the Settings dialog close request.

            public ClosingEvent(bool cancellable, bool closeAction)
            {
                this.cancellable = cancellable;
                this.closeAction = closeAction;
            }
        }
    }
}
