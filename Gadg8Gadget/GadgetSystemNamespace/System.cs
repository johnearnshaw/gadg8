﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Security.Permissions;
using System.Runtime;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Management;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
    public class SystemNamespace : MarshalByRefObject, Gadg8Gadget.GadgetSystemNamespace.ISystemNamespace
    {
        public SystemNamespace(Gadg8Gadget.Gadget gadgetWindow)
        {
            this.GadgetWindow = gadgetWindow;
            this.Gadget = new GadgetSystemNamespace.Gadget(this.GadgetWindow);
            this.Debug = new GadgetSystemNamespace.Debug(this.GadgetWindow);
            this.Environment = new GadgetSystemNamespace.Environment(this.GadgetWindow);
            this.Machine = new GadgetSystemNamespace.Machine(this.GadgetWindow);
            this.Shell = new GadgetSystemNamespace.Shell(this.GadgetWindow);
            this.Sound = new GadgetSystemNamespace.Sound(this.GadgetWindow);
            this.Time = new GadgetSystemNamespace.Time(this.GadgetWindow);
        }

        [ComVisible(false)]
        public Gadg8Gadget.Gadget GadgetWindow;

        public Gadget Gadget { get; set; }
        public Debug Debug { get; set; }
        public Environment Environment { get; set; }
        public Machine Machine { get; set; }
        public Shell Shell { get; set; }
        public Sound Sound { get; set; }
        public Time Time { get; set; }

        public struct Contact
	    {
		    public string city;
		    public string country;
		    public string defaultEmail;
		    public string filePath;
		    public string homePhone;
		    public string mobilePhone;
		    public string name;
		    public string POBox;
		    public string postalCode;
		    public string state;
		    public string street;
		    public string workPhone;
	    }

	    private List<Contact> contactCollection;
	    public List<Contact> Contacts
        {
		    get
            {
			    if (contactCollection == null)
                {
				    contactCollection = new List<Contact>();
				    Contact cont = new Contact();
				    cont.city = "Empty City";
				    cont.country = "Empty Country";
				    cont.defaultEmail = "empty@cont.act";
				    cont.filePath = "Empty\\File.path";
				    cont.homePhone = "0123456789";
				    cont.mobilePhone = "0123456789";
				    cont.name = "Empty Name";
				    cont.POBox = "1234";
				    cont.postalCode = "3MP7Y";
				    cont.state = "Empty State";
				    cont.street = "Empty Street";
				    cont.workPhone = "0123456789";
				    contactCollection.Add(cont);
			    }
			    return contactCollection;
		    }
	    }
    }
}
