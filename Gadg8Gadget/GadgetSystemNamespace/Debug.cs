﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Security.Permissions;
using mshtml;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public class Debug : MarshalByRefObject
    {
        private Gadg8Gadget.Gadget GadgetWindow;
        public Debug(Gadg8Gadget.Gadget gadgetWindow)
        {
            this.GadgetWindow = gadgetWindow;
        }
        public void outputString(string strOutputString)
        {
            this.GadgetWindow.OutputString(strOutputString);
        }
    }
}
