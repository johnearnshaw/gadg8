﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Security.Permissions;
using System.Runtime;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
    public class Gadget : MarshalByRefObject, Gadg8Gadget.GadgetSystemNamespace.IGadget
    {
        private Gadg8Gadget.Gadget GadgetWindow;

        // Fields
        public Flyout Flyout  { get; set; }
        public Settings Settings { get; set; }
        public Sidebar Sidebar { get; set; }

        public Gadget(Gadg8Gadget.Gadget gadgetWindow)
        {
            this.GadgetWindow = gadgetWindow;
            Flyout = new Gadg8Gadget.GadgetSystemNamespace.Flyout(GadgetWindow);
            Settings = new Gadg8Gadget.GadgetSystemNamespace.Settings(GadgetWindow);
            Sidebar = new Gadg8Gadget.GadgetSystemNamespace.Sidebar();

            //GadgetForm.VisibleChanged += onEventHandler;
        }

	    #region "Events"
	    //onDock Event fired when the gadget is docked on the Windows Sidebar. 
	    //Note  For Windows 7, because there is no Sidebar associated with the Gadget Platform, the onDock and onUndock event listeners are linked to a new gadget icon ("Larger size" or "Smaller size"). Clicking this icon resizes the gadget and raises the dock ("Smaller size") or undock ("Larger size") event. 
	    //onSettingsClosed Event fired when the gadget Settings dialog is closed.  
	    //onSettingsClosing Event fired when the gadget Settings dialog begins the process of closing.  
	    //onShowSettings Event fired when the gadget Settings dialog is requested.  
	    //onUndock Event fired when the gadget is docked on the Sidebar. 
	    //Note  For Windows 7, because there is no Sidebar associated with the Gadget Platform, the onDock and onUndock event listeners are linked to a new gadget icon ("Larger size" or "Smaller size"). Clicking this icon resizes the gadget and raises the dock ("Smaller size") or undock ("Larger size") event. 
        //visibilityChanged Event fired when the gadget visibility changes due to the Sidebar being hidden or displayed. 

        #region CallbackInvokers
        public void InvokeDockUndockHandler()
        {
            if (GadgetWindow.Docked)
            {
                if (this.onDock != null)
                        this.onDock.GetType().InvokeMember("call", System.Reflection.BindingFlags.InvokeMethod, null, this.onDock, new object[] { null });
            }
            else
            {
                if (this.onUndock != null)
                    this.onUndock.GetType().InvokeMember("call", System.Reflection.BindingFlags.InvokeMethod, null, this.onUndock, new object[] { null });
            }
        }

        public void InvokeOnShowSettingsHandler()
        {
            if (onShowSettings != null)
                this.onShowSettings.GetType().InvokeMember("call", System.Reflection.BindingFlags.InvokeMethod, null, this.onShowSettings, new object[] { null });
        }

        public void InvokeOnSettingsClosedHandler(Settings.ClosingEvent closingEvent)
        {
            if (this.onSettingsClosed != null)
                this.onSettingsClosed.GetType().InvokeMember("call", System.Reflection.BindingFlags.InvokeMethod, null, this.onSettingsClosed, new object[] { null, closingEvent });
        }

        public void InvokeOnSettingsClosingHandler(Settings.ClosingEvent closingEvent)
        {
            if (this.onSettingsClosing != null)
                this.onSettingsClosing.GetType().InvokeMember("call", System.Reflection.BindingFlags.InvokeMethod, null, this.onSettingsClosing, new object[] { null, closingEvent });
        }

        public void InvokeVisibilityChangedHandler()
        {
            if (this.visibilityChanged != null)
                this.visibilityChanged.GetType().InvokeMember("call", System.Reflection.BindingFlags.InvokeMethod, null, this.visibilityChanged, new object[] { null });
        }
        #endregion

        private object ponDock;
        public object onDock
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            get 
            {
                return ponDock;
            }
            set
            {
                ponDock = value;
                GadgetWindow.HasUndock = (value != null && onUndock != null);
            }
        }


        
        private object pOnSettingsClosed;
        public object onSettingsClosed
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            get
            {
                return pOnSettingsClosed;
            }
            set
            {
                pOnSettingsClosed = value;    
            }
        }

        private object pOnSettingsClosing;
        public object onSettingsClosing
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            get
            {
                return pOnSettingsClosing;
            }
            set
            {
                pOnSettingsClosing = value;
            }
        }

        private object pOnShowSettings;
        public object onShowSettings
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            get
            {
                return pOnShowSettings;
            }
            set
            {
                pOnShowSettings = value;
            }
        }

        private object ponUndock;
        public object onUndock
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            get 
            { 
                return ponUndock; 
            }
            set
            {
                ponUndock = value;
                GadgetWindow.HasUndock = (value != null && onDock != null);
            }
        }

        private object pVisibilityChanged;
        public object visibilityChanged
        {
            [return: MarshalAs(UnmanagedType.IUnknown)]
            get
            {
                return pVisibilityChanged;
            }
            set
            {
                pVisibilityChanged = value;
            }
        }
	    #endregion

	    #region "Methods"
	    public void beginTransition()
	    {
            //TODO
	    }

	    public void close()
	    {
            this.GadgetWindow.CloseGadget();
	    }

	    public void endTransition(int transition, float seconds)
	    {
		    //TODO
	    }
	    #endregion

	    #region "Properties"
        private string pBackground;
	    public string background {
            get
            {
                return pBackground;
            }
            set
            {
                pBackground = value;
                GadgetWindow.SetBackground();
            }
        }

	    public bool docked {
		    get { return GadgetWindow.Docked; }
		    set 
            {
                GadgetWindow.Docked = value;
                InvokeDockUndockHandler();
            }
	    }

	    public object document {
		    get { return GadgetWindow.WebBrowser.Document.DomDocument; }
	    }

	    public string name {
		    get { return GadgetWindow.GadgetXML.Name; }
	    }

	    public int opacity {
		    get { return (int)GadgetWindow.Opacity * 100; }
	    }

	    public string path {
		    get 
            {
                return GadgetWindow.Path.Substring(0, GadgetWindow.Path.Length - 1); 
            }
	    }

	    public string platformVersion {
		    get
            {
                return GadgetWindow.GadgetXML.hosts[0].Platform.minPlatformVersion;
            }
	    }

        private string psettingsUI;
        public string settingsUI
        {
            get { return psettingsUI; }
            set
            {
                psettingsUI = value;
                GadgetWindow.HasSettings = (!string.IsNullOrEmpty(value));
            }
        }

	    public string version {
		    get
            {
                return GadgetWindow.GadgetXML.Version;
            }
	    }

	    public bool visible {
		    get
            {
                return GadgetWindow.Visible;
            }
	    }
	    #endregion
    }
}
