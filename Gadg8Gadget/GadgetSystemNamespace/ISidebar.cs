﻿using System;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [ComVisible(true)]
    [Guid("A1285E2A-0037-4224-BA56-ABC669306F37")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    interface ISidebar
    {
        string dockSide { get; }
        object onDockSideChanged { get; set; }
    }
}
