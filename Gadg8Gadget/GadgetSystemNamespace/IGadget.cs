﻿using System;
using System.Runtime.InteropServices;

namespace Gadg8Gadget.GadgetSystemNamespace
{
    [ComVisible(true)]
    [Guid("F3275E2D-0139-4126-BD5F-BBC669306F36")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGadget
    {
        string background { get; set; }
        void beginTransition();
        void close();
        bool docked { get; set; }
        object document { get; }
        void endTransition(int transition, float seconds);
        Flyout Flyout { get; set; }
        string name { get; }
        object onDock { get; set; }
        object onSettingsClosed { get; set; }
        object onSettingsClosing { get; set; }
        object onUndock { get; set; }
        int opacity { get; }
        string path { get; }
        string platformVersion { get; }
        Settings Settings { get; set; }
        string settingsUI { get; set; }
        string version { get; }
        object visibilityChanged { get; set; }
        bool visible { get; }
    }
}
