﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Windows.Forms;

namespace Gadg8Gadget
{
    [ComVisible(true)]
    [Guid("bcbb73be-1eec-4eca-a23d-a1ce0efbcf4c")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGadgetHost
    {
        bool SettingsChanged { get; }

        t ReadSetting<t>(string name, t defaultValue, string uniqueId);
        void WriteSetting(string name, object value, string uniqueId, bool privateSetting);
        void SaveSettings();

        void OutputString(string outputString, bool javascriptError, string filePath = "", int lineNumber = 0);
        void WriteLog(string text);

        void Handshake(IntPtr handle, string uniqueId, GadgetManifest gadgetManifest);
        Point GadgetMoved(Rectangle gadgetRect, string uniqueId);
        bool GadgetClosing(IntPtr handle, string uniqueId);
        void AddGadgets();
    }
}
