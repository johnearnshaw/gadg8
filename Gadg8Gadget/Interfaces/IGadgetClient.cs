﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Gadg8Gadget
{
    [ComVisible(true)]
    [Guid("bcbb73be-1eec-4eca-a23d-a1ce0efbcf4c")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IGadgetClient
    {
        IntPtr GadgetWindowHandle { get; }
        string UniqueId { get; }

        bool StillResponding();
        void ReadPrivateSettings();
    }
}
