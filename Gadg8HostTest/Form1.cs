﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using Gadg8Gadget;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace Gadg8HostTest
{
    [ProgId("Gadg8.GadgetHost")]
    [Guid("e802896d-8ca2-41e1-9bfe-7334c05f27af")]
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [ComVisible(true)]
    public partial class Form1 : Form, IGadgetHost
    {
        public override object InitializeLifetimeService()
        {
            return null;
        }

        GadgetServer Server;
        string LaunchPath = Application.StartupPath + "\\Gadg8 Gadget.exe";
        int PortNumber = 0;

        public Form1()
        {
            this.StartPosition = FormStartPosition.Manual;

            Server = new GadgetServer();
            PortNumber = Server.CreateChannel();
            if (PortNumber == 0)
                Application.Exit();

            RunningObjectTable rot = new RunningObjectTable();
            rot.AddToROT<IGadgetHost>(this, "Gadg8.GadgetHost." + PortNumber);

            InitializeComponent();

            this.Top = Screen.PrimaryScreen.WorkingArea.Bottom - this.Height;
            this.Left = Screen.PrimaryScreen.WorkingArea.Right - this.Width;

            textBox1.Text = PortNumber.ToString();
            textBox2.KeyUp += textBox2_KeyUp;
        }

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            if (textBox2.Text != "" && e.KeyCode == Keys.Enter)
            {
                RunGadget(textBox2.Text, Guid.NewGuid().ToString());
                textBox2.Text = "";
            }
        }

        #region IGadgetHost
        public bool SettingsChanged
        {
            get { return Settings.SettingsChanged; }
        }

        public void SaveSettings()
        {
            Settings.SaveSettings();
        }

        public t ReadSetting<t>(string name, t defaultValue, string uniqueId)
        {
            return Settings.Read<t>(name, defaultValue, uniqueId);
        }

        public void WriteSetting(string name, object value, string uniqueId, bool privateSetting)
        {
            Settings.Write(name, value, uniqueId, privateSetting);
        }

        public void Handshake(IntPtr h, string ui, GadgetManifest gm)
        {
        }

        public bool GadgetClosing(IntPtr handle, string uniqueId)
        {
            this.Invoke(new MethodInvoker(() => { this.Show(); }));
            return true;
        }

        public void OutputString(string outputString, bool javascriptError, string filePath, int lineNumber)
        {
            System.Diagnostics.Debug.WriteLine(outputString);
        }


        public Point GadgetMoved(Rectangle gadgetRect, string uniqueId)
        {
            return new Point(0, 0);
        }

        public void AddGadgets()
        {
            MessageBox.Show("Add Gadgets...");
        }
        #endregion

        private void RunGadget(string path, string uniqueId)
        {
            string gadgetPath = path;
            int top = 400;
            int left = 400;
            int hostIpcPort = this.PortNumber;
            string args = gadgetPath.Replace(" ", "+") + " " + left.ToString() + " " + top.ToString() + " " + uniqueId + " " + hostIpcPort.ToString();

            Process process = new Process();

            // Configure the process using the StartInfo properties.
            process.StartInfo.FileName = LaunchPath;
            process.StartInfo.Arguments = args;
            process.Start();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Settings.ClearSettings();
            Settings.SaveSettings();
        }




        public void WriteLog(string text)
        {
            throw new NotImplementedException();
        }
    }
}
