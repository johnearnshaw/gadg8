﻿using System; 
using System.Collections.Generic; 
using System.IO; 
using System.Reflection; 
using System.Text; 
using System.Windows.Forms;
using System.Xml; 
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters;
using System.Runtime.Serialization.Formatters.Binary;

namespace Gadg8HostTest
{
    public static class Settings
    {
        public const char Separator = ((char)007); // ASCII BEL

        private static string SettingsPath = Application.StartupPath.ToString() + "\\settings";

        public static List<Setting> SettingsList;

        public static bool BinaryMode = true;
        public static bool AutoSave = false;
        public static bool SettingsChanged;
        

        public static t Read<t>(String name, t defaultValue, string uniqueId = "")
        {
            if (SettingsList == null)
                LoadSettings();

            Setting findSet = SettingsList.Find(x => x.Name.ToLower() == name.ToLower() && x.UniqueId == uniqueId);
            if (findSet != null)
            {
                try
                {
                    return (t)findSet.Value;
                }
                catch
                {
                    return defaultValue;
                }
            }
            else
            {
                return defaultValue;
            }
        }

        public static void Write(String name, object value, string uniqueId = "", bool privateSetting = false)
        {
            SettingsChanged = true;

            if (SettingsList == null)
                LoadSettings();

            Setting setting = SettingsList.Find(x => x.Name == name && x.UniqueId == uniqueId);
            if (setting == null)
            {
                SettingsList.Add(new Setting(name, value, uniqueId, privateSetting));
                if (AutoSave)
                    SaveSettings();
            }
            else
            {
                if (privateSetting == true || setting.PrivateSetting == privateSetting)
                {
                    setting.Value = value;
                    setting.PrivateSetting = privateSetting;
                    if (AutoSave)
                        SaveSettings();
                }
            }
        }
        
        public static void DeleteAllSettingsById(string uniqueId)
        {
            SettingsList.RemoveAll(x => x.UniqueId == uniqueId);
        }

        public static void ClearSettings()
        {
            if (SettingsList == null)
                SettingsList = new List<Setting>();

            SettingsList.Clear();
        }

        public static void SaveSettings()
        {
            if (SettingsChanged == true)
            {
                if (BinaryMode)
                {
                    SerializeObjectToBinary(SettingsPath, SettingsList);
                }
                else
                {
                    string[] settings = new string[SettingsList.Count];
                    int idx = 0;
                    foreach (Setting setting in SettingsList)
                    {
                        settings[idx] = setting.Name + Separator + setting.Value + Separator + setting.UniqueId + Separator + setting.PrivateSetting;
                        idx++;
                    }
                    File.WriteAllLines(SettingsPath, settings);
                }
                
                SettingsChanged = false;
            }
        }

        public static void LoadSettings()
        {
            if (BinaryMode)
            {
                SettingsList = DeSerializeObjectFromBinary<List<Setting>>(SettingsPath, new List<Setting>());
            }
            else
            {
                SettingsList = new List<Setting>();

                if (File.Exists(SettingsPath))
                {
                    SettingsList.Clear();
                    string[] settings = File.ReadAllLines(SettingsPath);
                    foreach (string line in settings)
                    {
                        string[] lineArray = line.Split(new char[] { Separator }, StringSplitOptions.None);
                        SettingsList.Add(new Setting(lineArray[0], lineArray[1], lineArray[2], bool.Parse(lineArray[3])));
                    }
                }
            }
        }

        public static bool SerializeObjectToBinary(string filename, object obj)
        {
            try
            {
                using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read))
                {
                    fs.Position = 0;

                    BinaryFormatter binFormatter = new BinaryFormatter();
                    binFormatter.Serialize(fs, obj);
                    fs.Close();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static  t DeSerializeObjectFromBinary<t>(string filename, t defaultValue)
        {
            try
            {
                t ret = default(t);

                using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    BinaryFormatter binFormatter = new BinaryFormatter();
                    ret = (t)binFormatter.Deserialize(fs);
                    fs.Close();
                }
                return ret;
            }
            catch
            {
                return defaultValue;
            }
        }

        [Serializable]
        public class Setting
        {
            public string Name { get; set; }
            public object Value { get; set; }
            public string UniqueId { get; set; }
            public bool PrivateSetting { get; set; }
            public Setting(string name, object value, string uniqueId = "", bool privateSetting = false)
            {
                this.Value = value;
                this.Name = name;
                this.UniqueId = uniqueId;
                this.PrivateSetting = privateSetting;
            }
        }
    }
}
