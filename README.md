### Gadg8 - Windows Desktop Gadgets for Windows 8 ###

For Gadg8 Installer/Binaries, screenshots and more details see the homepage: [http://gadg8.org/](http://gadg8.org/)

Gadg8 is an opensource replacement for the Microsoft Windows Desktop/Sidebar Gadgets aiming to bring gadgets to Windows XP, Windows 8 and back to Vista and 7 - Hopefully with all the security issues and bugs of Windows Desktop Gadgets ironed out in the future :)

This project was started about 4 years ago originally in VB.net to try and add Sidebar Gadgets to Windows XP. Since then It's been restarted C#. The project is shaping up nicely.

Currently tested and fully compatible with many gadgets such as Orbmu2k's Battery Status, Jonathan Abbott's Asteroids, Tordai Levente's SideTris, Gareth William's Calculator and many standard Microsoft gadgets such as Slide Show, Clock and Picture Puzzle. Partially compatible with many gadgets such as Intelligroup's Auction Sidebar Tool & SuperSearch and Stedy Software's SideBar Outlook.

### Current Features ###

* Multi-process gadget loading (each gadget runs in it's own process similar to Google Chrome tabs)
* .gadget package installation with developer's security certificate details and install progress information.
* Windows Desktop context menu integration for opening gadget browser.
* Windows Explorer style gadget browser with gadget details and context menu with options for removing gadgets.
* Gadgets stay visible when "Show Desktop" button (or Ctrl+D) is pressed, similar to MS's Sidebar behavior.
* Gadgets stay visible through Windows AeroPeek, similar to MS's Sidebar behavior.
* Running gadgets list with options to show/close gadgets.
* Win+G brings gadgets to front of other windows, similar to MS's Sidebar behavior.
* "Always On Top" and Opacity options in gadget's context menu, similar to MS's Sidebar behavior.

### Some To Do & Some Known Issues ###

* More work needed in GBackground, GImage and definitely GText objects (things like addShadow & addGlow needs adding).
* Bug fix for IInternetSecurityManager in WebBrowser control to allow frames and flash objects to load in html.
* Invoke QueryService for IElementBehaviorFactory in WebBrowser (comments and workaround in OnDocumentCompleted override).
* Add an auto-updater option to keep Gadg8.exe & Gadg8 Gadget.exe binaries at current stable version on end-user machines.
* Differed script loading broken in WebBrowser (tested with Intelligroups XML Settings Transform Javascript library).
* Syntax coloring is broken in gadget output window (under advanced options in Gadg8 Properties).

### Project Details ###

* Current Status: Pre-alpha
* Current Version: 0.0.6.1

### Development Environment ###

* Visual Studio 2013
* .Net 2.0 CLR

### Contribution ###

Any help from some super-savvy Windows Api & Internet Explorer interface developers would be awesomely appreciated ;)
Get in touch with the repo owner to discuss contribution guidelines.

### License ###

comming soon.